import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'intro', loadChildren: () => import('./auth/intro/intro.module').then(m => m.IntroPageModule) },
  { path: 'numberadd', loadChildren: () => import('./auth/numberadd/numberadd.module').then(m => m.NumberAddPageModule) },
  { path: 'numberverify', loadChildren: () => import('./auth/numberverify/numberverify.module').then(m => m.NumberVerifyPageModule) },
  { path: 'setpin', loadChildren: () => import('./auth/setpin/setpin.module').then(m => m.AuthSetPinPageModule) },
  { path: 'numberverified', loadChildren: () => import('./auth/numberverified/numberverified.module').then(m => m.NumberVerifiedPageModule) },
  { path: 'accountsetup', loadChildren: () => import('./auth/accountsetup/accountsetup.module').then(m => m.AccountSetupPageModule) },
  { path: 'verifypin', loadChildren: () => import('./auth/verifypin/verifypin.module').then(m => m.VerifyPinPageModule) },
  { path: 'authprofile', loadChildren: () => import('./auth/profile/profile.module').then(m => m.AuthProfilePageModule) },
  { path: 'countryselect', loadChildren: () => import('./authv2/countryselect/countryselect.module').then(m => m.CountrySelectPageModule) },
  { path: 'login', loadChildren: () => import('./authv2/login/login.module').then(m => m.LoginPageModule) },
  { path: 'register', loadChildren: () => import('./authv2/register/register.module').then(m => m.RegisterPageModule) },
  { path: 'verifynumber', loadChildren: () => import('./authv2/verifyphone/verifyphone.module').then(m => m.VerifyPhonePageModule) },
  {path: 'forgotpin', loadChildren:() => import('./authv2/forgotpin/forgotpin.module').then(m => m.ForgotPinPageModule)},
 
  { path: 'dashboard', loadChildren: () => import('./access/dashboard/dashboard.module').then(m => m.DashboardPageModule) },
  { path: 'profile', loadChildren: () => import('./access/account/profile/profile.module').then(m => m.AccessProfilePageModule) },
  { path: 'updatepin', loadChildren: () => import('./access/account/updatepin/updatepin.module').then(m => m.UpdatePinPageModule) },
  {path: 'updateprofile', loadChildren:() => import('./access/account/updateprofile/updateprofile.module').then(m => m.UpdateProfilePageModule)},

  { path: 'notifications', loadChildren: () => import('./access/general/notifications/notifications.module').then(m => m.NotificationsPageModule) },
  { path: 'faq', loadChildren: () => import('./access/general/faq/faq/faq.module').then(m => m.FaqPageModule) },
  { path: 'faqdetails', loadChildren: () => import('./access/general/faq/faqdetails/faqdetails.module').then(m => m.FaqDetailsPageModule) },

  { path: 'saleshistory', loadChildren: () => import('./access/transactions/saleshistory/saleshistory.module').then(m => m.SalesHistoryPageModule) },
  { path: 'pointshistory', loadChildren: () => import('./access/transactions/points/points.module').then(m => m.PointsPageModule) },

  { path: 'lcctopup', loadChildren: () => import('./access/payments/lcctopup/lcctopup.module').then(m => m.LccTopupPageModule) },
  { path: 'selectbiller', loadChildren: () => import('./access/payments/selectbiller/selectbiller.module').then(m => m.SelectBillerPageModule) },
  { path: 'billerpayment', loadChildren: () => import('./access/payments/billerpayment/billerpayment.module').then(m => m.BillerPaymentPageModule) },
  { path: 'paymentdetails', loadChildren: () => import('./access/payments/paymentdetails/paymentdetails.module').then(m => m.PaymentDetailsPageModule) },

  { path: 'stores', loadChildren: () => import('./access/features/stores/stores.module').then(m => m.StoresPageModule) },
  { path: 'storedetails', loadChildren: () => import('./access/features/stores/store/store.module').then(m => m.StorePageModule) },

  { path: 'addresslocator', loadChildren: () => import('./access/account/addressmanager/addresslist/addresslist.module').then(m => m.AddressListPageModule) },
  { path: 'addaddress', loadChildren: () => import('./access/account/addressmanager/addaddress/addaddress.module').then(m => m.AddAddressPageModule) },
  { path: 'editaddress', loadChildren: () => import('./access/account/addressmanager/editaddress/editaddress.module').then(m => m.EditAddressPageModule) },

  { path: 'dealsdashboard', loadChildren: () => import('./access/features/maddeals/dashboard/dashboard.module').then(m => m.DashboardPageModule) },
  { path: 'dealsearch', loadChildren: () => import('./access/features/maddeals/search/search.module').then(m => m.SearchPageModule) },
  { path: 'deals', loadChildren: () => import('./access/features/maddeals/list/dealslist.module').then(m => m.DealsPageModule) },
  { path: 'flashdeals', loadChildren: () => import('./access/features/maddeals/flashdeals/flashdeals.module').then(m => m.FlashDealsPageModule) },
  { path: 'dealdetails', loadChildren: () => import('./access/features/maddeals/details/dealdetails.module').then(m => m.DealDetailsPageModule) },
  { path: 'dealcheckout', loadChildren: () => import('./access/features/maddeals/dealcheckout/dealcheckout.module').then(m => m.DealCheckoutPageModule) },
  { path: 'dealpurchasehistory', loadChildren: () => import('./access/features/maddeals/purchase/dealpuchaselist/dealpuchaselist.module').then(m => m.DealPurchaseHistoryPageModule) },
  { path: 'dealpurchasedetails', loadChildren: () => import('./access/features/maddeals/purchase/dealpurchasedetails/dealpurchasedetails.module').then(m => m.PurchaseDetailsPageModule) },
  { path: 'productpurchasehistory', loadChildren: () => import('./access/features/maddeals/purchase/productpurchaselist/productpurchaselist.module').then(m => m.ProductPurchaseListPageModule) },
  { path: 'productpurchasedetails', loadChildren: () => import('./access/features/maddeals/purchase/productpurchasedetails/productpurchasedetails.module').then(m => m.ProductPurchaseDetailsPageModule) },


  { path: 'buypointinitialize', loadChildren: () => import('./access/buypoints/initialize/initialize.module').then(m => m.BuyPointsPageModule) },
  { path: 'buypointconfirm', loadChildren: () => import('./access/buypoints/confirm/confirm.module').then(m => m.BuyPointsConfirmPageModule) },
  { path: 'wallettopuphistory', loadChildren: () => import('./access/buypoints/list/list.module').then(m => m.WalletTopupHistoryPageModule) },

  { path: 'scanredeemcode', loadChildren: () => import('./access/redeem/scancode/scancode.module').then(m => m.ScanRedeemCodePageModule) },
  { path: 'redeeminitialize', loadChildren: () => import('./access/redeem/redeeminitialize/redeeminitialize.module').then(m => m.RedeemInitializePageModule) },
  { path: 'redeemconfirm', loadChildren: () => import('./access/redeem/redeemconfirm/redeemconfirm.module').then(m => m.RedeemConfirmPageModule) },
  { path: 'referral', loadChildren: () => import('./access/features/referral/referral/referral.module').then(m => m.ReferralDetailsPageModule) },
  { path: 'referralbonus', loadChildren: () => import("./access/transactions/referralbonus/referralbonus.module").then(m => m.ReferralBonusPageModule) },


  { path: 'vasproviders', loadChildren: () => import("./access/features/vas/vasprovider/vasprovider.module").then(m => m.VasProvidersPageModule) },
  { path: 'vasproviderpackage', loadChildren: () => import("./access/features/vas/vasproviderpackage/vasproviderpackage.module").then(m => m.VasProviderPackagePageModule) },
  { path: 'vasprovideraccount', loadChildren: () => import("./access/features/vas/vasprovideraccount/vasprovideraccount.module").then(m => m.VasProviderAccountPageModule) },
  { path: 'vaspaymentdetails', loadChildren: () => import("./access/features/vas/vaspaymentdetails/vaspaymentdetails.module").then(m => m.VVasPaymentDetailsPageModule) },
  { path: 'vaspaymenthistory', loadChildren: () => import("./access/features/vas/list/list.module").then(m => m.VasPaymentHistoryPageModule) },

  { path: 'cardmanger', loadChildren: () => import("./access/account/cardmanager/cardmanager.module").then(m => m.CardManagerPageModule) },
  { path: 'bankmanager', loadChildren: () => import("./access/account/bankmanager/bankmanager.module").then(m => m.BankManagerPageModule) },
  { path: 'cashout', loadChildren: () => import("./access/features/cashout/cashout.module").then(m => m.CashOutPageModule) },
  { path: 'accountmanager', loadChildren: () => import("./access/account/accountmanager/accountmanager.module").then(m => m.AccountMangerPageModule) },

  { path: 'bnpl/home', loadChildren: () => import("./access/features/bnpl/home/home.module").then(m => m.BnplHomePageModule) },
  { path: 'bnpl/checklimit', loadChildren: () => import("./access/features/bnpl/checklimit/checklimit.module").then(m => m.ChecklimitModule) },
  { path: 'bnpl/planselector', loadChildren: () => import("./access/features/bnpl/planselector/planselector.module").then(m => m.BnplPlanSelectorPageModule) },
  { path: 'bnpl/planconfirm', loadChildren: () => import("./access/features/bnpl/planconfirm/planconfirm.module").then(m => m.BnplPlanConfirmPageModule) },
  { path: 'bnpl/loanprofile', loadChildren: () => import("./access/features/bnpl/loanprofile/loanprofile.module").then(m => m.BnplLoanProfilePageModule) },
  { path: 'bnpl/loans', loadChildren: () => import("./access/features/bnpl/loans/list/list.module").then(m => m.LoansHistoryPageModule) },
  { path: 'bnpl/loan', loadChildren: () => import("./access/features/bnpl/loans/details/details.module").then(m => m.LoansDetailsPageModule) },
  //


  { path: 'about', loadChildren: () => import('./access/general/about/about.module').then(m => m.AboutPageModule) },
  { path: 'support', loadChildren: () => import('./access/general/support/support.module').then(m => m.SupportPageModule) },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
