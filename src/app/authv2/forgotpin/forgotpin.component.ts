import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { stringify } from 'querystring';
@Component({
    selector: 'app-forgotpin',
    templateUrl: 'forgotpin.component.html'
})
export class ForgotPin {
   
    PhoneNumber='';
    modalImage:boolean=true;
    constructor(
        public _ModalController: ModalController,
        private _StatusBar: StatusBar,
        private _MenuController: MenuController,
        public _HelperService: HelperService,
        private _AlertController: AlertController,
    ) {}
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }
    
    MobileVerify(){
    if (this.PhoneNumber == undefined || this.PhoneNumber == null || this.PhoneNumber == "") {
        this._HelperService.NotifyToastError('Enter mobile number');
    }
    else if (this.PhoneNumber != undefined && isNaN(parseInt(this.PhoneNumber)) == true) {
        this._HelperService.NotifyToastError('Enter valid mobile number');
    }
    else{
        let countryDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig)

        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var pData = {
            Task: 'requestotpvoicecall',
            CountryIsd:countryDetails.SelectedCountry.Isd ,
            MobileNumber: this.PhoneNumber,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.modalImage=false;
                    setTimeout(() => {
                        this.modalImage=true;
                        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Login)
                       }, 4000);
                 }
                 else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
            this._HelperService.Notify('Operation failed', _Error);
            this._HelperService.HandleException(_Error);
        });
        
    }
}
}