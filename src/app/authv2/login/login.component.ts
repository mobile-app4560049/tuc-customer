import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation,OAddressComponent } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ModalSelectCountry } from '../../auth/modalselectcountry/modalselectcountry.component';
import { Mixpanel, MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';
import * as _ from 'underscore';
declare var $:any;
@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})
export class LoginPage {
    public AuthMobileNumber = "";
    public AuthPin = "";
    public DeviceInformation: ODeviceInformation;
    public _ActiveAddress: OAddressComponent =
    {
        ReferenceId: 0,
        ReferenceKey: null,
        Reference: null,

        Task: "updateuseraddress",
        CountryId: 1,
        CountryKey: null,
        CountryName: null,

        Name: null,
        ContactNumber: null,
        EmailAddress: null,
        AddressLine1: null,
        AddressLine2: null,
        Landmark: null,
        AlternateMobileNumber: null,

        CityAreaId: 0,
        CityAreaKey: null,
        CityAreaName: null,

        CityId: 0,
        CityKey: null,
        CityName: null,

        StateId: 0,
        StateKey: null,
        StateName: null,

        ZipCode: null,
        Instructions: null,

        MapAddress: null,
        Latitude: -1,
        Longitude: -1,

        IsPrimary: false,
        LocationTypeId: 800
    }
    constructor(
        public _ModalController: ModalController,
        private _StatusBar: StatusBar,
        private _MenuController: MenuController,
        public _HelperService: HelperService,
        private _AlertController: AlertController,
        private _Mixpanel:Mixpanel,
        private _MixpanelPeople:MixpanelPeople,
    ) {
        _MenuController.enable(false);
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
        var _AppConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig);
        if (_AppConfig == null) {
            this.GetConfiguration();
        }
        else {
            this._AppConfig = _AppConfig;
        }
    }

    _AppConfig =
        {
            HomeSlider: [],
            SelectedCountry: null,
            Countries: [
            ],
        }
    GetConfiguration() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        var pData;
        if (DeviceI != null) {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: DeviceI.SerialNumber,
                OsName: DeviceI.OsName,
                OsVersion: DeviceI.OsVersion,
                Brand: DeviceI.Brand,
                Model: DeviceI.Model,
                Width: DeviceI.Width,
                Height: DeviceI.Height,
                CarrierName: DeviceI.CarrierName,
                CountryCode: DeviceI.CountryCode,
                Mcc: DeviceI.Mcc,
                Mnc: DeviceI.Mnc,
                NotificationUrl: DeviceI.NotificationUrl,
                Latitude: DeviceI.Latitude,
                Longitude: DeviceI.Longitude,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }
        else {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: 'xx',
                OsName: "android",
                Brand: 'android',
                Model: "android",
                Width: 0,
                Height: 0,
                CarrierName: "airtel",
                CountryCode: "ng",
                Mcc: "000",
                Mnc: "000",
                NotificationUrl: "00",
                Latitude: 0,
                Longitude: 0,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.App, pData);
        _OResponse.subscribe(
            _Response => {
                setTimeout(() => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                }, 2000);
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._AppConfig = _Response.Result;
                    if (this._AppConfig.HomeSlider != undefined || this._AppConfig.HomeSlider != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Slider.Home, this._AppConfig.HomeSlider);
                    }
                    this._AppConfig.SelectedCountry = this._AppConfig.Countries[0];
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, _Response.Result);
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }

    public ionViewDidEnter() {
        this._HelperService.SetPageName("Auth-NumberAdd");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }

    NavRegister() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Register)
    }

    async SetNumber() {
        if (this._AppConfig.SelectedCountry == undefined || this._AppConfig.SelectedCountry == null) {
            this._HelperService.NotifyToastError('Select your country');
        }
        else if (this.AuthMobileNumber == undefined || this.AuthMobileNumber == null || this.AuthMobileNumber == "") {
            this._HelperService.NotifyToastError('Enter mobile number');
        }
        else if (this.AuthMobileNumber != undefined && isNaN(parseInt(this.AuthMobileNumber)) == true) {
            this._HelperService.NotifyToastError('Enter valid mobile number');
        }
        else if (this.AuthMobileNumber.length < this._AppConfig.SelectedCountry.NumberLength || this.AuthMobileNumber.length > (this._AppConfig.SelectedCountry.NumberLength + 3)) {
            this._HelperService.NotifyToastError('Enter valid mobile number');
        }
        else if (this.AuthPin == undefined || this.AuthPin == null || this.AuthPin == "") {
            this._HelperService.NotifyToastError('Enter your account pin');
        }
        else if (this.AuthPin != undefined && isNaN(parseInt(this.AuthPin)) == true) {
            this._HelperService.NotifyToastError('Enter valid account pin');
        }
        else if (this.AuthPin.length != 4) {
            this._HelperService.NotifyToastError('Enter valid 4 digit account pin');
        }
        else {
            var VMobileNumber = this.AuthMobileNumber.toString();
            VMobileNumber = VMobileNumber.replace("+", "");
            VMobileNumber = VMobileNumber.replace(" ", "");
            var ExPrefix = this._AppConfig.SelectedCountry.Isd + "0";
            var CountryIsd = this._AppConfig.SelectedCountry.Isd;
            if (VMobileNumber.startsWith(ExPrefix)) {
                VMobileNumber = VMobileNumber.replace(ExPrefix, "");
            }
            if (VMobileNumber.startsWith("0")) {
                VMobileNumber = CountryIsd + VMobileNumber.substr(1, (VMobileNumber.length - 1));
            }
            else if (VMobileNumber.startsWith(CountryIsd)) {
                VMobileNumber = "" + VMobileNumber;
            }
            else {
                VMobileNumber = CountryIsd + VMobileNumber;
            }
            this.RequestOtp(VMobileNumber);
        }
    }

    RequestOtp(MobileNumber) {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var pData = {
            Task: 'appusersigninwithpin',
            CountryIsd: this._AppConfig.SelectedCountry.Isd,
            CountryIso: this._AppConfig.SelectedCountry.Iso,
            Type: 1,
            MobileNumber: MobileNumber,
            AccessPin: this.AuthPin,
            CountryId: this._AppConfig.SelectedCountry.ReferenceId,
            CountryKey: this._AppConfig.SelectedCountry.ReferenceKey,
        };
        this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.LoginInitiated,{MobileNumber:pData.MobileNumber} );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.LoginSuccessful,{MobileNumber:pData.MobileNumber});
                    this._Mixpanel.identify(pData.MobileNumber);
                    // this.VerificationDetails.Stage = 4;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
                    this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
                    this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.UserAppTempProfile);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                    this._HelperService.HideSpinner();
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Accessforgotpin,true)
                    // we are showing different pages (update pin page or dashboard page) user based on Istempin
                    if(!_Response.Result.IsTempPin){
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.AccountSetup);
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Accessforgotpin,false)
                    }

                    else{
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.UpdateProfile);
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Accessforgotpin,true)
                        
                    }
                    // Below if condition helps us to set the _Response.Result.Address address as active address
                    if(_Response.Result.Address != null)
                    {
                        this._ActiveAddress = _Response.Result.Address;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
                    }
                    
                    // var VerificationDetails =
                    // {
                    //     Stage: 2, 
                    //     Status: 0,
                    //     Type: this._HelperService.AppConfig.VerificationType.System,
                    //     MobileNumber: _Response.Result.MobileNumber,
                    //     CodeStart: _Response.Result.CodeStart,
                    //     RequestToken: _Response.Result.RequestToken,
                    //     VNumber: MobileNumber,
                    //     CountryIsd: this._AppConfig.SelectedCountry.Isd,
                    //     CountryId: this._AppConfig.SelectedCountry.ReferenceId,
                    //     CountryKey: this._AppConfig.SelectedCountry.ReferenceKey,
                    //     CountryName: this._AppConfig.SelectedCountry.Name,
                    // };
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, VerificationDetails);
                    // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.NumberVerify)
                }
                else {
                    this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.LoginFailed,{MobileNumber:pData.MobileNumber} );
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.LoginFailed,{MobileNumber:pData.MobileNumber} );
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    };

    async CountryChangeClick() {
        const modal = await this._ModalController.create({
            component: ModalSelectCountry,
            componentProps: this._AppConfig
        });
        modal.onDidDismiss().then(data => {
            if (data.data.PaymentSource == 'noaction') {
            }
            else {
                if (data.data != undefined && data.data != null && data.data != "") {
                    this._AppConfig.SelectedCountry = data.data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, this._AppConfig);
                }
                else {
                    this._AppConfig.SelectedCountry = this._AppConfig.Countries[0];
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, this._AppConfig);
                    // {
                    //     ReferenceId: 1,
                    //     ReferenceKey: "nigeria",
                    //     Name: "Nigeria",
                    //     IconUrl: "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/flag/ng.png",
                    //     Isd: "234",
                    //     Iso: "ng",
                    //     CurrencyName: "Naira",
                    //     CurrencyNotation: "NGN",
                    //     CurrencySymbol: "₦",
                    //     CurrencyHex: "&#8358;",
                    //     NumberLength: 10
                    // };
                }
            }
        });
        return await modal.present();
    }

    ForgotPin(){
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.ForgotPin)
    }
}