import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ModalSelectCountry } from '../../auth/modalselectcountry/modalselectcountry.component';

@Component({
  selector: 'app-countryselect',
  templateUrl: 'countryselect.component.html'
})
export class CountrySelectPage {
  public AuthMobileNumber = "";
  public AuthPin = "";
  public DeviceInformation: ODeviceInformation;
  constructor(
    public _ModalController: ModalController,
    private _StatusBar: StatusBar,
    private _MenuController: MenuController,
    public _HelperService: HelperService,
    private _AlertController: AlertController,
  ) {
    _MenuController.enable(false);
    var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    if (DeviceInformationStorage != null) {
      this.DeviceInformation = DeviceInformationStorage;
    }

  }

  _AppConfig =
    {
      HomeSlider: [],
      SelectedCountry: null,
      Countries: [
      ],
    }

  ngOnInit() {
    this._HelperService.PageLoaded();
    this._HelperService.TrackPixelPageView();
    // var _AppConfig =    this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig);
    // if( _AppConfig == null)
    // {
    this.GetConfiguration();
    // }
  }

  GetConfiguration() {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.ShowSpinner('Please wait');
    var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    var pData;
    if (DeviceI != null) {
      pData = {
        Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
        SerialNumber: DeviceI.SerialNumber,
        OsName: DeviceI.OsName,
        OsVersion: DeviceI.OsVersion,
        Brand: DeviceI.Brand,
        Model: DeviceI.Model,
        Width: DeviceI.Width,
        Height: DeviceI.Height,
        CarrierName: DeviceI.CarrierName,
        CountryCode: DeviceI.CountryCode,
        Mcc: DeviceI.Mcc,
        Mnc: DeviceI.Mnc,
        NotificationUrl: DeviceI.NotificationUrl,
        Latitude: DeviceI.Latitude,
        Longitude: DeviceI.Longitude,
        AppVersion: "1.1.10",
        UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
      };
    }
    else {
      pData = {
        Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
        SerialNumber: 'xx',
        OsName: "android",
        Brand: 'android',
        Model: "android",
        Width: 0,
        Height: 0,
        CarrierName: "airtel",
        CountryCode: "ng",
        Mcc: "000",
        Mnc: "000",
        NotificationUrl: "00",
        Latitude: 0,
        Longitude: 0,
        AppVersion: "1.1.10",
        UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
      };
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.App, pData);
    _OResponse.subscribe(
      _Response => {
        setTimeout(() => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HideSpinner();
        }, 2000);
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HideSpinner();
        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
          this._AppConfig = _Response.Result;
          if (this._AppConfig.HomeSlider != undefined || this._AppConfig.HomeSlider != null) {
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Slider.Home, this._AppConfig.HomeSlider);
          }
        }
        else {
          this._HelperService.Notify('Operation failed', _Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HideSpinner();
        this._HelperService.HandleException(_Error);
      });
  }
  CountrySelected(_Item) {
    this._AppConfig.SelectedCountry = _Item;
    var _ActiveAddress =
    {
      ReferenceId: 0,
      ReferenceKey: null,

      Task: "updateuseraddress",
      CountryId: this._AppConfig.SelectedCountry.ReferenceId,
      CountryKey: this._AppConfig.SelectedCountry.ReferenceKey,
      CountryName: this._AppConfig.SelectedCountry.Name,

      Name: null,
      ContactNumber: null,
      EmailAddress: null,
      AddressLine1: null,
      AddressLine2: null,
      Landmark: null,
      AlternateMobileNumber: null,

      CityAreaId: 0,
      CityAreaKey: null,
      CityAreaName: null,

      CityId: 0,
      CityKey: null,
      CityName: null,

      StateId: 0,
      StateKey: null,
      StateName: null,

      ZipCode: null,
      Instructions: null,

      MapAddress: null,
      Latitude: -1,
      Longitude: -1,

      IsPrimary: false,
      LocationTypeId: 800
    }
    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, _ActiveAddress);
    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, this._AppConfig);
    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
  }
}