import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VerifyPhonePage } from './verifyphone.component';
import { NgOtpInputModule } from 'ng-otp-input';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgOtpInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: VerifyPhonePage
            }
        ])
    ],
    declarations: [VerifyPhonePage]
})
export class VerifyPhonePageModule { }
