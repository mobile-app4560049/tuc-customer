import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ModalSelectCountry } from '../../auth/modalselectcountry/modalselectcountry.component';
import { Mixpanel, MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';
import * as _ from 'underscore';
declare var $:any;

@Component({
    selector: 'app-register',
    templateUrl: 'register.component.html'
})
export class RegisterPage {
    _Profile =
        {
            Name: null,
            EmailAddress: null,
            MobileNumber: null,
            Pin: null,
            ReferralCode: null,
        }


    // public AuthMobileNumber = "";
    // public AuthPin = "";
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ModalController: ModalController,
        private _StatusBar: StatusBar,
        private _MenuController: MenuController,
        public _HelperService: HelperService,
        private _AlertController: AlertController,
        private _Mixpanel:Mixpanel,
        private _MixpanelPeople:MixpanelPeople,
    ) {
        _MenuController.enable(false);
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
        this.GetConfiguration();
    }

    _AppConfig =
        {
            HomeSlider: [],
            SelectedCountry: null,
            Countries: [
            ],
        }
    GetConfiguration() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        var pData;
        if (DeviceI != null) {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: DeviceI.SerialNumber,
                OsName: DeviceI.OsName,
                OsVersion: DeviceI.OsVersion,
                Brand: DeviceI.Brand,
                Model: DeviceI.Model,
                Width: DeviceI.Width,
                Height: DeviceI.Height,
                CarrierName: DeviceI.CarrierName,
                CountryCode: DeviceI.CountryCode,
                Mcc: DeviceI.Mcc,
                Mnc: DeviceI.Mnc,
                NotificationUrl: DeviceI.NotificationUrl,
                Latitude: DeviceI.Latitude,
                Longitude: DeviceI.Longitude,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }
        else {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: 'xx',
                OsName: "android",
                Brand: 'android',
                Model: "android",
                Width: 0,
                Height: 0,
                CarrierName: "airtel",
                CountryCode: "ng",
                Mcc: "000",
                Mnc: "000",
                NotificationUrl: "00",
                Latitude: 0,
                Longitude: 0,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.App, pData);
        _OResponse.subscribe(
            _Response => {
                setTimeout(() => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                }, 2000);
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._AppConfig = _Response.Result;
                    if (this._AppConfig.HomeSlider != undefined || this._AppConfig.HomeSlider != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Slider.Home, this._AppConfig.HomeSlider);
                    }
                    this._AppConfig.SelectedCountry = this._AppConfig.Countries[0];
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, _Response.Result);
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }

    public ionViewDidEnter() {
        this._HelperService.SetPageName("Auth-NumberAdd");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }

    NavRegister() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Login)
    }
    async SetNumber() {
        if (this._AppConfig.SelectedCountry == undefined || this._AppConfig.SelectedCountry == null) {
            this._HelperService.NotifyToastError('Select your country');
        }
        else if (this._Profile.MobileNumber == undefined || this._Profile.MobileNumber == null || this._Profile.MobileNumber == "") {
            this._HelperService.NotifyToastError('Enter mobile number');
        }
        else if (this._Profile.MobileNumber != undefined && isNaN(parseInt(this._Profile.MobileNumber)) == true) {
            this._HelperService.NotifyToastError('Enter valid mobile number');
        }
        else if (this._Profile.MobileNumber.length < this._AppConfig.SelectedCountry.NumberLength || this._Profile.MobileNumber.length > (this._AppConfig.SelectedCountry.NumberLength + 3)) {
            this._HelperService.NotifyToastError('Enter valid mobile number');
        }
        else if (this._Profile.Pin == undefined || this._Profile.Pin == null || this._Profile.Pin == "") {
            this._HelperService.NotifyToastError('Enter your account pin');
        }
        else if (this._Profile.Pin != undefined && isNaN(parseInt(this._Profile.Pin)) == true) {
            this._HelperService.NotifyToastError('Enter valid account pin');
        }
        else if (this._Profile.Pin.length != 4) {
            this._HelperService.NotifyToastError('Enter valid 4 digit account pin');
        }
        else {
            var VMobileNumber = this._Profile.MobileNumber.toString();
            VMobileNumber = VMobileNumber.replace("+", "");
            VMobileNumber = VMobileNumber.replace(" ", "");
            var ExPrefix = this._AppConfig.SelectedCountry.Isd + "0";
            var CountryIsd = this._AppConfig.SelectedCountry.Isd;
            if (VMobileNumber.startsWith(ExPrefix)) {
                VMobileNumber = VMobileNumber.replace(ExPrefix, "");
            }
            if (VMobileNumber.startsWith("0")) {
                VMobileNumber = CountryIsd + VMobileNumber.substr(1, (VMobileNumber.length - 1));
            }
            else if (VMobileNumber.startsWith(CountryIsd)) {
                VMobileNumber = "" + VMobileNumber;
            }
            else {
                VMobileNumber = CountryIsd + VMobileNumber;
            }
            this.Register(VMobileNumber);
        }
    }
    Register(MobileNumber) {
        var Not = this._HelperService.GetStorageValue('dnot');
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var pData = {
            Task: 'appuserregister',
            CountryIsd: this._AppConfig.SelectedCountry.Isd,
            CountryIso: this._AppConfig.SelectedCountry.Iso,
            Type: 1,
            MobileNumber: MobileNumber,
            AccessPin: this._Profile.Pin,
            CountryId: this._AppConfig.SelectedCountry.ReferenceId,
            CountryKey: this._AppConfig.SelectedCountry.ReferenceKey,
            EmailAddress: this._Profile.EmailAddress,
            Name: this._Profile.Name,
            // DateOfBirth: this._Profile.DateOfBirth,
            // DisplayName: this._Profile.FirstName,
            ReferralCode: this._Profile.ReferralCode,
            OsName: this.DeviceInformation.OsName,
            OsVersion: this.DeviceInformation.OsVersion,
            SerialNumber: this.DeviceInformation.SerialNumber,
            Brand: this.DeviceInformation.Brand,
            Model: this.DeviceInformation.Model,
            Width: this.DeviceInformation.Width,
            Height: this.DeviceInformation.Height,
            CarrierName: this.DeviceInformation.CarrierName,
            Mcc: this.DeviceInformation.Mcc,
            Mnc: this.DeviceInformation.Mnc,
            NotificationUrl: Not,
        };
        this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.RegistrationInitiated,{MobileNumber:pData.MobileNumber} );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
        _OResponse.subscribe(
           async (_Response) => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.RegisteredSuccessfully,{MobileNumber:pData.MobileNumber});
                    await this._Mixpanel.alias(pData.MobileNumber);

                    await this._MixpanelPeople.set({
                        $name: _Response.Result.User.Name,
                         $email: _Response.Result.User.EmailAddress ,
                         $mobileNumber:pData.MobileNumber,
                         $countryIsd:_Response.Result.UserCountry.CountryIsd,
                    });
                    await this._Mixpanel.identify(_Response.Result.User.MobileNumber)
                    // this.VerificationDetails.Stage = 4;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
                    this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
                    this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.UserAppTempProfile);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                    this._HelperService.RefreshProfile();
                    this._HelperService.HideSpinner();
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.VerifyNumber);

                    // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.AccountSetup);
                    // var VerificationDetails =
                    // {
                    //     Stage: 2, 
                    //     Status: 0,
                    //     Type: this._HelperService.AppConfig.VerificationType.System,
                    //     MobileNumber: _Response.Result.MobileNumber,
                    //     CodeStart: _Response.Result.CodeStart,
                    //     RequestToken: _Response.Result.RequestToken,
                    //     VNumber: MobileNumber,
                    //     CountryIsd: this._AppConfig.SelectedCountry.Isd,
                    //     CountryId: this._AppConfig.SelectedCountry.ReferenceId,
                    //     CountryKey: this._AppConfig.SelectedCountry.ReferenceKey,
                    //     CountryName: this._AppConfig.SelectedCountry.Name,
                    // };
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, VerificationDetails);
                    // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.NumberVerify)
                }
                else if(_Response.Message.includes('Account already registered')){
                    this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.RegistrationUserAlreadyExist,{MobileNumber:pData.MobileNumber});
                }
                else{
                    this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.RegistrationFailed,{MobileNumber:pData.MobileNumber});
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }





    async CountryChangeClick() {
        const modal = await this._ModalController.create({
            component: ModalSelectCountry,
            componentProps: this._AppConfig
        });
        modal.onDidDismiss().then(data => {
            if (data.data.PaymentSource == 'noaction') {
            }
            else {
                if (data.data != undefined && data.data != null && data.data != "") {
                    this._AppConfig.SelectedCountry = data.data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, this._AppConfig);
                }
                else {
                    this._AppConfig.SelectedCountry = this._AppConfig.Countries[0];
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, this._AppConfig);
                    // {
                    //     ReferenceId: 1,
                    //     ReferenceKey: "nigeria",
                    //     Name: "Nigeria",
                    //     IconUrl: "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/flag/ng.png",
                    //     Isd: "234",
                    //     Iso: "ng",
                    //     CurrencyName: "Naira",
                    //     CurrencyNotation: "NGN",
                    //     CurrencySymbol: "₦",
                    //     CurrencyHex: "&#8358;",
                    //     NumberLength: 10
                    // };
                }
            }
        });
        return await modal.present();
    }
}