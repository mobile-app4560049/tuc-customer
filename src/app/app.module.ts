import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DatePipe } from '@angular/common';
import { Sim } from '@ionic-native/sim/ngx';
import { Device } from "@ionic-native/device/ngx";
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { DataService } from './service/data.service';
import { HelperService } from './service/helper.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { ChartsModule } from 'ng2-charts';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { NgCalendarModule } from 'ionic2-calendar';
import { FormsModule } from '@angular/forms';
import { MileStoneDetailsModal } from './access/modals/milestonedetails/milestonedetails.module.component';
import { TransactionDetailsModal } from './access/modals/transactiondetails/transactiondetails.modal.component';
import { PaymentTypeSelectModal } from './access/modals/paymenttypeselect/paymenttypeselect.module.component';
import { BillerPackageSelectModal } from './access/modals/billerpackageselect/billerpackageselect.modal.component';
import { FaqInfoDetailsModal } from './access/general/faq/faqinfo/faqinfo.modal.component';
import { DeliveryAddressModal } from './access/modals/deliveryaddress/deliveryaddress.modal.component';
import { LocationSelectorModal } from './access/modals/locationselector/locationselector.module.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { FCM } from '@ionic-native/fcm/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InputTrimModule } from 'ng2-trim-directive';
import { CountdownModule, CountdownGlobalConfig } from 'ngx-countdown';
import { DealDetailsModal } from './access/modals/dealdetails/dealdetails.modal.component';
import { QRCodeModule } from 'angularx-qrcode';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ScanMyCodeModal } from './access/features/tucpay/scanmycode/scanmycode.modal.component';
import { ReferralEditModal } from './access/modals/referraledit/referraledit.modal.component';
import { ScratchCardModal } from './access/modals/scratchcard/scratchcard.modal.component';
import { PaymentManagerModal } from './access/modals/payment/payment.modal.component';
import { Angular4PaystackModule } from 'angular4-paystack';
import { BuyPointDetailsModal } from './access/modals/buypointdetails/buypointdetails.modal.component';
import { VasPaymentDetailsModal } from './access/modals/vaspaymentdetails/vaspaymentdetails.component';
import { AddBankModal } from './access/account/bankmanager/modaladdbank/modaladdbank.component';
import { BankDetailsModal } from './access/account/bankmanager/modalbankdetails/modalbankdetails.component';
import { AddCashoutModal } from './access/features/cashout/modaladdcashout/modaladdcashout.component';
import { CashOutDetailsModal } from './access/features/cashout/modalcashoutdetails/modalcashoutdetails.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ImagesGalModal } from './access/modals/imagesgal/imagesgal.modal.component';
import { AppUpdateModal } from './access/modals/appupdate/appupdate.modal.component';
import { Market } from '@ionic-native/market/ngx';
import { BnplPlanModal } from './access/features/bnpl/modalplandetails/bnplplan.modal.component';
import { BnplInfoModal } from './access/features/bnpl/modalhowitworks/bnplinfo.modal.component';
import { ModalSelectCountry } from './auth/modalselectcountry/modalselectcountry.component';
import { ModalSelect } from './auth/modalselect/modalselect.component';
// import { Deeplinks } from '@awesome-cordova-plugins/deeplinks/ngx';
import { NgOtpInputModule } from 'ng-otp-input';
import { UploadkycComponent } from './access/features/bnpl/uploadkyc/uploadkyc.component';
import { ModalbacktohomeComponent } from './access/features/bnpl/modalbacktohome/modalbacktohome.component';
import { ModaldebitmandateComponent } from './access/features/bnpl/modaldebitmandate/modaldebitmandate.component';
import { AddressSelectorModalPage } from './access/account/addressmanager/addressselector/addressselector.modal.component';
import { File } from '@ionic-native/file/ngx';
import { Mixpanel, MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';
import { FlutterwaveModule } from "flutterwave-angular-v3"
@NgModule({
  declarations: [AppComponent, TransactionDetailsModal, MileStoneDetailsModal,
    PaymentTypeSelectModal, BillerPackageSelectModal, FaqInfoDetailsModal,
    DeliveryAddressModal, LocationSelectorModal, DealDetailsModal, ScanMyCodeModal, ReferralEditModal, ImagesGalModal,
    AddressSelectorModalPage,
    AppUpdateModal,
    ScratchCardModal, PaymentManagerModal, BuyPointDetailsModal,
    ModaldebitmandateComponent,
    VasPaymentDetailsModal, AddBankModal, BankDetailsModal,
    ModalSelectCountry,
    ModalSelect,
    BnplPlanModal,
    BnplInfoModal,
    CashOutDetailsModal,
    AddCashoutModal, UploadkycComponent, ModalbacktohomeComponent],
  entryComponents: [TransactionDetailsModal, MileStoneDetailsModal, PaymentTypeSelectModal, BillerPackageSelectModal, ImagesGalModal,
    FaqInfoDetailsModal, DeliveryAddressModal, LocationSelectorModal,
    ModalSelect,
    AddressSelectorModalPage,
    ModaldebitmandateComponent,
    AppUpdateModal,
    DealDetailsModal, ScanMyCodeModal, ReferralEditModal, ScratchCardModal,
    PaymentManagerModal, BuyPointDetailsModal, VasPaymentDetailsModal,
    CashOutDetailsModal, BnplPlanModal,
    BnplInfoModal,
    AddBankModal, BankDetailsModal,
    ModalSelectCountry,
    AddCashoutModal, UploadkycComponent, ModalbacktohomeComponent],
  imports: [HttpClientModule, BrowserModule, FormsModule, NgCalendarModule, ChartsModule, IonicModule.forRoot(), AppRoutingModule,
    GooglePlaceModule,
    CountdownModule,
    InputTrimModule,
    QRCodeModule,
    NgOtpInputModule,
    Angular4PaystackModule.forRoot('pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12'),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA6nUouKi8QeQBh6hcgTnhKjoxNlUShh_E'
    }), AgmDirectionModule,
    FlutterwaveModule],
  providers: [
    FormsModule,
    Device,
    Sim,
    Market,
    StatusBar,
    SplashScreen,
    DataService,
    HelperService,
    CountdownGlobalConfig,
    Geolocation,
    DatePipe,
    ImagePicker,
    LaunchNavigator,
    InAppBrowser,
    ChartsModule,
    FingerprintAIO,
    FCM,
    File, MixpanelPeople,
    CallNumber,
    Mixpanel,
    SocialSharing,
    QRScanner,
    // Deeplinks,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
