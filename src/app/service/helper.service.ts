import { DatePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ModalController, NavController, Platform, ToastController, MenuController } from '@ionic/angular';
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import "../../assets/js/systemhelper.js";
import { OResponse, OAccountInfo, OAccountOwner, OAddressComponent } from './object.service';
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScanMyCodeModal } from '../access/features/tucpay/scanmycode/scanmycode.modal.component.js';
import { Mixpanel, MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';
declare var $: any;
declare var moment: any;
declare var SystemHelper: any;
declare let fbq: Function; import { Location } from "@angular/common";
@Injectable()
export class HelperService {
    public AccountInfo: OAccountInfo;
    // public AppCountry: OAppCountry = null;
    _ActiveFeatures =
        {
            ussdverification: false,
            service_deals: false,
            service_nearby: false,
            service_bnpl: false,
            service_booking: false,
            service_vas_airtime: false,
            service_vas_tvrecharge: false,
            service_vas_electricity: false,
            service_vas_utilities: false,
            service_vas_lcctopup: false,
            general_about: false,
            general_support: false,
            general_faq: false,
            account_resetpin: false,
            account_updatepin: false,
            account_storedcards: false,
            account_bankaccount: false,
            account_profile: false,

            payments_ussd: false,
            payments_paystack: false,

        };
    constructor(
        public _StatusBar: StatusBar,
        public _MenuController: MenuController,
        public _ToastController: ToastController,
        public _ModalController: ModalController,
        public _Platform: Platform,
        private _Http: HttpClient,
        public _NavController: NavController,
        public _Geolocation: Geolocation,
        public _NavLocation: Location,
        public _DatePipe: DatePipe,
        public _LoadingController: LoadingController,
        public _AlertController: AlertController, 
        private _Mixpanel: Mixpanel, 
        private mixpanelPeople: MixpanelPeople
    ) {
        this.RefreshProfile();
        this.HideProgress();
        this.HideSpinner();

        this.IsFormProcessing = false;
    }
    otpConfig =
        {
            allowNumbersOnly: true,
            isPasswordInput: false,
            disableAutoFocus: true,
            length: 4,
            inputClass: 'otpinput'
        }
    TrackPixelPageView() {
        // fbq('track', 'PageView');
    }
    PageLoaded() {
        this.HideProgress();
        this.HideSpinner();
        this.IsFormProcessing = false;
        // fbq('track', 'PageView');
    }

    public Features =
        {
            ussdverification: 'ussdverification',
        }

    AnaSlideOptions = {
        initialSlide: 1,
        speed: 400,
        slidesPerView: 1.1,
        centeredSlides: true,
        spaceBetween: 16
    }
    TopSlideOptions = {
        initialSlide: 1,
        slidesPerView: 1.15,
        centeredSlides: false,
        spaceBetween: 0
    }
    public _Modal =
        {
            Show: false,
            Title: "",
            SubTitle: ""
        }

    ShowModalSuccess(Title, SubTitle) {
        this._Modal.Show = true;
        this._Modal.Title = Title;
        this._Modal.SubTitle = SubTitle;
        setTimeout(() => {
            this._Modal.Show = false;
        }, 10000);
    }
    CloseModalSuccess() {

        this._Modal.Show = false;

    }

    public CardColors = [
        "#0047BB",
        "#E0881D",
        "#9FC23E",
        "#F15BB6",
        "#2AB764",
        "#FFBC4E",
        "#02BBF9",
        "#EE4266",
        "#7E62C7",
        "#613DC1",
        "#3E81F0",
        "#9B5DE4",
        "#03085E",
        "#ED617E",
        "#00F5D4",
        "#D90D29",
        "#537995",
        "#087E8B",
        "#E35647",
        "#0B3954",

    ]
    async ModalDismiss(Data) {
        await this._ModalController.dismiss(Data);
    }

    public ionViewWillEnter() {
        // alert('ionViewWillEnter');
        // this._StatusBar.backgroundColorByHexString('#AF1482');
    }
    public ionViewDidEnter() {
        // alert('ionViewDidEnter');
        // this._StatusBar.backgroundColorByHexString('#AF1482');
    }
    public _AppConfigurations =
        {
            ServerAppVersion: null,
            IsAppAvailable: false,
            IsReceiverAvailable: null,
            IsDonationAvailable: false,
            Donation:
            {
                ReceiverTitle: null,
                ReceiverMessage: null,
                Title: null,
                Message: null,
                Description: null,
                Conditions: null,
                IconUrl: null,
                DonationRanges: [],
            },
            Referral:
            {
                Title: null,
                Description: null,
                ShareMessageTitle: null,
                ShareMessageDescription: null,
            },
            AboutApp: "Welcome! ThankUCash is a rewards program operated by Connected Analytics Limited (referred to as “ThankUCash” or “we” or “us”). ThankUCash lets you earn ThankUCash rewards points (“Points”) by making qualifying purchases at certain retail and service companies and through other promotions and offers that may be provided by merchants (“merchants,” whom we may also refer to as “partners”) or other merchants.",
            Contacts: [
                {
                    type: 'call',
                    icon: 'las la-phone-volume',
                    title: 'Call us on',
                    value: '0700 0433433',
                    systemvalue: '+2347000433433',
                },
                {
                    type: 'call',
                    icon: 'las la-phone-volume',
                    title: 'Call us on',
                    value: '01 888 8177',
                    systemvalue: '+23418888177',
                },
                {
                    type: 'whatsapp',
                    icon: 'lab la-whatsapp',
                    title: 'Message us on',
                    value: '0813 3178563',
                    systemvalue: 'https://api.whatsapp.com/send?phone=+2348133178563',
                },
                {
                    type: 'email',
                    icon: 'las la-envelope',
                    title: 'Email us on',
                    value: 'hello@thankucash.com',
                    systemvalue: 'mailto:hello@thankucash.com',
                }
            ]
        }
    RefreshAppConfiguration() {
        var _Config = this.GetStorage(this.AppConfig.StorageHelper.AppConfig);
        if (_Config != null) {
            this._AppConfigurations = _Config;
        }
        var DeviceI = this.GetStorage(this.AppConfig.StorageHelper.Device);
        var pData;
        if (DeviceI != null) {
            pData = {
                Task: this.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: DeviceI.SerialNumber,
                OsName: DeviceI.OsName,
                OsVersion: DeviceI.OsVersion,
                Brand: DeviceI.Brand,
                Model: DeviceI.Model,
                Width: DeviceI.Width,
                Height: DeviceI.Height,
                CarrierName: DeviceI.CarrierName,
                CountryCode: DeviceI.CountryCode,
                Mcc: DeviceI.Mcc,
                Mnc: DeviceI.Mnc,
                NotificationUrl: DeviceI.NotificationUrl,
                Latitude: DeviceI.Latitude,
                Longitude: DeviceI.Longitude,
                AppVersion: "1.1.10",
                UserAccountKey: this.AccountInfo.UserAccount.AccountKey,
            };
        }
        else {
            pData = {
                Task: this.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: 'xx',
                OsName: "android",
                Brand: 'android',
                Model: "android",
                Width: 0,
                Height: 0,
                CarrierName: "airtel",
                CountryCode: "ng",
                Mcc: "000",
                Mnc: "000",
                NotificationUrl: "00",
                Latitude: 0,
                Longitude: 0,
                AppVersion: "1.1.10",
                UserAccountKey: this.AccountInfo.UserAccount.AccountKey,
            };
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V3.App, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._AppConfigurations = _Response.Result;
                    if (_Response.Result.Countries != undefined && _Response.Result.Countries != null) {
                        var Configs = this.GetStorage(this.AppConfig.StorageHelper.hcAppConfig);
                        var SelectedCountry = Configs.SelectedCountry;
                        if (Configs != undefined && Configs != null) {
                            Configs.Countries = _Response.Result.Countries;
                            var _MCat = Configs.Countries.filter(x => x.ReferenceId == SelectedCountry.ReferenceId);
                            if (_MCat != undefined && _MCat != null) {
                                SelectedCountry = _MCat[0];
                                Configs.SelectedCountry = _MCat[0];
                            }
                            this.SaveStorage(this.AppConfig.StorageHelper.hcAppConfig, Configs);
                        }
                    }
                    // this.SaveStorage(this.AppConfig.StorageHelper.hcAppConfig, _Response.Result);
                    this.SaveStorage(this.AppConfig.StorageHelper.AppConfig, _Response.Result);
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            }
        );
    }

    ManageLogin(NaviageUrl, NavigateRoot: boolean) {
        console.log(NaviageUrl);
        if (this.AccountInfo.UserAccount.AccountId > 0) {
            if (NavigateRoot) {
                this.NavigateRoot(NaviageUrl);
            }
            else {
                this.NavigatePush(NaviageUrl);
            }
        }
        else {
            this.NavigatePush(this.AppConfig.Pages.Auth.Login);
        }
    }
    CheckIsLogin() {
        if (this.AccountInfo.UserAccount.AccountId > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    UserCustomAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,

            Task: "updateuseraddress",
            CountryId: null,
            CountryKey: null,
            CountryName: null,

            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityAreaId: 0,
            CityAreaKey: null,
            CityAreaName: null,

            CityId: 0,
            CityKey: null,
            CityName: null,

            StateId: 0,
            StateKey: null,
            StateName: null,

            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: -1,
            Longitude: -1,

            IsPrimary: false,
            LocationTypeId: 800

        }
    RefreshProfile() {
        var Account = this.GetStorage(this.AppConfig.StorageHelper.Account);
        if (Account != null) {
            this.AccountInfo = Account as OAccountInfo;
            // AppConfig
            if (this.AccountInfo.UserCountry != undefined && this.AccountInfo.UserCountry != null) {
                if (this.AccountInfo.UserCountry.CurrencyCode != undefined && this.AccountInfo.UserCountry.CurrencyCode != null) {
                    this.AppConfig.CurrencySymbol = this.AccountInfo.UserCountry.CurrencyCode;
                }
            }
            if (this.AccountInfo.User.EmailAddress != undefined && this.AccountInfo.User.EmailAddress != null) {
                this.AccountInfo.User.EmailAddress = this.AccountInfo.User.EmailAddress.trim();
            }
            if (this.AccountInfo.UserOwner != undefined && this.AccountInfo.UserOwner != null) {
                // this.AccountOwner = this.AccountInfo.UserOwner;
            }
            else {
                // this.AccountOwner =
                // {
                //     AccountCode: this.AccountInfo.UserAccount.AccountCode,
                //     AccountKey: this.AccountInfo.UserAccount.AccountKey,
                //     Name: this.AccountInfo.User.Name,
                //     IsTucPlusEnabled: this.AccountInfo.UserAccount.IsTucPlusEnabled,
                //     IconUrl: this.AccountInfo.UserAccount.IconUrl,
                //     DisplayName: this.AccountInfo.UserAccount.DisplayName,
                //     AccountTypeCode: this.AccountInfo.UserAccount.AccountTypeCode,
                //     AccountId: this.AccountInfo.UserAccount.AccountId,
                // }
            }
        }
        else {
            // this.AccountOwner =
            // {
            //     AccountCode: null,
            //     AccountId: 0,
            //     AccountKey: null,
            //     AccountTypeCode: null,
            //     DisplayName: null,
            //     IconUrl: null,
            //     IsTucPlusEnabled: false,
            //     Name: null,
            // }
            this.AccountInfo =
            {
                AccessKey: null,
                LoginTime: null,
                PublicKey: null,
                User:
                {
                    Address: "na",
                    AddressLatitude: 0,
                    AddressLongitude: 0,
                    ContactNumber: "na",
                    ContactNumberVerificationStatus: 0,
                    EmailAddress: "na",
                    EmailVerificationStatus: 0,
                    FirstName: "na",
                    LastName: "na",
                    MobileNumber: "na",
                    Name: "na",
                    UserName: "na",
                    DateOfBirth: null,
                    Gender: "na",
                    GenderCode: "na"
                },
                UserAccount:
                {
                    AccountCode: "na",
                    AccountId: 0,
                    AccountKey: null,
                    AccountType: "na",
                    AccountTypeCode: "na",
                    CreateDate: null,
                    DisplayName: "Guest User",
                    IconUrl: 'https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/defaults/defaulticon.png',
                    IsAccessPinSet: "na",
                    IsTucPlusEnabled: false,
                    PosterUrl: "na",
                    ReferralCode: "na",
                    UserKey: "na",
                },
                UserCountry:
                {
                    CountryIsd: "na",
                    CountryIso: "na",
                    CountryKey: "na",
                    CountryName: "na",
                    CurrencyName: "na",
                    CurrencyNotation: "na",
                    CountryId: 0,
                    CurrencyCode: 'na',
                    CurrencySymbol: 'na'
                },
                UserOwner:
                {
                    AccountCode: null,
                    AccountId: null,
                    AccountKey: null,
                    AccountTypeCode: null,
                    DisplayName: null,
                    IconUrl: null,
                    IsTucPlusEnabled: null,
                    Name: null,
                }
            }


        }
        var UserCustomAddress = this.GetStorage(this.AppConfig.StorageHelper.DeliveryLocation);
        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
    }
    public AppConfig = {
        TucConfirmModal: false,
        TucModalBackdrop: false,
        IsProcessing: false,
        ShowProgressBar: false,
        AccountOwnerId: 0,
        AccountOwnerKey: null,
        AccountOwnerDisplayName: null,
        AccountOwnerIconUrl: null,
        CurrencySymbol: "&#8358;",
        StatusSuccess: "Success",
        StatusError: "Error",
        ActiveLocation:
        {
            Lat: 0,
            Lon: 0,
        },
        DeliveryLocation:
        {
            Lat: 0,
            Lon: 0,
        },

        ServerUrl: "https://appconnect.thankucash.com/",
        PayStackKey: "pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12",
        MixPanelToken: "afa4e0085a26823cc6bd58f894c0176c", //production mixpanel token
        NetworkKeys: {
            Android: {
                AppVersion: "2.0.2",
                AppKey: "7108cbb6221e4504ba490d0e292c469a",
                AppVersionKey: "757de534b3a84f3db4f10584b06fc91c"
            },
            Ios: {
                AppVersion: "2.0.2",
                AppKey: "e9c91dd719f043bbb0ea910863f35eb6",
                AppVersionKey: "d896ebc649fb44a186c128030162096c"
            }
        },
        Topics:
        {
            ninjaorders: 'ninjaorders',
            tuccustomer: 'tuccustomer',
        },

        // ServerUrl: "https://testappconnect.thankucash.com/",
        // ServerUrl: "https://appconnect.thankucash.co/",
        // ServerUrl: "https://appconnect.thankucash.tech/",
        // ServerUrl: "https://appconnect.thankucash.dev/",
        // MixPanelToken: "d8b70ab020d81b13e67c1b1a90f45b77", //QA mixpanel token
        // MixPanelToken:"ad52dd9b8eb5bf430c10127ef2afb01d", // DEV Mixpanel token
        // PayStackKey: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6",
        // NetworkKeys: {
        //     Android: {
        //         AppVersion: "2.0.0.0",
        //         AppKey: "8ea170ea9b1d475c9eb0faf38ecc4dda",
        //         AppVersionKey: "559d17c454511f0114eb20f1d4d41b6bc0fa2e53"
        //     },
        //     Ios: {
        //         AppVersion: "2.0.0.0",
        //         AppKey: "bc516b75c84c4332b38e2ab2571a044c",
        //         AppVersionKey: "afa26d630c9840b7889d01d9c88c6ac6"
        //     }
        // },
        // Topics:
        // {
        //     ninjaorders: 'ninjaorders_stest',
        //     tuccustomer: 'tuccustomer_stest',
        // },

     
        
            isMixpanelDisable: false,
            MinPanelEvents: {
                LandingPage: "APP_LANDING_PAGE",
                home:"APP_HOME_PAGE",
                DealDashboard:"APP_DEALDASHBOARD_PAGE",
                Profile:"APP_PROFILE_PAGE",
                deals: "APP_DEALS_LIST_PAGE",
                buydeal: "APP_BUY_DEAL_PAGE",
                DealViewed: "APP_DEALS_VIEW_PAGE",
                Checkout: "APP_CHECKOUT",
                BuyNow: "APP_BUY_NOW",
                ProductBuy:"APP_PRODUCT_BUY",
                Payment: "APP_PAYMENT",
                Registration: "APP_REGISTRATION",
                DealSearch: "APP_USER_SEARCH",
                LoginInitiated: "APP_LOGIN_INITIATED",
                LoginFailed: "APP_LOGIN_FAILED",
                LoginSuccessful: "APP_LOGIN_SUCCESSFUL",
                RegistrationInitiated: "APP_REGISTRATION_INITIATED",
                RegistrationFailed: "APP_REGISTRATION_FAILED",
                RegisteredSuccessfully: "APP_REGISTERED_SUCCESSFULLY",
                RegistrationUserAlreadyExist: "APP_REGISTRATION_USER_ALREADY_EXIST"
              },
        // ServerUrl: "http://localhost:5001/",
        // PayStackKey: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6",
        // NetworkKeys: {
        //     Android: {
        //         AppVersion: "1.1.1",
        //         AppKey: "8ea170ea9b1d475c9eb0faf38ecc4dda",
        //         AppVersionKey: "c294c07f7d73d9774f2e06ecc594ad007f20ca15"
        //     },
        //     Ios: {
        //         AppVersion: "1.1.1",
        //         AppKey: "bc516b75c84c4332b38e2ab2571a044c",
        //         AppVersionKey: "b8380b7bc75bde95d1c9b57dfa8f5ed2c437a3be"
        //     }
        // },
        // Topics:
        // {
        //     // ninjaorders: 'ninjaorders',
        //     tuccustomer: 'tuccustomer',
        // },
        // NetworkKeys: {
        //     Android: {
        //         AppVersion: "1.0.9",
        //         AppKey: "8ea170ea9b1d475c9eb0faf38ecc4dda",
        //         AppVersionKey: "521f0000bca53a8e1550d9eb58d06e00bfe051e4"
        //     },
        //     Ios: {
        //         AppVersion: "1.0.9",
        //         AppKey: "bc516b75c84c4332b38e2ab2571a044c",
        //         AppVersionKey: "d8ac616f3a7a8cb52da1967ec98bd0f38b29534b"
        //     }
        // },
        // Topics:
        // {
        //     ninjaorders: 'ninjaorders_stest',
        //     tuccustomer: 'tuccustomer_stest',
        // },
        ActiveReferenceId: null,
        ActiveReferenceKey: null,
        TimeZone: "Africa/Lagos",
        TimeFormat: "h:mm a",
        DateFormat: "Do MMM YYYY",
        DateMonthFormat: "DD-MMM",
        DateTimeFormat: "DD-MM-YYYY h:mm a",
        // DealType:
        // {
        //     ProductDeal: 802,
        //     ServiceDeal: 803,
        // },
        DealTypeCode:
        {
            ProductDeal: 'dealtype.product',
            ServiceDeal: 'dealtype.service',
        },
        DeliveryTypeCode:
        {
            InStorePickUp: 'deliverytype.instore',
            Delivery: 'deliverytype.delivery',
            InStoreAndDelivery: 'deliverytype.instoreanddelivery',
        },
        Network: {
            V1: {
                System: "api/v1/system/",
                // ThankU: "api/v1/thanku/",
                Account: "api/v1/account/",
                // Core: "api/v1/core/",
            },
            V2: {
                System: "api/v2/system/",
                ThankU: "api/v2/thanku/",
                TUCAcc: "api/v2/tucacccore/",
                TUCApp: "api/v2/tucapp/",
                HCProduct: "api/v2/hcproduct/",
                HCFaq: "api/v2/faq/",
                TUCCustomerApp: 'api/v2/tuccustomer/',
                // TUCTransCore: "api/v2/tuctranscore/",
                TUCAccCore: "api/v2/tucacccore/",
                // TUCRmManager: "api/v2/rmmanager/",
                // ThankUCashLead: "api/v2/tucashlead/",
                // TUCCampaign: "api/v2/tuccampaign/",
                // TUCAnalytics: "api/v2/tucanalytics/",
                // TUCGiftPoints: "api/v2/tucgiftpoints/",
                // CAProduct: "api/v2/caproduct/",
                // HCWorkHorse: "api/v2/workhorse/"
            },
            V3:
            {
                App: "api/v3/cust/app/",
                Deals: "api/v3/plugins/dealsop/",
                MadDeals: "api/v3/cust/deals/",
                Acc: "api/v3/cust/account/",
                Redeem: "api/v3/cust/redeem/",
                Payments: "api/v3/cust/payments/",
                Vas: "api/v3/plugins/vasops/",
                Bnpl: "api/v3/plugins/bnpl/",
                Evolve: "api/v3/evolve/evolve/",
                AddressManager: "api/v3/tuc/address/",
                Logistics: "api/v3/plugins/lsaddress/",
                LSOperations: "api/v3/plugins/lsops/",
            }
        },
        NetworkApi:
        {
            Logout: "logout",
            Feature:
            {
                SaveUserAccount: 'saveuseraccount',
                // Get_SliderImages: "getsliderimages",
                getappconfiguration: 'getappconfiguration',
                getuseraccountbalance: "getuseraccountbalance"
            },
            getaccountparameters: 'getaccountparameters',
            saveaccountparameter: 'saveaccountparameter',
            v2:
            {
                getcategories: 'getcategories',
                getmerchants: 'getmerchants',
                getmerchantcategories: 'getmerchantcategories',
                getstores: 'getstores',
                gettransactions: 'gettransactions',
            },
            V3:
            {

                Vas:
                {
                    getvasoptions: 'getvasoptions',
                },
                Bnpl:
                {
                    getcustomerconfiguration: 'getcustomerconfiguration',
                    getmerchants: 'getmerchants',
                    savecustomer: 'savecustomer',
                    getloans: 'getloans',
                    getloan: 'getloan',
                    createloanrequest: 'createloanrequest',
                    validateotp: "validateotp",
                    monoauth: 'api/v3/mono/mono/',
                    evolve: 'api/v3/evolve/evolve/'
                }
            }
        },
        StorageHelper: {
            hcStates: "hcstates",
            hcAppConfig: "hcAppConfig",
            AppConfig: 'appconfig',
            OReqH: "hcscoreqh",
            Device: "hcscd",
            BankStatement: "bankstatements",
            Location: "hcsl",
            DeliveryLocation: "hccsl",
            Account: "hca",
            AddressList: "hcaaddresslist",
            ActiveReference: "hcark",
            ActiveDealReference: "hcadr",
            BnPlAccountReference: "bnplconfig",
            ActiveDealerReference: "hcadrk",
            // Verification: "hcv",
            Stations: "enstations",
            Products: "enproducts",
            PaymentsHistory: "paymentshistory",
            OrdersHistory: "ordershistory",
            ActiveOrder: "activeorder",
            SelectedProduct: "activeproduct",
            BillerDetails: "billerdetails",
            PaymentConfig: 'hcpaymentconfig',
            Package: 'hcpackage',
            DeviceNotifications: "hcdnotyfy",
            AccountBalance: "hcab",
            Permissions: "hcap",
            NumberVerification: "hcnvac",
            UserAppTempProfile: "hcutprofile",
            // UserAppStatus: "hcuas",
            ForgotPasswordVerification: "fpvr",
            ForgotAccessPinVerification: "fpvr",
            ChangeContactNumberVerification: "ccnvr",
            ChangeEmailAddressVerification: "ceavr",
            SliderImages: "sliderimages",
            Slider:
            {
                Home: 'home',
            },
            TMerchants: "tmerchants",
            TMerchantCategories: "tmerchantcategories",
            TStores: "tstores",
            TTransactions: "ttransactions",
            TUserMerchantVisits: "tusermerchantvisits",
            TRewards: "trewards",
            TCategory: "tcategory",
            TSyncTime: "tsynctime",
            TCategories: "tcategories",
            ActiveTOrder: 'activetorder',
            VasServices: 'vasservices',
            Bnpl:
            {
                AccountConfig: 'accconfig',
            },
            MadDeals:
            {
                Categories: 'mdcat',
                ActiveCategory: 'mdcatactve',
                Merchanats: 'mdmerchants',
                DealsPromoEndingSoon: 'DealsPromoEndingSoon',
                DealsPromoDiscounts: 'DealsPromoDiscounts',
                DealsPromoTop: 'DealsPromoTop',
                DealsNew: 'DealsNew',
                HomeSl1: 'HomeSl1'
            },
            Accessforgotpin: "pinVerify"
        },
        Pages:
        {
            Auth:
            {
                Intro: 'intro',
                Login: 'login',
                Register: 'register',
                VerifyNumber: 'verifynumber',
                AccountSetup: 'accountsetup',
                // NumberAdd: 'numberadd',
                // NumberVerify: 'numberverify',
                // SetPin: 'setpin',
                // NumberVerified: 'numberverified',
                // AuthProfile: 'authprofile',
                ForgotPin: 'forgotpin',
            },
            Access:
            {
                CountrySelect: 'countryselect',
                Dashboard: 'dashboard',
                Profile: 'profile',
                notifications: 'notifications',
                ChangePin: 'updatepin',
                Faq: 'faq',
                referral: 'referral',
                referralbonus: 'referralbonus',
                FaqDetails: 'faqdetails',
                Terminals: 'terminals',
                Cashiers: 'cashiers',
                SalesHistory: 'saleshistory',
                OrdersHistory: 'ordershistory',
                PointsHistory: 'pointshistory',
                Wallet: 'wallet',
                TransactionsHistory: 'saleshistory',
                About: 'about',
                UpdateProfile: 'updateprofile',
                // LiveSupport: 'livesupport',
                StoreLocations: 'storelocations',
                St1OrderDetails: 'st1orderdetails',
                St2OrderPayment: 'st2orderpayment',
                St3OrderConformation: 'st3orderconfirmation',

                slgasselectproduct: 'slgasselectproduct',
                slgasselectproductvarient: 'slgasselectproductvarient',

                d2dcreateorder: 'd2dcreateorder',
                orderaddresssetup: 'orderaddresssetup',
                orderpaymentprocess: 'orderpaymentprocess',
                orderdetails: 'orderdetails',

                servicehistory: 'servicehistory',
                addresslocator: 'addresslocator',
                Payments:
                {
                    PaymentDetails: 'paymentdetails',
                    LccTopup: 'lcctopup',
                    SelectBiller: 'selectbiller',
                    BillerPayment: 'billerpayment',
                },

                General:
                {
                    Support: 'support',
                },
                Stores: 'stores',
                Store:
                {
                    Details: 'storedetails',
                },
                Deals:
                {
                    Dashboard: "dealsdashboard",
                    Search: "dealsearch",
                    List: "deals",
                    FlashDeals: "flashdeals",
                    Details: "dealdetails",
                    Checkout: "dealcheckout",
                    ProductPurchaseHistory: "productpurchasehistory",
                    ProductPurchaseDetails: "productpurchasedetails",
                    DealPurchaseHistory: "dealpurchasehistory",
                    DealPurchaseDetails: 'dealpurchasedetails'
                },
                MerchantRedeem:
                {
                    scanredeemcode: 'scanredeemcode',
                    redeeminitialize: 'redeeminitialize',
                    redeemconfirm: 'redeemconfirm'
                },
                BuyPoints:
                {
                    initialize: 'buypointinitialize',
                    buypointconfirm: 'buypointconfirm',
                    list: 'wallettopuphistory'
                },
                Vas:
                {
                    vasproviders: 'vasproviders',
                    vasproviderpackage: 'vasproviderpackage',
                    vasprovideraccount: 'vasprovideraccount',
                    vaspaymentdetails: 'vaspaymentdetails',
                    list: 'vaspaymenthistory'
                },
                BankManager:
                {
                    list: "bankmanager",
                },
                Cashout:
                {
                    list: 'cashout'
                },
                CardsManager:
                {
                    cardmanger: 'cardmanger'
                },
                Address:
                {
                    add: 'addaddress',
                    edit: 'editaddress'
                },
                Bnpl:
                {
                    checkCreditLimit: 'bnpl/checklimit',
                    home: 'bnpl/home',
                    Loans: 'bnpl/loans',
                    LoanDetails: 'bnpl/loan',
                    planselector: 'bnpl/planselector',
                    planconfirm: 'bnpl/planconfirm',
                    loanprofile: 'bnpl/loanprofile',
                },
            }
        },
        LoaderItem: undefined,
        VerificationType:
        {
            MobiCheck: 'mobicheck',
            Duplex: 'duplex',
            System: 'system',
        },
        DataType: {
            Text: "text",
            Number: "number",
            Date: "date",
            Decimal: "decimal"
        },
        TransactionType:
        {
            OnlineReward: 'transaction.type.onlinerewardpoints',
            OnlineGiftPoint: 'transaction.type.onlinegiftpoints',
            CardReward: 'transaction.type.cardreward',
            CardChange: 'transaction.type.cardchange',
            MobileNumberReward: 'transaction.type.mobilenumebrrewards',
            InvoiceReward: 'transaction.type.inovicerewards',
            QRCodeScan: 'transaction.type.qucodescan'
        },
        TransactionTypeCategory:
        {
            Payments: "payments",
            Reward: "reward",
            Redeem: "redeem",
            PointsTransfer: "pointtransfer"
        },
        TransactionSource:
        {
            App: "transaction.source.app",
            Card: "transaction.source.card",
            Cashier: "transaction.source.cashier",
            Settlement: "transaction.source.settlement",
            Bank: "transaction.source.payments",
        },
        TransactionMode:
        {
            Credit: "transaction.mode.credit",
            Debit: "transaction.mode.debit",
        },
    }
    SetPageName(PageName) {
        try {
            // this._Platform.ready().then(() => {
            //     this._FirebaseAnalytics.setCurrentScreen(PageName)
            //         .then((res: any) =>  (res))
            //         .catch((error: any) => console.error(error));
            // });

        } catch (error) {
        }
    }
    FormatCardNumber(number) {
        let joy = number.match(/.{1,4}/g);
        return joy.join(' ');
    }
    public exc = 0;
    HandleException(Exception: HttpErrorResponse) {
        this.HideSpinner();
        if (Exception.status != undefined && Exception.status == 401) {
            var Ex = Exception as any;
            var ResponseData = JSON.parse(atob(Ex.error.zx)) as OResponse;
            this.exc = 0;
            if (ResponseData.ResponseCode == "HCA121") {
                if (this.exc == 0) {
                    this.exc = 1;
                    this.NotifyToastError(ResponseData.Message);
                    // this.Notify("Session Expired", ResponseData.Message);
                }
                this.DeleteStorage(this.AppConfig.StorageHelper.Account);
                this.DeleteStorage(this.AppConfig.StorageHelper.ActiveReference);
                this.DeleteStorage(this.AppConfig.StorageHelper.Stations);
                this.DeleteStorage(this.AppConfig.StorageHelper.Products);
                this.DeleteStorage(this.AppConfig.StorageHelper.PaymentsHistory);
                this.DeleteStorage(this.AppConfig.StorageHelper.OrdersHistory);
                this.DeleteStorage(this.AppConfig.StorageHelper.ActiveOrder);
                this.DeleteStorage(this.AppConfig.StorageHelper.SelectedProduct);
                this.NavigateRoot(this.AppConfig.Pages.Auth.Login);
            } else {
                this.NotifyToastError(ResponseData.Message);
                // this.Notify("Operation failed", ResponseData.Message);
            }
        }
    }
    GeneratePassoword() {
        var plength = 14;
        var keylistalpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var keylistint = "123456789";
        var keylistspec = "!@#_";
        var temp = "";
        var len = plength / 2;
        var len = len - 1;
        var lenspec = plength - len - len;

        for (let index = 0; index < len; index++)
            temp += keylistalpha.charAt(
                Math.floor(Math.random() * keylistalpha.length)
            );

        for (let index = 0; index < lenspec; index++)
            temp += keylistspec.charAt(
                Math.floor(Math.random() * keylistspec.length)
            );

        for (let index = 0; index < len; index++)
            temp += keylistint.charAt(Math.floor(Math.random() * keylistint.length));

        temp = temp
            .split("")
            .sort(function () {
                return 0.5 - Math.random();
            })
            .join("");

        return temp;
    }
    GetRandomNumber() {
        return Math.floor(1000 + Math.random() * 9000);
    }
    GenerateGuid() {
        var arr = new Uint8Array(40 / 2);
        window.crypto.getRandomValues(arr);
        return Array.from(arr, this.dec2hex).join("");
    }
    dec2hex(dec) {
        return ("0" + dec.toString(16)).substr(-2);
    }

    async NotifyToastSuccess(Message) {
        const toast = await this._ToastController.create({
            position: 'top',
            message: Message,
            duration: 2000,
            cssClass: 'toast-success'
            // color: "#a42e80"
        });
        toast.present();
    }
    async NotifyToastError(Message) {
        const toast = await this._ToastController.create({
            position: 'top',
            message: Message,
            duration: 3000,
            cssClass: 'toast-error'
            // color: "#a42e80"
        });
        toast.present();
    }
    async NotifyToast(Message) {
        const toast = await this._ToastController.create({
            position: 'top',
            message: Message,
            duration: 3000,
            // color: "#a42e80"
        });
        toast.present();
    }
    async Notify(Title, Message) {
        const alert = await this._AlertController.create({
            header: Title,
            message: Message,
            buttons: [{
                text: 'Ok',
                cssClass: 'text-light  alert-btn',
            },],

        });
        await alert.present();
    }
    async NotifySimple(Message) {
        const alert = await this._AlertController.create({
            message: Message,
            buttons: [{
                text: 'Ok',
                cssClass: 'text-light  alert-btn',
            },],
        });
        await alert.present();
    }
    private ProgressItem = 0;
    ShowProgress() {
        this.ProgressItem += 1;
        this.AppConfig.ShowProgressBar = true;
    }
    HideProgress() {
        this.ProgressItem = this.ProgressItem - 1;
        if (this.ProgressItem == 0) {
            setTimeout(() => {
                this.AppConfig.ShowProgressBar = false;
            }, 300);
        }
        if (this.ProgressItem < 0) {
            setTimeout(() => {
                this.AppConfig.ShowProgressBar = false;
            }, 300);
        }
    }
    async ShowSpinner(message = "Please wait") {
        this.AppConfig.IsProcessing = true;
        if (this.AppConfig.LoaderItem == undefined) {
            this.AppConfig.LoaderItem = await this._LoadingController.create({
                // message: message,

                spinner: null,
                mode: 'ios',
                cssClass: 'hcx-loadingcontroller',
                message: `
                <div class="hcx-loadingcontroller">
                <div class="hcx-loadingcontroller-img">   </div>
                <div class="hcx-loadingcontroller-title">
               `+ message + `
              </div>
              </div>`
            });
            return await this.AppConfig.LoaderItem.present();

        }
    }
    // async ShowSpinner(message = "Please wait...") {
    //     this.AppConfig.IsProcessing = true;
    //     if (this.AppConfig.LoaderItem == undefined) {
    //         this.AppConfig.LoaderItem = await this._LoadingController.create({
    //             message: message,
    //             spinner: "dots"
    //         });
    //         setTimeout(() => {
    //             // this.HideSpinner();
    //         }, 3000);
    //         return await this.AppConfig.LoaderItem.present();
    //     }
    // }
    HideSpinner() {
        this.AppConfig.IsProcessing = false;
        if (this.AppConfig.LoaderItem != undefined) {
            this.AppConfig.LoaderItem.dismiss();
            this.AppConfig.LoaderItem = undefined;
        }
        // setTimeout(() => {
        //     if (this.AppConfig.LoaderItem != undefined) {
        //         this.AppConfig.LoaderItem.dismiss();
        //         this.AppConfig.LoaderItem = undefined;
        //     }
        // }, 20000);
    }
    NavigateRoot(PageName) {
        this._NavController.navigateRoot(PageName);
    }
    NavigatePush(PageName) {
        this._NavController.navigateForward(PageName);
    }
    NavigatePushItem(PageName, Data) {
        this._NavController.navigateForward([PageName, Data]);
    }
    RefreshLocation() {
        var GeoLocationOptions = {
            maximumAge: 600000,
            timeout: 20000
        };
        var DeviceStorage = this.GetStorage(this.AppConfig.StorageHelper.Device);
        this.AppConfig.ActiveLocation.Lat = DeviceStorage.Latitude;
        this.AppConfig.ActiveLocation.Lon = DeviceStorage.Longitude;
        if (DeviceStorage != null) {
            this._Geolocation
                .getCurrentPosition()
                .then(resp => {
                    DeviceStorage.Latitude = resp.coords.latitude;
                    DeviceStorage.Longitude = resp.coords.longitude;
                    this.AppConfig.ActiveLocation.Lat = resp.coords.latitude;
                    this.AppConfig.ActiveLocation.Lon = resp.coords.longitude;
                    this.SaveStorage(this.AppConfig.StorageHelper.Device, DeviceStorage);
                })
                .catch(error => { });
        }
    }


    ShowTucConfirmModal() {
        this.AppConfig.TucConfirmModal = true;
        this.AppConfig.TucModalBackdrop = true;
    }

    HideTucConfirmModal() {
        this.AppConfig.TucConfirmModal = false;
        this.AppConfig.TucModalBackdrop = false;
    }


    NavStores() {
        this.NavigateRoot(this.AppConfig.Pages.Access.Stores);
    }
    // NavDealerLocations() {
    //     this.NavigateRoot(this.AppConfig.Pages.Access.OrderManager.DealerLocations);
    // }
    NavPoints() {
        this.NavigateRoot(this.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }
    NavDeals() {
        this.NavigateRoot(this.AppConfig.Pages.Access.Deals.List);
    }
    NavFlashDeals() {
        this.NavigatePush(this.AppConfig.Pages.Access.Deals.FlashDeals);
    }
    NavPaymentDetails() {
        this.NavigateRoot(this.AppConfig.Pages.Access.Payments.PaymentDetails);
    }
    NavBuyPointsConfirm() {
        this.NavigateRoot(this.AppConfig.Pages.Access.BuyPoints.buypointconfirm);
    }
    NavVerifyNumber() {
        this.NavigatePush(this.AppConfig.Pages.Auth.VerifyNumber);
    }
    //#region  Navigation
    NavLogin() {
        this.NavigatePush(this.AppConfig.Pages.Auth.Login);
    }
    NavDashboard() {
        this.NavigateRoot(this.AppConfig.Pages.Access.Dashboard);
    }
    NavDashboardDeals() {
        if (this._ActiveFeatures.service_deals == true) {
            this.NavigateRoot(this.AppConfig.Pages.Access.Deals.Dashboard);
        }
    }
    NavDashboardStores() {
        if (this._ActiveFeatures.service_nearby == true) {
            this.NavigateRoot(this.AppConfig.Pages.Access.Stores);
        }
    }
    NavDashboardTucPay() {
        this.ManageLogin(this.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode, true);
        // this.NavigateRoot(this.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode);
    }
    NavDashboardMore() {
        if (this.CheckIsLogin()) {
            this._MenuController.open('end');
        }
        else {
            this.NavLogin();
        }
    }
    NavLocationManager() {
        this.NavigatePush(this.AppConfig.Pages.Access.addresslocator);
    }
    NavBack() {
        this._NavLocation.back();
    }
    // async NavOpenQR() {
    //     const modal = await this._ModalController.create({
    //         component: ScanMyCodeModal,
    //     });
    //     modal.onDidDismiss().then(data => {
    //     });
    //     return await modal.present();
    // }
    NavNotification() {
        this.NavigatePush(this.AppConfig.Pages.Access.notifications);
    }
    //#endregion


    // SaveStorage(StorageName, StorageValue) {
    //     try {
    //         var StringV = btoa(JSON.stringify(StorageValue));
    //         localStorage.setItem(StorageName, StringV);
    //         return true;
    //     } catch (e) {
    //         alert(e);
    //         return false;
    //     }
    // }
    // SaveStorageValue(StorageName, StorageValue) {
    //     try {
    //         localStorage.setItem(StorageName, btoa(StorageValue));
    //         return true;
    //     } catch (e) {
    //         alert(e);
    //         return false;
    //     }
    // }
    // GetStorage(StorageName) {
    //     var StorageValue = localStorage.getItem(StorageName);
    //     if (StorageValue != undefined) {
    //         if (StorageValue != null) {
    //             return JSON.parse(atob(StorageValue));
    //         } else {
    //             return null;
    //         }
    //     } else {
    //         return null;
    //     }
    // }
    // GetStorageValue(StorageName) {
    //     var StorageValue = localStorage.getItem(StorageName);
    //     if (StorageValue != undefined) {
    //         if (StorageValue != null) {
    //             return atob(StorageValue);
    //         } else {
    //             return null;
    //         }
    //     } else {
    //         return null;
    //     }
    // }
    SaveStorage(StorageName, StorageValue) {
        try {
            var StringV = JSON.stringify(StorageValue);
            localStorage.setItem(StorageName, StringV);
            return true;
        } catch (e) {
            alert(e);
            return false;
        }
    }
    SaveStorageValue(StorageName, StorageValue) {
        try {
            localStorage.setItem(StorageName, StorageValue);
            return true;
        } catch (e) {
            alert(e);
            return false;
        }
    }
    GetStorage(StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                try {
                    var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");
                    if (base64Matcher.test(StorageValue)) {
                        var c = atob(StorageValue);
                        return JSON.parse(c);
                    } else {
                        return JSON.parse(StorageValue);
                    }

                } catch (error) {
                    return JSON.parse(StorageValue);
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    GetStorageValue(StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                try {
                    var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");
                    if (base64Matcher.test(StorageValue)) {
                        var c = atob(StorageValue);
                        return c;
                    } else {
                        return StorageValue;
                    }
                } catch (error) {
                    return StorageValue;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    RefreshAppActiveFeature() {
        Object.keys(this._ActiveFeatures).forEach(key => {
            this._ActiveFeatures[key] = false;
        });
        var Config = this.GetStorage(this.AppConfig.StorageHelper.hcAppConfig);
        if (Config != undefined && Config != null) {
            var Country = Config.SelectedCountry;
            if (Country != undefined && Country != null && Country.ActiveFeatures != undefined && Country.ActiveFeatures != null) {
                Country.ActiveFeatures.forEach(element => {
                    if (this._ActiveFeatures[element.SystemName] != undefined && this._ActiveFeatures[element.SystemName] != null) {
                        this._ActiveFeatures[element.SystemName] = true;
                    }
                });
            }
            else {
                // console.log(3, key, false);
                // return false;
            }
        }
        else {
            // console.log(4, key, false);
            // return false;
        }
    }

    // ValidateFeature(key) {
    //     console.log('validating');
    //     var Config = this.GetStorage(this.AppConfig.StorageHelper.hcAppConfig);
    //     if (Config != undefined && Config != null) {
    //         var Country = Config.SelectedCountry;
    //         if (Country != undefined && Country != null && Country.ActiveFeatures != undefined && Country.ActiveFeatures != null) {
    //             Country.ActiveFeatures .forEach(element => {
    //                 this._ActiveFeatures.element= true;
    //             });
    //             var Features = Country.ActiveFeatures as any[];
    //             var IsAvailable = Features.findIndex(x => x.SystemName == key);
    //             if (IsAvailable > -1) {
    //                 console.log(1, key, true);
    //                 return true;
    //             }
    //             else {
    //                 console.log(2, key, false);
    //                 return false;
    //             }
    //         }
    //         else {
    //             console.log(3, key, false);
    //             return false;
    //         }
    //     }
    //     else {
    //         console.log(4, key, false);
    //         return false;
    //     }
    // }
    // GetStorage(StorageName) {
    //     var StorageValue = localStorage.getItem(StorageName);
    //     if (StorageValue != undefined) {
    //         if (StorageValue != null) {
    //             return JSON.parse(StorageValue);
    //         } else {
    //             return null;
    //         }
    //     } else {
    //         return null;
    //     }
    // }
    // GetStorageValue(StorageName) {
    //     var StorageValue = localStorage.getItem(StorageName);
    //     if (StorageValue != undefined) {
    //         if (StorageValue != null) {
    //             return StorageValue;
    //         } else {
    //             return null;
    //         }
    //     } else {
    //         return null;
    //     }
    // }
    DeleteStorage(StorageName) {
        localStorage.removeItem(StorageName);
        return true;
    }
    payWithPaystack(Amount) {
        return SystemHelper.payWithPaystack(this.AccountInfo, Amount);
    }
    GetDateTime(Date) {
        return SystemHelper.GetDateTime(Date, this.AppConfig.TimeZone);
    }
    GetTimeS(Date) {
        return this._DatePipe.transform(Date, "h:mm a", this.AppConfig.TimeZone);
    }
    GetDateS(Date) {
        return this._DatePipe.transform(Date, "dd-MM-yyyy", this.AppConfig.TimeZone);
    }
    GetTodaysDate(Date) {
        return this._DatePipe.transform(Date, "dd-MM-yyyy", this.AppConfig.TimeZone);
    }
    GetCurrentDateTimeS() {
        return this._DatePipe.transform(new Date(), "dd-MM-yyyy h:mm a", this.AppConfig.TimeZone);
    }
    CheckDateIsAfter(Date, CompareTo) {
        return SystemHelper.CheckDateIsAfter(Date, CompareTo);
    }
    CheckDateIsBefore(Date, CompareTo) {
        return SystemHelper.CheckDateIsBefore(Date, CompareTo);
    }
    GetTimeDifference(Date, CompareTo) {
        return SystemHelper.GetTimeDifference(Date, CompareTo);
    }
    GetTimeDifferenceS(Date, CompareTo) {
        return SystemHelper.GetTimeDifferenceS(Date, CompareTo);
    }
    GetDateTimeS(Date) {
        if (Date != undefined && Date != null && Date != '') {
            return SystemHelper.GetDateTimeS(Date, this.AppConfig.TimeZone, this.AppConfig.DateTimeFormat);
        }
        else {
            return null;
        }
    }
    GetTimeInterval(Date, CompareTo) {
        return SystemHelper.GetTimeInterval(Date, CompareTo, this.AppConfig.TimeZone);
    }
    GetDateMonthS(Date) {
        return SystemHelper.GetDateS(Date, this.AppConfig.TimeZone, this.AppConfig.DateMonthFormat);
    }
    FormatDate(Date, Format) {
        return SystemHelper.GetDateS(Date, this.AppConfig.TimeZone, Format);
    }
    GetSearchConditionStrict(
        BaseString,
        ColumnName,
        ColumnType,
        ColumnValue,
        Condition
    ) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            ColumnType != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            //ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
            // ColumnValue != ''
        ) {
            if (ColumnType == "text") {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression = "( " + SearchExpression + ") AND ";
                }
                if (ColumnValue == "") {
                    SearchExpression += ColumnName + " " + Condition + ' "" ';
                } else if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                } else {
                    SearchExpression +=
                        "( " + ColumnName + " " + Condition + " null " + ")";
                }
            } else if (ColumnType == "number") {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression = "( " + SearchExpression + ") AND ";
                }
                if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                } else {
                    SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                }
            }

            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " OR ";
            }

            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    GetSearchCondition(BaseString, ColumnName, ColumnType, ColumnValue) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (
            ColumnName != undefined &&
            ColumnType != undefined &&
            ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" &&
            ColumnValue != ""
        ) {
            if (ColumnType == "text") {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression += " OR ";
                }
                SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
            } else if (ColumnType == "number") {
                if (isNaN(ColumnValue) == false) {
                    if (
                        SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != ""
                    ) {
                        SearchExpression = "( " + SearchExpression + ") AND ";
                    }
                    SearchExpression += ColumnName + " = " + ColumnValue + " ";
                }
            }

            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " OR ";
            }

            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    GetSearchConditionSubStrictFromArray(BaseString, ColumnName, ColumnType, ColumnValueArray, Condition) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = '';
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            ColumnType != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            ColumnName != '' &&
            ColumnType != '' //&&
        ) {
            if (ColumnValueArray != undefined || ColumnValueArray != null || ColumnValueArray.length != 0) {
                if (ColumnType == "text") {
                    var TExpression = '';
                    for (let index = 0; index < ColumnValueArray.length; index++) {
                        const ColumnValue = ColumnValueArray[index];
                        if (TExpression != '') {
                            TExpression += ' OR ';
                        }
                        if (ColumnValue != null && ColumnValue != '') {

                            TExpression += ColumnName + ' ' + Condition + ' "' + ColumnValue + '" ';
                        }
                        else {
                            TExpression += " " + ColumnName + ' ' + Condition + ' null ' + " ";
                        }
                    }
                    if (TExpression != '') {
                        if (SearchExpression != '') {
                            SearchExpression = "( " + SearchExpression + ") AND (" + TExpression + ")";
                        }
                        else {
                            SearchExpression = TExpression;
                        }
                    }
                }
                else if (ColumnType == "number") {
                    var TColSearch = '';
                    if (ColumnValueArray.length == 1) {
                        const ColumnValue = ColumnValueArray[0];
                        if (ColumnValue != null && ColumnValue != '') {
                            TColSearch += ' ' + ColumnName + ' ' + Condition + ' "' + ColumnValue + '"';
                        }
                        else {
                            TColSearch += " " + ColumnName + ' ' + Condition + ' ' + "";
                        }
                    }
                    else {
                        for (let index = 0; index < ColumnValueArray.length; index++) {
                            const ColumnValue = ColumnValueArray[index];
                            if (TColSearch != '') {
                                TColSearch += ' OR ';
                            }
                            if (ColumnValue != null && ColumnValue != '') {
                                TColSearch += ' ' + ColumnName + ' ' + Condition + ' "' + ColumnValue + '" ';
                            }
                            else {
                                TColSearch += "  " + ColumnName + ' ' + Condition + ' ' + " ";
                            }
                        }
                    }
                    if (TColSearch != '') {
                        if (SearchExpression != '') {
                            SearchExpression = "( " + SearchExpression + ") AND (" + TColSearch + ")";
                        }
                        else {
                            SearchExpression = TColSearch;
                        }
                    }
                }
            }
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    }

    GetSearchConditionStrictOr(
        BaseString,
        ColumnName,
        ColumnType,
        ColumnValue,
        Condition
    ) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (
            ColumnName != undefined &&
            ColumnType != undefined &&
            // ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            //ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
            // ColumnValue != ''
        ) {
            if (ColumnType == this.AppConfig.DataType.Text) {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression = "( " + SearchExpression + ") OR ";
                }
                // SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
                if (ColumnValue == "") {
                    SearchExpression += ColumnName + " " + Condition + ' "" ';
                } else if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                } else {
                    SearchExpression +=
                        "( " + ColumnName + " " + Condition + " null " + ")";
                }
            } else if (ColumnType == this.AppConfig.DataType.Number) {
                if (
                    isNaN(ColumnValue) == false &&
                    ColumnValue.toString().indexOf(".") == -1
                ) {
                    if (
                        SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != ""
                    ) {
                        SearchExpression = "( " + SearchExpression + ") OR ";
                    }
                    if (ColumnValue != null && ColumnValue != "") {
                        SearchExpression +=
                            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                    } else {
                        SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                    }
                }
            } else if (ColumnType == this.AppConfig.DataType.Decimal) {
                if (isNaN(ColumnValue) == false) {
                    if (
                        SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != ""
                    ) {
                        SearchExpression = "( " + SearchExpression + ") OR ";
                    }
                    if (ColumnValue != null && ColumnValue != "") {
                        SearchExpression +=
                            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                    } else {
                        SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                    }
                }
            }
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }

            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    GetDateCondition(BaseString, ColumnName, StartTime, EndTime) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (
            ColumnName != undefined &&
            StartTime != undefined &&
            EndTime != undefined &&
            ColumnName != null &&
            StartTime != null &&
            EndTime != null &&
            ColumnName != "" &&
            StartTime != "" &&
            EndTime != ""
        ) {

            StartTime = moment(StartTime).subtract(1, 'seconds');
            EndTime = moment(EndTime).add(1, 'seconds');
            var FSd = new Date(StartTime);
            var TStartDateM = moment(FSd)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss");
            var ESd = new Date(EndTime);
            var TEndTimeM = moment(ESd)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss");
            if (
                SearchExpression != undefined &&
                SearchExpression != null &&
                SearchExpression != ""
            ) {
                SearchExpression = "( " + SearchExpression + ") AND ";
            }
            SearchExpression +=
                "( " +
                ColumnName +
                ' > "' +
                TStartDateM +
                '" AND ' +
                ColumnName +
                ' < "' +
                TEndTimeM +
                '")';
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    PostData(NetworkLocation: string, Data: any): Observable<OResponse> {
        try {
            var DeviceInformation = this.GetStorage(this.AppConfig.StorageHelper.Device);
            var UserAccount = this.GetStorage(this.AppConfig.StorageHelper.Account);
            var PostUrl = this.AppConfig.ServerUrl + NetworkLocation + Data.Task;
            // console.log(PostUrl);

            var ehcak = null;
            var ehcavk = null;
            if (this._Platform.is("android")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            } else if (this._Platform.is("ios")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else if (this._Platform.is("ipad")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else if (this._Platform.is("iphone")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else {
                ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            }

            var _Headers;
            if (UserAccount != null && UserAccount.AccessKey != undefined) {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber))
                    .set("hcuak", UserAccount.AccessKey)
                    .set("hcupk", btoa(UserAccount.PublicKey))
                    ;
            }
            else {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber));
            }
            var ORequest = JSON.stringify({ zx: btoa(JSON.stringify(Data)) });
            return this._Http
                .post<ONetworkResponse>(PostUrl, ORequest, { headers: _Headers })
                .pipe(map(_Response => JSON.parse(atob(_Response.zx)) as OResponse))
                .pipe(catchError((error: HttpErrorResponse) => throwError(error)));

        } catch (error) {
            this.HideSpinner();
        }
    }

    CreateUser(NetworkLocation: string, Data: any): Observable<OResponse> {
        try {
            var DeviceInformation = this.GetStorage(this.AppConfig.StorageHelper.Device);
            var UserAccount = this.GetStorage(this.AppConfig.StorageHelper.Account);
            var PostUrl = this.AppConfig.ServerUrl + NetworkLocation + Data.Task;
            // console.log(PostUrl);

            var ehcak = null;
            var ehcavk = null;
            if (this._Platform.is("android")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            } else if (this._Platform.is("ios")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else if (this._Platform.is("ipad")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else if (this._Platform.is("iphone")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else {
                ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            }

            var _Headers;
            if (UserAccount != null && UserAccount.AccessKey != undefined) {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber))
                    .set("hcuak", UserAccount.AccessKey)
                    .set("hcupk", btoa(UserAccount.PublicKey))
                    ;
            }
            else {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber));
            }
            var ORequest = JSON.stringify({ zx: btoa(JSON.stringify(Data)) });
            return this._Http
                .post<ONetworkResponse>(PostUrl, ORequest, { headers: _Headers })
                .pipe(map(_Response => JSON.parse(atob(_Response.zx)) as OResponse))
                .pipe(catchError((error: HttpErrorResponse) => throwError(error)));

        } catch (error) {
            this.HideSpinner();
        }
    }

    CheckLimit(NetworkLocation: string, Params: any, Data: any): Observable<OResponse> {
        try {
            var DeviceInformation = this.GetStorage(this.AppConfig.StorageHelper.Device);
            var UserAccount = this.GetStorage(this.AppConfig.StorageHelper.Account);
            var PostUrl = this.AppConfig.ServerUrl + NetworkLocation + Data.Task + '?UserId=' + Params;
            // console.log(PostUrl);

            var ehcak = null;
            var ehcavk = null;
            if (this._Platform.is("android")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            } else if (this._Platform.is("ios")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else if (this._Platform.is("ipad")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else if (this._Platform.is("iphone")) {
                ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            } else {
                ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
                ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            }

            var _Headers;
            if (UserAccount != null && UserAccount.AccessKey != undefined) {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber))
                    .set("hcuak", UserAccount.AccessKey)
                    .set("hcupk", btoa(UserAccount.PublicKey))
                    ;
            }
            else {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber));
            }
            var ORequest = JSON.stringify({ zx: btoa(JSON.stringify(Data)) });
            return this._Http
                .post<ONetworkResponse>(PostUrl, ORequest, { headers: _Headers })
                .pipe(map(_Response => JSON.parse(atob(_Response.zx)) as OResponse))
                .pipe(catchError((error: HttpErrorResponse) => throwError(error)));

        } catch (error) {
            this.HideSpinner();
        }
    }
    GetStatusClass(StatusCode) {
        var ColorGrey = "grey";
        var ColorGreen = "green";
        var ColorOrange = "orange";
        var ColorRed = "red";
        var StatusIcon = ColorGrey;
        if (StatusCode == "transaction.initialized") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.processing") {
            StatusIcon = ColorOrange;
        }
        if (StatusCode == "transaction.pending") {
            StatusIcon = ColorOrange;
        }
        if (StatusCode == "transaction.success") {
            StatusIcon = ColorGreen;
        }
        if (StatusCode == "transaction.cancelled") {
            StatusIcon = ColorRed;
        }
        if (StatusCode == "transaction.refundinitialized") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.refundprocessing") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.refunded") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.failed") {
            StatusIcon = ColorRed;
        }
        if (StatusCode == "transaction.error") {
            StatusIcon = ColorRed;
        }
        return StatusIcon;
    }
    calculateDistance(lat1: number, lat2: number, long1: number, long2: number) {
        let p = 0.017453292519943295;    // Math.PI / 180
        let c = Math.cos;
        let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
        let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
        if (dis < 1) {
            return Math.round(dis * 100) / 100;
        }
        else {
            return Math.round(dis);
        }
    }

    RoundNumber(Value) {
        return Math.round((Value + Number.EPSILON) * 100) / 100
    }

    public TCategories = [];
    public TMerchants = [];
    public TStores = [];
    public TMerchantCategories = [];
    public TTransactions = [];
    public TUserMerchantVisits = [];
    public FetchDataFromServer() {
        this.SaveStorageValue(this.AppConfig.StorageHelper.TSyncTime, new Date());
        this.GetTUCCategories();
    }
    public GetTUCCategories() {
        var pData = {
            Task: this.AppConfig.NetworkApi.v2.getcategories,
            Offset: 0,
            Limit: 100,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this.TCategories = _Response.Result.Data;
                    this.SaveStorage(this.AppConfig.StorageHelper.TCategories, _Response.Result.Data);
                    this.GetTUCMerchantCategories();
                }
                else {
                    this.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            });
    }
    public GetTUCMerchantCategories() {
        var pData = {
            Task: this.AppConfig.NetworkApi.v2.getmerchantcategories,
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this.TMerchantCategories = _Response.Result.Data;
                    this.TMerchantCategories.forEach(element => {
                        var Details = this.TCategories.find(x => x.ReferenceId == element.CategoryId);
                        if (Details != undefined) {
                            element.Name = Details.Name;
                        }
                    });
                    this.TCategories.forEach(_Category => {
                        var _MCat = this.TMerchantCategories.filter(x => x.CategoryId == _Category.ReferenceId);
                        if (_MCat != undefined) {
                            _Category.Merchants = _MCat.length;
                        }
                    });
                    this.SaveStorage(this.AppConfig.StorageHelper.TCategories, this.TCategories);
                    this.SaveStorage(this.AppConfig.StorageHelper.TMerchantCategories, _Response.Result.Data);
                    this.GetTUCMerchants();
                }
                else {
                    this.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            });
    }
    public GetTUCMerchants() {
        var pData = {
            Task: this.AppConfig.NetworkApi.v2.getmerchants,
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this.TMerchants = [];
                    this.TMerchants = _Response.Result.Data;
                    this.TMerchants.forEach(_Merchant => {
                        _Merchant.Categories = [];
                        var MerchantCategories = this.TMerchantCategories.filter(x => x.MerchantId == _Merchant.ReferenceId);
                        if (MerchantCategories != undefined && MerchantCategories.length > 0) {
                            _Merchant.Categories = MerchantCategories;
                        }
                    });

                    this.SaveStorage(this.AppConfig.StorageHelper.TMerchants, this.TMerchants);
                    this.GetTUCMerchantStores();
                }
                else {
                    this.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            });
    }
    public GetTUCMerchantStores() {
        var pData = {
            Task: this.AppConfig.NetworkApi.v2.getstores,
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this.TStores = _Response.Result.Data;
                    this.TStores.forEach(_Store => {
                        var MerchantDetails = this.TMerchants.find(x => x.ReferenceId == _Store.MerchantId);
                        if (MerchantDetails != undefined && MerchantDetails != null) {
                            if (MerchantDetails.Locations == undefined) {
                                MerchantDetails.Locations = 0;
                            }
                            MerchantDetails.Locations = MerchantDetails.Locations + 1;
                            _Store.MerchantDisplayName = MerchantDetails.DisplayName;
                            _Store.IconUrl = MerchantDetails.IconUrl;
                            _Store.RewardPercentage = MerchantDetails.RewardPercentage;
                            if (MerchantDetails.Categories != undefined && MerchantDetails.Categories != null && MerchantDetails.Categories.length > 0) {
                                _Store.Categories = MerchantDetails.Categories;
                            }
                            else {
                                _Store.Categories = [];
                            }
                        }
                    });
                    this.SaveStorage(this.AppConfig.StorageHelper.TStores, this.TStores);
                    this.GetTUCTransactions();
                }
                else {
                    this.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            });
    }
    public GetTUCTransactions() {
        var pData = {
            Task: this.AppConfig.NetworkApi.v2.gettransactions,
            Offset: 0,
            Limit: 90,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    var TransactionsHistory = _Response.Result.Data;
                    this.TTransactions = [];
                    this.TUserMerchantVisits = [];
                    TransactionsHistory.forEach(element => {
                        var MerchantDetails = this.TMerchants.find(x => x.ReferenceId == element.MerchantId);
                        if (MerchantDetails != undefined) {
                            element.MerchantDisplayName = MerchantDetails.DisplayName;
                            element.MerchantIconUrl = MerchantDetails.IconUrl;
                        }
                        var StoreDetails = this.TStores.find(x => x.ReferenceId == element.StoreId);
                        if (StoreDetails != undefined) {
                            element.StoreDisplayName = StoreDetails.DisplayName;
                            element.StoreContactNumber = StoreDetails.ContactNumber;
                            element.StoreEmailAddress = StoreDetails.EmailAddress;
                            element.Address = StoreDetails.Address;
                            element.Latitude = StoreDetails.Latitude;
                            element.Longitude = StoreDetails.Longitude;
                        }
                        element.TransactionDate = this.GetDateTimeS(element.TransactionDate);
                        this.TTransactions.push(element);
                    });
                    this.TMerchants.forEach(element => {
                        var MerchantTransactions = this.TTransactions.filter(x => x.MerchantId == element.ReferenceId);
                        if (MerchantTransactions.length > 0) {
                            element.Visits = MerchantTransactions.length;
                            var Rewards = 0;
                            var InvoiceAmount = 0;
                            MerchantTransactions.forEach(element => {
                                Rewards = Rewards + element.TotalAmount;
                                InvoiceAmount = InvoiceAmount + element.InvoiceAmount;
                            });
                            element.Rewards = Rewards;
                            element.InvoiceAmount = InvoiceAmount;
                            this.TUserMerchantVisits.push(element);
                        }
                    });
                    this.SaveStorage(this.AppConfig.StorageHelper.TTransactions, TransactionsHistory);
                    this.SaveStorage(this.AppConfig.StorageHelper.TUserMerchantVisits, this.TUserMerchantVisits);
                    this.GetTUCBalance();

                }
                else {
                    this.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            });
    }
    public GetTUCBalance() {
        var pData = {
            Task: 'getbalance',
            UserAccountKey: this.AccountInfo.UserAccount.AccountKey,
            Source: "transactionsource.app",
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAcc, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    var Balance = _Response.Result;
                    this.SaveStorage(this.AppConfig.StorageHelper.AccountBalance, Balance);
                    this.HideSpinner();
                    // this._App.getRootNav().setRoot(TabsPage);
                }
                else {
                    this.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            });
    }
    public IsFormProcessing = false;






}

interface ONetworkResponse {
    fx: string;
    vx: string;
    zx: string;
}
enum NavType {
    root,
    push
}


[
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
]



