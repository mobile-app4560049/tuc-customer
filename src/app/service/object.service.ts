export class OResponse {
    public Status: string;
    public Message: string;
    public ResponseCode: string;
    public Result: any;
}

export class OListResponse {
    Data: [any];
    Offset: number = 0;
    Limit: number = 0;
    TotalRecords: number = 0;
}
export class OStorageContent {
    public Name: string;
    public Content: string;
    public Extension: string;
    public TypeCode: string;
    public Width: number = 0;
    public Height: number = 0;
}

export class OSelect {
    public Task: string;
    public Location: string;
    public SearchCondition?: string;
    public SortCondition?: string[];
    public Fields: OSelectField[];
}

export class OSelectField {
    public SystemName: string;
    public Type: string;
    public Id?: boolean;
    public Text?: boolean;
    public SearchCondition?: string;
    public SearchValue?: string;
}

export class OList {
    public ListType?: number = 0;
    public Task: string;
    public Location: string;
    public Title: string = "List";
    public TableFields: OListField[];

    public ReferenceId?: number = null;
    public SubReferenceId?: number = null;
    public ReferenceKey?: string = null;
    public Type?: string = null;
    public RefreshCount?: boolean = true;
    public TitleResourceId?: string = null;
    public RefreshData?: boolean = false;
    public IsDownload?: boolean = false;
    public ToggleFilter?: boolean = false;
    public ActivePage?: number = 1;
    public PageRecordLimit?: number = 10;
    public TotalRecords?: number = 0;
    public ShowingStart?: number = 0;
    public ShowingEnd?: number = 0;
    public StartTime?: any = null;
    public EndTime?: any = null;
    public StartDate?: any = null;
    public EndDate?: any = null;
    public SearchParameter?: string;
    public SearchCondition?: string;
    public SearchBaseCondition?: string;
    public SearchBaseConditions?: any[];
    public ColumnSearchParameter?: string;
    public AvailableSearchColumns?: any[];
    public SearchColumns?: any[];
    public Data?: any[];
    public StatusType?: string = "default";
    public Status?: number = 0;
    public StatusOptions?: any[];
    public VisibleHeaders?: any[];
    public Visiblity?: any[];
    public VisiblityOptions?: any[];
    public SortExpression?: string;
    public SortExpressionOption?: any[];
    public DefaultSortExpression?: string;
    public OriginalResponse?: any;
}

export class OListField {
    public DefaultValue?: string = "--";
    public DisplayName: string;
    public DownloadDisplayName?: string;
    public SystemName: string;
    public Content?: string;
    public DataType: string = "text";
    public Class?: string = "";
    public ResourceId?: string = "";
    public Sort?: boolean = true;
    public Show?: boolean = true;
    public Search?: boolean = true;
    public NavigateLink?: string = "";
    public NavigateField?: string = "";
    public IsDateSearchField?: boolean = false;
}

export class OAccountInfo {
    public LoginTime: Date;
    public AccessKey: string;
    public PublicKey: string;
    public User: OUser;
    public UserAccount: OAccount;
    public UserOwner: OAccountOwner;
    public UserCountry: OAccountCountry;
}
export class OUser {
    public UserName: string;
    public FirstName: string;
    public LastName: string;
    public Name: string;
    public MobileNumber: string;
    public ContactNumber: string;
    public EmailAddress: string;
    public GenderCode: string;
    public Gender: string;
    public Address: string;
    public AddressLatitude: number;
    public AddressLongitude: number;
    public EmailVerificationStatus: number;
    public ContactNumberVerificationStatus: number;
    public DateOfBirth: Date;
}

export class OUserInfo {
    Id: number;
    GenderId: number;
    Reference: string;
    Is_Active: boolean;
    Source: string;
    Created_At: Date;
    Updated_At: Date;
    FirstName: string;
    LastName: string;
    Email: string;
    Phone: number;

}
export class OAccount {
    public AccountId: number;
    public DisplayName: string;
    public IsTucPlusEnabled: boolean;
    public UserKey: string;
    public AccountKey: string;
    public AccountType: string;
    public AccountTypeCode: string;
    public AccountCode: string;
    public IconUrl: string;
    public PosterUrl: string;
    public ReferralCode: string;
    public IsAccessPinSet: string;
    public CreateDate: Date;

}
export class OAccountOwner {
    public AccountId: number;
    public AccountKey: string;
    public DisplayName: string;
    public Name: string;
    public AccountCode: string;
    public AccountTypeCode: string;
    public IsTucPlusEnabled: boolean;
    public IconUrl: string;

}
export class OAccountCountry {
    public CountryId: number;
    public CountryKey: string;
    public CountryName: string;
    public CountryIso: string;
    public CountryIsd: string;
    public CurrencyName: string;
    public CurrencyNotation: string;
    public CurrencyCode: string;
    public CurrencySymbol: string;
}
export class OBalance {
    public InvoiceAmount?: number | null;
    public AccountClassPercentage?: number | null;
    public AccountClassPercentageBar?: number | null;
    public AccountClass?: string | null;
    public AccountClassLimit?: number | null;
    public Credit?: number | null;
    public Debit?: number | null;
    public Balance?: number | null;
    public BalanceValidity?: number | null;
    public ExpiaryTimeMinutes?: number | null;
    public PendingReward?: number | null;


    public TucPlusCredit?: number | null;
    public TucPlusDebit?: number | null;
    public TucPlusBalance?: number | null;
    public TucPlusBalanceValidity?: number | null;
    public TucPlusExpiaryTimeMinutes?: number | null;
    public UserMerchantBalance?: any[];

}

export class OBankStatement {
    Status: string;
    Message: string;
    Data: any[];
    Pre_Qualified_Amount: number;
}

export class ODeviceInformation {
    public MobileNumber: string | null;
    public SerialNumber: string | null;
    public OsName: string | null;
    public OsVersion: string | null;
    public Brand: string | null;
    public Model: string | null;
    public Width: number | null;
    public Height: number | null;
    public CarrierName: string | null;
    public CountryCode: string | null;
    public Mcc: string | null;
    public Mnc: string | null;
    public Latitude: number | null;
    public Longitude: number | null;
    public NotificationUrl: string | null;
}

// export class OAppCountry {
//     public ReferenceId: number | null;
//     public ReferenceKey: string | null;
//     public IconUrl: string | null;
//     public Name: string | null;
//     public Iso: string | null;
//     public Isd: string | null;
//     public NumberLength: number | null;
//     public CurrencyName: string | null;
//     public CurrencyNotation: string | null;
//     public CurrencySymbol: string | null;
//     public CurrencyHex: string | null;

// }


export class OAddressComponent {

    public ReferenceId: number | null;
    public ReferenceKey: string | null;
    public Reference?: string | null;

    public Task?: string | null;

    public CountryId: number | null;
    public CountryKey: string | null;
    public CountryName: string | null;

    public Name: string | null;
    public ContactNumber: string | null;
    public EmailAddress: string | null;

    public AddressLine1: string | null;
    public AddressLine2: string | null;

    public Landmark: string | null;
    public AlternateMobileNumber: string | null;

    public CityAreaId: number | null;
    public CityAreaKey: string | null;
    public CityAreaName: string | null;


    public CityId: number | null;
    public CityKey: string | null;
    public CityName: string | null;

    public StateId: number | null;
    public StateKey: string | null;
    public StateName: string | null;

    public ZipCode: string | null;
    public Instructions: string | null;
    public MapAddress: string | null;
    public Latitude: number | null;
    public Longitude: number | null;
    public IsPrimary?: boolean | null;
    public LocationTypeId?: number | null;
}





export interface ICategory {
    ReferenceKey: string;
    Name: string;
    IconUrl: string;
    Deals: number;
    IsChecked?: boolean;
    SubCategories?: ISubCategory[];
}
export interface ISubCategory {
    ReferenceKey: string;
    Name: string;
    IconUrl: string;
    Deals: number;
    IsChecked?: boolean;
}
export interface IDeal {
    ReferenceId: number;
    ReferenceKey: string;
    Title: string;
    MerchantKey: string;
    MerchantName: string;
    MerchantIconUrl: string;
    CategoryKey: string;
    CategoryName: string;
    EndDate: Date;
    ImageUrl: string;
    ActualPrice: string;
    SellingPrice: string;
    IsBookmarked: boolean;
    IsDealpurchased: boolean;
    Location: string;
    Gallery: string[];
    Address: string;
    DealTypeCode: string;
    DeliveryTypeCode: string;
    Distance: number;
    DiscountPercentage: number;
    CountdownConfig:
    {
        leftTime: number,
    };
}

export interface IDealReview {
    Offset: number,
    Limit: number,
    TotalRecords: number,
    List: IDealReviewItem[];
}
export interface IDealReviewItem {
    ReferenceId: number,
    ReferenceKey: string,
    AccountId: string,
    AccountKey: string,
    AccountDisplayName: string,
    AccountMobileNumber: string,
    AccountIconUrl: string,
    Rating: string,
    IsWorking: string,
    Review: string,
    Comment: string,
    StatusId: string,
    CreateDate: Date,
    RatingContent: string
}
export interface IMerchants {
    ReferenceKey: string;
    Name: string;
    IconUrl: string;
    BackgroundColor?: any;
    Deals: number;
    IsChecked?: boolean;
    CategoryDistribution: IMerchantCategoryDistribution[];
}
export interface IMerchantCategoryDistribution {
    CategoryKey: string;
    Count: number;
}



export interface IDealRequest {
    Task: string;
    ReferenceKey?: string;
    SearchCondition?: string;
    SortExpression?: string;
    Offset: number;
    Limit: number;
    Categories: string[];
    Merchants: string[];
    City: string;
    CityId?: number;
    CityKey?: string;
    Latitude?: number;
    Longitude: number;
    CountryId: number;
    CountryKey: string;
}
