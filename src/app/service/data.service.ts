import { DatePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ModalController, NavController, Platform } from '@ionic/angular';
import { Observable, throwError, Subscription } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { OResponse, OAccount, OAccountInfo, OBalance } from './object.service';
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { HelperService } from '../service/helper.service';
import { interval } from 'rxjs';

@Injectable()
export class DataService {
    public TUCTransactions: any[] = [];
    public TUCCategories: any[] = [];
    public TUCMerchants: any[] = [];
    public TUCUserMerchantVisits: any[] = [];
    public TUCMerchantCategories: any[] = [];
    public TUCStores: any[] = [];
    constructor(
        public _HelperService: HelperService,
    ) {
        this.InitializeData();
    }


    InitializeData() {
        this._OBalance =
        {

            AccountClass: 'purple',
            AccountClassLimit: 0,
            Credit: 0,
            Debit: 0,
            Balance: 0,
            BalanceValidity: 0,
            ExpiaryTimeMinutes: 0,
            TucPlusCredit: 0,
            TucPlusDebit: 0,
            TucPlusBalance: 0,
            TucPlusBalanceValidity: 0,
            TucPlusExpiaryTimeMinutes: 0,
            UserMerchantBalance: [],
        }
        this.TUCTransactions = [];
        this.TUCCategories = [];
        this.TUCMerchants = [];
        this.TUCMerchantCategories = [];
        this.TUCStores = [];
        this.TUCUserMerchantVisits = [];
        var Latitude = 0;
        var Longitude = 0;
        var DeviceInformation = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformation != null) {
            if (DeviceInformation.Latitude != undefined && DeviceInformation.Latitude != null && DeviceInformation.Latitude != 0) {
                Latitude = DeviceInformation.Latitude;
            }
            if (DeviceInformation.Longitude != undefined && DeviceInformation.Longitude != null && DeviceInformation.Longitude != 0) {
                Longitude = DeviceInformation.Longitude;
            }
        }
        if (this.TUCCategories == undefined || this.TUCCategories.length == 0) {
            var SavedData = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TCategories);
            if (SavedData != null) {
                this.TUCCategories = SavedData;
            }
        }
        if (this.TUCMerchantCategories == undefined || this.TUCMerchantCategories.length == 0) {
            var SavedData = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TMerchantCategories);
            if (SavedData != null) {
                this.TUCMerchantCategories = SavedData;
            }
        }
        if (this.TUCTransactions == undefined || this.TUCTransactions.length == 0) {
            var SavedData = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TTransactions);
            if (SavedData != null) {
                this.TUCTransactions = SavedData;
            }
        }
        if (this.TUCStores == undefined || this.TUCStores.length == 0) {
            var StoresList = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TStores) as any[];
            if (StoresList != null) {
                if (Latitude != 0 && Longitude != 0) {
                    StoresList.forEach(_Store => {
                        _Store.Distance = this._HelperService.calculateDistance(Latitude, _Store.Latitude, Longitude, _Store.Longitude);
                    });
                }
                var SortedData: any[] = StoresList.sort((obj1, obj2) => {
                    if (obj1.Distance > obj2.Distance) {
                        return 1;
                    }
                    if (obj1.Distance < obj2.Distance) {
                        return -1;
                    }
                    return 0;
                });
                this.TUCStores = SortedData;
            }
        }
        else {
            if (Latitude != 0 && Longitude != 0) {
                this.TUCStores.forEach(_Store => {
                    _Store.Distance = this._HelperService.calculateDistance(Latitude, _Store.Latitude, Longitude, _Store.Longitude);
                });
            }
            var SortedData: any[] = this.TUCStores.sort((obj1, obj2) => {
                if (obj1.Distance > obj2.Distance) {
                    return 1;
                }
                if (obj1.Distance < obj2.Distance) {
                    return -1;
                }
                return 0;
            });
            this.TUCStores = SortedData;
        }

        if (this.TUCMerchants == undefined || this.TUCMerchants.length == 0) {
            var MerchantsData = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TMerchants);
            if (MerchantsData != null) {
                MerchantsData.forEach(_Merchant => {
                    var MerchantNearestStore = this.TUCStores.find(x => x.MerchantId == _Merchant.ReferenceId);
                    if (MerchantNearestStore != undefined && MerchantNearestStore != null) {
                        _Merchant.StoreId = MerchantNearestStore.ReferenceId;
                        _Merchant.StoreKey = MerchantNearestStore.ReferenceKey;
                        _Merchant.Distance = MerchantNearestStore.Distance;
                        _Merchant.Address = MerchantNearestStore.Address;
                        _Merchant.AverageValue = MerchantNearestStore.AverageValue;
                        _Merchant.Latitude = MerchantNearestStore.Latitude;
                        _Merchant.Longitude = MerchantNearestStore.Longitude;
                    }
                });
                var SortedData: any[] = MerchantsData.sort((obj1, obj2) => {
                    if (obj1.Distance > obj2.Distance) {
                        return 1;
                    }
                    if (obj1.Distance < obj2.Distance) {
                        return -1;
                    }
                    return 0;
                });
                this.TUCMerchants = MerchantsData;
            }
        }
        else {
            this.TUCMerchants.forEach(_Merchant => {
                var MerchantNearestStore = this.TUCStores.find(x => x.MerchantId == _Merchant.ReferenceId);
                if (MerchantNearestStore != undefined && MerchantNearestStore != null) {
                    _Merchant.StoreId = MerchantNearestStore.ReferenceId;
                    _Merchant.StoreKey = MerchantNearestStore.ReferenceKey;
                    _Merchant.Distance = MerchantNearestStore.Distance;
                    _Merchant.Address = MerchantNearestStore.Address;
                    _Merchant.AverageValue = MerchantNearestStore.AverageValue;
                    _Merchant.Latitude = MerchantNearestStore.Latitude;
                    _Merchant.Longitude = MerchantNearestStore.Longitude;
                }
            });
            var SortedData: any[] = this.TUCMerchants.sort((obj1, obj2) => {
                if (obj1.Distance > obj2.Distance) {
                    return 1;
                }
                if (obj1.Distance < obj2.Distance) {
                    return -1;
                }
                return 0;
            });
            this.TUCMerchants = MerchantsData;
        }
        if (this.TUCUserMerchantVisits == undefined || this.TUCUserMerchantVisits.length == 0) {
            var SavedData = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TUserMerchantVisits);
            if (SavedData != null) {
                this.TUCUserMerchantVisits = SavedData;
            }
        }
        var TBalance = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance);
        if (TBalance != null) {
            this.UpdateBalance(TBalance as OBalance);
        }
    }
    private IsServerDataFetch = false;
    public ForceFetchDataFromServer() {
        this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.TSyncTime, new Date());
        this.IsServerDataFetch = true;
        this.GetTUCCategories();
    }
    public FetchDataFromServer() {
        var StorageValue = this._HelperService.GetStorageValue(this._HelperService.AppConfig.StorageHelper.TSyncTime);
        if (StorageValue != null) {
            var LastSyncTime = new Date(StorageValue);
            var CurrentTime = new Date();
            var hours = Math.abs(LastSyncTime.valueOf() - CurrentTime.valueOf()) / 36e5;
            if (hours > 3) {
                this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.TSyncTime, new Date());
                this.IsServerDataFetch = true;
                this.GetTUCCategories();
            }
        }
        else {
            this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.TSyncTime, new Date());
            this.IsServerDataFetch = true;
            this.GetTUCCategories();
        }
    }
    public GetTUCCategories() {
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.v2.getcategories,
            Offset: 0,
            Limit: 100,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TUCCategories = _Response.Result.Data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TCategories, _Response.Result.Data);
                    this.GetTUCMerchantCategories();
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public GetTUCMerchantCategories() {
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.v2.getmerchantcategories,
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TUCMerchantCategories = _Response.Result.Data;
                    this.TUCMerchantCategories.forEach(element => {
                        var Details = this.TUCCategories.find(x => x.ReferenceId == element.CategoryId);
                        if (Details != undefined) {
                            element.Name = Details.Name;
                        }
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TMerchantCategories, _Response.Result.Data);
                    this.GetTUCMerchants();
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public GetTUCMerchants() {
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.v2.getmerchants,
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TUCMerchants = [];
                    this.TUCMerchants = _Response.Result.Data;
                    this.TUCMerchants.forEach(_Merchant => {
                        _Merchant.Categories = [];
                        var MerchantCategories = this.TUCMerchantCategories.filter(x => x.MerchantId == _Merchant.ReferenceId);
                        if (MerchantCategories != undefined && MerchantCategories.length > 0) {
                            _Merchant.Categories = MerchantCategories;
                        }
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TMerchants, this.TUCMerchants);
                    if (this.IsServerDataFetch == true) {
                        this.GetTUCMerchantStores();
                    }
                    this.TUCCategories.forEach(_Category => {
                        var _MCat = this.TUCMerchantCategories.filter(x => x.CategoryId == _Category.ReferenceId);
                        if (_MCat != undefined) {
                            _Category.Merchants = _MCat.length;
                        }
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TCategories, this.TUCCategories);
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public GetTUCMerchantStores() {
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.v2.getstores,
            Offset: 0,
            Limit: 200,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TUCStores = _Response.Result.Data;
                    this.TUCStores.forEach(_Store => {
                        var MerchantDetails = this.TUCMerchants.find(x => x.ReferenceId == _Store.MerchantId);
                        if (MerchantDetails != undefined && MerchantDetails != null) {
                            if (MerchantDetails.Locations == undefined) {
                                MerchantDetails.Locations = 0;
                            }
                            MerchantDetails.Locations = MerchantDetails.Locations + 1;
                            _Store.MerchantDisplayName = MerchantDetails.DisplayName;
                            _Store.IconUrl = MerchantDetails.IconUrl;
                            _Store.RewardPercentage = MerchantDetails.RewardPercentage;
                            if (MerchantDetails.Categories != undefined && MerchantDetails.Categories != null && MerchantDetails.Categories.length > 0) {
                                _Store.Categories = MerchantDetails.Categories;
                            }
                            else {
                                _Store.Categories = [];
                            }
                        }
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TMerchants, this.TUCMerchants);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TStores, this.TUCStores);
                    if (this.IsServerDataFetch == true) {
                        this.GetTUCTransactions();
                    }
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public GetTUCTransactions() {
        var SavedData = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TMerchants);
        if (SavedData != null) {
            this.TUCMerchants = SavedData;
        }
        var SavedData = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TStores);
        if (SavedData != null) {
            this.TUCStores = SavedData;
        }
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.v2.gettransactions,
            Offset: 0,
            Limit: 90,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    if (this.IsServerDataFetch == true) {
                        this.IsServerDataFetch = false;
                        this.GetTUCBalance();
                    }
                    var TransactionsHistory = _Response.Result.Data;
                    this.TUCTransactions = [];
                    this.TUCUserMerchantVisits = [];
                    TransactionsHistory.forEach(element => {
                        var MerchantDetails = this.TUCMerchants.find(x => x.ReferenceId == element.MerchantId);
                        if (MerchantDetails != undefined) {
                            element.MerchantDisplayName = MerchantDetails.DisplayName;
                            element.MerchantIconUrl = MerchantDetails.IconUrl;
                        }
                        var StoreDetails = this.TUCStores.find(x => x.ReferenceId == element.StoreId);
                        if (StoreDetails != undefined) {
                            element.StoreDisplayName = StoreDetails.DisplayName;
                            element.StoreContactNumber = StoreDetails.ContactNumber;
                            element.StoreEmailAddress = StoreDetails.EmailAddress;
                            element.Address = StoreDetails.Address;
                            element.Latitude = StoreDetails.Latitude;
                            element.Longitude = StoreDetails.Longitude;
                        }
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.TUCTransactions.push(element);
                    });
                    this.TUCMerchants.forEach(element => {
                        var MerchantTransactions = this.TUCTransactions.filter(x => x.MerchantId == element.ReferenceId);
                        if (MerchantTransactions.length > 0) {
                            element.Visits = MerchantTransactions.length;
                            var Rewards = 0;
                            var InvoiceAmount = 0;
                            MerchantTransactions.forEach(element => {
                                Rewards = Rewards + element.TotalAmount;
                                InvoiceAmount = InvoiceAmount + element.InvoiceAmount;
                            });
                            element.Rewards = Rewards;
                            element.InvoiceAmount = InvoiceAmount;
                            this.TUCUserMerchantVisits.push(element);
                        }
                    });
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TTransactions, TransactionsHistory);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TUserMerchantVisits, this.TUCUserMerchantVisits);
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public GetTUCBalance() {
        if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {

            var TBalance = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance);
            if (TBalance != null) {
                this.UpdateBalance(TBalance as OBalance);
            }
            var pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getuseraccountbalance,
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                Source: this._HelperService.AppConfig.TransactionSource.App,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        var Balance = _Response.Result;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, Balance);
                        this.UpdateBalance(_Response.Result as OBalance);
                    }
                    else {
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    public _OBalance: OBalance =
        {
            AccountClass: 'purple',
            AccountClassLimit: 0,
            Credit: 0,
            Debit: 0,
            Balance: 0,
            BalanceValidity: 0,
            ExpiaryTimeMinutes: 0,
            PendingReward: 0,
            TucPlusCredit: 0,
            TucPlusDebit: 0,
            TucPlusBalance: 0,
            TucPlusBalanceValidity: 0,
            TucPlusExpiaryTimeMinutes: 0,
        }
    public future: Date;
    public tucplusfuture: Date;
    public counter$: Observable<number>;
    public tucpluscounter$: Observable<number>;
    public subscription: Subscription;
    public tucplussubscription: Subscription;
    public tucremainingtime: string;
    public tucplusremainingtime: string;
    addDays(date: Date, days: number): Date {
        date.setDate(date.getDate() + days);
        return date;
    }
    UpdateBalance(Balance: OBalance) {
        if (this.subscription != undefined) {
            this.subscription.unsubscribe();
        }
        this._OBalance = Balance;
        this._OBalance.Credit = (this._OBalance.Credit / 100);
        this._OBalance.Debit = (this._OBalance.Debit / 100);
        this._OBalance.Balance = (this._OBalance.Balance / 100);
        this._OBalance.TucPlusCredit = (this._OBalance.TucPlusCredit / 100);
        this._OBalance.TucPlusDebit = (this._OBalance.TucPlusDebit / 100);
        this._OBalance.TucPlusBalance = (this._OBalance.TucPlusBalance / 100);
        this._OBalance.AccountClassPercentage = Math.round((this._OBalance.InvoiceAmount / (this._OBalance.AccountClassLimit + this._OBalance.InvoiceAmount)) * 100);
        this._OBalance.AccountClassPercentageBar = this._OBalance.AccountClassPercentage / 100;
        if (this._OBalance.UserMerchantBalance != undefined && this._OBalance.UserMerchantBalance.length > 0) {
            this._OBalance.UserMerchantBalance.forEach(element => {
                element.Credit = (element.Credit / 100);
                element.Debit = (element.Debit / 100);
                element.Balance = (element.Balance / 100);
            });
        }
        // this.future = this.addDays(new Date(), 90)
        // this.counter$ = interval(1000).pipe(map((x) => {
        //     return Math.floor((this.future.getTime() - new Date().getTime()) / 1000);
        // }));
        // this.subscription = this.counter$.subscribe((x) => this.tucremainingtime = this.dhms(x));
        if (this._OBalance.Balance > 0) {
            this.future = this.addDays(new Date(), this._OBalance.BalanceValidity)
            this.counter$ = interval(1000).pipe(map((x) => {
                return Math.floor((this.future.getTime() - new Date().getTime()) / 1000);
            }));
            this.subscription = this.counter$.subscribe((x) => this.tucremainingtime = this.dhms(x));
        }
        if (this._OBalance.TucPlusBalance > 0) {
            this.tucplusfuture = this.addDays(new Date(), this._OBalance.TucPlusBalanceValidity)
            this.tucpluscounter$ = interval(1000).pipe(map((x) => {
                return Math.floor((this.tucplusfuture.getTime() - new Date().getTime()) / 1000);
            }));
            this.tucplussubscription = this.tucpluscounter$.subscribe((x) => this.tucplusremainingtime = this.dhms(x));
        }
    }
    dhms(t) {
        var days, hours, minutes, seconds;
        days = Math.floor(t / 86400);
        t -= days * 86400;
        hours = Math.floor(t / 3600) % 24;
        t -= hours * 3600;
        minutes = Math.floor(t / 60) % 60;
        t -= minutes * 60;
        seconds = t % 60;
        return [
            days + 'd',
            hours + 'h',
            minutes + 'm',
            seconds + 's'
        ].join(' ');
    }
    ngOnDestroy(): void {
        if (this.subscription != undefined) {
            this.subscription.unsubscribe();
            this.tucplussubscription.unsubscribe();
        }
    }
    public GetVasServices() {
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.V3.Vas.getvasoptions,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Vas, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.VasServices, _Response.Result);
                    // this.UpdateBalance(_Response.Result as OBalance);
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}