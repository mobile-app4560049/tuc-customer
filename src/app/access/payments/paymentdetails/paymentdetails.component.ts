import { Component, Sanitizer, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OListResponse, OBalance } from '../../../service/object.service';
import { TransactionDetailsModal } from '../../modals/transactiondetails/transactiondetails.modal.component';
import { DomSanitizer } from "@angular/platform-browser";
import { DataService } from '../../../service/data.service';
declare var google;
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
@Component({
    selector: 'app-paymentdetails',
    templateUrl: 'paymentdetails.component.html'
})
export class PaymentDetailsPage {

    public _TransactionDetails =
        {
            Status: null,
            Message: null,
            Result: null,
            ResponseCode: null,
        };
    constructor(
        public _NavController: NavController,
        public _LaunchNavigator: LaunchNavigator,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._TransactionDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
    }



    public TimerConfig =
        {
            leftTime: 0,
        }

    async ModalDismiss(Type) {
        this._HelperService.NavDashboard();
        // await this._ModalController.dismiss(Type);
    }


}