import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../service/object.service';
import { PaymentTypeSelectModal } from '../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../service/data.service';
declare var moment: any;

@Component({
    selector: 'app-lcctopup',
    templateUrl: 'lcctopup.component.html'
})
export class LccTopupPage {
    //
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _DataService: DataService,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Payment-LccTopup");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.GetPaymentConfiguration();
        this.GetTUCBalance();
    }
    public PaymentConfiguration =
        {
            AccountNumber: null,
            MinAmount: 500,
            MaxAmount: 50000,
            ChargeType: "percentage",
            ChargeValue: 1.5,
            MaxChargeValue: 2500,
            FixedCharge: 100,

            UserCards: [],
            PaymentReference: null,
            PaymentType: null,
            PaymentAmount: null,
            PaymentSource: 0,
            Task: 'confirmpayment',
            Type: 'lcctopup',
            AccountId: null,
            AccountKey: null,
            TotalAmount: null,
            RewardType: "percentage",
            RewardValue: null,
            MaxRewardAmount: 100,
            RewardAmount: null,
        }

    public GetPaymentConfiguration() {
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "getpaymentsconfiguration",
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            ReferenceId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Type: "lcctopup",
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PaymentConfiguration = _Response.Result;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
                    this.PaymentConfiguration.PaymentReference = this.PaymentConfiguration.PaymentReference;
                    this.PaymentConfiguration.AccountId = this._HelperService.AccountInfo.UserAccount.AccountId;
                    this.PaymentConfiguration.AccountKey = this._HelperService.AccountInfo.UserAccount.AccountKey;
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public QuickAmountClick(Amount) {
        this.PaymentConfiguration.PaymentAmount = Amount;
        this.CalculateReward();
    }

    CalculateReward() {
        this.PaymentConfiguration.RewardAmount = 0;
        this.PaymentConfiguration.TotalAmount = 0;
        if (this.PaymentConfiguration.PaymentAmount == undefined || this.PaymentConfiguration.PaymentAmount == null || isNaN(this.PaymentConfiguration.PaymentAmount) == true) {
        }
        // else if (this.PaymentConfiguration.PaymentAmount < this.PaymentConfiguration.MinAmount) {
        // }
        // else if (this.PaymentConfiguration.PaymentAmount > this.PaymentConfiguration.MaxAmount) {
        // }
        else {
            if (this.PaymentConfiguration.RewardType == "percentage") {
                this.PaymentConfiguration.RewardAmount = (parseFloat(this.PaymentConfiguration.PaymentAmount) / parseFloat(this.PaymentConfiguration.RewardValue)) / 100;
            }
            else if (this.PaymentConfiguration.RewardType == "fixedamount") {
                this.PaymentConfiguration.RewardAmount = this.PaymentConfiguration.RewardValue;
            }
            this.PaymentConfiguration.TotalAmount = this.PaymentConfiguration.RewardAmount;
        }
    }


    GetTUCBalance() {
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.Feature.getuseraccountbalance,
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            Source: this._HelperService.AppConfig.TransactionSource.App,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._DataService.UpdateBalance(_Response.Result as OBalance);
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    public IsUseTucBalance = false;

    async InitializeTransaction() {
        if (this.PaymentConfiguration.AccountNumber == undefined || this.PaymentConfiguration.AccountNumber == null) {
            this._HelperService.Notify('Account number', "Please enter valid  account number");
        }
        else if (this.PaymentConfiguration.PaymentAmount == undefined || this.PaymentConfiguration.PaymentAmount == null || isNaN(this.PaymentConfiguration.PaymentAmount) == true) {
            this._HelperService.Notify('Invalid amount', "Please enter valid amount for purchase");
        }
        else if (this.PaymentConfiguration.PaymentAmount < this.PaymentConfiguration.MinAmount) {
            this._HelperService.Notify("Invalid amount", "Amount must be greater than " + this.PaymentConfiguration.MinAmount);
        }
        else if (this.PaymentConfiguration.PaymentAmount > this.PaymentConfiguration.MaxAmount) {
            this._HelperService.Notify("Invalid amount", "Amount must be less than " + this.PaymentConfiguration.MaxAmount);
        }
        else {

            this._HelperService.ShowSpinner('Validating account');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "paymentaccountverify",
                Type: "lcctopup",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                Amount: this.PaymentConfiguration.PaymentAmount,
                PaymentReference: this.PaymentConfiguration.PaymentReference,
                RefTransactionId: this.PaymentConfiguration.PaymentReference,
                RefStatus: "sucess",
                RefMessage: "tuc balance used",
                AccountNumber: this.PaymentConfiguration.AccountNumber,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {

                        if (this._DataService._OBalance.Balance > this.PaymentConfiguration.PaymentAmount && this.IsUseTucBalance) {
                            this.ProcessTransaction();
                        }
                        else {
                            this.StartOnline();
                        }

                    }
                    else {
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });

        }
    }
    async StartOnline() {
        if (this.PaymentConfiguration.UserCards != undefined && this.PaymentConfiguration.UserCards != null && this.PaymentConfiguration.UserCards.length > 0) {
            this.PaymentConfiguration.PaymentAmount = this.PaymentConfiguration.PaymentAmount;
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
            const modal = await this._ModalController.create({
                component: PaymentTypeSelectModal,
                componentProps: this.PaymentConfiguration
            });
            modal.onDidDismiss().then(data => {
                if (data.data.PaymentSource == 'noaction') {
                    this._HelperService.Notify('Payment method required', "Select payment method");
                }
                else if (data.data.PaymentSource == 'new') {
                    document.getElementById('paymentbutton').click();
                }
                else {
                    if (data.data.PaymentSource != undefined && data.data.PaymentSource != null && data.data.PaymentSource != 0) {
                        this.ProcessOnline_Charge(data.data.PaymentSource);
                    }
                }
            });
            return await modal.present();
        }
        else {
            document.getElementById('paymentbutton').click();
        }
    }
    async ProcessTransaction() {
        if (this.PaymentConfiguration.AccountNumber == undefined || this.PaymentConfiguration.AccountNumber == null) {
            this._HelperService.Notify('Account number', "Please enter valid  account number");
        }
        else if (this.PaymentConfiguration.PaymentAmount == undefined || this.PaymentConfiguration.PaymentAmount == null || isNaN(this.PaymentConfiguration.PaymentAmount) == true) {
            this._HelperService.Notify('Invalid amount', "Please enter valid amount for purchase");
        }
        else if (this.PaymentConfiguration.PaymentAmount < this.PaymentConfiguration.MinAmount) {
            this._HelperService.Notify("Invalid amount", "Amount must be greater than " + this.PaymentConfiguration.MinAmount);
        }
        else if (this.PaymentConfiguration.PaymentAmount > this.PaymentConfiguration.MaxAmount) {
            this._HelperService.Notify("Invalid amount", "Amount must be less than " + this.PaymentConfiguration.MaxAmount);
        }
        else {
            if (this._DataService._OBalance.Balance > this.PaymentConfiguration.PaymentAmount && this.IsUseTucBalance) {
                this._HelperService.ShowSpinner('processing payment');
                this._HelperService.AppConfig.IsProcessing = true;
                var pData = {
                    Task: "confirmpayment",
                    Type: "lcctopup",
                    AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                    AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                    Amount: this.PaymentConfiguration.PaymentAmount,
                    PaymentReference: this.PaymentConfiguration.PaymentReference,
                    RefTransactionId: this.PaymentConfiguration.PaymentReference,
                    RefStatus: "sucess",
                    RefMessage: "tuc balance used",
                    AccountNumber: this.PaymentConfiguration.AccountNumber,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.HideSpinner();
                        this._HelperService.AppConfig.IsProcessing = false;
                        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                            this._HelperService.Notify(_Response.Status, _Response.Message);
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response);
                            this._HelperService.NavPaymentDetails();
                        }
                        else {
                            this._HelperService.Notify(_Response.Status, _Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.HandleException(_Error);
                    });
            }
            else {
                if (this.PaymentConfiguration.UserCards != undefined && this.PaymentConfiguration.UserCards != null && this.PaymentConfiguration.UserCards.length > 0) {
                    this.PaymentConfiguration.PaymentAmount = this.PaymentConfiguration.PaymentAmount;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
                    const modal = await this._ModalController.create({
                        component: PaymentTypeSelectModal,
                        componentProps: this.PaymentConfiguration
                    });
                    modal.onDidDismiss().then(data => {
                        if (data.data.PaymentSource == 'noaction') {
                            this._HelperService.Notify('Payment method required', "Select payment method");
                        }
                        else if (data.data.PaymentSource == 'new') {
                            document.getElementById('paymentbutton').click();
                        }
                        else {
                            if (data.data.PaymentSource != undefined && data.data.PaymentSource != null && data.data.PaymentSource != 0) {
                                this.ProcessOnline_Charge(data.data.PaymentSource);
                            }
                        }
                    });
                    return await modal.present();
                }
                else {
                    document.getElementById('paymentbutton').click();
                }
            }

        }
    }
    ProcessOnline_Cancel() {
        this._HelperService.Notify('Cancelled', "Payment cancelled.");
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    ProcessOnline_Confirm(Item) {
        if (Item.status == "success") {
            this._HelperService.ShowSpinner('Processing ...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "confirmpayment",
                Type: "lcctopup",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                Amount: this.PaymentConfiguration.PaymentAmount,
                PaymentReference: this.PaymentConfiguration.PaymentReference,
                RefTransactionId: Item.transaction,
                RefStatus: Item.status,
                RefMessage: Item.message,
                AccountNumber: this.PaymentConfiguration.AccountNumber,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response);
                        this._HelperService.NavPaymentDetails();
                    }
                    else {
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }


    }

    ProcessOnline_Charge(PaymentSource) {
        this._HelperService.ShowSpinner('processing payment...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "paymentcharge",
            Type: "buypoint",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.PaymentAmount,
            ReferenceId: PaymentSource,
            AccountNumber: this.PaymentConfiguration.AccountNumber,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response);
                    this._HelperService.NavPaymentDetails();
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    GetTUCPostBalance() {
        this._HelperService.ShowSpinner('processing payment...');
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.Feature.getuseraccountbalance,
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            Source: this._HelperService.AppConfig.TransactionSource.App,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._DataService.UpdateBalance(_Response.Result as OBalance);
                    this.InitializeTransaction();
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }



}