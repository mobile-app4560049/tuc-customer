import { Component, ViewChild, Sanitizer, HostListener } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime, Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
declare let L;
// import 'leaflet';
import { DataService } from '../../service/data.service';
import { DatePipe } from '@angular/common';
import { OResponse, OListResponse, OBalance, OBankStatement, } from '../../service/object.service';
declare var moment: any;
import { DomSanitizer } from "@angular/platform-browser";
import { TransactionDetailsModal } from '../modals/transactiondetails/transactiondetails.modal.component';
import { LocationSelectorModal } from '../modals/locationselector/locationselector.module.component';
import { FCM } from '@ionic-native/fcm/ngx';
import { ScanMyCodeModal } from '../features/tucpay/scanmycode/scanmycode.modal.component';
import { PaymentManagerModal } from '../modals/payment/payment.modal.component';
// import { BuyPointsModal } from '../modals/buypoints/buypoints.modal.component';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppUpdateModal } from '../modals/appupdate/appupdate.modal.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Mixpanel,MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';
@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardPage {

    public bankDetails: OBankStatement;
    async NavOpenQR() {
        if (this._HelperService.CheckIsLogin()) {
            const modal = await this._ModalController.create({
                component: ScanMyCodeModal,
            });
            modal.onDidDismiss().then(data => {
            });
            return await modal.present();

        }
        else {
            this._HelperService.NavLogin();
        }

    }
    //#region Navigation
    NavProfile() {
        if (this._HelperService.CheckIsLogin()) {
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
        }
        else {
            this._HelperService.NavLogin();
        }
    }

    NavStores() {
        if (this._HelperService._ActiveFeatures.service_nearby) {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
        }
        else {
            this._HelperService.NotifySimple("This features is coming soon in your area");
        }
    }
    NavDeals() {
        if (this._HelperService._ActiveFeatures.service_deals) {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.List);
        }
        else {
            this._HelperService.NotifySimple("This features is coming soon in your area");
        }
    }
    NavFlashDeals() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.FlashDeals);
    }
    NavPoints() {
        this._HelperService.ManageLogin(this._HelperService.AppConfig.Pages.Access.PointsHistory, true);
        // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavSaleHistory() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.SalesHistory);
    }
    NavFaq() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Faq);
    }
    NavWallet() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Wallet);
    }
    async BuyPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.initialize);
    }
    OpenLocationSelector() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    NavNotification() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.notifications);
    }

    // NavBnpl() {
    //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.home);
    // }



    NavCashout() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Cashout.list);
    }

    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }

    SelectBiller(Type) {
        //console.log(this._HelperService._ActiveFeatures);
        if (this._HelperService.CheckIsLogin()) {

            if (Type == 'airtime' && this._HelperService._ActiveFeatures.service_vas_airtime == false) {

            }
            else if (Type == 'tv' && this._HelperService._ActiveFeatures.service_vas_utilities == false) {

            }
            else {
                this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, Type);
                this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviders);
            }


        }
        else {
            this._HelperService.NavLogin();
        }
    }


    NavDealDetails(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
    }

    NavReferralEarning() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.referralbonus);
    }
    NavReferral() {
        if (this._HelperService.CheckIsLogin()) {
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.referral);
        }
        else {
            this._HelperService.NavLogin();
        }

    }
    //#endregion
    slideOpts = {
        initialSlide: 0,
        autoplay: true,
        slidesPerView: 1.9,
        spaceBetween: 0
    };
    slideOpts_Deals = {
        initialSlide: 0,
        autoplay: true,
        slidesPerView: 1.1,
        spaceBetween: 0
    };
    config =
        {
            stopTime: new Date().setHours(2),
        }
    ActiveTab = 1;
    @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
    constructor(
        private _Platform: Platform,
        private faio: FingerprintAIO,
        public _StatusBar: StatusBar,
        public _Firebase: FCM,
        public _Sanitizer: DomSanitizer,
        public _DatePipe: DatePipe,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        private iab: InAppBrowser,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _DataService: DataService,
        private _Mixpanel:Mixpanel
    ) {
        this._Firebase.subscribeToTopic(this._HelperService.AppConfig.Topics.tuccustomer);
        this._MenuController.enable(true);
        this._HelperService.RefreshAppActiveFeature();
        var BankDetailsStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.BankStatement);
        if (BankDetailsStorage != null) {
            this.bankDetails = BankDetailsStorage;
        }
    }
    public ionViewWillEnter() {
        this._HelperService.SetPageName("Home");
    }

    public UserCustomAddress =
        {
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityId: null,
            CityKey: null,
            CityName: null,

            StateId: null,
            StateKey: null,
            StateName: null,

            CountryName: null,
            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: null,
            Longitude: null,
        }
    public TUCNearByMerchants: any[] = [];
    public TUCCategories: any[] = [];
    public TUCMerchantCategories: any[] = [];
    public TUCTransactions: any[] = [];
    public SliderImages = [];
    ngAfterViewInit() {
        setTimeout(() => {
            this._MenuController.enable(true);
        }, 300);
    }
    public tItem = "";
    public inCartOrderCount = 0;
    countryName = ""
    ionViewDidEnter() {
        // Actions
        var UserCustomAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);

        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
    }
    ngOnInit() {
        this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.home, {page:"home", path:"/dashboard"} )
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._HelperService.RefreshProfile();
        this._HelperService.RefreshLocation();
        this._MenuController.enable(true);
        var UserCustomAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
        var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceStorage != null) {
            this._HelperService.AppConfig.ActiveLocation.Lat = DeviceStorage.Latitude;
            this._HelperService.AppConfig.ActiveLocation.Lon = DeviceStorage.Longitude;
        }
        var Config = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig);
        if (Config != null) {
            this.countryName = Config.SelectedCountry.Name;
            let Accesstologin = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Accessforgotpin);
            //  If condition help us to restict the user to access the app(similar to logout) in the scenario
            //  where he closed the app without updating pin in forgot pin userstory
            if (Accesstologin) {
                localStorage.clear();
                // this._HelperService.RefreshProfile();
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CountrySelect);

            }
            else {
                if (this._HelperService.AccountInfo.UserAccount.AccountCode != undefined) {
                    let dummyTxt = this._HelperService.AccountInfo.UserAccount.AccountCode;
                    let joy = dummyTxt.match(/.{1,4}/g);
                    this.tItem = joy.join(' ');
                }
                var TOrder = this._HelperService.GetStorage("incartorder");
                if (TOrder != undefined && TOrder != null) {
                    this.inCartOrderCount = 1;
                }
                else {
                    this.inCartOrderCount = 0;
                }
                this.LoadData();

            }

        }
        else {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CountrySelect);
        }
        this.LoadPayment();
    }

    async LoadPayment() {
        // var Payment =
        // {
        //     PaymentAmount: 100,
        //     PaymentTitle: 'TUC Pay',
        //     PaymentSubtitle: 'process payment to proceed'
        // }
        // const modal = await this._ModalController.create({
        //     component: PaymentManagerModal,
        //     componentProps: Payment
        // });
        // modal.onDidDismiss().then(data => {
        //     if (data.data.Status == 'success') {
        //         // this.ProcessOnline_Confirm();
        //     }
        // });
        // return await modal.present();
    }

    TabChange(TabId) {
        this.ActiveTab = TabId;
    }
    BuildCardNumber(dummyTxt) {
        let joy = dummyTxt.match(/.{1,4}/g);
        return joy.join(' ');
    }

    public SelectedItemDate = null;
    public DStartTimeS = "";
    public DEndTimeS = "";
    LoadData() {
        this._HelperService.RefreshAppConfiguration();
        var TSliderImages = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.SliderImages);
        if (TSliderImages != null) { this.SliderImages = TSliderImages };
        this.GetBalance();
        this.BalHistory_Setup();
        this.UpdateToken();
        this.AppVersionCheck();
        this._DataService.GetVasServices();
        this._DataService.GetTUCCategories();
        this.GetSliderImages();
        this.GetBnplConfiguration(false);
        if (this._DataService.TUCCategories != undefined && this._DataService.TUCCategories != null && this._DataService.TUCCategories.length > 0) {
            var TCat: any[] = this._DataService.TUCCategories.sort((obj1, obj2) => {
                if (obj1.Merchants > obj2.Merchants) {
                    return -1;
                }
                if (obj1.Merchants < obj2.Merchants) {
                    return 1;
                }
                return 0;
            });
            this.TUCCategories = this.chunkify(TCat, Math.round(TCat.length / 2), true);
            for (let CatIndex = 0; CatIndex < 5; CatIndex++) {
                const _Cat = TCat[CatIndex];
                var Category =
                {
                    ReferenceId: _Cat.ReferenceId,
                    IconUrl: _Cat.IconUrl,
                    Name: _Cat.Name,
                    Merchants: [],
                }
                var CatMerchants = [];
                var MerchantsByCategory = this._DataService.TUCMerchantCategories.filter(x => x.CategoryId == _Cat.ReferenceId);
                if (MerchantsByCategory != undefined && MerchantsByCategory.length > 0) {
                    MerchantsByCategory.forEach(element => {
                        var TMerchant = this._DataService.TUCMerchants.find(x => x.ReferenceId == element.MerchantId);
                        if (TMerchant != undefined) {
                            CatMerchants.push(TMerchant);
                        }
                    });
                    if (CatMerchants != undefined && CatMerchants.length > 0) {
                        if (CatMerchants.length < 3) {
                            Category.Merchants = CatMerchants;
                        }
                        else {
                            for (let index = 0; index < 3; index++) {
                                const element = CatMerchants[index];
                                Category.Merchants.push(element);
                            }
                        }
                    }
                }
                if (Category.Merchants != undefined && Category.Merchants.length > 0) {
                    this.TUCNearByMerchants.push(Category);
                }
            }
        }
    }
    chunkify(a, n, balanced) {

        if (n < 2)
            return [a];

        var len = a.length,
            out = [],
            i = 0,
            size;

        if (len % n === 0) {
            size = Math.floor(len / n);
            while (i < len) {
                out.push(a.slice(i, i += size));
            }
        }

        else if (balanced) {
            while (i < len) {
                size = Math.ceil((len - i) / n--);
                out.push(a.slice(i, i += size));
            }
        }
        else {

            n--;
            size = Math.floor(len / n);
            if (len % size === 0)
                size--;
            while (i < size * n) {
                out.push(a.slice(i, i += size));
            }
            out.push(a.slice(size * n));
        }
        return out;
    }
    CategoryClick(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TCategory, Item);
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }

    AppVersionCheck() {
        var ehcak = null;
        var ehcavk = null;
        if (this._Platform.is("android")) {
            ehcak = (this._HelperService.AppConfig.NetworkKeys.Android.AppKey);
            ehcavk = (this._HelperService.AppConfig.NetworkKeys.Android.AppVersionKey);
        } else if (this._Platform.is("ios")) {
            ehcak = (this._HelperService.AppConfig.NetworkKeys.Ios.AppKey);
            ehcavk = (this._HelperService.AppConfig.NetworkKeys.Ios.AppVersionKey);
        } else if (this._Platform.is("ipad")) {
            ehcak = (this._HelperService.AppConfig.NetworkKeys.Ios.AppKey);
            ehcavk = (this._HelperService.AppConfig.NetworkKeys.Ios.AppVersionKey);
        } else if (this._Platform.is("iphone")) {
            ehcak = (this._HelperService.AppConfig.NetworkKeys.Ios.AppKey);
            ehcavk = (this._HelperService.AppConfig.NetworkKeys.Ios.AppVersionKey);
        } else {
            ehcak = (this._HelperService.AppConfig.NetworkKeys.Android.AppKey);
            ehcavk = (this._HelperService.AppConfig.NetworkKeys.Android.AppVersionKey);
        }
        var pData = {
            Task: "appversioncheck",
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AppKey: ehcak,
            AppVersionKey: ehcak,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                }
                else {
                    this.ShowAppUpdateModal(_Response.Result);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    GetBalance() {
        if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
            var pData = {
                Task: "getuseraccountbalance",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                Source: "transactionsource.app",
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._DataService.UpdateBalance(_Response.Result as OBalance);
                    } else {
                        this._HelperService.NotifySimple(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }
    }
    async ShowMyCode() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }

    async ShowAppUpdateModal(Data) {
        const modal = await this._ModalController.create({
            component: AppUpdateModal,
            componentProps: Data,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }



    UpdateToken() {
        var Token = this._HelperService.GetStorageValue('dnot');
        if (Token != null) {
            if (this._HelperService.AccountInfo.UserAccount.AccountId != undefined && this._HelperService.AccountInfo.UserAccount.AccountId != null && this._HelperService.AccountInfo.UserAccount.AccountId != 0) {
                var DeviceInfo =
                {
                    Task: "updatedevicenotificationurl",
                    UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                    UserAccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                    NotificationUrl: Token,
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, DeviceInfo);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        }
                        else {
                            this._HelperService.Notify(_Response.Status, _Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.HandleException(_Error);
                    });

            }
        }
    }
    public BalHistory_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    BalHistory_Setup() {
        if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
            if (this.BalHistory_Data.Offset == -1) {
                this.BalHistory_Data.Offset = 0;
            }
            var SCon = "";
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountInfo.UserAccount.AccountId, '==');
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'SourceKey', this._HelperService.AppConfig.DataType.Text, "transaction.source.app", '!=');
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'Balance', this._HelperService.AppConfig.DataType.Number, "0", '>');

            var pData = {
                Task: 'getuserbalancehistory',
                TotalRecords: this.BalHistory_Data.TotalRecords,
                Offset: this.BalHistory_Data.Offset,
                Limit: this.BalHistory_Data.Limit,
                RefreshCount: true,
                SearchCondition: SCon,
                SortExpression: 'TransactionDate desc',
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this.BalHistory_Data.Offset = this.BalHistory_Data.Offset + this.BalHistory_Data.Limit;
                        this.BalHistory_Data.TotalRecords = _Response.Result.TotalRecords;
                        var BalHistory_Data = _Response.Result.Data;
                        BalHistory_Data.forEach(element => {
                            if (element.Credit > 0 || element.Debit > 0) {
                                element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
                                this.BalHistory_Data.Data.push(element);
                            }
                        });
                    }
                    else {
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    //#region 
    GetSliderImages() {
        var Sliders = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Slider.Home);
        if (Sliders != null) {
            this.SliderImages = Sliders;
        }
        var pData = {
            Task: "getappslider",
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            Location: "home",
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.App, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    if (_Response.Result != undefined && _Response.Result != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Slider.Home, _Response.Result);
                        this.SliderImages = _Response.Result;
                    }
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    SliderClick(Item) {
        if (Item != undefined && Item != null) {
            if (Item.NavigationType != undefined && Item.NavigationType != null && Item.NavigationType != '') {
                if (Item.NavigationType == 'url') {
                    if (Item.ReferenceId != undefined || Item.ReferenceId != null) {
                        this.SaveSliderClick(Item.ReferenceId);
                    }
                    const options = 'location=yes,zoom=no,hideurlbar=yes,hidenavigationbuttons=yes,closebuttoncaption=Close,closebuttoncolor=#ffffff,footercolor=#AF1482,toolbarcolor=#AF1482,disallowoverscroll=yes';
                    this.iab.create(Item.NavigateUrl, '_blank', options);
                }
                else if (Item.NavigationType == 'app') {
                    if (Item.NavigationData != undefined && Item.NavigationData != null && Item.NavigationData != '') {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item.NavigationData);
                    }
                    this.SaveSliderClick(Item.ReferenceId);
                    this._HelperService.NavigatePush(Item.NavigateUrl);
                }
                else {

                }
            }
        }
    }
    SaveSliderClick(ReferenceId) {
        if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
            var pData = {
                Task: "saveappsliderclick",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                ReferenceId: ReferenceId,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.App, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    } else {
                    }
                },
                _Error => {
                    // this._HelperService.HandleException(_Error);
                }
            );
        }

    }
    //#endregion

    //#region BNPL
    BnplConfiguration = {
        IsFeatureEnabled: false,
        FeatureEnabledMessage: "",
        IsConfigLoaded: false,
        IsBnplEnable: false,
        ReferenceId: 0,
        ReferenceKey: null,
        IsLoanActive: false,
        ActiveLoan: 0,
        TotalLoan: 0,
        MaximumLoanAmount: 0,
        MinimumLoanAmount: 0,
        Plans: [],
        SystemConfig:
        {
            monoKey: null,
        }
    };
    NavBnpl() {
        if (this._HelperService.CheckIsLogin()) {
            if (this._HelperService._ActiveFeatures.service_bnpl == true) {
                var bnplConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Bnpl.AccountConfig);
                if (bnplConfig != null) {
                    this.BnplConfiguration = bnplConfig;
                    this.GetBnplConfiguration(false);
                    if (this.BnplConfiguration.IsFeatureEnabled == true) {
                        if (this.BnplConfiguration.IsBnplEnable == true) {
                            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Bnpl.home);
                        }
                        else {
                            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.checkCreditLimit);
                        }
                    }
                    else {
                        this._HelperService.NotifyToastError(this.BnplConfiguration.FeatureEnabledMessage);
                    }
                }
                else {
                    this.GetBnplConfiguration(true);
                }
            }
            else {
                this._HelperService.NotifySimple("This features is coming soon in your area");

            }

        }
        else {
            this._HelperService.NavLogin();
        }






        // if (this.BnplConfiguration.IsBnplEnable == true && this.BnplConfiguration.IsConfigLoaded == true) {
        //     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Bnpl.home);
        // }
        // else {
        //     if (this.BnplConfiguration.IsConfigLoaded == false) {
        //         this.GetBnplConfiguration(true);
        //     }
        //     else {
        //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.checkCreditLimit);
        //     }
        // }
    }
    GetBnplConfiguration(NavigateCustomer = false) {
        var mode = this._HelperService.GetStorage('bnplmode');
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.V3.Bnpl.getcustomerconfiguration,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            Mode: mode,
            user: {
                email: this._HelperService.AccountInfo.User.EmailAddress,
            }
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.BnplConfiguration = _Response.Result;
                    this.BnplConfiguration.IsConfigLoaded = true;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Bnpl.AccountConfig, this.BnplConfiguration);
                    if (NavigateCustomer == true) {
                        this.NavBnpl();
                    }
                } else {
                    this.BnplConfiguration.IsConfigLoaded = true;
                    this._HelperService.NotifySimple(_Response.Message);
                }
            }
        )
    }
    //#endregion
}