import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DashboardPage } from './dashboard.component';
import { NgCalendarModule } from 'ionic2-calendar';
import { ChartsModule } from 'ng2-charts';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { CountdownModule } from 'ngx-countdown';
import { Angular4PaystackModule } from 'angular4-paystack';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgCalendarModule,
        ChartsModule,
        GooglePlaceModule,
        CountdownModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA6nUouKi8QeQBh6hcgTnhKjoxNlUShh_E'
        }),
        // Angular4PaystackModule.forRoot('pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12'),
        RouterModule.forChild([
            {
                path: '',
                component: DashboardPage
            }
        ])
    ],
    declarations: [DashboardPage]
})
export class DashboardPageModule { }
