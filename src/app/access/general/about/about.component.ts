import { Component, Sanitizer } from '@angular/core';
import { HelperService } from '../../../service/helper.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Platform } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-about',
    templateUrl: 'about.component.html',
})
export class AboutPage {
    constructor(
        private callNumber: CallNumber, public _HelperService: HelperService, public _Platform: Platform, public san: DomSanitizer,
    ) {
        this._HelperService.RefreshAppConfiguration();
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("About");
    }


    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }
    ContactClick(Item) {
        if (Item.type = 'call') {
            this.callNumber.callNumber(Item.systemvalue, true)
                .then()
                .catch(err => console.log('Error launching dialer', err));
        }
        else {
            this._Platform.ready().then(() => {
                window.open(Item.systemvalue);
            });
        }
    }
}
