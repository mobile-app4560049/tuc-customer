import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../../service/object.service';
@Component({
    templateUrl: 'faqinfo.modal.component.html',
    selector: 'modal-faqinfo'
})
export class FaqInfoDetailsModal {

    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    tItem = "";
    ItemDetails =
        {
            Name: null,
            Description: null,
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.ItemDetails = this.navParams.data as any;
    }
    async ModalDismiss() {
        await this._ModalController.dismiss();
    }
}