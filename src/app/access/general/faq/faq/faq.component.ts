import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';
declare var moment: any;
@Component({
    selector: 'app-faq',
    templateUrl: 'faq.component.html',
})
export class FaqPage {

    public DeviceInformation: ODeviceInformation;
    constructor(
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Faq");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.LoadData();
    }
    LoadData() {
        this.FaqCat_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.FaqCat_Setup();
    }





    public FaqCat_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    FaqCat_Setup() {
        this._HelperService.ShowProgress();

        if (this.FaqCat_Data.Offset == -1) {
            this.FaqCat_Data.Offset = 0;
        }
        var SCon = "";
        // SCon = this._HelperService.GetSearchConditionStrict('', 'AccountTypeCode', 'text', 'appuser', '=');
        if (this.FaqCat_Data.SearchContent != undefined && this.FaqCat_Data.SearchContent != null && this.FaqCat_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'Name', 'text', this.FaqCat_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'Description', 'text', this.FaqCat_Data.SearchContent);
        }
        var pData = {
            Task: 'getfaqcategoriesclient',
            TotalRecords: this.FaqCat_Data.TotalRecords,
            Offset: this.FaqCat_Data.Offset,
            Limit: this.FaqCat_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.HCFaq, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.FaqCat_Data.Offset = this.FaqCat_Data.Offset + this.FaqCat_Data.Limit;
                    this.FaqCat_Data.TotalRecords = _Response.Result.TotalRecords;
                    var FaqCat_Data = _Response.Result.Data;
                    FaqCat_Data.forEach(element => {
                        this.FaqCat_Data.Data.push(element);
                    });
                    if (this.FaqCat_LoaderEvent != undefined) {
                        this.FaqCat_LoaderEvent.target.complete();
                        if (this.FaqCat_Data.TotalRecords == this.FaqCat_Data.Data.length) {
                            this.FaqCat_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public FaqCat_LoaderEvent: any = undefined;
    FaqCat_NextLoad(event) {
        this.FaqCat_LoaderEvent = event;
        this.FaqCat_Setup();
    }
    private FaqCat_DelayTimer;
    FaqCat_DoSearch() {
        clearTimeout(this.FaqCat_DelayTimer);
        this.FaqCat_DelayTimer = setTimeout(x => {
            this.FaqCat_Data.TotalRecords = -1;
            this.FaqCat_Data.Offset = -1;
            this.FaqCat_Data.Limit = 10;
            this.FaqCat_Data.Data = [];
            this.FaqCat_Setup();
        }, 1000);
    }
    NavDealerProducts(Item) {
        this._HelperService.SaveStorage('ActiveFaqCat', Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.FaqDetails);
    }

}
