import { Component, Sanitizer } from '@angular/core';
import { HelperService } from '../../../service/helper.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Platform } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-support',
    templateUrl: 'support.component.html',
})
export class SupportPage {
    helpUrl: any;
    constructor(
        private callNumber: CallNumber, public _HelperService: HelperService, public _Platform: Platform, public san: DomSanitizer,
    ) {

        this.helpUrl = this.san.bypassSecurityTrustResourceUrl(
            'https://thankucash.com/appsupport.html'
        );

    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("About");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }
    CallNumber() {
        this.callNumber.callNumber("2347000433433", true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
    mailto(email) {
        this._Platform.ready().then(() => {
            window.open('mailto:' + email);
        });
    }
}
