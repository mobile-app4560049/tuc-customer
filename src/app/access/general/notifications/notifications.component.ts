import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
declare var moment: any;
@Component({
    selector: 'app-notifications',
    templateUrl: 'notifications.component.html',
})
export class NotificationsPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public Notitifcations = [];
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Notifications");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.LoadData();
    }
    LoadData() {
        var Nots = this._HelperService.GetStorage('tucnots');
        if (Nots != null) {
            this.Notitifcations = Nots;
        }
    }

    NavigatePush(data) {
        if (data != undefined && data != null) {
            if (data.Task != undefined && data.Task != null) {
                var Task = data.Task;
                if (Task == "dashboard") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                }
                if (Task == "about") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.About);
                }
                if (Task == "profile") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Profile);
                }
                if (Task == "updatepin") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.ChangePin);
                }
                if (Task == "faq") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Faq);
                }
                if (Task == "cashout") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Cashout.list);
                }
                if (Task == "cardmanager") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CardsManager.cardmanger);
                }
                if (Task == "cardmanager") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CardsManager.cardmanger);
                }
                if (Task == "bankmanager") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BankManager.list);
                }
                if (Task == "faqdetails") {
                    var _ItemFaq =
                    {
                        ReferenceId: data.ReferenceId,
                        ReferenceKey: data.ReferenceKey,
                        Name: data.Name,
                        Description: data.Description,
                    };
                    this._HelperService.SaveStorage('ActiveFaqCat', _ItemFaq);
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.FaqDetails);
                }
                if (Task == "saleshistory") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.SalesHistory);
                }
                if (Task == "pointshistory") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
                }
                if (Task == "lcctopup") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Payments.LccTopup);
                }
                if (Task == "addresslocator") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
                }

                if (Task == "deals") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.List);
                }
                if (Task == "flashdeals") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.FlashDeals);
                }

                if (Task == "scanredeemcode") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode);
                }
                if (Task == "referral") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.referral);
                }
                if (Task == "referralhistory") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.referralbonus);
                }

                if (Task == "ordershistory") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrdersHistory);
                }
                if (Task == "buypoints") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.initialize);
                }
                if (Task == "wallettopuphistory") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.list);
                }
                if (Task == "stores") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
                }

                //dealdetails
                if (Task == "airtime") {
                    this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, "airtime");
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviders);
                }
                if (Task == "tv") {
                    this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, "tv");
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviders);
                }
                if (Task == "electricity") {
                    this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, "electricity");
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviders);
                }

                if (Task == "dealpurchasehistory") {
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.DealPurchaseHistory);
                }

                if (Task == "pointpurchasereceipt") {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, data);
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.buypointconfirm);
                }
                if (Task == "dealpurchasedetails") {
                    var _Item1 =
                    {
                        ReferenceId: data.ReferenceId,
                        ReferenceKey: data.ReferenceKey
                    };
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Item1);
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.DealPurchaseDetails);
                }
                if (Task == "paymentdetails") {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, data);
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BuyPoints.buypointconfirm);
                }
                if (Task == "vaspaymenthistory") {
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.list);
                }
                if (Task == "deal") {
                    var _Itemdeal =
                    {
                        ReferenceId: data.ReferenceId,
                        ReferenceKey: data.ReferenceKey
                    };
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Itemdeal);
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
                }
                if (Task == "order") {
                    if (data.ReferenceId != undefined && data.ReferenceKey != null) {
                        var _OrderDetails =
                        {
                            OrderId: data.ReferenceId,
                            OrderKey: data.ReferenceKey
                        };
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
                        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
                    }
                }
            }
        }
    }

}
