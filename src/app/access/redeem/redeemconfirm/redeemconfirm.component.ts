import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
import { PaymentTypeSelectModal } from '../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../service/data.service';
declare var moment: any;

@Component({
    selector: 'app-redeemconfirm',
    templateUrl: 'redeemconfirm.component.html'
})
export class RedeemConfirmPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Payment-BuyPoints");
    }

    _MerchantDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,
            DisplayName: null,
            IconUrl: null,
            RedeemAmount: 0,
            Pin: null,
            AccountBalance: 0,
            Comment: null,
            AccountNumber: null,

            RewardAmount: 0,
            TransactionReferenceId: null,
            TransactionDate: null,
            StatusName: null,
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._MerchantDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        if (this._MerchantDetails.TransactionDate != null) {
            this._MerchantDetails.TransactionDate = this._HelperService.GetDateTimeS(this._MerchantDetails.TransactionDate);
        }
    }


    ProcessOnline_Cancel() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }



    // LoadData() {
    //     this.TranList_Data =
    //         {
    //             SearchContent: "",
    //             TotalRecords: 0,
    //             Offset: -1,
    //             Limit: 10,
    //             Data: []
    //         };
    //     this.TranList_Setup();
    // }
    // public TranList_Data =
    //     {
    //         SearchContent: "",
    //         TotalRecords: 0,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    // TranList_Setup() {
    //     // if (this.TranList_Data.Offset == -1) {
    //     //     this._HelperService.ShowSpinner();
    //     //     this.TranList_Data.Offset = 0;
    //     // }
    //     var SCon = "";
    //     // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', 'number', this._HelperService.AccountInfo.UserAccount.AccountId, "=");
    //     // if (this.TranList_Data.SearchContent != undefined && this.TranList_Data.SearchContent != null && this.TranList_Data.SearchContent != '') {
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     // }
    //     // SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this.StartTime, this.EndTime);
    //     // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountOwner.AccountId, '==');
    //     var pData = {
    //         Task: 'getsaletransactions',
    //         TotalRecords: this.TranList_Data.TotalRecords,
    //         Offset: this.TranList_Data.Offset,
    //         Limit: this.TranList_Data.Limit,
    //         RefreshCount: true,
    //         SearchCondition: SCon,
    //         SortExpression: 'TransactionDate desc',
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCTransCore, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.HideSpinner();
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
    //                 this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
    //                 var TranList_Data = _Response.Result.Data;
    //                 TranList_Data.forEach(element => {
    //                     if (element.CardBrandName != undefined) {
    //                         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
    //                     }
    //                     element.TypeName = element.TypeName.toLowerCase().trim();
    //                     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
    //                     this.TranList_Data.Data.push(element);
    //                 });
    //                 if (this.LoaderEvent != undefined) {
    //                     this.LoaderEvent.target.complete();
    //                     if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
    //                         this.LoaderEvent.target.disabled = true;
    //                     }
    //                 }
    //             }
    //             else {
    //                 this._HelperService.HideSpinner();
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // TranList_RowSelected(ReferenceData) {
    //     if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
    //     }
    //     else {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
    //     }


    // }
    // public LoaderEvent: any = undefined;
    // NextLoad(event) {
    //     this.LoaderEvent = event;
    //     this.TranList_Setup();

    // }

    // private delayTimer;
    // doSearch(text) {
    //     clearTimeout(this.delayTimer);
    //     this.delayTimer = setTimeout(x => {
    //         this.TranList_Data =
    //             {
    //                 SearchContent: text,
    //                 TotalRecords: 0,
    //                 Offset: -1,
    //                 Limit: 10,
    //                 Data: []
    //             };
    //         this.TranList_Setup();
    //     }, 1000);
    // }

    // async OpenAddModal(Item) {
    //     const modal = await this._ModalController.create({
    //         component: TransactionDetailsModal,
    //         componentProps: Item
    //     });
    //     return await modal.present();
    // }




}