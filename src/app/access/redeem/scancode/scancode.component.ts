import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';


@Component({
    selector: 'app-scancode',
    templateUrl: 'scancode.component.html'
})
export class ScanRedeemCodePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        private qrScanner: QRScanner,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ScannerTimer;
    private scanSub: any;
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        // this.DealCode = '6971294975390';
        // this.Redeem_Initialize();
        setTimeout(() => {
            this.qrScanner.prepare()
                .then((status: QRScannerStatus) => {
                    if (status.authorized) {
                        this.qrScanner.show().then(x => {
                            this.ScannerTimer = setTimeout(() => {
                                if (this.DealCode == undefined || this.DealCode == null || this.DealCode == '') {
                                    this.qrScanner.hide(); // hide camera preview
                                    this.qrScanner.destroy();
                                    this.scanSub.unsubscribe(); // stop scanning
                                    this._HelperService.NavDashboard();
                                    this._HelperService.NotifyToast('Deal qr code not found. Please scan again to continue');
                                }
                            }, 15000);

                            this.scanSub = this.qrScanner.scan().subscribe((text: string) => {
                                this.DealCode = text.trim();
                                this.qrScanner.hide(); // hide camera preview
                                this.scanSub.unsubscribe(); // stop scanning
                                this.qrScanner.destroy();
                                if (this.ScannerTimer != undefined && this.ScannerTimer != null) {
                                    clearTimeout(this.ScannerTimer);
                                }
                                this.Redeem_Initialize();
                            });

                        });
                    } else if (status.denied) {
                        this._HelperService.NotifySimple('Camera permission required to scan qr code');
                    } else {
                        this._HelperService.NotifySimple('Camera permission required to scan qr code');
                        // permission was denied, but not permanently. You can ask for permission again at a later time.
                    }
                })
                .catch((e: any) => console.log('Error is', e));
        }, 300);
    }

    public DealCode = null;
    public Redeem_Initialize() {
        if (this.DealCode == undefined || this.DealCode == null || this.DealCode == '') {
            this._HelperService.NotifySimple('Unable to read merchant code');
        }
        else {
            this._HelperService.ShowSpinner("please wait");
            var pData = {
                Task: 'redeeminitialize',
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountNumber: this.DealCode,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Redeem, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        // if (this.qrScanner != undefined) {
                        //     this.qrScanner.hide(); // hide camera preview
                        // }
                        // if (this.scanSub != undefined) {
                        //     this.scanSub.unsubscribe(); // stop scanning
                        //     this.qrScanner.destroy();
                        // }
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.MerchantRedeem.redeeminitialize);
                    } else {
                        // if (this.qrScanner != undefined) {
                        //     this.qrScanner.hide(); // hide camera preview
                        // }
                        // if (this.scanSub != undefined) {
                        //     this.scanSub.unsubscribe(); // stop scanning
                        // }
                        this._HelperService.NotifySimple(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }

    }

    Redeem_Initialize_Cancel() {
        if (this.ScannerTimer != undefined && this.ScannerTimer != null) {
            clearTimeout(this.ScannerTimer);
        }
        if (this.scanSub != undefined) {
            this.scanSub.unsubscribe(); // stop scanning
        }
        if (this.qrScanner != undefined) {
            this.qrScanner.hide(); // hide camera preview
            this.qrScanner.destroy();
        }
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    public ionViewWillLeave() {
        if (this.ScannerTimer != undefined && this.ScannerTimer != null) {
            clearTimeout(this.ScannerTimer);
        }
        if (this.scanSub != undefined) {
            this.scanSub.unsubscribe(); // stop scanning
        }
        if (this.qrScanner != undefined) {
            this.qrScanner.hide(); // hide camera preview
            this.qrScanner.destroy();
        }
    }
    public ionViewDidLeave() {
        if (this.ScannerTimer != undefined && this.ScannerTimer != null) {
            clearTimeout(this.ScannerTimer);
        }
        if (this.scanSub != undefined) {
            this.scanSub.unsubscribe(); // stop scanning
        }
        if (this.qrScanner != undefined) {
            this.qrScanner.hide(); // hide camera preview
            this.qrScanner.destroy();
        }
    }


}