import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OAddressComponent } from '../../../../service/object.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ModalSelect } from 'src/app/auth/modalselect/modalselect.component';
@Component({
    selector: 'app-editaddress',
    templateUrl: 'editaddress.component.html'
})
export class EditAddressPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public navCtrl: NavController,
        public _ModalController: ModalController,
        public _ImagePicker: ImagePicker,
        public _HelperService: HelperService,
        public _AlertController: AlertController
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("AddressSetup");
    }
    public _ActiveAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,

            Task: "updateuseraddress",
            CountryId: null,
            CountryKey: null,
            CountryName: null,

            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityAreaId: null,
            CityAreaKey: null,
            CityAreaName: null,

            CityId: null,
            CityKey: null,
            CityName: null,

            StateId: null,
            StateKey: null,
            StateName: null,

            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: -1,
            Longitude: -1,

            IsPrimary: false,
            LocationTypeId: 800
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._ActiveAddress.ContactNumber = this._HelperService.AccountInfo.User.MobileNumber;
        this._ActiveAddress.EmailAddress = this._HelperService.AccountInfo.User.EmailAddress;
        this._ActiveAddress.Latitude = this._HelperService.AppConfig.ActiveLocation.Lat;
        this._ActiveAddress.Longitude = this._HelperService.AppConfig.ActiveLocation.Lon;
        var TAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        if (TAddress != null) {
            this._ActiveAddress = TAddress;
        }
    }


    NavigateDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    placeMarker(Item) {
        this._ActiveAddress.Latitude = Item.coords.lat;
        this._ActiveAddress.Longitude = Item.coords.lng;
    }

    public Form_UpdateUser_AddressChange(address: Address) {
        this._ActiveAddress.Latitude = address.geometry.location.lat();
        this._ActiveAddress.Longitude = address.geometry.location.lng();
        this._ActiveAddress.MapAddress = address.formatted_address;
        // this._ActiveAddress.AddressLine2 = address.formatted_address;
        address.address_components.forEach(address_component => {
            // if (address_component.types[0] == "locality") {
            //     this._AccountActiveAddress.CityName = address_component.long_name;
            // }
            // if (address_component.types[0] == "country") {
            //     this.UserCustomAddress.CountryName = address_component.long_name;
            // }
            // if (address_component.types[0] == "postal_code") {
            //     this.UserCustomAddress.ZipCode = address_component.long_name;
            // }
            // if (address_component.types[0] == "administrative_area_level_1") {
            //     this.UserCustomAddress.StateName = address_component.long_name;
            // }
        });
    }


    States = [];
    GetStates() {
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var pData = {
            Task: 'getstates',
            ReferenceId: this._ActiveAddress.CountryId,
            ReferenceKey: this._ActiveAddress.CountryKey,
            Offset: 0,
            Limit: 1000
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.States = _Response.Result.Data;
                    this.StateClick();
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async StateClick() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var _Request =
        {
            Title: "State",
            SubTitle: "Select your state",
            Items: []
        };
        this.States.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this._ActiveAddress.StateId = data.data.ReferenceId;
                this._ActiveAddress.StateKey = data.data.ReferenceKey;
                this._ActiveAddress.StateName = data.data.Name;
                this._ActiveAddress.CityId = null;
                this._ActiveAddress.CityKey = null;
                this._ActiveAddress.CityName = null;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
            }
        });
        return await modal.present();
    }
    Cities = [];
    GetCities() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var pData = {
            Task: 'getcities',
            ReferenceId: this._ActiveAddress.StateId,
            ReferenceKey: this._ActiveAddress.StateKey,
            Offset: 0,
            Limit: 1000
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Cities = _Response.Result.Data;
                    this.CityClick();
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async CityClick() {
        var _Request =
        {
            Title: " City",
            SubTitle: "Select your city",
            Items: []
        };
        this.Cities.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this._ActiveAddress.CityId = data.data.ReferenceId;
                this._ActiveAddress.CityKey = data.data.ReferenceKey;
                this._ActiveAddress.CityName = data.data.Name;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
            }
        });
        return await modal.present();
    }


    //#region Save Address -- Start
    SaveAddress() {
        if (this._ActiveAddress.Name == null || this._ActiveAddress.Name == '') {
            this._HelperService.NotifyToast('Enter name');
        }
        else if (this._ActiveAddress.ContactNumber == null || this._ActiveAddress.ContactNumber == '') {
            this._HelperService.NotifyToast('Enter mobile number');
        }
        else if (this._ActiveAddress.AddressLine1 == null || this._ActiveAddress.AddressLine1 == '') {
            this._HelperService.NotifyToast('Enter address');
        }
        else if (this._ActiveAddress.StateId == null || this._ActiveAddress.StateId < 1) {
            this._HelperService.NotifyToast('Select state');
        }
        else if (this._ActiveAddress.CityId == null || this._ActiveAddress.CityId < 1) {
            this._HelperService.NotifyToast('Select city');
        }
        else {
            this._HelperService.DeleteStorage('incartorder');
            this._HelperService.AppConfig.DeliveryLocation.Lat = this._ActiveAddress.Latitude;
            this._HelperService.AppConfig.DeliveryLocation.Lon = this._ActiveAddress.Longitude;
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
            this._HelperService.ShowProgress();
            this._HelperService.ShowSpinner();
            var _Request =
            {
                Task: 'updateaddress',
                ReferenceId: this._ActiveAddress.ReferenceId,
                ReferenceKey: this._ActiveAddress.ReferenceKey,
                IsPrimary: this._ActiveAddress.IsPrimary,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                LocationTypeId: this._ActiveAddress.LocationTypeId,
                Name: this._ActiveAddress.Name,
                ContactNumber: this._ActiveAddress.ContactNumber,
                EmailAddress: this._ActiveAddress.EmailAddress,
                AddressLine1: this._ActiveAddress.AddressLine1,
                AddressLine2: this._ActiveAddress.AddressLine2,
                ZipCode: this._ActiveAddress.ZipCode,
                Landmark: this._ActiveAddress.Landmark,
                CityId: this._ActiveAddress.CityId,
                StateId: this._ActiveAddress.StateId,
                CountryId: this._ActiveAddress.CountryId,
                Latitude: this._ActiveAddress.Latitude,
                Longitude: this._ActiveAddress.Longitude,
                MapAddress: this._ActiveAddress.MapAddress,
                Instructions: this._ActiveAddress.Instructions,
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, _Request);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HideProgress();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifyToastSuccess(_Response.Message);
                        this.navCtrl.back();
                    }
                    else {
                        this._HelperService.NotifyToastError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    //#endregion Save Address -- End
    backnav(){
        this.navCtrl.back();
    }
}