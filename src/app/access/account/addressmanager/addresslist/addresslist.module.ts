import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AddressListPage } from './addresslist.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA6nUouKi8QeQBh6hcgTnhKjoxNlUShh_E'
        }),
        RouterModule.forChild([
            {
                path: '',
                component: AddressListPage
            }
        ])
    ],
    declarations: [AddressListPage]
})
export class AddressListPageModule { }
