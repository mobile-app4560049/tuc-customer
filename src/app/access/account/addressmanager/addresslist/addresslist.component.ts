import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, ActionSheetController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OAddressComponent } from '../../../../service/object.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ModalSelect } from 'src/app/auth/modalselect/modalselect.component';
@Component({
    selector: 'app-addresslist',
    templateUrl: 'addresslist.component.html'
})
export class AddressListPage {
    public DeviceInformation: ODeviceInformation;
    isFromDealPage :boolean= false;
    constructor(
        public navCtrl: NavController,
        public _ModalController: ModalController,
        public _ImagePicker: ImagePicker,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ActionSheetController: ActionSheetController
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        var isRefresh = this._HelperService.GetStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        if (isRefresh != null) {
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
            this.ResetList();
        }
        this._HelperService.SetPageName("AddressSetup");
    }
    public _ActiveAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Task: "updateuseraddress",
            CountryId: null,
            CountryKey: null,
            CountryName: null,

            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityAreaId: null,
            CityAreaKey: null,
            CityAreaName: null,

            CityId: null,
            CityKey: null,
            CityName: null,

            StateId: null,
            StateKey: null,
            StateName: null,

            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: -1,
            Longitude: -1,

            IsPrimary: false,
            LocationTypeId: 800
        }

    ngOnInit() {
        this.isFromDealPage =this._HelperService.GetStorage('fromDealPage');
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        var TAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (TAddress != null) {
            this._ActiveAddress = TAddress;
        }
        else {
            // this._ActiveAddress.ContactNumber = this._HelperService.AccountInfo.User.MobileNumber;
            // this._ActiveAddress.EmailAddress = this._HelperService.AccountInfo.User.EmailAddress;
            // this._ActiveAddress.Latitude = this._HelperService.AppConfig.ActiveLocation.Lat;
            // this._ActiveAddress.Longitude = this._HelperService.AppConfig.ActiveLocation.Lon;
        }
        this.ResetList();
    }

    NavigateAddAddress() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Address.add);
    }
    NavigateDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    placeMarker(Item) {
        this._ActiveAddress.Latitude = Item.coords.lat;
        this._ActiveAddress.Longitude = Item.coords.lng;
    }

    public Form_UpdateUser_AddressChange(address: Address) {
        this._ActiveAddress.Latitude = address.geometry.location.lat();
        this._ActiveAddress.Longitude = address.geometry.location.lng();
        this._ActiveAddress.MapAddress = address.formatted_address;
        // this._ActiveAddress.AddressLine2 = address.formatted_address;
        address.address_components.forEach(address_component => {
            // if (address_component.types[0] == "locality") {
            //     this._AccountActiveAddress.CityName = address_component.long_name;
            // }
            // if (address_component.types[0] == "country") {
            //     this.UserCustomAddress.CountryName = address_component.long_name;
            // }
            // if (address_component.types[0] == "postal_code") {
            //     this.UserCustomAddress.ZipCode = address_component.long_name;
            // }
            // if (address_component.types[0] == "administrative_area_level_1") {
            //     this.UserCustomAddress.StateName = address_component.long_name;
            // }
        });
    }


    States = [];
    GetStates() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var pData = {
            Task: 'getstates',
            ReferenceId: this._ActiveAddress.CountryId,
            ReferenceKey: this._ActiveAddress.CountryKey,
            Offset: 0,
            Limit: 1000
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.States = _Response.Result.Data;
                    this.StateClick();
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async StateClick() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var _Request =
        {
            Title: "State",
            SubTitle: "Select your state",
            Items: []
        };
        this.States.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this._ActiveAddress.StateId = data.data.ReferenceId;
                this._ActiveAddress.StateKey = data.data.ReferenceKey;
                this._ActiveAddress.StateName = data.data.Name;
                this._ActiveAddress.CityId = null;
                this._ActiveAddress.CityKey = null;
                this._ActiveAddress.CityName = null;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
            }
        });
        return await modal.present();
    }
    Cities = [];
    GetCities() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var pData = {
            Task: 'getcities',
            ReferenceId: this._ActiveAddress.StateId,
            ReferenceKey: this._ActiveAddress.StateKey,
            Offset: 0,
            Limit: 1000
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Cities = _Response.Result.Data;
                    this.CityClick();
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async CityClick() {
        var _Request =
        {
            Title: " City",
            SubTitle: "Select your city",
            Items: []
        };
        this.Cities.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this._ActiveAddress.CityId = data.data.ReferenceId;
                this._ActiveAddress.CityKey = data.data.ReferenceKey;
                this._ActiveAddress.CityName = data.data.Name;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
            }
        });
        return await modal.present();
    }


    //#region  Address List -- Start

    ResetList() {
        this.AddressM_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: 0,
            Limit: 100,
            Data: []
        };
        this.AddressM_Setup();
    }
    public AddressM_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: 0,
            Limit: 100,
            Data: []
        };
    AddressM_Setup() {

        this._HelperService.ShowProgress();
        if (this.AddressM_Data.Offset == -1) {
            this.AddressM_Data.Offset = 0;
        }
        var SCon = "";
        if (this.AddressM_Data.SearchContent != undefined && this.AddressM_Data.SearchContent != null && this.AddressM_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'AddressLine1', 'text', this.AddressM_Data.SearchContent);
        }
        var pData = {
            Task: 'getaddresslist',
            TotalRecords: this.AddressM_Data.TotalRecords,
            Offset: this.AddressM_Data.Offset,
            Limit: this.AddressM_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey
        };
        // var AddressList = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.AddressList);
        // if (AddressList != null) {
        //     this.AddressM_Data = AddressList;
        // }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.AddressM_Data.TotalRecords = _Response.Result.TotalRecords;
                    this.AddressM_Data.Data = _Response.Result.Data;
                    // AddressM_Data.forEach(element => {
                    //     this.AddressM_Data.Data.push(element);
                    // });
                    // if (this.AddressM_LoaderEvent != undefined) {
                    //     this.AddressM_LoaderEvent.target.complete();
                    //     if (this.AddressM_Data.TotalRecords == this.AddressM_Data.Data.length) {
                    //         this.AddressM_LoaderEvent.target.disabled = true;
                    //     }
                    // }
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AddressList, this.AddressM_Data);
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public AddressM_LoaderEvent: any = undefined;
    AddressM_NextLoad(event) {
        this.AddressM_LoaderEvent = event;
        this.AddressM_Setup();
    }
    private AddressM_DelayTimer;
    AddressM_DoSearch() {
        clearTimeout(this.AddressM_DelayTimer);
        this.AddressM_DelayTimer = setTimeout(x => {
            this.AddressM_Data.TotalRecords = -1;
            this.AddressM_Data.Offset = -1;
            this.AddressM_Data.Limit = 10;
            this.AddressM_Data.Data = [];
            this.AddressM_Setup();
        }, 1000);
    }

    async AddressM_Selected(Item) {
        // this._HelperService.SaveStorage('ActiveFaqCat', Item);
        // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.FaqDetails);
        const actionSheet = await this._ActionSheetController.create({
            header: 'Action',
            buttons: [{
                text: 'Set Address',
                icon: 'locate',
                cssClass: 'actionsheetcolor',
                handler: () => {
                    console.log('Set Address clicked');
                    this._ActiveAddress = Item;
                    this._HelperService.IsFormProcessing = true;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
                    if(this.isFromDealPage === true){
                        this.navCtrl.back();
                        this._HelperService.DeleteStorage('fromDealPage');
                    }
                    // this._HelperService.ShowSpinner();
                    // var pData = {
                    //     Task: "saveaddress",
                    //     ReferenceId: Item.ReferenceId,
                    //     ReferenceKey: Item.ReferenceKey,
                    //     Name: Item.Name,
                    //     MobileNumber: Item.ContactNumber,
                    //     AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                    //     AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                    //     AddressLine1: Item.AddressLine1,
                    //     AddressLine2: this._ActiveAddress.AddressLine1,
                    //     EmailAddress: Item.EmailAddress,
                    //     CountryId: Item.CountryId,
                    //     CountryKey: Item.CountryKey,
                    //     CountryName: Item.CountryName,
                    //     StateId: Item.StateId,
                    //     StateKey: Item.StateKey,
                    //     StateName: Item.StateName,
                    //     CityId: Item.CityId,
                    //     CityKey: Item.CityKey,
                    //     CityName: Item.CityName,
                    //     FirstName: this._HelperService.AccountInfo.User.FirstName,
                    //     LastName: this._HelperService.AccountInfo.User.LastName,
                    //     Is_Residetnial: false,
                    // };

                    // let _OResponse: Observable<OResponse>;
                    // _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Logistics, pData);
                    // _OResponse.subscribe(
                    //     _Response => {
                    //         this._ActiveAddress = Item;
                    //         this._HelperService.IsFormProcessing = false;
                    //         this._HelperService.HideSpinner();
                    //         if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    //             this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, Item);
                    //             // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    //             this.navCtrl.back();
                    //         }
                    //         else {
                    //             this._HelperService.IsFormProcessing = false;
                    //         }
                    //     },
                    //     _Error => {
                    //         this._HelperService.IsFormProcessing = false;
                    //         this._HelperService.HandleException(_Error);
                    //     });
                }
            }, {
                text: 'Edit',
                icon: 'create',
                cssClass: 'actionsheetcolor',
                handler: () => {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Address.edit);
                }
            },
            {
                text: 'Delete',
                role: 'destructive',
                icon: 'trash',
                cssClass: 'actionsheetcolor',
                handler: () => {
                    this.DeleteAddress(Item);
                }
            },
            {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                cssClass: 'actionsheetcolor',
                handler: () => {
                }
            }]
        });
        await actionSheet.present();
    }
    //#endregion Address List -- End

    //#region  Delete Address -- Start
    async DeleteAddress(Item) {
        const alert = await this._AlertController.create({
            header: 'Remove address ?',
            message: 'Tap on continue to remove address',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'text-light  alert-btn',
                    handler: () => {
                    }
                },
                {
                    text: 'Confirm',
                    cssClass: 'text-primary alert-btn',
                    handler: () => {
                        this._HelperService.IsFormProcessing = true;
                        this._HelperService.ShowSpinner();
                        var pData = {
                            Task: "deleteaddress",
                            ReferenceId: Item.ReferenceId,
                            ReferenceKey: Item.ReferenceKey,
                            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                        };
                        let _OResponse: Observable<OResponse>;
                        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
                        _OResponse.subscribe(
                            _Response => {
                                this._HelperService.IsFormProcessing = false;
                                this._HelperService.HideSpinner();
                                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                                    this.ResetList();
                                    this._HelperService.NotifyToastSuccess(_Response.Message);

                                    this._ActiveAddress.CityId = null,
                                        this._ActiveAddress.CityKey = null,
                                        this._ActiveAddress.CityName = null,

                                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress)



                                }
                                else {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.NotifyToastError(_Response.Message);
                                }
                            },
                            _Error => {
                                this._HelperService.IsFormProcessing = false;
                                this._HelperService.HandleException(_Error);
                            });
                    }
                }
            ]
        });
        return await alert.present();
    }
    //#endregion Delete Address -- End
    back(){
        if(this.isFromDealPage === true){
            // this.navCtrl.back();
            this._HelperService.DeleteStorage('fromDealPage');
        }
    }
}