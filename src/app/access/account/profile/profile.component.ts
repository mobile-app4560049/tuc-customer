import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController } from '@ionic/angular';
import { on } from 'process';
import { Mixpanel,MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';

@Component({
    selector: 'app-accessprofile',
    templateUrl: 'profile.component.html'
})
export class AccessProfilePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ImagePicker: ImagePicker,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public actionSheetController: ActionSheetController,
        private file: File,
        private _Mixpanel:Mixpanel,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Store-Profile");
    }
    ngOnInit() {
        this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.Profile, {page:"Store-Profile", path:"/profile"} )
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.GetProfile();
    }

    EmailValid(email: any) {
        if (email) {
            if (email.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')) {
                return true
            }
            else {
                this._Profile.EmailAddress = email;
                return false
            }
        }
    }

    


    UpdateProfile() {
        if (this._Profile.DisplayName == undefined || this._Profile.DisplayName == null || this._Profile.DisplayName == "") {
            this._HelperService.NotifySimple('Enter  display name');
        }
        else if (this._Profile.FirstName == undefined || this._Profile.FirstName == null || this._Profile.FirstName == "") {
            this._HelperService.NotifySimple('Enter  first name');
        }
        else if (this._Profile.LastName == undefined || this._Profile.LastName == null || this._Profile.LastName == "") {
            this._HelperService.NotifySimple('Enter last name');
        }
        else if (this._Profile.GenderCode == undefined || this._Profile.GenderCode == null || this._Profile.GenderCode == "") {
            this._HelperService.NotifySimple('Enter select gender');
        }
        else if (this._Profile.EmailAddress == undefined || this._Profile.EmailAddress == null || this._Profile.EmailAddress == "") {
            this._HelperService.NotifySimple('Enter  email address');
        }
        // else if (this._Profile.DateOfBirth == undefined || this._Profile.DateOfBirth == null || this._Profile.DateOfBirth == "") {
        //     this._HelperService.NotifySimple('Select date of birth');
        // }
        else {
            if (this._Profile.EmailAddress != undefined || this._Profile.EmailAddress != null || this._Profile.EmailAddress != "" ){
                let validEmail=this.EmailValid(this._Profile.EmailAddress);
                 if(!validEmail){
                     this._HelperService.NotifySimple('Enter valid email address');
                 }
                 else{
                    this.AppLogin();
                 }
                 
             }
             
            // const oneDay = 24 * 60 * 60 * 1000;
            // const firstDate: any = new Date();
            // const secondDate: any = new Date(this._Profile.DateOfBirth);
            // const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
            // if (diffDays < 5475) {
            //     this._HelperService.NotifySimple('Enter valid date of birth');
            // }
            // else {
            // this.AppLogin();
            // }
        }
    }

    AppLogin() {
        this._HelperService.IsFormProcessing = true;
        var IconContent = null;
        if (this.ImgContent.Name != "") {
            IconContent = this.ImgContent;
        }
        var _RequestData = {
            Task: "updateuseraccount",
            ReferenceKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            DisplayName: this._Profile.DisplayName,
            Name: this._Profile.FirstName + ' ' + this._Profile.LastName,
            FirstName: this._Profile.FirstName,
            LastName: this._Profile.LastName,
            MobileNumber: this._Profile.MobileNumber,
            EmailAddress: this._Profile.EmailAddress,
            GenderCode: this._Profile.GenderCode,
            // DateOfBirth: this._Profile.DateOfBirth,
            IconContent: IconContent,
        };
        this._HelperService.ShowSpinner('updating profile ...');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.AccountInfo.User.FirstName = this._Profile.FirstName;
                        this._HelperService.AccountInfo.User.LastName = this._Profile.LastName;
                        this._HelperService.AccountInfo.User.Name = this._Profile.FirstName + " " + this._Profile.LastName;
                        this._HelperService.AccountInfo.User.EmailAddress = this._Profile.EmailAddress;
                        this._HelperService.AccountInfo.User.Gender = this._Profile.GenderCode;
                        this._HelperService.AccountInfo.User.GenderCode = this._Profile.GenderCode;
                        // this._HelperService.AccountInfo.User.DateOfBirth = this._Profile.DateOfBirth;
                        this._HelperService.AccountInfo.UserAccount.DisplayName = this._Profile.DisplayName;
                        this._HelperService.AccountInfo.UserAccount.IconUrl = this._Profile.IconUrl;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, this._HelperService.AccountInfo);
                        this._HelperService.NotifySimple('Profile updated');

                    } else {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.Notify('Update failed', _Response.Message);
                    }
                },
                (_Error) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }


    public _Profile =
        {
            ReferenceId: null,
            ReferenceKey: null,
            AccountTypeCode: null,
            AccountTypeName: null,
            AccountOperationTypeCode: null,
            AccountOperationTypeName: null,
            OwnerIconUrl: null,
            DisplayName: null,
            AccessPin: null,
            AccountCode: null,
            IconUrl: null,
            PosterUrl: null,
            ReferralCode: null,
            Description: null,
            RegistrationSourceCode: null,
            RegistrationSourceName: null,
            // DateOfBirth: null,
            AppKey: null,
            AppName: null,
            AppVersionKey: null,
            AppVersionName: null,
            LastLoginDate: null,
            RequestKey: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            UserName: null,
            Password: null,
            SystemPassword: null,
            Name: null,
            FirstName: null,
            LastName: null,
            MobileNumber: null,
            ContactNumber: null,
            EmailAddress: null,
            SecondaryEmailAddress: null,
            Address: null,
            Latitude: null,
            Longitude: null,
            CountryKey: null,
            CountryName: null,
            WebsiteUrl: null,
            GenderCode: null,
            EmailVerificationStatus: null,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: null,
            NumberVerificationStatusDate: null,
            SubAccounts: null,
        }
    ToggleGender(Gender) {
        this._Profile.GenderCode = Gender;
    }
    GetProfile() {
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "getuseraccount",
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AccountInfo.UserAccount.AccountKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.HideSpinner();
                    this._Profile = _Response.Result;
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    ImageBase64Content = undefined;
    ImgContent =
        {
            Name: "",
            Extension: "",
            Content: ""
        }
    ChangePhoto() {
        var options = {
            // max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            maximumImagesCount: 1,

            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 128,
            height: 128,
            outputType: 1,
            // quality of resized image, defaults to 100
            quality: 50
        };
        this._ImagePicker.getPictures(options).then((results) => {
            for (var i = 0; i < results.length; i++) {
                this.ImageBase64Content = 'data:image/jpeg;base64,' + results[i];
                this.ImgContent.Content = results[i];
                this.ImgContent.Name = "profileimage";
                this.ImgContent.Extension = "jpeg";
            }
            // this.SetProfile();
        }, (err) => { });
    }

    public UserCustomAddress =
        {
            Latitude: null,
            Longitude: null,
            MapAddress: null,
            AddressLine2: null,
            CityName: null,
            CountryName: null,
            ZipCode: null,
            StateName: null,
        };

    placeMarker(Item) {
        this.UserCustomAddress.Latitude = Item.coords.lat;
        this.UserCustomAddress.Longitude = Item.coords.lng;
    }

    public Form_UpdateUser_AddressChange(address: Address) {
        this.UserCustomAddress.Latitude = address.geometry.location.lat();
        this.UserCustomAddress.Longitude = address.geometry.location.lng();
        this.UserCustomAddress.MapAddress = address.formatted_address;
        this.UserCustomAddress.AddressLine2 = address.formatted_address;
        address.address_components.forEach(address_component => {
            if (address_component.types[0] == "locality") {
                this.UserCustomAddress.CityName = address_component.long_name;
            }
            if (address_component.types[0] == "country") {
                this.UserCustomAddress.CountryName = address_component.long_name;
            }
            if (address_component.types[0] == "postal_code") {
                this.UserCustomAddress.ZipCode = address_component.long_name;
            }
            if (address_component.types[0] == "administrative_area_level_1") {
                this.UserCustomAddress.StateName = address_component.long_name;
            }
        });
    }

    // pickImage(sourceType) { 
    //     const options: CameraOptions = {
    //       quality: 100,
    //       sourceType: sourceType,
    //       destinationType: this.camera.DestinationType.DATA_URL,
    //       encodingType: this.camera.EncodingType.JPEG,
    //       mediaType: this.camera.MediaType.PICTURE
    //     }
    //     this.camera.getPicture(options).then((imageData) => {
    //       // imageData is either a base64 encoded string or a file URI
    //       // If it's base64 (DATA_URL):
    //       this.ImageBase64Content = 'data:image/jpeg;base64,' + imageData;
    //       // let base64Image = 'data:image/jpeg;base64,' + imageData;
    //             this.ImgContent.Content = imageData;
    //             this.ImgContent.Name = "profileimage";
    //             this.ImgContent.Extension = "jpeg";
    //             this._Profile.IconUrl = imageData;
    //     }, (err) => {
    //       // Handle error
    //       alert("error "+JSON.stringify(err))
    //     });
    //   }
    
    //   async selectImage() {
    //     const actionSheet = await this.actionSheetController.create({
    //       header: "Select Image source",
    //       buttons: [{
    //         text: 'Choose Photo',
    //         handler: () => {
    //         //   this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
    //         this.getGallery()
    //         }
    //       },
    //       {
    //         text: 'Take Photo',
    //         handler: () => {
    //         //   this.pickImage(this.camera.PictureSourceType.CAMERA);
    //         this.getCamera()
    //         }
    //       },
    //       {
    //         text: 'Cancel',
    //         role: 'cancel'
    //       }
    //       ]
    //     });
    //     await actionSheet.present();
    //   }

    //   //getcamera and getgallery methods help us to open the camera and gallery (PluginsUsed :camera:4.0.3 ,  File:4.3.3 )
    //   ImagePic;
    //   getCamera(){
    //     this.camera.getPicture({
    //         sourceType: this.camera.PictureSourceType.CAMERA,
    //       destinationType: this.camera.DestinationType.DATA_URL,

    //     }).then((res)=>{
    //         this._Profile.IconUrl = 'data:image/jpeg;base64,' + res;
    //             this.ImgContent.Content = res;
    //             this.ImgContent.Name = "profileimage";
    //             this.ImgContent.Extension = "jpeg";

    //     }).catch(e=>{
    //         alert("error is:"+ e)
    //     })
    //   }
    //   getGallery(){
    //     this.camera.getPicture({
    //         sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //       destinationType: this.camera.DestinationType.DATA_URL,

    //     }).then((res)=>{
    //         this._Profile.IconUrl = 'data:image/jpeg;base64,' + res;
    //         this.ImgContent.Content = res;
    //             this.ImgContent.Name = "profileimage";
    //             this.ImgContent.Extension = "jpeg";
    //     }).catch(e=>{
    //         alert("error is:"+ e)
    //     })
    //   }



}
 