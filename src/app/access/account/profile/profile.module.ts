import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AccessProfilePage } from './profile.component';
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA6nUouKi8QeQBh6hcgTnhKjoxNlUShh_E'
        }),
        RouterModule.forChild([
            {
                path: '',
                component: AccessProfilePage
            }
        ])
    ],
    declarations: [AccessProfilePage]
})
export class AccessProfilePageModule { }
