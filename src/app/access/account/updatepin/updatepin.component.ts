import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';

@Component({
    selector: 'app-updatepin',
    templateUrl: 'updatepin.component.html'
})
export class UpdatePinPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ImagePicker: ImagePicker,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Store-Profile");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }

    onOldOtpChange(pin) {
        this._Profile.OldPin = pin;
    }

    onNewOtpChange(pin) {
        this._Profile.NewPin = pin;
    }

    public _Profile =
        {
            OldPin: null,
            NewPin: null,
        }

    UpdateProfile() {
        if (this._Profile.OldPin == undefined || this._Profile.OldPin == null || this._Profile.OldPin == "") {
            this._HelperService.NotifyToast('Enter old pin');
        }
        else if (this._Profile.NewPin == undefined || this._Profile.NewPin == null || this._Profile.NewPin == "") {
            this._HelperService.NotifyToast('Enter new pin');
        }
        else if (this._Profile.OldPin.length != 4) {
            this._HelperService.NotifyToast('Old pin must be of 4 digits');
        }
        else if (this._Profile.NewPin.length != 4) {
            this._HelperService.NotifyToast('New pin must be of 4 digits');
        }
        else {
            this.UpdatePin();
        }
    }

    async UpdatePin() {
        const alert = await this._AlertController.create({
            header: 'Update transaction pin',
            message: 'Do you want to continue ?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Yes',
                    cssClass: 'c-en-b-primary',
                    handler: () => {
                        this._HelperService.IsFormProcessing = true;
                        var _RequestData = {
                            Task: "updatepin",
                            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                            OldPin: this._Profile.OldPin,
                            NewPin: this._Profile.NewPin,
                        };
                        this._HelperService.ShowSpinner('updating pin');
                        try {
                            let _OResponse: Observable<OResponse>;
                            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Acc, _RequestData);
                            _OResponse.subscribe(
                                (_Response) => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HideSpinner();
                                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                                        this._HelperService.NotifyToast(_Response.Message);
                                        this._HelperService.NavDashboard();

                                    } else {
                                        this._HelperService.Notify('Update failed', _Response.Message);
                                    }
                                },
                                (_Error) => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HideSpinner();
                                    this._HelperService.HandleException(_Error);
                                }
                            );
                        } catch (_Error) {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HideSpinner();
                            if (_Error.status == 0) {
                                this._HelperService.Notify('Operation failed', "Please check your internet connection");
                            }
                            else if (_Error.status == 401) {
                                var EMessage = JSON.parse(_Error._body).error;
                                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
                            }
                            else {
                                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
                            }
                        }
                    }
                }
            ]
        });
        return await alert.present();



    }

    async ResetPin() {
        const alert = await this._AlertController.create({
            header: 'Reset transaction pin',
            message: 'Do you want to continue ?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Yes',
                    cssClass: 'c-en-b-primary',
                    handler: () => {
                        this._HelperService.IsFormProcessing = true;
                        var _RequestData = {
                            Task: "resetpin",
                            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                        };
                        this._HelperService.ShowSpinner();
                        try {
                            let _OResponse: Observable<OResponse>;
                            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Acc, _RequestData);
                            _OResponse.subscribe(
                                (_Response) => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HideSpinner();
                                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                                        this._HelperService.Notify('Reset successful', _Response.Message);
                                        this._HelperService.NavDashboard();
                                    } else {
                                        this._HelperService.Notify('Reset failed', _Response.Message);
                                        this._HelperService.NavDashboard();
                                    }
                                },
                                (_Error) => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HideSpinner();
                                    this._HelperService.HandleException(_Error);
                                }
                            );
                        } catch (_Error) {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HideSpinner();
                            if (_Error.status == 0) {
                                this._HelperService.Notify('Operation failed', "Please check your internet connection");
                            }
                            else if (_Error.status == 401) {
                                var EMessage = JSON.parse(_Error._body).error;
                                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
                            }
                            else {
                                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
                            }
                        }
                    }
                }
            ]
        });
        return await alert.present();
    }
}