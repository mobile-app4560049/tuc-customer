import { Component } from '@angular/core';
import { NavController, MenuController, } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DataService } from 'src/app/service/data.service';

@Component({
    selector: 'app-updateprofile',
    templateUrl: 'updateprofile.component.html',
    styles: [`.updateProfile:disabled,
    .updateProfile[disabled]{
        background-color: #b85f9d;
        color: #ffffff;
        opacity :0.5px;
    }`]
})
export class UpdateProfilePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        private _StatusBar: StatusBar,
        public _HelperService: HelperService,
        private _DataHelperService: DataService,
    ) {
        this._HelperService.RefreshLocation();
    }
    modalScuccess:boolean=true;
    public _ProfileDetails =
    {
       
        Name: null,
        MobileNumber: null,
        EmailAddress: null,
    }
     CreatePin=null;
     ConfirmPin=null;
    public validateEmail:boolean =true;
    Missmatch:boolean=false;
    correctMatch:boolean;

    ngOnInit(){
        this._HelperService.IsFormProcessing=false;
        this.modalScuccess=true;
        this.UserDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
        this.GetProfileDetails()
    }
    GetProfileDetails(){
        this._ProfileDetails.Name=this.UserDetails.User.Name;
        this._ProfileDetails.EmailAddress=this.UserDetails.User.EmailAddress;
        this._ProfileDetails.MobileNumber=this.UserDetails.User.MobileNumber.substring(3)
    }
    
    
    public UserDetails:any;
    pinMatch(event:any){
        if(this.CreatePin != this.ConfirmPin){
            this.Missmatch = true;
            this.correctMatch=false;
        }
        else{
            this.Missmatch = false;
            this.correctMatch=true;
        }
    }
  
    UpdateProfile(){
        let validEmail=this.EmailValid(this._ProfileDetails.EmailAddress);        
       if (this._ProfileDetails.EmailAddress == undefined || this._ProfileDetails.EmailAddress == null || this._ProfileDetails.EmailAddress == "") {
        this._HelperService.HideSpinner();
        this._HelperService.NotifySimple('Enter  email address');
        }else if(!validEmail){
                this._HelperService.NotifySimple('Enter Valid Email Adress');  
        }
        else if(!this.correctMatch){
            this._HelperService.NotifySimple('Please create PIN');

        }else if(this._ProfileDetails.Name === undefined || this._ProfileDetails.Name === null || this._ProfileDetails.Name === ""){
            this._HelperService.NotifySimple('Please Enter your Name ');
        }
        else if(this._ProfileDetails.MobileNumber === undefined || this._ProfileDetails.MobileNumber === null || this._ProfileDetails.MobileNumber === ""){
            this._HelperService.NotifySimple('Please Enter your Mobile Number ');
        }
        else {
            this._HelperService.ShowSpinner();
            this._HelperService.IsFormProcessing = true;
             let countryDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig)
             var _RequestData = {
                 Task: "updatetemppin",
                 Name: this._ProfileDetails.Name,
                 MobileNumber: this._ProfileDetails.MobileNumber,
                 EmailAddress: this._ProfileDetails.EmailAddress,
                 AccessPin:this.ConfirmPin,
                 CountryIsd:countryDetails.SelectedCountry.Isd 
             };
             let _OResponse: Observable<OResponse>;
                 _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, _RequestData);
                 _OResponse.subscribe(
                     (_Response) => {
                         this._HelperService.HideSpinner();
                         if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                             this.modalScuccess=false;
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Accessforgotpin,false)
                            //  this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, this._HelperService.AccountInfo);
                            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard)
                             
                             
                         } else {
                             this._HelperService.IsFormProcessing = false;
                             this._HelperService.Notify('UpdatePin failed', _Response.Message);
                         }
                     },
                     (_Error) => {
                         this._HelperService.IsFormProcessing = false;
                         this._HelperService.HideSpinner();
                         this._HelperService.HandleException(_Error);
                     }
                 );
     
     
     
             
         }
        }
    EmailValid(email: any) {
        if (email) {
            if (email.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')) {
                return true
            }
            else {
                this.validateEmail=false
                this._ProfileDetails.EmailAddress = email;
                return false
            }
        }
    }

}