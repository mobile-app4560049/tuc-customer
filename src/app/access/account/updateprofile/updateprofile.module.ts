import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {UpdateProfilePage } from './updateprofile.component';
import { InputTrimModule } from 'ng2-trim-directive';
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { IonicSelectableModule } from 'ionic-selectable';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        InputTrimModule,
        GooglePlaceModule,
        IonicSelectableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA6nUouKi8QeQBh6hcgTnhKjoxNlUShh_E'
        }),
        RouterModule.forChild([
            {
                path: '',
                component: UpdateProfilePage
            }
        ])
    ],
    declarations: [UpdateProfilePage]
})
export class UpdateProfilePageModule { }
