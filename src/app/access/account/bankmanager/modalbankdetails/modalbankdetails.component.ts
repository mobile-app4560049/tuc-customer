import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'modalbankdetails.component.html',
    selector: 'modal-modalbankdetails'
})
export class BankDetailsModal {
    public _BankDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,
            Name: null,
            AccountNumber: null,
            BankName: null,
            BankCode: null,
            BankId: null,
            BankReferenceCode: null,
            BankReferenceKey: null,
            BvnNumber: null,
            Comment: null,
            StatusCode: null,
            StatusName: null,
        }
    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    ngOnInit() {
        this._HelperService.PageLoaded();

        this._BankDetails = this.navParams.data as any;
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }

    async DeleteBank() {
        const alert = await this._AlertController.create({
            // cssClass: 'my-custom-class',
            header: 'Delete account ?',
            message: 'Once deleted account cannot be recovered. Do you want to continue ?',

            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                    }
                }, {
                    text: 'Continue',
                    handler: (data) => {
                        this.DeleteBank_Confirm();
                    }
                }
            ]
        });

        await alert.present();
    }
    DeleteBank_Confirm() {
        this._HelperService.ShowSpinner('Please wait');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "deletebankaccount",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            ReferenceId: this._BankDetails.ReferenceId,
            ReferenceKey: this._BankDetails.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.ModalDismiss('success');
                }
                else {
                    this._HelperService.NotifyToast(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

}