import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'modaladdbank.component.html',
    selector: 'modal-modaladdbank'
})
export class AddBankModal {


    public _BankDetails =
        {
            Name: null,
            AccountNumber: null,
            BankName: null,
            BankCode: null,
        }

    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    BankCodes = [];
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.LoadBankList();
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
    LoadBankList() {
        // this._HelperService.Notify('Cancelled', "Payment cancelled.");
        // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
        this._HelperService.ShowSpinner('Please wait');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "getbankcodes",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.BankCodes = _Response.Result;
                }
                else {
                    this._HelperService.NotifyToast(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }


    BankSelected(Item) {
        this._BankDetails.BankName = Item.Name;
        this._BankDetails.BankCode = Item.Code;
    }
    SaveBank() {
        if (this._BankDetails.Name == undefined || this._BankDetails.Name == null || this._BankDetails.Name == "") {
            this._HelperService.NotifyToast("Enter name");
        }
        else if (this._BankDetails.AccountNumber == undefined || this._BankDetails.AccountNumber == null || this._BankDetails.AccountNumber == "") {
            this._HelperService.NotifyToast("Enter your bank account number");
        }
        else if (this._BankDetails.BankName == undefined || this._BankDetails.BankName == null || this._BankDetails.BankName == "") {
            this._HelperService.NotifyToast("Select bank");
        }
        else if (this._BankDetails.BankCode == undefined || this._BankDetails.BankCode == null || this._BankDetails.BankCode == "") {
            this._HelperService.NotifyToast("Select bank");
        }
        else {
            this._HelperService.ShowSpinner('Please wait');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "savebankaccount",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                Name: this._BankDetails.Name,
                AccountNumber: this._BankDetails.AccountNumber,
                BankName: this._BankDetails.BankName,
                BankCode: this._BankDetails.BankCode,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.ModalDismiss('success');
                    }
                    else {
                        this._HelperService.NotifyToast(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    this._HelperService.HandleException(_Error);
                });


        }
    }

}