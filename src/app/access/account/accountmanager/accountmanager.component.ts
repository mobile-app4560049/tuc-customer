import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';

@Component({
    selector: 'app-accountmanager',
    templateUrl: 'accountmanager.component.html'
})
export class AccountMangerPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ImagePicker: ImagePicker,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Store-Profile");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.GetProfile();
    }


    UpdateProfile() {
        if (this._Profile.DisplayName == undefined || this._Profile.DisplayName == null || this._Profile.DisplayName == "") {
            this._HelperService.NotifySimple('Enter  display name');
        }
        else if (this._Profile.FirstName == undefined || this._Profile.FirstName == null || this._Profile.FirstName == "") {
            this._HelperService.NotifySimple('Enter  first name');
        }
        else if (this._Profile.LastName == undefined || this._Profile.LastName == null || this._Profile.LastName == "") {
            this._HelperService.NotifySimple('Enter last name');
        }
        else if (this._Profile.GenderCode == undefined || this._Profile.GenderCode == null || this._Profile.GenderCode == "") {
            this._HelperService.NotifySimple('Enter select gender');
        }
        else if (this._Profile.EmailAddress == undefined || this._Profile.EmailAddress == null || this._Profile.EmailAddress == "") {
            this._HelperService.NotifySimple('Enter  email address');
        }
        else if (this._Profile.DateOfBirth == undefined || this._Profile.DateOfBirth == null || this._Profile.DateOfBirth == "") {
            this._HelperService.NotifySimple('Select date of birth');
        }
        else {
            const oneDay = 24 * 60 * 60 * 1000;
            const firstDate: any = new Date();
            const secondDate: any = new Date(this._Profile.DateOfBirth);
            const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
            if (diffDays < 5475) {
                this._HelperService.NotifySimple('Enter valid date of birth');
            }
            else {
                this.AppLogin();
            }
        }
    }

    AppLogin() {
        var IconContent = null;
        if (this.ImgContent.Name != "") {
            IconContent = this.ImgContent;
        }
        var _RequestData = {
            Task: "updateuseraccount",
            ReferenceKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            DisplayName: this._Profile.DisplayName,
            Name: this._Profile.FirstName + ' ' + this._Profile.LastName,
            FirstName: this._Profile.FirstName,
            LastName: this._Profile.LastName,
            MobileNumber: this._Profile.MobileNumber,
            EmailAddress: this._Profile.EmailAddress,
            GenderCode: this._Profile.GenderCode,
            DateOfBirth: this._Profile.DateOfBirth,
            IconContent: IconContent,
        };
        this._HelperService.ShowSpinner('updating profile ...');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.AccountInfo.User.FirstName = this._Profile.FirstName;
                        this._HelperService.AccountInfo.User.LastName = this._Profile.LastName;
                        this._HelperService.AccountInfo.User.Name = this._Profile.FirstName + " " + this._Profile.LastName;
                        this._HelperService.AccountInfo.User.EmailAddress = this._Profile.EmailAddress;
                        this._HelperService.AccountInfo.User.Gender = this._Profile.GenderCode;
                        this._HelperService.AccountInfo.User.GenderCode = this._Profile.GenderCode;
                        this._HelperService.AccountInfo.User.DateOfBirth = this._Profile.DateOfBirth;
                        this._HelperService.AccountInfo.UserAccount.DisplayName = this._Profile.DisplayName;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, this._HelperService.AccountInfo);
                        this._HelperService.NotifySimple('Profile updated');

                    } else {
                        this._HelperService.Notify('Update failed', _Response.Message);
                    }
                },
                (_Error) => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }


    public _Profile =
        {
            ReferenceId: null,
            ReferenceKey: null,
            AccountTypeCode: null,
            AccountTypeName: null,
            AccountOperationTypeCode: null,
            AccountOperationTypeName: null,
            OwnerIconUrl: null,
            DisplayName: null,
            AccessPin: null,
            AccountCode: null,
            IconUrl: null,
            PosterUrl: null,
            ReferralCode: null,
            Description: null,
            RegistrationSourceCode: null,
            RegistrationSourceName: null,
            DateOfBirth: null,
            AppKey: null,
            AppName: null,
            AppVersionKey: null,
            AppVersionName: null,
            LastLoginDate: null,
            RequestKey: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            UserName: null,
            Password: null,
            SystemPassword: null,
            Name: null,
            FirstName: null,
            LastName: null,
            MobileNumber: null,
            ContactNumber: null,
            EmailAddress: null,
            SecondaryEmailAddress: null,
            Address: null,
            Latitude: null,
            Longitude: null,
            CountryKey: null,
            CountryName: null,
            WebsiteUrl: null,
            GenderCode: null,
            EmailVerificationStatus: null,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: null,
            NumberVerificationStatusDate: null,
            SubAccounts: null,
        }
    ToggleGender(Gender) {
        this._Profile.GenderCode = Gender;
    }
    GetProfile() {
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "getuseraccount",
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AccountInfo.UserAccount.AccountKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.HideSpinner();
                    this._Profile = _Response.Result;
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    ImageBase64Content = undefined;
    ImgContent =
        {
            Name: "",
            Extension: "",
            Content: ""
        }
    ChangePhoto() {
        var options = {
            // max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            maximumImagesCount: 1,

            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 128,
            height: 128,
            outputType: 1,
            // quality of resized image, defaults to 100
            quality: 50
        };
        this._ImagePicker.getPictures(options).then((results) => {
            for (var i = 0; i < results.length; i++) {
                this.ImageBase64Content = 'data:image/jpeg;base64,' + results[i];
                this.ImgContent.Content = results[i];
                this.ImgContent.Name = "profileimage";
                this.ImgContent.Extension = "jpeg";
            }
            // this.SetProfile();
        }, (err) => { });
    }
}