import { Component, Sanitizer, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OListResponse, OBalance } from '../../../service/object.service';
import { TransactionDetailsModal } from '../../modals/transactiondetails/transactiondetails.modal.component';
import { DomSanitizer } from "@angular/platform-browser";
import { DataService } from '../../../service/data.service';
declare var google;
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
@Component({
    selector: 'app-buypoints-confirm',
    templateUrl: 'confirm.component.html'
})
export class BuyPointsConfirmPage {

    public PaymentConfiguration =
        {
            AccountId: null,
            AccountKey: null,
            Amount: 0,
            Charge: 0,
            TotalAmount: 0,
            PaymentMode: 'paystack',
            PaymentReference: null,
            ReferenceId: null,
            TransactionDate: null,
            StatusName: null,
            StatusCode: null,
            UserCards: [],
            Balance:
            {
                Credit: 0,
                Debit: 0,
                Balance: 0
            }
        };
    constructor(
        public _DataService: DataService,
        public _NavController: NavController,
        public _LaunchNavigator: LaunchNavigator,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.PaymentConfiguration = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        if (this.PaymentConfiguration.TransactionDate != undefined && this.PaymentConfiguration.TransactionDate != null) {
            this.PaymentConfiguration.TransactionDate = this._HelperService.GetDateTimeS(this.PaymentConfiguration.TransactionDate);
        }
    }

    public TimerConfig =
        {
            leftTime: 0,
        }

    async ModalDismiss() {
        this._HelperService.NavDashboard();
        // await this._ModalController.dismiss(Type);
    }


    ProcessUssd_Confirm() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointverify",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                    if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                        this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                    }
                    this._HelperService.NotifyToast(_Response.Message);
                    this.PaymentConfiguration = _Response.Result;
                    // this._HelperService.NavBuyPointsConfirm();
                    // this._HelperService.Notify(_Response.Status, _Response.Message);
                    // this._HelperService.NavPaymentDetails();
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

}