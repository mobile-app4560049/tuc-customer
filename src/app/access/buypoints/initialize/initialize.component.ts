import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../service/object.service';
import { PaymentTypeSelectModal } from '../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../service/data.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
declare var moment: any;
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
    selector: 'app-buypoints-initialize',
    templateUrl: 'initialize.component.html'
})
export class BuyPointsInitializePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _StatusBar: StatusBar,
        private faio: FingerprintAIO,
        public callNumber: CallNumber,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Payment-BuyPoints-Initialize");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.Stage = 0;
    }
    public PaymentConfiguration =
        {
            AccountId: null,
            AccountKey: null,
            Amount: null,
            Charge: 0,
            TotalAmount: 0,
            PaymentMode: 'paystack',
            PaymentReference: null,
            PayStackKey: null,
            ReferenceId: null,
            TransactionDate: null,
            StatusName: null,
            StatusCode: null,
            UserCards: [],
            Banks: [],
            Balance:
            {
                Credit: 0,
                Debit: 0,
                Balance: 0
            }
        }


    public GetPaymentConfiguration() {
        this._HelperService.ShowSpinner('Please wait..')
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "getpaymentsconfiguration",
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            ReferenceId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Type: "buypoint",
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PaymentConfiguration = _Response.Result;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
                    this.PaymentConfiguration.PaymentReference = this.PaymentConfiguration.PaymentReference;
                    this.PaymentConfiguration.AccountId = this._HelperService.AccountInfo.UserAccount.AccountId;
                    this.PaymentConfiguration.AccountKey = this._HelperService.AccountInfo.UserAccount.AccountKey;
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public QuickAmountClick(Amount) {
        this.PaymentConfiguration.Amount = Amount;
        this.CalculateReward();
    }

    CalculateReward() {
    }
    CurrencyNotation = 'NGN';
    async InitializeTransaction(Mode) {
        if (this.PaymentConfiguration.Amount == undefined || this.PaymentConfiguration.Amount == null || isNaN(this.PaymentConfiguration.Amount) == true) {
            this._HelperService.Notify('Invalid amount', "Please enter valid amount for purchase");
        }
        else if (this.PaymentConfiguration.Amount < 0) {
            this._HelperService.Notify("Invalid amount", "Amount must be greater than  0");
        }
        else {

            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "buypointinitialize",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                Amount: this.PaymentConfiguration.Amount,
                PaymentMode: Mode,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
            _OResponse.subscribe(
                _Response => {

                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        // this._DataService.GetTUCBalance();
                        var Config = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig);
                        if (Config != undefined && Config != null) {
                            var Country = Config.SelectedCountry;
                            this.CurrencyNotation = Country.CurrencyNotation;
                        }
                        this.PaymentConfiguration.Amount = _Response.Result.Amount;
                        this.PaymentConfiguration.Charge = _Response.Result.Charge;
                        this.PaymentConfiguration.TotalAmount = _Response.Result.TotalAmount;
                        this.PaymentConfiguration.PaymentMode = _Response.Result.PaymentMode;
                        this.PaymentConfiguration.PaymentReference = _Response.Result.PaymentReference;
                        this.PaymentConfiguration.Banks = _Response.Result.Banks;
                        this.PaymentConfiguration.UserCards = _Response.Result.Cards;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response);
                        // this._HelperService.Notify(_Response.Status, _Response.Message);
                        if (this.PaymentConfiguration.PaymentMode == 'paystack') {
                            this.PaymentConfiguration.PayStackKey = _Response.Result.PaystackKey;
                            if (this.PaymentConfiguration.UserCards != undefined && this.PaymentConfiguration.UserCards != null && this.PaymentConfiguration.UserCards.length > 0) {
                                this.Stage = 2;
                                //PaystackKey

                                this._HelperService.HideSpinner();
                                this._HelperService.AppConfig.IsProcessing = false;
                            }
                            else {
                                setTimeout(() => {
                                    this._HelperService.HideSpinner();
                                    this._HelperService.AppConfig.IsProcessing = false;
                                    document.getElementById('paymentbutton').click();
                                }, 200);
                            }
                        }
                        else if (this.PaymentConfiguration.PaymentMode == 'ussd') {
                            this.Stage = 1;
                            this._HelperService.HideSpinner();
                            this._HelperService.AppConfig.IsProcessing = false;
                        }
                        else {
                            this.Stage = 1;
                            this._HelperService.HideSpinner();
                            this._HelperService.AppConfig.IsProcessing = false;
                        }
                    }
                    else {
                        this._HelperService.HideSpinner();
                        this._HelperService.AppConfig.IsProcessing = false;
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    OpenPayStack() {
        this._HelperService.HideSpinner();
        this._HelperService.AppConfig.IsProcessing = false;
        document.getElementById('paymentbutton').click();
    }

    DialCodeDetails =
        {
            IsSelected: false,
            SystemName: null,
            Name: null,
            IconUrl: null,
            LocalUrl: null,
            Code: null,
            BankCode: null,
            DialCode: null,
        }

    CallNumber() {
        this.callNumber.callNumber(this.DialCodeDetails.DialCode, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
    BankItemClick(Item) {
        this.DialCodeDetails = Item;
        this.DialCodeDetails.IsSelected = true;
    }
    ProcessPaystack() {
        document.getElementById('paymentbutton').click();
    }


    ProcessOnline_Cancel() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointcancel",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    this.Stage = 0;
                    this._HelperService.NavDashboard()
                    this._HelperService.NotifyToast(_Response.Message);
                }
                else {
                    this._HelperService.NavDashboard()
                    // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    this._HelperService.NotifyToast(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }

    public Stage = 0;

    ProcessOnline_Confirm(Item) {
        if (Item.status == "success") {
            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "buypointverify",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                Amount: this.PaymentConfiguration.Amount,
                PaymentReference: this.PaymentConfiguration.PaymentReference,
                RefTransactionId: Item.transaction,
                RefStatus: Item.status,
                RefMessage: Item.message,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._DataService.GetTUCBalance();
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this.Stage = 0;
                        if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                            this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                        }
                        this._HelperService.NavBuyPointsConfirm();
                    }
                    else {
                        this.Stage = 0;
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }


    }
    ProcessOnline_Charge(PaymentSource) {
        this._HelperService.ShowSpinner('processing payement...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "paymentcharge",
            Type: "buypoint",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            ReferenceId: PaymentSource,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                    this.GetPaymentConfiguration();
                    this._DataService.GetTUCBalance();

                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    ProcessUssd_Confirm() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointverify",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // this._DataService.GetTUCBalance();
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                    this.Stage = 0;
                    if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                        this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                    }
                    this._HelperService.NavBuyPointsConfirm();
                    // this._HelperService.Notify(_Response.Status, _Response.Message);
                    // this._HelperService.NavPaymentDetails();
                }
                else {
                    this.Stage = 0;
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public BuyPointCharge(Ref) {
        this.faio.isAvailable()
            .then(x => {
                this.faio.show(
                    {
                        title: 'Security Check',
                        subtitle: 'Authenticate yourself to process payment',
                        cancelButtonTitle: 'cancel',
                        description: "ThankUCash security check",
                    }
                ).then((result: any) => {
                    if (result == "Success") {
                        this.PostAuth(Ref, 'device', this._HelperService.AccountInfo.UserAccount.AccountCode);
                    }
                    else {
                        this._HelperService.NotifyToast('Unable to validate your identity');
                        this.PinAuth(Ref);
                    }
                }).catch((error: any) => {
                    this.PinAuth(Ref);
                    this._HelperService.NotifyToast('Unable to validate your identity')
                });
            })
            .catch(x => {
                this.PinAuth(Ref);
                // this.PostAuth(Ref);
            });
    }
    private TxtType: any = "password";
    async PinAuth(Ref) {
        const alert = await this._AlertController.create({
            // cssClass: 'my-custom-class',
            header: 'Enter you pin',
            message: 'Enter your 4 digit ThankUCash pin',
            inputs: [
                {
                    name: 'accesspin',
                    type: this.TxtType,
                    // type: "password",
                    min: 4,
                    max: 4,

                    // placeholder: 'Enter your ',
                    // cssClass: 'specialClass',
                    // attributes: {
                    //     maxlength: 4,
                    //     minlength: 4,
                    //     inputmode: 'number'
                    // }
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                    }
                }, {
                    text: 'Continue',
                    handler: (data) => {
                        this.PostAuth(Ref, 'pin', data.accesspin);
                    }
                }
            ]
        });

        await alert.present();
    }


    PostAuth(Ref, AuthMode, AuthPin) {
        this._HelperService.ShowSpinner('Please wait..')
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointcharge",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
            ReferenceId: Ref.ReferenceId,
            AuthMode: AuthMode,
            AuthPin: AuthPin,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // this.PaymentConfiguration = _Response.Result;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
                    // this.PaymentConfiguration.PaymentReference = this.PaymentConfiguration.PaymentReference;
                    // this.PaymentConfiguration.AccountId = this._HelperService.AccountInfo.UserAccount.AccountId;
                    // this.PaymentConfiguration.AccountKey = this._HelperService.AccountInfo.UserAccount.AccountKey;
                    this.Vx();
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    Vx() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointverify",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._DataService.GetTUCBalance();
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                    this.Stage = 0;
                    if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                        this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                    }
                    this._HelperService.NavBuyPointsConfirm();
                    // this._HelperService.Notify(_Response.Status, _Response.Message);
                    // this._HelperService.NavPaymentDetails();
                }
                else {
                    this.Stage = 0;
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}