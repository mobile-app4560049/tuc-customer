import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReferralBonusPage } from './referralbonus.component';
import { ChartsModule } from 'ng2-charts';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChartsModule,
        CountdownModule,
        RouterModule.forChild([
            {
                path: '',
                component: ReferralBonusPage
            }
        ])
    ],
    declarations: [ReferralBonusPage]
})
export class ReferralBonusPageModule { }
