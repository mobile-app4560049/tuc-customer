import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime, Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../service/object.service';
import { TransactionDetailsModal } from '../../modals/transactiondetails/transactiondetails.modal.component';
import { DataService } from '../../../service/data.service';
import { MileStoneDetailsModal } from '../../modals/milestonedetails/milestonedetails.module.component';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ReferralEditModal } from '../../modals/referraledit/referraledit.modal.component';
import { ScratchCardModal } from '../../modals/scratchcard/scratchcard.modal.component';
declare var moment: any;

@Component({
    selector: 'app-referralbonus',
    templateUrl: 'referralbonus.component.html',
    styles: ['.modal-wrapper{ background: transparent !important; -background: transparent !important; }']
})
export class ReferralBonusPage {
    @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _Platform: Platform,
        private _SocialSharing: SocialSharing,
        public _DataService: DataService,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    BuyPoints() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BuyPoints.initialize);
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("PointsHistory");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.LoadData();
    }
    tItem = "";

    LoadData() {
        this.tItem = this._HelperService.FormatCardNumber(this._HelperService.AccountInfo.UserAccount.AccountCode);
        this.TranList_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.TranList_Setup();
    }
    async OpenClassDetails(Item) {
        const modal = await this._ModalController.create({
            component: MileStoneDetailsModal,
            componentProps: Item
        });
        return await modal.present();
        // this._HelperService.Notify(Item.Title, Item.Description);
    }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    // NavDealerLocations() {
    //     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrderManager.DealerLocations);
    // }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }
    Share(way) {
        var RefUrl = 'https://thankucash.com/download.html?uid=' + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // var RefUrl = '';
        // if (this._Platform.is("ios") || this._Platform.is("iphone") || this._Platform.is("ipad")) {
        //     RefUrl = "https://itunes.apple.com/us/app/thank-u-cash/id1386747905?mt=8&uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        // else {
        //     RefUrl = "https://play.google.com/store/apps/details?id=com.hm.thankucard?uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        this._SocialSharing.share("Find great deals up to 90% off and earn money On Every Purchase. Just Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", null, RefUrl);
        // if (way == 'wa') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // else if (way == 'fb') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // // else if (way == 'sms') {
        // //     // this._SocialSharing.shareViaSMS("Earn Money On Every Purchase. Download ThankUCash App. https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode, "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", "https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode)
        // // }
        // else {
        //     this._SocialSharing.share("Earn Money On Every Purchase. Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", null,RefUrl);
        // }
        // var RefUrl = '';
        // if (this._Platform.is("ios") || this._Platform.is("iphone") || this._Platform.is("ipad")) {
        //     RefUrl = "https://itunes.apple.com/us/app/thank-u-cash/id1386747905?mt=8&uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        // else {
        //     RefUrl = "https://play.google.com/store/apps/details?id=com.hm.thankucard?uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        // if (way == 'wa') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // else if (way == 'fb') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // // else if (way == 'sms') {
        // //     // this._SocialSharing.shareViaSMS("Earn Money On Every Purchase. Download ThankUCash App. https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode, "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", "https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode)
        // // } 
        // else {
        //     this._SocialSharing.share("Earn Money On Every Purchase. Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", RefUrl);
        // }
    }

    async OpenEditModal() {
        const modal = await this._ModalController.create({
            component: ReferralEditModal,
        });
        modal.onDidDismiss().then(data => {

        });
        return await modal.present();
    }

    ModalDismiss() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }


    public TranList_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    TranList_Setup() {
        this._HelperService.ShowProgress();

        if (this.TranList_Data.Offset == -1) {
            this.TranList_Data.Offset = 0;
        }
        var SCon = "";
        // if (this.TranList_Data.SearchContent != undefined && this.TranList_Data.SearchContent != null && this.TranList_Data.SearchContent != '') {
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     // SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     // SCon = this._HelperService.GetSearchCondition(SCon, 'CreatedByDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     // SCon = this._HelperService.GetSearchCondition(SCon, 'SubParentDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        // }
        // SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this.StartTime, this.EndTime);
        // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountInfo.UserAccount.AccountId, '==');
        var pData = {
            Task: 'gettransactions',
            TotalRecords: this.TranList_Data.TotalRecords,
            Offset: this.TranList_Data.Offset,
            Limit: this.TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            Type: 'referralbonus'
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
                    this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TranList_Data = _Response.Result.Data;
                    TranList_Data.forEach(element => {
                        element.TotalAmount = Math.round(element.TotalAmount);
                        element.leftTime = Math.round((new Date(element.ExpiaryDate).getTime() - new Date().getTime()) / 1000);
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.TranList_Data.Data.push(element);
                    });
                    if (this.TranList_LoaderEvent != undefined) {
                        this.TranList_LoaderEvent.target.complete();
                        if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
                            this.TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public TranList_LoaderEvent: any = undefined;
    TranList_NextLoad(event) {
        this.TranList_LoaderEvent = event;
        this.TranList_Setup();

    }
    private TranList_DelayTimer;
    TranList_DoSearch(text) {
        clearTimeout(this.TranList_DelayTimer);
        this.TranList_DelayTimer = setTimeout(x => {
            this.TranList_Data =
            {
                SearchContent: text,
                TotalRecords: -1,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.TranList_Setup();
        }, 1000);
    }
    async TranList_RowSelected(Item) {
        const modal = await this._ModalController.create({
            component: ScratchCardModal,
            componentProps: Item,
            id: "dfrer"
        });
        modal.onDidDismiss().then(data => {
            if (data.data != 'noaction') {
                this.LoadData();
            }
        });
        return await modal.present();
    }



}