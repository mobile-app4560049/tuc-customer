import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../service/object.service';
import { TransactionDetailsModal } from '../../modals/transactiondetails/transactiondetails.modal.component';
import { DataService } from '../../../service/data.service';
import { MileStoneDetailsModal } from '../../modals/milestonedetails/milestonedetails.module.component';
import { ScanMyCodeModal } from '../../features/tucpay/scanmycode/scanmycode.modal.component';
declare var moment: any;

@Component({
    selector: 'app-points',
    templateUrl: 'points.component.html'
})
export class PointsPage {
    @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _DataService: DataService,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    async NavOpenQR() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    NavTucPay() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode);
    }
    BuyPoints() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BuyPoints.initialize);
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("PointsHistory");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.LoadData();
    }
    tItem = "";
    public UserCustomAddress =
        {
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityName: null,
            StateName: null,
            CountryName: null,
            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: null,
            Longitude: null,
        }
    NavProfile() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
    }
    LoadData() {
        var UserCustomAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
        this.tItem = this._HelperService.FormatCardNumber(this._HelperService.AccountInfo.UserAccount.AccountCode);
        this.BalanceHistory_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 50,
            Data: []
        };
        this.TranList_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: [],
            Source: 'reward'
        };
        this.BalanceHistory_Setup();
        this.TranList_Setup();
        this.GetConfiguration();
    }

    GetTUCBalance() {
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.Feature.getuseraccountbalance,
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            Source: this._HelperService.AppConfig.TransactionSource.App,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._DataService.UpdateBalance(_Response.Result as OBalance);
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    public _AppConfigurations =
        {
            ServerAppVersion: null,
            IsAppAvailable: false,
            IsReceiverAvailable: null,
            IsDonationAvailable: false,
            TucMileStones: [],
            Donation:
            {
                ReceiverTitle: null,
                ReceiverMessage: null,
                Title: null,
                Message: null,
                Description: null,
                Conditions: null,
                IconUrl: null,
                DonationRanges: [],
            },
        }
    GetConfiguration() {
        this._AppConfigurations = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.AppConfig);
        var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        var pData;
        if (DeviceI != null) {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: DeviceI.SerialNumber,
                OsName: DeviceI.OsName,
                OsVersion: DeviceI.OsVersion,
                Brand: DeviceI.Brand,
                Model: DeviceI.Model,
                Width: DeviceI.Width,
                Height: DeviceI.Height,
                CarrierName: DeviceI.CarrierName,
                CountryCode: DeviceI.CountryCode,
                Mcc: DeviceI.Mcc,
                Mnc: DeviceI.Mnc,
                NotificationUrl: DeviceI.NotificationUrl,
                Latitude: DeviceI.Latitude,
                Longitude: DeviceI.Longitude,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }
        else {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: 'xx',
                OsName: "android",
                Brand: 'android',
                Model: "android",
                Width: 0,
                Height: 0,
                CarrierName: "airtel",
                CountryCode: "ng",
                Mcc: "000",
                Mnc: "000",
                NotificationUrl: "00",
                Latitude: 0,
                Longitude: 0,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._AppConfigurations = _Response.Result;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AppConfig, _Response.Result);
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );

    }

    public BalanceHistory_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 50,
            Data: []
        };

    BalanceHistory_Setup() {

        this._HelperService.ShowProgress();
        if (this.BalanceHistory_Data.Offset == -1) {
            this.BalanceHistory_Data.Offset = 0;
        }
        var SCon = "";
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountInfo.UserAccount.AccountId, '==');
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'SourceKey', this._HelperService.AppConfig.DataType.Text, "transaction.source.app", '!=');
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'Balance', this._HelperService.AppConfig.DataType.Number, "0", '>');
        var pData = {
            Task: 'getuserbalancehistory',
            TotalRecords: this.BalanceHistory_Data.TotalRecords,
            Offset: this.BalanceHistory_Data.Offset,
            Limit: this.BalanceHistory_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.BalanceHistory_Data.Offset = this.BalanceHistory_Data.Offset + this.BalanceHistory_Data.Limit;
                    this.BalanceHistory_Data.TotalRecords = _Response.Result.TotalRecords;
                    var BalanceHistory_Data = _Response.Result.Data;
                    BalanceHistory_Data.forEach(element => {
                        if (element.Credit > 0 || element.Debit > 0) {
                            element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
                            this.BalanceHistory_Data.Data.push(element);
                        }
                    });
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    BalanceHistory_RowSelected(ReferenceData) {
    }

    async OpenClassDetails(Item) {
        const modal = await this._ModalController.create({
            component: MileStoneDetailsModal,
            componentProps: Item
        });
        return await modal.present();
    }


    async ShowMyCode() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    OpenLocationSelector() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    NavNotification() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.notifications);
    }


    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    NavDeals() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.List);
    }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }
    public TranList_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: [],
            Source: 'reward'
        };
    SourceChange(NewSource) {
        this.TranList_Data =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: [],
            Source: NewSource
        };
        this.TranList_Setup();
    }

    TranList_Setup() {
        this._HelperService.ShowProgress();

        if (this.TranList_Data.Offset == -1) {
            this.TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.TranList_Data.Source == 'reward') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'TypeCategoryCode', this._HelperService.AppConfig.DataType.Text, 'reward', '=');
        }
        else if (this.TranList_Data.Source == 'reward') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'TypeCategoryCode', this._HelperService.AppConfig.DataType.Text, 'redeem', '=');
        }
        else if (this.TranList_Data.Source == 'pending') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'TypeCategoryCode', this._HelperService.AppConfig.DataType.Text, 'reward', '=');
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'transaction.pending', '=');
        }
        var pData = {
            Task: 'gettransactions',
            TotalRecords: this.TranList_Data.TotalRecords,
            Offset: this.TranList_Data.Offset,
            Limit: this.TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            Type: 'loyalty'
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
                    this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TranList_Data = _Response.Result.Data;
                    TranList_Data.forEach(element => {
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.TranList_Data.Data.push(element);
                    });
                    if (this.TranList_LoaderEvent != undefined) {
                        this.TranList_LoaderEvent.target.complete();
                        if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
                            this.TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public TranList_LoaderEvent: any = undefined;
    TranList_NextLoad(event) {
        this.TranList_LoaderEvent = event;
        this.TranList_Setup();

    }
    private TranList_DelayTimer;
    TranList_DoSearch(text) {
        clearTimeout(this.TranList_DelayTimer);
        this.TranList_DelayTimer = setTimeout(x => {
            this.TranList_Data =
            {
                SearchContent: text,
                TotalRecords: -1,
                Offset: -1,
                Limit: 10,
                Data: [],
                Source: 'reward'
            };
            this.TranList_Setup();
        }, 1000);
    }
    async TranList_RowSelected(Item) {
        const modal = await this._ModalController.create({
            component: TransactionDetailsModal,
            componentProps: Item
        });
        return await modal.present();
    }

}