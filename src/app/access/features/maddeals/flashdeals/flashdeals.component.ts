import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { DataService } from '../../../../service/data.service';
import { OResponse, ODeviceInformation, OAddressComponent, ICategory, IMerchants, IDeal, IDealRequest } from '../../../../service/object.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DomSanitizer } from "@angular/platform-browser";
@Component({
    selector: 'app-flashdeals',
    templateUrl: 'flashdeals.component.html'
})
export class FlashDealsPage {
    public IsFirstLoad = false;
    public _CategoriesSelected: ICategory[] = [];
    public _CategoriesSorted: ICategory[] = [];
    public _Categories: ICategory[] = [];
    public _MerchantSelected: IMerchants[] = [];
    public _MerchantsSorted: IMerchants[] = [];
    public _Merchants: IMerchants[] = [];
    public _Deals: IDeal[] = [];
    public _DealsPromotionNew: IDeal[] = [];
    public _DealsPromotionTop: IDeal[] = [];
    public _DealsPromotionDiscount: IDeal[] = [];
    public _DealsCategeory: IDeal[] = [];
    public _DealsMerchant: IDeal[] = [];
    public UserCustomAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityName: null,
            StateName: null,
            CountryName: null,
            ZipCode: null,
            Instructions: null,


            CityAreaId: null,
            CityAreaKey: null,
            CityAreaName: null,

            MapAddress: null,
            Latitude: null,
            Longitude: null,
            CityId: null,
            CityKey: null,
            CountryId: null,
            CountryKey: null,
            StateId: null,
            StateKey: null,
            Task: null,
        }
    public DeviceInformation: ODeviceInformation;
    constructor(
        private iab: InAppBrowser, public _MenuController: MenuController,
        public _Sanitizer: DomSanitizer,
        public _DataService: DataService,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ModalController: ModalController
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    SliderClick(Item) {
        if (Item != undefined && Item != null) {
            if (Item.Url != undefined && Item.Url != null && Item.Url != '') {
                const options = 'location=yes,zoom=no,hideurlbar=yes,hidenavigationbuttons=yes,closebuttoncaption=Close,closebuttoncolor=#ffffff,footercolor=#AF1482,toolbarcolor=#AF1482,disallowoverscroll=yes';
                this.iab.create(Item.Url, '_blank', options);
            }
            else if (Item.DealKey != undefined && Item.DealKey != null && Item.DealKey != '') {
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
                this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
            }
            else {
            }
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Stores");
    }
    public TUCCategories: any[] = [];
    Refresh() {
        this.IsFirstLoad = true;
        this.LoadMadDeals();
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.IsFirstLoad = true;
        this._HelperService.TrackPixelPageView();
        var UserCustomAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
        this._HelperService.RefreshLocation();

        this.LoadMadDeals();
    }
    //#region  Navigation
    NavProfile() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
    }
    OpenLocationSelector() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    // NavOrderCart() {
    //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.OrderManager.OrderCart);
    // }
    NavNotification() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.notifications);
    }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    NavDeals() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.List);
    }
    // NavDealerLocations() {
    //     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrderManager.DealerLocations);
    // }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }
    //#endregion 


    LoadMadDeals() {
        this._GetSliderImages(); // this.GetCategories();
        // this.GetMerchants();
        var ActiveCategory = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.ActiveCategory);
        if (ActiveCategory != null) {
            this._CategoriesSelected = [];
            this._CategoriesSelected.push(ActiveCategory);
        }
        this.GetDealsReset();
        this.GetDeals();
    }
    public DealsSortOrder = "Flash";
    public DealsSearchCondition = "";
    public Offset = 0;
    public Limit = 12;
    onScrollDown() {
        this.GetDeals();
    }
    onUp() {

    }
    DealClick(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
    }
    SearchDeal() {
        this.GetDealsReset();
        this.GetDeals();
    }
    public CategorySearchContent = "";
    SearchCategory() {
        if (this.CategorySearchContent != undefined && this.CategorySearchContent != null && this.CategorySearchContent != "") {
            this._CategoriesSorted = this._Categories.filter(x => x.Name.toLowerCase() == this.CategorySearchContent || x.Name.toLowerCase().startsWith(this.CategorySearchContent));
        }
        else {
            this._CategoriesSorted = this._Categories;
            this._CategoriesSelected = [];
        }
    }
    CategoryClickAll() {
        this._CategoriesSorted = this._Categories;
        this._CategoriesSelected = [];
        this.GetDealsReset();
        this.GetDeals();
    }
    CategorySelected(Item) {
        this._CategoriesSelected = [];
        this._CategoriesSelected.push(Item);
        this.GetDealsReset();
        this.GetDeals();
    }
    CategoryUnSelected(Item) {
        this._CategoriesSelected = [];
        // this._CategoriesSelected.push(Item);
        this.GetDealsReset();
        this.GetDeals();
    }
    public List_LoaderEvent: any = undefined;
    public MerchantSearchContent = "";
    SearchMerchant() {
        if (this.MerchantSearchContent != undefined && this.MerchantSearchContent != null && this.MerchantSearchContent != "") {
            this._MerchantsSorted = this._Merchants.filter(x => x.Name.toLowerCase() == this.MerchantSearchContent || x.Name.toLowerCase().startsWith(this.MerchantSearchContent));
        }
        else {
            this._MerchantsSorted = this._Merchants;
        }
    }
    MerchantSelected(Item) {
        var SCat = this._MerchantsSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SCat != undefined) {
            if (SCat.IsChecked) {
                SCat.IsChecked = false;
            }
            else {
                SCat.IsChecked = true;
            }
        }

        var Items = this._MerchantSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
        if (Items.length > 0) {
            var ItemIndex = this._MerchantSelected.indexOf(Items[0]);
            this._MerchantSelected.splice(ItemIndex, 1);
        }
        else {
            this._MerchantSelected.push(Item);
        }
        this.GetDealsReset();
        this.GetDeals();
    }
    _SortBy(Item) {
        this.DealsSortOrder = Item;
        this.GetDealsReset();
        this.GetDeals();
    }
    GetDeals_NextLoad(event) {
        this.List_LoaderEvent = event;
        this.GetDeals();
    }
    GetDealsReset() {
        this._Deals = [];
        this.Offset = 0;
    }
    GetDeals() {
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: this.Offset,
            Limit: this.Limit,
            SearchCondition: this.DealsSearchCondition,
            SortExpression: this.DealsSortOrder,
            Categories: [],
            Merchants: [],
            City: this.UserCustomAddress.CityName,
            CityId: this.UserCustomAddress.CityId,
            CityKey: this.UserCustomAddress.CityKey,
            CountryId: this.UserCustomAddress.CountryId,
            CountryKey: this.UserCustomAddress.CountryKey,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
        };
        if (this._CategoriesSelected.length > 0) {
            this._CategoriesSelected.forEach(element => {
                pData.Categories.push(element.ReferenceKey)
            });
        }
        if (this._MerchantSelected.length > 0) {
            this._MerchantSelected.forEach(element => {
                pData.Merchants.push(element.ReferenceKey)
            });
        }
        this._HelperService.ShowProgress();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                this.IsFirstLoad = false;
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Offset = this.Offset + this.Limit;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig =
                        {
                            leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000),
                        }
                        this._Deals.push(element);
                    });
                    if (this.List_LoaderEvent != undefined) {
                        this.List_LoaderEvent.target.complete();
                        if (_Response.Result.Data.length == 0) {
                            this.List_LoaderEvent.target.disabled = true;
                        }
                    }
                }
            },
            _Error => {
                this._HelperService.HideProgress();
                this._HelperService.HandleException(_Error);
            });
    }
    GetCategories() {
        var CacheCategories = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Categories);
        if (CacheCategories != null) {
            this._Categories = CacheCategories;
            this._CategoriesSorted = CacheCategories;
        }
        var pData = {
            Task: 'getcategories',
            Offset: 0,
            Limit: 1000,
            Type: this.UserCustomAddress.CityName
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Categories, _Response.Result.Data);
                    var Categories = _Response.Result.Data;
                    var RootCategories = Categories.filter(x => x.ParentCategoryId == undefined);
                    if (RootCategories.length > 0) {
                        RootCategories.forEach(RootCategory => {
                            var SubCategories = Categories.filter(x => x.ParentCategoryId == RootCategory.ReferenceId);
                            if (SubCategories.length > 0) {
                                RootCategory.SubCategories = SubCategories;
                            }
                        });
                    }
                    this._Categories = RootCategories;
                    this._CategoriesSorted = RootCategories;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Categories, this._Categories);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    GetMerchants() {
        var CacheMerchants = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Merchanats);
        if (CacheMerchants != null) {
            this._Merchants = CacheMerchants;
            this._MerchantsSorted = CacheMerchants;
        }
        var pData = {
            Task: 'getmerchants',
            Offset: 0,
            Limit: 1000,
            Type: this.UserCustomAddress.CityName
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._Merchants = _Response.Result.Data;
                    this._MerchantsSorted = _Response.Result.Data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Merchanats, this._Merchants);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    //#endregion
    _HomeSl1: any = [];
    _GetSliderImages() {
        var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.HomeSl1);
        if (Cache != null) {
            this._HomeSl1 = Cache;
        }
        var pData = {
            Task: 'getsliderimages',
            Location: 'home1'
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HomeSl1 = _Response.Result.Items;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.HomeSl1, this._HomeSl1);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}