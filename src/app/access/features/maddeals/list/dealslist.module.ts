import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DealsPage } from './dealslist.component';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CountdownModule,
        RouterModule.forChild([
            {
                path: '',
                component: DealsPage
            }
        ])
    ],
    declarations: [DealsPage]
})
export class DealsPageModule { }
