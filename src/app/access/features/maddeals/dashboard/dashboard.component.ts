import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OAddressComponent, ICategory, IMerchants, IDeal, IDealRequest } from '../../../../service/object.service';
import { DataService } from '../../../../service/data.service';
import { DomSanitizer } from "@angular/platform-browser";
import { ScanMyCodeModal } from '../../tucpay/scanmycode/scanmycode.modal.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Mixpanel } from '@awesome-cordova-plugins/mixpanel/ngx';
@Component({
    selector: 'app-dealsdashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardPage {
    logScrolling(i) {
    }
    logScrollStart() {
    }
    async NavOpenQR() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    DealSearch() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Search);
    }
    public _CategoriesSelected: ICategory[] = [];
    public _CategoriesSorted: ICategory[] = [];
    public _Categories: ICategory[] = [];
    public _MerchantSelected: IMerchants[] = [];
    public _MerchantsSorted: IMerchants[] = [];
    public _Merchants: IMerchants[] = [];
    public _Deals: IDeal[] = [];
    public _DealsPromotionNew: IDeal[] = [];
    public _DealsPromotionTop: IDeal[] = [];
    public _DealsPromotionDiscount: IDeal[] = [];
    public _DealsCategeory: IDeal[] = [];
    public _DealsMerchant: IDeal[] = [];
    public UserCustomAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,


            CityAreaId: null,
            CityAreaKey: null,
            CityAreaName: null,

            CityName: null,
            StateName: null,
            CountryName: null,
            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: null,
            Longitude: null,
            CityId: null,
            CityKey: null,
            CountryId: null,
            CountryKey: null,
            StateId: null,
            StateKey: null,
            Task: null,
        }
    public DeviceInformation: ODeviceInformation;
    constructor(
        private iab: InAppBrowser,
        public _MenuController: MenuController,
        public _Sanitizer: DomSanitizer,
        public _DataService: DataService,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ModalController: ModalController,
        private _Mixpanel:Mixpanel
       
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }

    //#region PAGE MANAGER
    PageType = 3;
    PageTypeChange(PageType) {
        this.PageType = PageType;
        // this.LoadTab();
        if (PageType == 1) {
            this.GetDealsReset("Popularity");
            this.GetDeals();
        }
        else if (PageType == 2) {
            this.GetDealsReset("Trending");
            this.GetDeals();
        }
        else if (PageType == 3) {
            this.GetDealsReset("Newest");
            this.GetDeals();
        }
        else if (PageType == 4) {
            this.GetDealsReset("Discount");
            this.GetDeals();
        }
    }
    //#endregion

    public ionViewDidEnter() {
        this._HelperService.SetPageName("Stores");
        this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.DealDashboard, {page:"Stores", path:"/dealsdashboard"} )
    }
    public TUCCategories: any[] = [];
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._HelperService.RefreshLocation();
        var UserCustomAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
        this.LoadMadDeals();
        // this.LoadData();
    }


    // //#region Deals List
    // CategoryClick(Item) {
    //     this.NearByDeals_Data =
    //     {
    //         ActiveCategoryId: 0,
    //         SearchContent: "",
    //         TotalRecords: -1,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    //     this.NearByDeals_Data.ActiveCategoryId = Item.ReferenceId;
    //     this.NearByDeals_Setup();
    // }
    // CategoryClickAll() {
    //     this.NearByDeals_Data =
    //     {
    //         ActiveCategoryId: 0,
    //         SearchContent: "",
    //         TotalRecords: -1,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    //     this.NearByDeals_Data.ActiveCategoryId = 0;
    //     this.NearByDeals_Setup();
    // }
    // LoadData() {
    //     this.NearByDeals_Data =
    //     {
    //         ActiveCategoryId: 0,
    //         SearchContent: "",
    //         TotalRecords: -1,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    //     this.NearByDeals_Setup();

    // }
    // public NearByDeals_Data =
    //     {
    //         ActiveCategoryId: 0,
    //         SearchContent: "",
    //         TotalRecords: -1,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    // NearByDeals_Setup() {
    //     this._HelperService.ShowProgress();
    //     if (this.NearByDeals_Data.Offset == -1) {
    //         this.NearByDeals_Data.Offset = 0;
    //     }
    //     var SCon = "";
    //     if (this.NearByDeals_Data.SearchContent != undefined && this.NearByDeals_Data.SearchContent != null && this.NearByDeals_Data.SearchContent != '') {
    //         SCon = this._HelperService.GetSearchCondition('', 'Title', 'text', this.NearByDeals_Data.SearchContent);
    //         SCon = this._HelperService.GetSearchCondition(SCon, 'MerchantDisplayName', 'text', this.NearByDeals_Data.SearchContent);
    //     }
    //     if (this.NearByDeals_Data.ActiveCategoryId != null && this.NearByDeals_Data.ActiveCategoryId > 0) {
    //         SCon = this._HelperService.GetSearchConditionStrict(SCon, 'CategoryId', 'number', this.NearByDeals_Data.ActiveCategoryId, "=");
    //     }

    //     var pData = {
    //         Task: 'getdeals',
    //         TotalRecords: this.NearByDeals_Data.TotalRecords,
    //         Offset: this.NearByDeals_Data.Offset,
    //         Limit: this.NearByDeals_Data.Limit,
    //         RefreshCount: true,
    //         SearchCondition: SCon,
    //         Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
    //         Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
    //         SubReferenceId: this.NearByDeals_Data.ActiveCategoryId,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.HideProgress();
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.NearByDeals_Data.Offset = this.NearByDeals_Data.Offset + this.NearByDeals_Data.Limit;
    //                 this.NearByDeals_Data.TotalRecords = _Response.Result.TotalRecords;
    //                 var NearByDeals_Data = _Response.Result.Data;
    //                 NearByDeals_Data.forEach(element => {
    //                     element.leftTime = Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000);
    //                     element.BackgroundColor = this._HelperService.CardColors[Math.floor(Math.random() * this._HelperService.CardColors.length)];
    //                     this.NearByDeals_Data.Data.push(element);
    //                 });
    //                 if (this.NearByDeals_LoaderEvent != undefined) {
    //                     this.NearByDeals_LoaderEvent.target.complete();
    //                     if (this.NearByDeals_Data.TotalRecords == this.NearByDeals_Data.Data.length) {
    //                         this.NearByDeals_LoaderEvent.target.disabled = true;
    //                     }
    //                 }
    //             }
    //             else {
    //                 this._HelperService.HideSpinner();
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // public NearByDeals_LoaderEvent: any = undefined;
    // NearByDeals_NextLoad(event) {
    //     this.NearByDeals_LoaderEvent = event;
    //     this.NearByDeals_Setup();
    // }
    // private NearByDeals_DelayTimer;
    // NearByDeals_DoSearch() {
    //     clearTimeout(this.NearByDeals_DelayTimer);
    //     this.NearByDeals_DelayTimer = setTimeout(x => {
    //         this.NearByDeals_Data.TotalRecords = -1;
    //         this.NearByDeals_Data.Offset = -1;
    //         this.NearByDeals_Data.Limit = 10;
    //         this.NearByDeals_Data.Data = [];
    //         this.NearByDeals_Setup();
    //     }, 1000);
    // }
    // //#endregion



    //#region  Navigation
    NavTucPay() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode);
    }
    NavProfile() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
    }

    async ShowMyCode() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    OpenLocationSelector() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    // NavOrderCart() {
    //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.OrderManager.OrderCart);
    // }
    NavNotification() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.notifications);
    }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    NavDeals() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.List);
    }
    // NavDealerLocations() {
    //     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrderManager.DealerLocations);
    // }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }
    //#endregion 

    //#region  NEW API
    // _DealsConfig =
    //     {
    //         CityId: '0',
    //         CityKey: '',
    //         CityName: '',
    //         Categories: [],
    //         CategoriesSorted: [],
    //     }
    //#region 
    LoadMadDeals() {
        this.GetCategories();
        this.GetMerchants();
        this.GetDealsReset();
        this.GetDeals();
        this._GetSliderImages();
        this._GetDeals_Promo_New();
        this._GetDeals_Promo_Top();
        this._GetDeals_Promo_Discount();
    }
    // GetCategories() {
    //     var _Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.categories);
    //     if (_Cache != null) {
    //         this._DealsConfig.Categories = _Cache;
    //     }
    //     var pData = {
    //         Task: 'getcategories',
    //         Offset: 0,
    //         Limit: 100,
    //         CityId : this.UserCustomAddress.CityId,
    //         CityKey : this.UserCustomAddress.CityKey,
    //         CityName : this.UserCustomAddress.CityName,
    //         Type: this.UserCustomAddress.CityName,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 var Categories = _Response.Result.Data;
    //                 var RootCategories = Categories.filter(x => x.ParentCategoryId == undefined);
    //                 if (RootCategories.length > 0) {
    //                     RootCategories.forEach(RootCategory => {
    //                         var SubCategories = Categories.filter(x => x.ParentCategoryId == RootCategory.ReferenceId);
    //                         if (SubCategories.length > 0) {
    //                             RootCategory.SubCategories = SubCategories;
    //                         }
    //                     });
    //                 }
    //                 this._DealsConfig.Categories = RootCategories;
    //                 this._DealsConfig.CategoriesSorted = RootCategories;
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.categories, this._DealsConfig.Categories);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    //#endregion
    //#endregion
    //#region  Deals List
    // public _DealsPromotionNew: IDeal[] = [];
    // public _DealsPromotionDiscount: IDeal[] = [];
    // public _DealsPromotionTop: IDeal[] = [];
    // public _DealsCategeory: IDeal[] = [];
    // public _DealsMerchant: IDeal[] = [];
    // public _Cities: ICity[] = [];

    public Deals_Data =
        {
            SortOrder: "Newest",
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 14,
            // Data: []
        };

    // public DealsSortOrder = "Newest";
    // public DealsSearchCondition = "";
    // public Offset = 0;
    // public Limit = 12;
    onScrollDown() {
        this.GetDeals();
    }
    onUp() {

    }
    DealClick(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
    }
    SliderClick(Item) {
        if (Item != undefined && Item != null) {
            if (Item.Url != undefined && Item.Url != null && Item.Url != '') {
                const options = 'location=yes,zoom=no,hideurlbar=yes,hidenavigationbuttons=yes,closebuttoncaption=Close,closebuttoncolor=#ffffff,footercolor=#AF1482,toolbarcolor=#AF1482,disallowoverscroll=yes';
                this.iab.create(Item.Url, '_blank', options);
            }
            else if (Item.DealKey != undefined && Item.DealKey != null && Item.DealKey != '') {
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
                this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
            }
            else {
            }
        }
    }
    SearchDeal() {
        this.GetDealsReset();
        this.GetDeals();
    }
    public CategorySearchContent = "";
    SearchCategory() {
        if (this.CategorySearchContent != undefined && this.CategorySearchContent != null && this.CategorySearchContent != "") {
            this._CategoriesSorted = this._Categories.filter(x => x.Name.toLowerCase() == this.CategorySearchContent || x.Name.toLowerCase().startsWith(this.CategorySearchContent));
        }
        else {
            this._CategoriesSorted = this._Categories;
            this._CategoriesSelected = [];
        }

    }
    CategoryClickAll() {
        this._CategoriesSorted = this._Categories;
        this._CategoriesSelected = [];
        this.GetDealsReset();
        this.GetDeals();
    }
    CategorySelected(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.ActiveCategory, Item);
        this.DealSearch();
    }
    public List_LoaderEvent: any = undefined;
    public MerchantSearchContent = "";
    SearchMerchant() {
        if (this.MerchantSearchContent != undefined && this.MerchantSearchContent != null && this.MerchantSearchContent != "") {
            this._MerchantsSorted = this._Merchants.filter(x => x.Name.toLowerCase() == this.MerchantSearchContent || x.Name.toLowerCase().startsWith(this.MerchantSearchContent));
        }
        else {
            this._MerchantsSorted = this._Merchants;
        }
    }
    MerchantSelected(Item) {
        var SCat = this._MerchantsSorted.find(x => x.ReferenceKey == Item.ReferenceKey);
        if (SCat != undefined) {
            if (SCat.IsChecked) {
                SCat.IsChecked = false;
            }
            else {
                SCat.IsChecked = true;
            }
        }

        var Items = this._MerchantSelected.filter(x => x.ReferenceKey == Item.ReferenceKey);
        if (Items.length > 0) {
            var ItemIndex = this._MerchantSelected.indexOf(Items[0]);
            this._MerchantSelected.splice(ItemIndex, 1);
        }
        else {
            this._MerchantSelected.push(Item);
        }
        this.GetDealsReset();
        this.GetDeals();
    }
    _SortBy(Item) {
        this.Deals_Data.SortOrder = Item;
        this.GetDealsReset();
        this.GetDeals();
    }
    GetDeals_NextLoad(event) {
        this.List_LoaderEvent = event;
        this.GetDeals();
    }
    GetDealsReset(SortOrder?) {
        this._Deals = [];
        // this.Offset = 0;
        this.Deals_Data =
        {
            SortOrder: "Newest",
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 14,
            // Data: []
        }
    }
    GetDeals() {
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: this.Deals_Data.Offset,
            Limit: this.Deals_Data.Limit,
            SearchCondition: this.Deals_Data.SearchContent,
            SortExpression: this.Deals_Data.SortOrder,
            Categories: [],
            Merchants: [],
            City: this.UserCustomAddress.CityName,
            CityId: this.UserCustomAddress.CityId,
            CityKey: this.UserCustomAddress.CityKey,
            CountryId: this.UserCustomAddress.CountryId,
            CountryKey: this.UserCustomAddress.CountryKey,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
        };
        if (this._CategoriesSelected.length > 0) {
            this._CategoriesSelected.forEach(element => {
                pData.Categories.push(element.ReferenceKey)
            });
        }
        if (this._MerchantSelected.length > 0) {
            this._MerchantSelected.forEach(element => {
                pData.Merchants.push(element.ReferenceKey)
            });
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Deals_Data.Offset = this.Deals_Data.Offset + this.Deals_Data.Limit;
                    this.Deals_Data.TotalRecords = _Response.Result.TotalRecords;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig =
                        {
                            leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000),
                        }
                        this._Deals.push(element);
                    });
                    if (this.List_LoaderEvent != undefined) {
                        this.List_LoaderEvent.target.complete();
                        if (_Response.Result.Data.length == 0) {
                            this.List_LoaderEvent.target.disabled = true;
                        }
                    }
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    GetCategories() {
        var CacheCategories = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Categories);
        if (CacheCategories != null) {
            this._Categories = CacheCategories;
            this._CategoriesSorted = CacheCategories;
        }
        var pData = {
            Task: 'getcategories',
            Offset: 0,
            Limit: 1000,
            Type: this.UserCustomAddress.CityName
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Categories, _Response.Result.Data);
                    var Categories = _Response.Result.Data;
                    var RootCategories = Categories.filter(x => x.ParentCategoryId == undefined);
                    if (RootCategories.length > 0) {
                        RootCategories.forEach(RootCategory => {
                            var SubCategories = Categories.filter(x => x.ParentCategoryId == RootCategory.ReferenceId);
                            if (SubCategories.length > 0) {
                                RootCategory.SubCategories = SubCategories;
                            }
                        });
                    }
                    this._Categories = RootCategories;
                    this._CategoriesSorted = RootCategories;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Categories, this._Categories);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    GetMerchants() {
        var CacheMerchants = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Merchanats);
        if (CacheMerchants != null) {
            this._Merchants = CacheMerchants;
            this._MerchantsSorted = CacheMerchants;
        }
        var pData = {
            Task: 'getmerchants',
            Offset: 0,
            Limit: 1000,
            Type: this.UserCustomAddress.CityName
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._Merchants = _Response.Result.Data;
                    this._MerchantsSorted = _Response.Result.Data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.Merchanats, this._Merchants);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    //#endregion




    //#region  Home Screen
    _GetDeals_Promo_New() {
        var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.DealsNew);
        if (Cache != null) {
            this._DealsPromotionNew = Cache;
        }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: 0,
            Limit: 12,
            SearchCondition: "",
            SortExpression: "Flash",
            Categories: [],
            Merchants: [],
            City: this.UserCustomAddress.CityName,
            CountryId: this.UserCustomAddress.CountryId,
            CountryKey: this.UserCustomAddress.CountryKey,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // this.Offset = this.Offset + this.Limit;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig =
                        {
                            leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000),
                        }
                    });
                    this._DealsPromotionNew = _Response.Result.Data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.DealsNew, this._DealsPromotionNew);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDeals_Promo_Top() {
        var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.DealsPromoTop);
        if (Cache != null) {
            this._DealsPromotionTop = Cache;
        }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: 0,
            Limit: 12,
            SearchCondition: "",
            SortExpression: "Popularity",
            Categories: [],
            Merchants: [],
            City: this.UserCustomAddress.CityName,
            CountryId: this.UserCustomAddress.CountryId,
            CountryKey: this.UserCustomAddress.CountryKey,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // this.Offset = this.Offset + this.Limit;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig =
                        {
                            leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000),
                        }
                    });
                    this._DealsPromotionTop = _Response.Result.Data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.DealsPromoTop, this._DealsPromotionTop);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _GetDeals_Promo_Discount() {
        var CacheDeal = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.DealsPromoDiscounts);
        if (CacheDeal != null) {
            this._DealsPromotionDiscount = CacheDeal;
        }
        var pData: IDealRequest = {
            Task: 'getdeals',
            Offset: 0,
            Limit: 12,
            SearchCondition: "",
            SortExpression: "Discount",
            Categories: [],
            Merchants: [],
            City: this.UserCustomAddress.CityName,
            CountryId: this.UserCustomAddress.CountryId,
            CountryKey: this.UserCustomAddress.CountryKey,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // this.Offset = this.Offset + this.Limit;
                    _Response.Result.Data.forEach(element => {
                        element.CountdownConfig =
                        {
                            leftTime: Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000),
                        }
                    });
                    this._DealsPromotionDiscount = _Response.Result.Data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.DealsPromoDiscounts, this._DealsPromotionDiscount);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    _HomeSl1: any = [];
    _GetSliderImages() {
        var Cache = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.HomeSl1);
        if (Cache != null) {
            this._HomeSl1 = Cache;
        }
        var pData = {
            Task: 'getsliderimages',
            Location: 'home1'
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.MadDeals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HomeSl1 = _Response.Result.Items;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.MadDeals.HomeSl1, this._HomeSl1);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    //#endregion
}