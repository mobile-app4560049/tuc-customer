import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DashboardPage } from './dashboard.component';
import { NgCalendarModule } from 'ionic2-calendar';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgCalendarModule,
        CountdownModule,
        RouterModule.forChild([
            {
                path: '',
                component: DashboardPage
            }
        ])
    ],
    declarations: [DashboardPage]
})
export class DashboardPageModule { }
