import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ProductPurchaseListPage } from './productpurchaselist.component';
import { BarRatingModule } from "ngx-bar-rating";
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        BarRatingModule,
        RouterModule.forChild([
            {
                path: '',
                component: ProductPurchaseListPage
            }
        ])
    ],
    declarations: [ProductPurchaseListPage]
})
export class ProductPurchaseListPageModule { }
