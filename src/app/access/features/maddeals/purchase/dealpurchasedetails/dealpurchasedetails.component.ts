import { Component, Sanitizer, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../../service/helper.service';
import { OResponse, ODeviceInformation, OListResponse, OBalance } from '../../../../../service/object.service';
import { TransactionDetailsModal } from '../../../../modals/transactiondetails/transactiondetails.modal.component';
import { DomSanitizer } from "@angular/platform-browser";
import { DataService } from '../../../../../service/data.service';
declare var google;
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { DealDetailsModal } from '../../../../modals/dealdetails/dealdetails.modal.component';
@Component({
    selector: 'app-purchasedetails',
    templateUrl: 'dealpurchasedetails.component.html'
})
export class PurchaseDetailsPage {
    public acttab = 'validity';
    TabChange(type) {
        this.acttab = type;
    }
    public _PurchaseDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Title: null,

            ItemCode: null,
            CategoryName: null,

            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            ImageUrl: null,

            DealReferenceId: null,
            DealReferenceKey: null,

            Description: null,
            UsageInformation: null,
            Terms: null,

            Comment: null,

            AccountIconUrl: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            AccountMobileNumber: null,

            MerchantReferenceId: null,
            MerchantReferenceKey: null,
            MerchantDisplayName: null,
            MerchantIconUrl: null,
            RedeemInstruction: null,


            StartDate: null,
            StartDateT: null,
            EndDate: null,
            EndDateT: null,


            UseDate: null,
            UseDateT: null,
            UseLocationId: null,
            UseLocationKey: null,
            UseLocationDisplayName: null,
            UseLocationAddress: null,

            SecondaryCode: null,
            SecondaryCodeMessage: null,

            SharedCustomerId: null,
            SharedCustomerKey: null,
            SharedCustomerDisplayName: null,
            SharedCustomerMobileNumber: null,
            SharedCustomerIconUrl: null,

            CreateDate: null,
            CreateDateT: null,

            StatusCode: null,
            StatusName: null,
            Locations: [],

            DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
            DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,
        };
    constructor(
        public _NavController: NavController,
        public _LaunchNavigator: LaunchNavigator,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._PurchaseDetails =
        {

            ReferenceId: 0,
            ReferenceKey: null,
            Title: null,

            ItemCode: null,
            CategoryName: null,


            AccountIconUrl: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            AccountMobileNumber: null,


            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            ImageUrl: null,

            Comment: null,

            DealReferenceId: null,
            DealReferenceKey: null,

            Description: null,
            UsageInformation: null,
            Terms: null,

            MerchantReferenceId: null,
            MerchantReferenceKey: null,
            MerchantDisplayName: null,
            MerchantIconUrl: null,
            RedeemInstruction: null,


            StartDate: null,
            StartDateT: null,
            EndDate: null,
            EndDateT: null,


            UseDate: null,
            UseDateT: null,
            UseLocationId: null,
            UseLocationKey: null,
            UseLocationDisplayName: null,
            UseLocationAddress: null,


            SecondaryCode: null,
            SecondaryCodeMessage: null,

            SharedCustomerId: null,
            SharedCustomerKey: null,
            SharedCustomerDisplayName: null,
            SharedCustomerMobileNumber: null,
            SharedCustomerIconUrl: null,

            CreateDate: null,
            CreateDateT: null,

            StatusCode: null,
            StatusName: null,
            Locations: [],
            DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
            DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,
        };
        this._PurchaseDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        this.GetTransactionDetails();
    }



    public TimerConfig =
        {
            leftTime: 0,
        }

    async ModalDismiss(Type) {
        this._NavController.pop();
        // await this._ModalController.dismiss(Type);
    }

    GetTransactionDetails() {
        this._HelperService.ShowProgress();
        var pData = {
            Task: "getdealcode",
            ReferenceId: this._PurchaseDetails.ReferenceId,
            ReferenceKey: this._PurchaseDetails.ReferenceKey,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                this._PurchaseDetails =
                {

                    ReferenceId: 0,
                    ReferenceKey: null,
                    Title: null,
                    Comment: null,

                    ItemCode: null,
                    CategoryName: null,

                    ActualPrice: null,
                    SellingPrice: null,
                    Amount: null,
                    ImageUrl: null,


                    AccountIconUrl: null,
                    AccountId: null,
                    AccountKey: null,
                    AccountDisplayName: null,
                    AccountMobileNumber: null,


                    DealReferenceId: null,
                    DealReferenceKey: null,

                    Description: null,
                    UsageInformation: null,
                    Terms: null,

                    MerchantReferenceId: null,
                    MerchantReferenceKey: null,
                    MerchantDisplayName: null,
                    MerchantIconUrl: null,

                    RedeemInstruction: null,

                    StartDate: null,
                    StartDateT: null,
                    EndDate: null,
                    EndDateT: null,


                    UseDate: null,
                    UseDateT: null,
                    UseLocationId: null,
                    UseLocationKey: null,
                    UseLocationDisplayName: null,
                    UseLocationAddress: null,

                    SecondaryCode: null,
                    SecondaryCodeMessage: null,

                    SharedCustomerId: null,
                    SharedCustomerKey: null,
                    SharedCustomerDisplayName: null,
                    SharedCustomerMobileNumber: null,
                    SharedCustomerIconUrl: null,


                    CreateDate: null,
                    CreateDateT: null,

                    StatusCode: null,
                    StatusName: null,
                    Locations: [],
                    DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
                    DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,
                };
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._PurchaseDetails = _Response.Result;
                    this._PurchaseDetails.StartDateT = this._HelperService.GetDateTimeS(this._PurchaseDetails.StartDate);
                    this._PurchaseDetails.EndDateT = this._HelperService.GetDateTimeS(this._PurchaseDetails.EndDate);
                    this._PurchaseDetails.CreateDateT = this._HelperService.GetDateTimeS(this._PurchaseDetails.CreateDate);
                    this.TimerConfig.leftTime = Math.round((new Date(this._PurchaseDetails.EndDate).getTime() - new Date().getTime()) / 1000);
                }
                else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    LocateStore(Item) {
        let options: LaunchNavigatorOptions = {
            app: this._LaunchNavigator.APP.USER_SELECT,
            destinationName: Item.DisplayName,
        };
        if (Item.Latitude != undefined && Item.Longitude != undefined) {
            var CoOrd = [Item.StoreLatitude, Item.StoreLongitude];
            this._LaunchNavigator.navigate(CoOrd, options)
                .then(
                    success => console.log('Launched navigator'),
                    error => console.log('Error launching navigator', error)
                );
        }
        else {
            this._HelperService.NotifyToast('Store location not available')
        }
    }

    UpdateComment() {
        if (this._PurchaseDetails.Comment == undefined || this._PurchaseDetails.Comment == null || this._PurchaseDetails.Comment == "") {
            this._HelperService.NotifyToastError('Enter comment');
        }
        else {
            this._HelperService.ShowProgress();
            var pData = {
                Task: "updatedealcodecomment",
                ReferenceId: this._PurchaseDetails.ReferenceId,
                ReferenceKey: this._PurchaseDetails.ReferenceKey,
                Comment: this._PurchaseDetails.Comment
            };

            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideProgress();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifyToastSuccess(_Response.Message);
                    }
                    else {
                        this._HelperService.NotifyToastError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }

    }

    public SharedMobileNumber = null;
    async ShareDeal() {
        if (this.SharedMobileNumber == undefined || this.SharedMobileNumber == null || this.SharedMobileNumber == "") {
            this._HelperService.NotifyToastError('Enter mobile number');
        }
        else {


            let alert = await this._AlertController.create({
                header: 'Confirm deal sharing',
                message: 'Once deal is shared it cannot be cancelled. Do you want to continue ?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'text-light  alert-btn',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Confirm',
                        cssClass: 'text-primary alert-btn',
                        handler: () => {
                            this._HelperService.ShowProgress();
                            this._HelperService.IsFormProcessing = true;
                            var pData = {
                                Task: "sharedealcode",
                                ReferenceId: this._PurchaseDetails.ReferenceId,
                                ReferenceKey: this._PurchaseDetails.ReferenceKey,
                                MobileNumber: this.SharedMobileNumber,
                            };

                            let _OResponse: Observable<OResponse>;
                            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
                            _OResponse.subscribe(
                                _Response => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HideProgress();
                                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                                        this._HelperService.ShowModalSuccess("Deal Shared successfully", "Deal shared to " + this.SharedMobileNumber + " successfully");
                                        this.GetTransactionDetails();
                                    }
                                    else {
                                        this._HelperService.NotifyToastError(_Response.Message);
                                    }
                                },
                                _Error => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HandleException(_Error);
                                });
                        }
                    }
                ]
            });
            alert.present();


        }

    }
}