import { Component, Sanitizer, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OListResponse, OBalance, OAddressComponent } from '../../../../service/object.service';
import { TransactionDetailsModal } from '../../../modals/transactiondetails/transactiondetails.modal.component';
import { DomSanitizer } from "@angular/platform-browser";
import { DataService } from '../../../../service/data.service';
declare var google;
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { DealDetailsModal } from '../../../modals/dealdetails/dealdetails.modal.component';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PaymentManagerModal } from '../../../modals/payment/payment.modal.component';
import { ImagesGalModal } from '../../../modals/imagesgal/imagesgal.modal.component';
import { ModalSelect } from 'src/app/auth/modalselect/modalselect.component';
import { AddressSelectorModalPage } from 'src/app/access/account/addressmanager/addressselector/addressselector.modal.component';
import { Mixpanel, MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';

@Component({
    selector: 'app-dealcheckout',
    templateUrl: 'dealcheckout.component.html'
})
export class DealCheckoutPage {
    public DealPurchaseType = 'delivery';
    restrict_getpricing_value = false;
    PurchaseTypeChange(Item) {
        this.DealPurchaseType = Item;
        this.PaymentInfo.Amount = 0;
        this.PaymentInfo.PaymentDetails.ReferenceId = 0;
        this.PaymentInfo.PaymentDetails.ReferenceKey = null;
        this.PaymentInfo.PaymentDetails.Amount = 0;
        this.PaymentInfo.PaymentDetails.Fee = 0;
        this.PaymentInfo.PaymentDetails.DeliveryFee = 0;
        this.PaymentInfo.PaymentDetails.TotalAmount = 0;
        this._Shipment =
        {
            DeliveryFee: 0,
            ShipmentId: 0,
            ShipmentKey: null,

            ParcelId: 0,
            ParcelKey: null,

            PackageId: 0,
            PackageKey: null,

            DeliveryPartners: [],
            DeliveryPartner: null,

            FromAddressId: null,
            FromAddressKey: null,
            FromAddressReference: null,

            ToAddressId: null,
            ToAddressKey: null,
            ToAddressReference: null,

            AccountId: 0,
            AccountKey: null,

        }
        this.CalculatePrice();
        if (this.DealPurchaseType == 'delivery') {
            if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
                this.GetDealPricing();
            }
            else {
                this.GetDeliveryPricing();
            }
        }
        else {
            this.CalculatePrice();
        }
    }
    public _ActiveAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Reference: null,

            Task: "updateuseraddress",
            CountryId: 1,
            CountryKey: null,
            CountryName: null,

            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityAreaId: 0,
            CityAreaKey: null,
            CityAreaName: null,

            CityId: 0,
            CityKey: null,
            CityName: null,

            StateId: 0,
            StateKey: null,
            StateName: null,

            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: -1,
            Longitude: -1,

            IsPrimary: false,
            LocationTypeId: 800
        }
    public acttab = 'loc';
    TabChange(type) {
        this.acttab = type;
    }
    @ViewChild('map', { static: true }) mapElement: ElementRef;
    map: any;
    public ShowWalletDebitOption = false;
    public ActiveTab = 1;
    public IsUseTucBalance = false;
    public DeviceInformation: ODeviceInformation;
    constructor(
        private _SocialSharing: SocialSharing,
        public _Platform: Platform,

        public _MenuController: MenuController,
        private _LaunchNavigator: LaunchNavigator,
        public _DataService: DataService,
        public _Sanitizer: DomSanitizer,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        private _Mixpanel:Mixpanel,
        private _MixpanelPeople:MixpanelPeople,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Deal-Home");
        this._ActiveAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation)
        if(this.StoreInfo.DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ProductDeal){
            this.DealPurchaseType == 'delivery';
            if(this.DealPurchaseType == 'delivery' && this._HelperService.AccountInfo.UserAccount.AccountId > 0){
                
                if(this.restrict_getpricing_value == true){
                    this.GetDealPricing();
                }
                
            }
            
        }
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._HelperService.RefreshLocation();
        this.StoreInfo = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveDealReference);
        var TAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (TAddress != null) {
            this._ActiveAddress = TAddress;
        }
        this.CalculatePrice();
        if (this.StoreInfo.DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ServiceDeal) {
            this.DealPurchaseType == 'instore'
        }
        if (this.StoreInfo.DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ProductDeal) {
            this.DealPurchaseType == 'delivery';
            if (this.DealPurchaseType == 'delivery' && this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
                this.GetDealPricing();
            }
        }
        if (this.StoreInfo.IsFlashDeal == true) {
            this.TimerConfig.leftTime = Math.round((new Date(this.StoreInfo.FlashDealEndDate).getTime() - new Date().getTime()) / 1000);
        }
        else {
            this.TimerConfig.leftTime = Math.round((new Date(this.StoreInfo.EndDate).getTime() - new Date().getTime()) / 1000);
        }

        //   this.GetDealDetails();


        // this.loadMap();
        this.Table_Reviews_Config =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        // this.Table_Reviews_GetData();
    }
    OpenLocationSelector() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    async ShowGallery() {
        if (this.StoreInfo.Images != undefined && this.StoreInfo.Images != null && this.StoreInfo.Images.length > 0) {
            var DataItem =
            {
                Title: this.StoreInfo.Title,
                Images: this.StoreInfo.Images
            }
            const modal = await this._ModalController.create({
                component: ImagesGalModal,
                componentProps: DataItem
            });
            modal.onDidDismiss().then(data => {
            });
            return await modal.present();
        }
        else {
            this._HelperService.NotifyToastError("Images not available");
        }
    }
    async LoadPaymentModal() {
        var Payment =
        {
            AccountId: this.PaymentInfo.Customer.AccountId,
            AccountKey: this.PaymentInfo.Customer.AccountKey,
            PaymentAmount: this.PaymentInfo.PaymentDetails.TotalAmount,
            PaymentTitle: 'Deal Purchase',
            PaymentSubtitle: 'proceed for deal payment',
            PaymentRequestFrom:"Deals"

        }
        const modal = await this._ModalController.create({
            component: PaymentManagerModal,
            componentProps: Payment
        });
        modal.onDidDismiss().then(data => {
            if (data.data.Status == 'success') {
                this.BuyDeal_Confirm(null, 0)
            }
            else {
                this._HelperService.IsFormProcessing = false;
            }
        });
        return await modal.present();
    }
    public TimerConfig =
        {
            leftTime: 0,
        }

    GetDealDetails() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "getdeal",
            ReferenceId: this.StoreInfo.ReferenceId,
            ReferenceKey: this.StoreInfo.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.HideSpinner();
                    this.StoreInfo = _Response.Result;
                    this.CalculatePrice();
                    if (this.StoreInfo.DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ServiceDeal) {
                        this.DealPurchaseType == 'instore'
                    }
                    if (this.StoreInfo.DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ProductDeal) {
                        this.DealPurchaseType == 'delivery';
                        if (this.DealPurchaseType == 'delivery') {
                            this.GetDealPricing();
                        }
                    }


                    if (this.StoreInfo.IsFlashDeal == true) {
                        this.TimerConfig.leftTime = Math.round((new Date(this.StoreInfo.FlashDealEndDate).getTime() - new Date().getTime()) / 1000);
                    }
                    else {
                        this.TimerConfig.leftTime = Math.round((new Date(this.StoreInfo.EndDate).getTime() - new Date().getTime()) / 1000);
                    }
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    BalanceRequest =
        {
            Amount: 0,
            PostbackUrl: ''
        }

    // GetTUCBalance() {
    //     var pData = {
    //         Task: this._HelperService.AppConfig.NetworkApi.Feature.getuseraccountbalance,
    //         UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
    //         Source: this._HelperService.AppConfig.TransactionSource.App,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this._DataService.UpdateBalance(_Response.Result as OBalance);
    //                 if (this.StoreInfo.Amount > 0) {
    //                     if ((this._DataService._OBalance.Balance > this.StoreInfo.Amount) || (this._DataService._OBalance.Balance == this.StoreInfo.Amount)) {
    //                         this.ShowWalletDebitOption = true;
    //                     }
    //                     else {
    //                         this.ShowWalletDebitOption = false;
    //                     }
    //                 }
    //                 else {
    //                     this.ShowWalletDebitOption = false;
    //                 }
    //             } else {
    //                 this._HelperService.NotifySimple(_Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         }
    //     );
    // }

    Share() {
        var RefUrl = 'https://thankucash.com/download.html?uid=' + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // if (this._Platform.is("ios") || this._Platform.is("iphone") || this._Platform.is("ipad")) {
        //     RefUrl = "https://itunes.apple.com/us/app/thank-u-cash/id1386747905?mt=8&uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        // else {
        //     RefUrl = "https://play.google.com/store/apps/details?id=com.hm.thankucard?uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        this._SocialSharing.share("I saw this great deal on ThankUCash - " + this.StoreInfo.Title + ". Also earn money On every purchase. Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", null, RefUrl);
        // if (way == 'wa') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // else if (way == 'fb') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // // else if (way == 'sms') {
        // //     // this._SocialSharing.shareViaSMS("Earn Money On Every Purchase. Download ThankUCash App. https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode, "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", "https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode)
        // // }
        // else {
        //     this._SocialSharing.share("Earn Money On Every Purchase. Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", null,RefUrl);
        // }
    }
    async BuyConfirm() {
        if (this._HelperService.CheckIsLogin() == false && (this._ActiveAddress.Name == undefined || this._ActiveAddress.Name == null || this._ActiveAddress.Name == '')) {
            this._HelperService.NotifyToastError('Please enter your name');
        }
        else if (this._HelperService.CheckIsLogin() == false && (this._ActiveAddress.ContactNumber == undefined || this._ActiveAddress.ContactNumber == null || this._ActiveAddress.ContactNumber == '')) {
            this._HelperService.NotifyToastError('Please enter your mobile number');
        }
        else if (this._HelperService.CheckIsLogin() == false && (this._ActiveAddress.EmailAddress == undefined || this._ActiveAddress.EmailAddress == null || this._ActiveAddress.EmailAddress == '')) {
            this._HelperService.NotifyToastError('Please enter email address');
        }
        else {
            this.BuyDeal_Initialize();
            // let alert = await this._AlertController.create({
            //     header: 'Confirm purchase',
            //     message: 'Confirm purchase of deal. Do you want to continue ?',
            //     buttons: [
            //         {
            //             text: 'Cancel',
            //             role: 'cancel',
            //             cssClass: 'text-light  alert-btn',
            //             handler: () => {
            //             }
            //         },
            //         {
            //             text: 'Confirm',
            //             cssClass: 'text-primary alert-btn',
            //             handler: () => {
            //                 this.BuyDeal_Initialize();
            //             }
            //         }
            //     ]
            // });
            // alert.present();
        }
    }

    public PaymentInfo =
        {
            ReferenceId: 0,
            ReferenceKey: null,

            DealId: 0,
            DealKey: null,

            Amount: 0,
            PaymentDetails:
            {
                ReferenceId: 0,
                ReferenceKey: 0,
                Amount: 0,
                Fee: 0,
                DeliveryFee: 0,
                TotalAmount: 0,
            },
            Customer:
            {
                IsGuestUser: true,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                Name: this._ActiveAddress.Name,
                MobileNumber: this._ActiveAddress.ContactNumber,
                EmailAddress: this._ActiveAddress.EmailAddress
            },
            // PaymentSource: 'online',
            // Charge: 0,
            // DeliveryCharge: 0,
            // TotalAmount: 0
        };
    BuyDeal_Initialize() {
        this._HelperService.IsFormProcessing = true;
        // if (this.IsUseTucBalance == true) {
        //     PaymentSource = 'wallet';
        // }
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "buydeal_initizalizev2",
            DealId: this.StoreInfo.ReferenceId,
            DealKey: this.StoreInfo.ReferenceKey,
            // DeliveryCharge: this.PurchaseDetails.DeliveryCharge,
            // DeliveryPartnerKey: null,
            // TrackingId: this._Shipment.TrackingId,
            // TrackingKey: this._Shipment.TrackingKey,
            // FromAddressId: 0,
            // FromAddressKey: null,
            // ToAddressId: 0,
            // ToAddressKey: null,
            // CarrierId: null,
            // DeliveryEta: 0,
            Customer:
            {
                IsGuestUser: true,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                Name: this._ActiveAddress.Name,
                MobileNumber: this._ActiveAddress.ContactNumber,
                EmailAddress: this._ActiveAddress.EmailAddress
            },
            AddressComponent: this._ActiveAddress,
            Delivery: this._Shipment,
        };
        if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
            pData.Customer.IsGuestUser = false;
        }
        else if (this._Shipment.AccountId  > 0) {
            pData.Customer.IsGuestUser = false;
            pData.Customer.AccountId = this._Shipment.AccountId;
            pData.Customer.AccountKey = this._Shipment.AccountKey;
        }
        // if (this.StoreInfo.DealTypeCode == this._HelperService.AppConfig.DealTypeCode.ProductDeal && this.DealPurchaseType == 'delivery' && this._Shipment.DeliveryPartner != undefined) {
        //     pData.DeliveryPartnerKey = this._Shipment.DeliveryPartner.ReferenceKey;
        //     pData.CarrierId = this._Shipment.DeliveryPartner.CarrierId;
        //     pData.DeliveryEta = this._Shipment.DeliveryPartner.DeliveryEta;
        //     pData.FromAddressId = this._Shipment.FromAddressId;
        //     pData.FromAddressKey = this._Shipment.FromAddressKey;

        //     pData.ToAddressId = this._Shipment.ToAddressId;
        //     pData.ToAddressKey = this._Shipment.ToAddressKey;
        // }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PaymentInfo = _Response.Result;
                    this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.Checkout, { Dealkey: _Response.Result.DealKey, DealName: this.StoreInfo.Title });
                    if (_Response.Result.ToAddressId > 0) {
                        this._Shipment.ToAddressId = _Response.Result.ToAddressId;
                        this._Shipment.ToAddressKey = _Response.Result.ToAddressKey;
                    }

                    if (_Response.Result.AddressComponent != undefined && _Response.Result.AddressComponent != null) {
                        this._ActiveAddress = _Response.Result.AddressComponent;
                    }
                    this._HelperService.HideSpinner();
                    this.LoadPaymentModal();

                    // if (((this._DataService._OBalance.Balance > this.PurchaseDetails.Amount) || (this._DataService._OBalance.Balance > this.PurchaseDetails.Amount)) && this.IsUseTucBalance) {
                    //     this.BuyDeal_Confirm(this.PurchaseDetails.ReferenceKey, this.PurchaseDetails.ReferenceId);
                    // }
                    // else {
                    //     setTimeout(() => {
                    //         document.getElementById('paymentbutton').click();
                    //     }, 300);
                    // }
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.Notify("Operation failed ", _Response.Message);
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    BuyDeal_Confirm(PaymentReference, TransactionId) {
        this._HelperService.ShowSpinner();
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "buydeal_confirmv2",
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,

            // ReferenceId: this.PaymentInfo.PaymentDetails.ReferenceId,
            // ReferenceKey: this.PaymentInfo.PaymentDetails.ReferenceKey,

            PaymentReference: PaymentReference,
            TransactionId: TransactionId,

            // DeliveryPartnerKey: null,
            // ShipmentId: this._Shipment.ShipmentId,
            // ShipmentKey: this._Shipment.ShipmentKey,
            // FromAddressId: 0,
            // FromAddressKey: null,
            // ToAddressId: 0,
            // ToAddressKey: null,
            // Amount: this.PaymentInfo.PaymentDetails.Amount,
            // Fee: this.PaymentInfo.PaymentDetails.Fee,
            // DeliveryFee: this.PaymentInfo.PaymentDetails.DeliveryFee,
            // TotalAmount: this.PaymentInfo.PaymentDetails.TotalAmount,
            // CarrierId: null,
            // DeliveryEta: 0,


            DealId: this.StoreInfo.ReferenceId,
            DealKey: this.StoreInfo.ReferenceKey,
            Customer: this.PaymentInfo.Customer,
            AddressComponent: this._ActiveAddress,
            PaymentDetails: this.PaymentInfo.PaymentDetails,
            Delivery: this._Shipment,
        };

        // if (this._Shipment.DeliveryPartner != undefined) {
        //     pData.DeliveryPartnerKey = this._Shipment.DeliveryPartner.ReferenceKey;
        //     pData.CarrierId = this._Shipment.DeliveryPartner.CarrierId;
        //     pData.DeliveryEta = this._Shipment.DeliveryPartner.DeliveryEta;
        //     pData.FromAddressId = this._Shipment.FromAddressId;
        //     pData.FromAddressKey = this._Shipment.FromAddressKey;
        //     pData.ToAddressId = this._Shipment.ToAddressId;
        //     pData.ToAddressKey = this._Shipment.ToAddressKey;
        // }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.Payment, { Dealkey: this.StoreInfo.ReferenceKey, DealName: this.StoreInfo.Title });
                    this.DealCodeDetails = _Response.Result;
                    this._HelperService.HideSpinner();
                    this.GetDealCode();
                }
                else {
                    this._HelperService.Notify("Operation failed ", _Response.Message);
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    ProcessOnline_Cancel() {
        this._HelperService.Notify('Cancelled', "Payment cancelled.");
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    ProcessOnline_Confirm(Item) {
        if (Item.status == "success") {
            this.BuyDeal_Confirm(this.PaymentInfo.PaymentDetails.ReferenceKey, Item.transaction);
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }
    }

    DealCodeDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
        }

    GetDealCode() {
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "getdealcode",
            ReferenceId: this.DealCodeDetails.ReferenceId,
            ReferenceKey: this.DealCodeDetails.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.ShowDealModal(_Response.Result);
                }
                else {
                    this._HelperService.Notify("Operation failed ", _Response.Message);
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    async ShowDealModal(Result) {
        const modal = await this._ModalController.create({
            component: DealDetailsModal,
            componentProps: Result
        });
        return await modal.present();
    }

    public StoreInfo =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            AccountIconUrl: null,
            AccountRating: null,
            Title: null,
            Description: null,
            Terms: null,
            UsageInformation: null,
            EndDate: null,
            CodeValidityDays: null,
            CodeValidityStartDate: null,
            CodeValidityEndDate: null,
            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            Charge: null,
            TotalAmount: null,
            MaximumUnitSale: null,
            MaximumUnitSalePerDay: null,
            ImageUrl: null,
            CategoryName: null,
            Views: null,
            Likes: null,
            DisLikes: null,
            TotalPurchase: null,

            Distance: 0,
            MerchantIconUrl: null,
            MerchantId: null,
            MerchantKey: null,
            MerchantName: null,
            RewardPercentage: null,
            StoreAddress: null,
            StoreId: null,
            StoreKey: null,
            StoreLatitude: null,
            StoreLongitude: null,
            StoreName: null,
            AverageRatings: 0,
            Categories: [],
            Images: [],
            Locations: [],
            TimeLeft: 0,

            IsFlashDeal: false,
            FlashDealAmount: 0,
            FlashDealStartDate: null,
            FlashDealEndDate: null,
            TotalPurchaseRemaining: -1,
            DiscountPercentage: 0,
            DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
            DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,

            AccountRatingCount: 0
        }





    // _DealPricing =
    //     {

    //     }
    AllowPayment = false;
    CalculatePrice() {
        this.PaymentInfo.PaymentDetails.Amount = this.StoreInfo.Amount.toFixed(2);
        this.PaymentInfo.PaymentDetails.Fee = this.StoreInfo.Charge.toFixed(2);
        this.PaymentInfo.PaymentDetails.TotalAmount = this.StoreInfo.TotalAmount.toFixed(2);
        this.PaymentInfo.PaymentDetails.TotalAmount = parseFloat((parseFloat(this.PaymentInfo.PaymentDetails.Amount.toString()) + parseFloat(this.PaymentInfo.PaymentDetails.Fee.toString()) + parseFloat(this.PaymentInfo.PaymentDetails.DeliveryFee.toString())).toFixed(2));
    }
    // _Pricings = [];
    // _SelectedPricing: any = {
    // }
    _Shipment =
        {
            DeliveryFee: 0,
            ShipmentId: 0,
            ShipmentKey: null,

            ParcelId: 0,
            ParcelKey: null,

            PackageId: 0,
            PackageKey: null,

            DeliveryPartners: [],
            DeliveryPartner: null,

            FromAddressId: 0,
            FromAddressKey: null,
            FromAddressReference: null,

            ToAddressId: 0,
            ToAddressKey: null,
            ToAddressReference: null,

            AccountId: 0,
            AccountKey: null,
        }

    ProviderChange(Item) {
        this._Shipment.DeliveryPartner = Item;
        this.PaymentInfo.PaymentDetails.DeliveryFee = this._Shipment.DeliveryPartner.DeliveryCharge;
        this._Shipment.DeliveryFee = this._Shipment.DeliveryPartner.DeliveryCharge;
        this.CalculatePrice();
    }

    GetDeliveryPricing() {
        // this._HelperService.ShowSpinner();
        if (this._ActiveAddress.Name == undefined || this._ActiveAddress.Name == null || this._ActiveAddress.Name == '') {
            this._HelperService.NotifyToastError('Please enter name');
        }
        else if (this._ActiveAddress.ContactNumber == undefined || this._ActiveAddress.ContactNumber == null || this._ActiveAddress.ContactNumber == '') {
            this._HelperService.NotifyToastError('Please enter mobile number');
        }
        else if (this._ActiveAddress.EmailAddress == undefined || this._ActiveAddress.EmailAddress == null || this._ActiveAddress.EmailAddress == '') {
            this._HelperService.NotifyToastError('Please enter email address');
        }
        else if (this._ActiveAddress.AddressLine1 == undefined || this._ActiveAddress.AddressLine1 == null || this._ActiveAddress.AddressLine1 == '') {
            this._HelperService.NotifyToastError('Please enter address');
        }
        else if (this._ActiveAddress.StateId == undefined || this._ActiveAddress.StateId == null || this._ActiveAddress.StateId < 1) {
            this._HelperService.NotifyToastError('Please select state');
        }
        else if (this._ActiveAddress.CityId == undefined || this._ActiveAddress.CityId == null || this._ActiveAddress.CityId < 1) {
            this._HelperService.NotifyToastError('Please select city');
        }
        else {
            this.PricingStatus.ShowEditBox = false;
            this.PricingStatus.IsPricingLoading = true;
            this.PricingStatus.PricingMessage = null;
            this._Shipment =
            {
                DeliveryFee: 0,
                ShipmentId: 0,
                ShipmentKey: null,

                ParcelId: 0,
                ParcelKey: null,

                PackageId: 0,
                PackageKey: null,

                DeliveryPartners: [],
                DeliveryPartner: null,

                FromAddressId: null,
                FromAddressKey: null,
                FromAddressReference: null,

                ToAddressId: null,
                ToAddressKey: null,
                ToAddressReference: null,

                AccountId: 0,
                AccountKey: null,

            };

            // this._HelperService.ShowProgress();
            this._HelperService.IsFormProcessing = true;
            var pData = {
                Task: "getdeliverypricing",
                DealId: this.StoreInfo.ReferenceId,
                DealKey: this.StoreInfo.ReferenceKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AddressId: this._ActiveAddress.ReferenceId,
                AddressKey: this._ActiveAddress.ReferenceKey,
                ToAddressComponent: this._ActiveAddress,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.LSOperations, pData);
            _OResponse.subscribe(
                _Response => {
                    // this._HelperService.HideProgress();
                    // this._HelperService.HideSpinner();
                    this.PricingStatus.IsPricingLoading = false;
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._Shipment = _Response.Result;
                        this._Shipment.ParcelId = _Response.Result.ParecelId;
                        this._Shipment.PackageId = _Response.Result.PackageId;

                        this._Shipment.AccountId = _Response.Result.AccountId;
                        this._Shipment.AccountKey = _Response.Result.AccountKey;

                        this._Shipment.FromAddressId = _Response.Result.FromAddressId;
                        this._Shipment.FromAddressKey = _Response.Result.FromAddressKey;
                        this._Shipment.FromAddressReference = _Response.Result.FromAddressReference;

                        this._Shipment.ToAddressId = _Response.Result.ToAddressId;
                        this._Shipment.ToAddressKey = _Response.Result.ToAddressKey;
                        this._Shipment.ToAddressReference = _Response.Result.ToAddressReference;

                        this._ActiveAddress.Reference = _Response.Result.ToAddressReference;
                        this.PricingStatus.ShowEditBox = false;

                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
                        if (_Response.Result.DeliveryPartners != undefined && _Response.Result.DeliveryPartners != null && _Response.Result.DeliveryPartners.length > 0) {
                            this.ProviderChange(_Response.Result.DeliveryPartners[0]);
                            this.CalculatePrice();
                        }
                        else {
                            this.PurchaseTypeChange('instore');
                        }
                    }
                    else {
                        this.PricingStatus.ShowEditBox = true;
                        this.PricingStatus.IsPricingLoading = false;
                        this.PricingStatus.PricingMessage = _Response.Message;
                        this._HelperService.NotifyToast(_Response.Message);
                        this._HelperService.HideSpinner();
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }


    }
    public PricingStatus =
        {
            IsPricingLoading: false,
            PricingMessage: "",
            ShowEditBox: true,
        }
    GetDealPricing() {
        this.PricingStatus.IsPricingLoading = true;
        this.PricingStatus.PricingMessage = null;
        // this._HelperService.ShowSpinner(); 
        // this._HelperService.ShowProgress();
        this._HelperService.IsFormProcessing = true;
        this._Shipment =
        {
            DeliveryFee: 0,
            ShipmentId: 0,
            ShipmentKey: null,

            ParcelId: 0,
            ParcelKey: null,

            PackageId: 0,
            PackageKey: null,

            DeliveryPartners: [],
            DeliveryPartner: null,

            FromAddressId: null,
            FromAddressKey: null,
            FromAddressReference: null,

            ToAddressId: null,
            ToAddressKey: null,
            ToAddressReference: null,

            AccountId: 0,
            AccountKey: null,

        };
        var pData = {
            Task: "getpricing",
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AddressId: this._ActiveAddress.ReferenceId,
            AddressKey: this._ActiveAddress.ReferenceKey,
            DealId: this.StoreInfo.ReferenceId,
            DealKey: this.StoreInfo.ReferenceKey,
            AddressComponent: this._ActiveAddress,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.LSOperations, pData);
        _OResponse.subscribe(
            _Response => {
                // this._HelperService.HideProgress();
                this.PricingStatus.IsPricingLoading = false;
                // this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._Shipment.ShipmentId = _Response.Result.ShipmentId;
                    this._Shipment.ShipmentKey = _Response.Result.ShipmentKey;

                    this._Shipment.FromAddressId = _Response.Result.FromAddressId;
                    this._Shipment.FromAddressKey = _Response.Result.FromAddressKey;

                    this._Shipment.ToAddressId = _Response.Result.ToAddressId;
                    this._Shipment.ToAddressKey = _Response.Result.ToAddressKey;
                    this._Shipment.DeliveryPartners = _Response.Result.DeliveryPartners;
                    if (_Response.Result.DeliveryPartners != undefined && _Response.Result.DeliveryPartners != null && _Response.Result.DeliveryPartners.length > 0) {
                        this.ProviderChange(_Response.Result.DeliveryPartners[0]);
                        this.CalculatePrice();
                    }
                    else {
                        this.PurchaseTypeChange('instore');
                    }
                }
                else {
                    this.PricingStatus.PricingMessage = _Response.Message;
                    this._HelperService.NotifyToastError(_Response.Message);
                    this._HelperService.HideSpinner();
                    this._HelperService.NotifyToastError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    NavStoreDetails() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }
    public UserRating = 0;
    public UserComment = "";
    onRatingChange(Rating) {
        this.UserRating = Rating;
    }

    PostReview() {
        if (this.UserRating == 0) {
            this._HelperService.NotifySimple('Please rate store to post review');
        }
        else {
            this._HelperService.ShowSpinner();
            var pData = {
                Task: 'saveuserparameter',
                TypeCode: "hcore.userreview",
                Name: this.UserComment,
                Value: this.UserRating,
                CountValue: this.UserRating,
                UserAccountKey: this.StoreInfo.StoreKey,
                StatusCode: 'default.active',
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifySimple('Thank U for your review');
                        this.UserRating = 0;
                        this.UserComment = null;
                        this.Table_Reviews_GetData();
                        // this.GetDetails();
                    }
                    else {
                        this._HelperService.NotifySimple("Unable to post review, try after some time");
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                });
        }

    }

    public Table_Reviews_Config =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: [],
        };
    Table_Reviews_GetData() {
        if (this.Table_Reviews_Config.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.Table_Reviews_Config.Offset = 0;
        }
        var SCon = "";
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'TypeCode', 'text', 'hcore.userreview', "=");
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountKey', 'text', this.StoreInfo.StoreKey, "=");
        var pData = {
            Task: 'getuserparameters',
            TotalRecords: this.Table_Reviews_Config.TotalRecords,
            Offset: this.Table_Reviews_Config.Offset,
            Limit: this.Table_Reviews_Config.Limit,
            SearchCondition: SCon,
            SortExpression: 'CreateDate desc',
            RefreshCount: true,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Table_Reviews_Config.Offset = this.Table_Reviews_Config.Offset + this.Table_Reviews_Config.Limit;
                    this.Table_Reviews_Config.TotalRecords = _Response.Result.TotalRecords;
                    var Table_Reviews_Config = _Response.Result.Data;

                    Table_Reviews_Config.forEach(element => {
                        element.CreateDateS = this._HelperService.GetDateTimeS(element.CreateDate);
                        this.Table_Reviews_Config.Data.push(element);
                    });
                    if (this.Table_Reviews_LoaderEvent != undefined) {
                        this.Table_Reviews_LoaderEvent.target.complete();
                        if (this.Table_Reviews_Config.TotalRecords == this.Table_Reviews_Config.Data.length) {
                            this.Table_Reviews_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public Table_Reviews_LoaderEvent: any = undefined;
    Table_Reviews_NextLoad(event) {
        this.Table_Reviews_LoaderEvent = event;
        this.Table_Reviews_GetData();
    }


    LocateStore(Item) {
        let options: LaunchNavigatorOptions = {
            app: this._LaunchNavigator.APP.USER_SELECT,
            destinationName: Item.DisplayName,
        };
        if (Item.Latitude != undefined && Item.Longitude != undefined) {
            var CoOrd = [Item.StoreLatitude, Item.StoreLongitude];
            this._LaunchNavigator.navigate(CoOrd, options)
                .then(
                    success => console.log('Launched navigator'),
                    error => console.log('Error launching navigator', error)
                );
        }
        else {
            this._HelperService.NotifyToast('Store location not available')
        }
    }

    async SelectAddressOpen() {
        const modal = await this._ModalController.create({
            component: AddressSelectorModalPage,
            componentProps: this._ActiveAddress,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                if (data.data != 'noaction') {
                    this._ActiveAddress = data.data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
                    this.GetDealPricing();
                }

            }
        });
        return await modal.present();
    }

    ChangeCustomAddress() {
        this.PricingStatus.ShowEditBox = true;
    }


    States = [];
    GetStates() {
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var pData = {
            Task: 'getstates',
            ReferenceId: this._ActiveAddress.CountryId,
            ReferenceKey: this._ActiveAddress.CountryKey,
            Offset: 0,
            Limit: 1000
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.States = _Response.Result.Data;
                    this.StateClick();
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async StateClick() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var _Request =
        {
            Title: "State",
            SubTitle: "Select your state",
            Items: []
        };
        this.States.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this._ActiveAddress.StateId = data.data.ReferenceId;
                this._ActiveAddress.StateKey = data.data.ReferenceKey;
                this._ActiveAddress.StateName = data.data.Name;
                this._ActiveAddress.CityId = null;
                this._ActiveAddress.CityKey = null;
                this._ActiveAddress.CityName = null;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
            }
        });
        return await modal.present();
    }
    Cities = [];
    GetCities() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var pData = {
            Task: 'getcities',
            ReferenceId: this._ActiveAddress.StateId,
            ReferenceKey: this._ActiveAddress.StateKey,
            Offset: 0,
            Limit: 1000
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Cities = _Response.Result.Data;
                    this.CityClick();
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async CityClick() {
        var _Request =
        {
            Title: " City",
            SubTitle: "Select your city",
            Items: []
        };
        this.Cities.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this._ActiveAddress.CityId = data.data.ReferenceId;
                this._ActiveAddress.CityKey = data.data.ReferenceKey;
                this._ActiveAddress.CityName = data.data.Name;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._ActiveAddress);
            }
        });
        return await modal.present();
    }



    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    // NavDealerLocations() {
    //     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrderManager.DealerLocations);
    // }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }


     //method for to restrict to getpricing api multiple times
    
     restrict_getpricing(){
        this._HelperService.SaveStorage('fromDealPage' , true);
            this.restrict_getpricing_value = true;
        }
}