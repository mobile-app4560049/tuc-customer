import { Component, Sanitizer, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OListResponse, OBalance, OAddressComponent } from '../../../../service/object.service';
import { TransactionDetailsModal } from '../../../modals/transactiondetails/transactiondetails.modal.component';
import { DomSanitizer } from "@angular/platform-browser";
import { DataService } from '../../../../service/data.service';
declare var google;
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { DealDetailsModal } from '../../../modals/dealdetails/dealdetails.modal.component';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { PaymentManagerModal } from '../../../modals/payment/payment.modal.component';
import { ImagesGalModal } from '../../../modals/imagesgal/imagesgal.modal.component';
import { Mixpanel, MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';

@Component({
    selector: 'app-dealdetails',
    templateUrl: 'dealdetails.component.html'
})
export class DealDetailsPage {
    public DealPurchaseType = 'instore';
    PurchaseTypeChange(Item) {
        this.DealPurchaseType = Item;
    }
    public _ActiveAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,

            Task: "updateuseraddress",
            CountryId: null,
            CountryKey: null,
            CountryName: null,

            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityAreaId: null,
            CityAreaKey: null,
            CityAreaName: null,

            CityId: null,
            CityKey: null,
            CityName: null,

            StateId: null,
            StateKey: null,
            StateName: null,

            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: -1,
            Longitude: -1,

            IsPrimary: false,
            LocationTypeId: 800
        }
    public acttab = 'about';
    TabChange(type) {
        this.acttab = type;
    }
    @ViewChild('map', { static: true }) mapElement: ElementRef;
    map: any;
    public ShowWalletDebitOption = false;
    public ActiveTab = 1;
    public IsUseTucBalance = false;
    public DeviceInformation: ODeviceInformation;
    constructor(
        private _SocialSharing: SocialSharing,
        public _Platform: Platform,

        public _MenuController: MenuController,
        private _LaunchNavigator: LaunchNavigator,
        public _DataService: DataService,
        public _Sanitizer: DomSanitizer,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        private _Mixpanel:Mixpanel,
        private _MixpanelPeople:MixpanelPeople,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Deal-Home");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._HelperService.RefreshLocation();
        this.StoreInfo = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        this.GetDealDetails();
        var TAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (TAddress != null) {
            this._ActiveAddress = TAddress;
        }

        // this.loadMap();
        this.Table_Reviews_Config =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        // this.Table_Reviews_GetData();
        this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.DealViewed, { Dealkey: this.StoreInfo.ReferenceKey, DealName: this.StoreInfo.Title, Page:'Deal-Home',Path:'/dealdetails' });
    }
    OpenLocationSelector() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    async ShowGallery() {
        if (this.StoreInfo.Images != undefined && this.StoreInfo.Images != null && this.StoreInfo.Images.length > 0) {
            var DataItem =
            {
                Title: this.StoreInfo.Title,
                Images: this.StoreInfo.Images
            }
            const modal = await this._ModalController.create({
                component: ImagesGalModal,
                componentProps: DataItem
            });
            modal.onDidDismiss().then(data => {
            });
            return await modal.present();
        }
        else {
            this._HelperService.NotifyToastError("Images not available");
        }
    }
    async LoadPaymentModal() {
        var Payment =
        {
            PaymentAmount: this.PurchaseDetails.Amount,
            PaymentTitle: 'Deal Purchase',
            PaymentSubtitle: 'proceed for deal payment'
        }
        const modal = await this._ModalController.create({
            component: PaymentManagerModal,
            componentProps: Payment
        });
        modal.onDidDismiss().then(data => {
            if (data.data.Status == 'success') {
                this.BuyDeal_Confirm(null, 0)
            }
            else {
                this._HelperService.IsFormProcessing = false;
            }
        });
        return await modal.present();
    }
    public TimerConfig =
        {
            leftTime: 0,
        }

    GetDealDetails() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "getdeal",
            ReferenceId: this.StoreInfo.ReferenceId,
            ReferenceKey: this.StoreInfo.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.HideSpinner();
                    this.StoreInfo = _Response.Result;
                    if (this.StoreInfo.IsFlashDeal == true) {
                        this.TimerConfig.leftTime = Math.round((new Date(this.StoreInfo.FlashDealEndDate).getTime() - new Date().getTime()) / 1000);
                    }
                    else {
                        this.TimerConfig.leftTime = Math.round((new Date(this.StoreInfo.EndDate).getTime() - new Date().getTime()) / 1000);
                    }
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveDealReference, _Response.Result);
                    this.SaveDealView();
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }



    BalanceRequest =
        {
            Amount: 0,
            PostbackUrl: ''
        }

    // GetTUCBalance() {
    //     var pData = {
    //         Task: this._HelperService.AppConfig.NetworkApi.Feature.getuseraccountbalance,
    //         UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
    //         Source: this._HelperService.AppConfig.TransactionSource.App,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this._DataService.UpdateBalance(_Response.Result as OBalance);
    //                 if (this.StoreInfo.Amount > 0) {
    //                     if ((this._DataService._OBalance.Balance > this.StoreInfo.Amount) || (this._DataService._OBalance.Balance == this.StoreInfo.Amount)) {
    //                         this.ShowWalletDebitOption = true;
    //                     }
    //                     else {
    //                         this.ShowWalletDebitOption = false;
    //                     }
    //                 }
    //                 else {
    //                     this.ShowWalletDebitOption = false;
    //                 }
    //             } else {
    //                 this._HelperService.NotifySimple(_Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         }
    //     );
    // }

    Share() {
        var RefUrl = 'https://thankucash.com/download.html?uid=' + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // if (this._Platform.is("ios") || this._Platform.is("iphone") || this._Platform.is("ipad")) {
        //     RefUrl = "https://itunes.apple.com/us/app/thank-u-cash/id1386747905?mt=8&uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        // else {
        //     RefUrl = "https://play.google.com/store/apps/details?id=com.hm.thankucard?uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        this._SocialSharing.share("I saw this great deal on ThankUCash - " + this.StoreInfo.Title + ". Also earn money On every purchase. Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", null, RefUrl);
        // if (way == 'wa') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // else if (way == 'fb') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // // else if (way == 'sms') {
        // //     // this._SocialSharing.shareViaSMS("Earn Money On Every Purchase. Download ThankUCash App. https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode, "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", "https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode)
        // // }
        // else {
        //     this._SocialSharing.share("Earn Money On Every Purchase. Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", null,RefUrl);
        // }
    }
    async BuyConfirm() {
        this._Mixpanel.track(this._HelperService.AppConfig.MinPanelEvents.BuyNow, { Dealkey: this.StoreInfo.ReferenceKey, DealName: this.StoreInfo.Title })
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Checkout);
        // let alert = await this._AlertController.create({
        //     header: 'Confirm purchase',
        //     message: 'Confirm purchase of deal. Do you want to continue ?',
        //     buttons: [
        //         {
        //             text: 'Cancel',
        //             role: 'cancel',
        //             cssClass: 'text-light  alert-btn',
        //             handler: () => {
        //             }
        //         },
        //         {
        //             text: 'Confirm',
        //             cssClass: 'text-primary alert-btn',
        //             handler: () => {
        //                 this.BuyDeal_Initialize();
        //             }
        //         }
        //     ]
        // });
        // alert.present();
    }

    public PurchaseDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Amount: 0,
            PaymentSource: 'online'
        };
    BuyDeal_Initialize() {
        this._HelperService.IsFormProcessing = true;
        var PaymentSource = 'online';
        // if (this.IsUseTucBalance == true) {
        //     PaymentSource = 'wallet';
        // }
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "buydeal_initizalize",
            ReferenceId: this.StoreInfo.ReferenceId,
            ReferenceKey: this.StoreInfo.ReferenceKey,
            PaymentSource: PaymentSource
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PurchaseDetails = _Response.Result;
                    this._HelperService.HideSpinner();
                    this.LoadPaymentModal();
                    // if (((this._DataService._OBalance.Balance > this.PurchaseDetails.Amount) || (this._DataService._OBalance.Balance > this.PurchaseDetails.Amount)) && this.IsUseTucBalance) {
                    //     this.BuyDeal_Confirm(this.PurchaseDetails.ReferenceKey, this.PurchaseDetails.ReferenceId);
                    // }
                    // else {
                    //     setTimeout(() => {
                    //         document.getElementById('paymentbutton').click();
                    //     }, 300);
                    // }
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.Notify("Operation failed ", _Response.Message);
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    BuyDeal_Confirm(PaymentReference, TransactionId) {
        this._HelperService.ShowSpinner();
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "buydeal_confirm",
            ReferenceId: this.PurchaseDetails.ReferenceId,
            ReferenceKey: this.PurchaseDetails.ReferenceKey,
            DealId: this.StoreInfo.ReferenceId,
            DealKey: this.StoreInfo.ReferenceKey,
            PaymentReference: PaymentReference,
            TransactionId: TransactionId,
            PaymentSource: 'wallet',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.DealCodeDetails = _Response.Result;
                    this._HelperService.HideSpinner();
                    this.GetDealCode();
                }
                else {
                    this._HelperService.Notify("Operation failed ", _Response.Message);
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    ProcessOnline_Cancel() {
        this._HelperService.Notify('Cancelled', "Payment cancelled.");
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    ProcessOnline_Confirm(Item) {
        if (Item.status == "success") {
            this.BuyDeal_Confirm(this.PurchaseDetails.ReferenceKey, Item.transaction);
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }
    }

    DealCodeDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
        }

    GetDealCode() {
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "getdealcode",
            ReferenceId: this.DealCodeDetails.ReferenceId,
            ReferenceKey: this.DealCodeDetails.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.ShowDealModal(_Response.Result);
                }
                else {
                    this._HelperService.Notify("Operation failed ", _Response.Message);
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    async ShowDealModal(Result) {
        const modal = await this._ModalController.create({
            component: DealDetailsModal,
            componentProps: Result
        });
        return await modal.present();
    }

    public StoreInfo =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            AccountIconUrl: null,
            AccountRating: null,
            Title: null,
            Description: null,
            Terms: null,
            UsageInformation: null,
            EndDate: null,
            CodeValidityDays: null,
            CodeValidityStartDate: null,
            CodeValidityEndDate: null,
            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            Charge: null,
            TotalAmount: null,
            MaximumUnitSale: null,
            MaximumUnitSalePerDay: null,
            ImageUrl: null,
            CategoryName: null,
            Views: null,
            Likes: null,
            DisLikes: null,
            TotalPurchase: null,

            Distance: 0,
            MerchantIconUrl: null,
            MerchantId: null,
            MerchantKey: null,
            MerchantName: null,
            RewardPercentage: null,
            StoreAddress: null,
            StoreId: null,
            StoreKey: null,
            StoreLatitude: null,
            StoreLongitude: null,
            StoreName: null,
            AverageRatings: 0,
            Categories: [],
            Images: [],
            Locations: [],
            TimeLeft: 0,

            IsFlashDeal: false,
            FlashDealAmount: 0,
            FlashDealStartDate: null,
            FlashDealEndDate: null,
            TotalPurchaseRemaining: -1,
            DiscountPercentage: 0,
            // DealTypeId: this._HelperService.AppConfig.DealType.ProductDeal,
            DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
            DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,
            AccountRatingCount: 0
        }

    SaveDealView() {
        var pData = {
            Task: "savedealview",
            ReferenceId: this.StoreInfo.ReferenceId,
            ReferenceKey: this.StoreInfo.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    NavStoreDetails() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }
    public UserRating = 0;
    public UserComment = "";
    onRatingChange(Rating) {
        this.UserRating = Rating;
    }

    PostReview() {
        if (this.UserRating == 0) {
            this._HelperService.NotifySimple('Please rate store to post review');
        }
        else {
            this._HelperService.ShowSpinner();
            var pData = {
                Task: 'saveuserparameter',
                TypeCode: "hcore.userreview",
                Name: this.UserComment,
                Value: this.UserRating,
                CountValue: this.UserRating,
                UserAccountKey: this.StoreInfo.StoreKey,
                StatusCode: 'default.active',
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifySimple('Thank U for your review');
                        this.UserRating = 0;
                        this.UserComment = null;
                        this.Table_Reviews_GetData();
                        // this.GetDetails();
                    }
                    else {
                        this._HelperService.NotifySimple("Unable to post review, try after some time");
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                });
        }

    }

    public Table_Reviews_Config =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: [],
        };
    Table_Reviews_GetData() {
        if (this.Table_Reviews_Config.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.Table_Reviews_Config.Offset = 0;
        }
        var SCon = "";
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'TypeCode', 'text', 'hcore.userreview', "=");
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountKey', 'text', this.StoreInfo.StoreKey, "=");
        var pData = {
            Task: 'getuserparameters',
            TotalRecords: this.Table_Reviews_Config.TotalRecords,
            Offset: this.Table_Reviews_Config.Offset,
            Limit: this.Table_Reviews_Config.Limit,
            SearchCondition: SCon,
            SortExpression: 'CreateDate desc',
            RefreshCount: true,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Table_Reviews_Config.Offset = this.Table_Reviews_Config.Offset + this.Table_Reviews_Config.Limit;
                    this.Table_Reviews_Config.TotalRecords = _Response.Result.TotalRecords;
                    var Table_Reviews_Config = _Response.Result.Data;

                    Table_Reviews_Config.forEach(element => {
                        element.CreateDateS = this._HelperService.GetDateTimeS(element.CreateDate);
                        this.Table_Reviews_Config.Data.push(element);
                    });
                    if (this.Table_Reviews_LoaderEvent != undefined) {
                        this.Table_Reviews_LoaderEvent.target.complete();
                        if (this.Table_Reviews_Config.TotalRecords == this.Table_Reviews_Config.Data.length) {
                            this.Table_Reviews_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public Table_Reviews_LoaderEvent: any = undefined;
    Table_Reviews_NextLoad(event) {
        this.Table_Reviews_LoaderEvent = event;
        this.Table_Reviews_GetData();
    }


    LocateStore(Item) {
        let options: LaunchNavigatorOptions = {
            app: this._LaunchNavigator.APP.USER_SELECT,
            destinationName: Item.DisplayName,
        };
        if (Item.Latitude != undefined && Item.Longitude != undefined) {
            var CoOrd = [Item.Latitude, Item.Longitude];
            this._LaunchNavigator.navigate(CoOrd, options)
                .then(
                    success => console.log('Launched navigator'),
                    error => console.log('Error launching navigator', error)
                );
        }
        else {
            this._HelperService.NotifyToast('Store location not available')
        }
    }


    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    // NavDealerLocations() {
    //     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrderManager.DealerLocations);
    // }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }
}