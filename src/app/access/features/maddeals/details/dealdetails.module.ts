import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DealDetailsPage } from './dealdetails.component';
import { Angular4PaystackModule } from 'angular4-paystack';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CountdownModule,
        Angular4PaystackModule.forRoot('pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12'),
        RouterModule.forChild([
            {
                path: '',
                component: DealDetailsPage
            }
        ])
    ],
    declarations: [DealDetailsPage]
})
export class DealDetailsPageModule { }
