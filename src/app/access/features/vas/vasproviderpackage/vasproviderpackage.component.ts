import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../../service/object.service';
import { PaymentTypeSelectModal } from '../../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../../service/data.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
declare var moment: any;

@Component({
    selector: 'app-vasproviderpackage',
    templateUrl: 'vasproviderpackage.component.html'
})
export class VasProviderPackagePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public callNumber: CallNumber,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Payment-BuyPoints-Initialize");
    }
    _PageConfig =
        {
            Type: "",
            Title: "",
            SubTitle: "",
            IconClass: "",
            Provider: null,
            SelectedProvider: null,
            SelectedProviderPackage: null,
            AccountNumber: null,
            Amount: null
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._PageConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
    }

    PackageSelected(Item) {
        this._PageConfig.SelectedProviderPackage = Item;
        if (Item.Amount != undefined && Item.Amount != null) {
            this._PageConfig.Amount = Item.Amount;
        }
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._PageConfig);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasprovideraccount);
    }
}