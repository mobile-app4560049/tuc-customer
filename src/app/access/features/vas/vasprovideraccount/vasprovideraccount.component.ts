import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../../service/object.service';
import { PaymentTypeSelectModal } from '../../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../../service/data.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { PaymentManagerModal } from '../../../modals/payment/payment.modal.component';
declare var moment: any;

@Component({
    selector: 'app-vasprovideraccount',
    templateUrl: 'vasprovideraccount.component.html'
})
export class VasProviderAccountPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public callNumber: CallNumber,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Payment-BuyPoints-Initialize");
    }
    _PageConfig =
        {
            Type: "",
            Title: "",
            SubTitle: "",
            IconClass: "",
            Provider: null,
            SelectedProvider: null,
            SelectedProviderPackage: null,
            AccountNumber: null,
            Amount: null,
            InitializeResponse: null,
            ConfirmResponse: null,
            UserRewardAmount: 0,
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._PageConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        this.Stage = 0;
        // this.GetPaymentConfiguration();
    }

    InitializePayment() {
        if (this._PageConfig.AccountNumber == undefined || this._PageConfig.AccountNumber == null || this._PageConfig.AccountNumber == '') {
            this._HelperService.NotifyToast('Enter your account / mobile number');
        }
        else if (this._PageConfig.Amount == undefined || this._PageConfig.Amount == null || this._PageConfig.Amount < 1) {
            this._HelperService.NotifyToast('Enter payment amount')
        }
        else if(this._PageConfig.Amount < 100){
            this._HelperService.NotifyToast('Sorry, the smallest amount of airtime you can buy is 100.')
        }
        else {
            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "vaspay_initialize",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                ReferenceId: 0,
                ProductId: this._PageConfig.SelectedProvider.ReferenceId,
                AccountNumber: this._PageConfig.AccountNumber,
                Amount: this._PageConfig.Amount,
            };
            if (this._PageConfig.SelectedProviderPackage != undefined && this._PageConfig.SelectedProviderPackage != null) {
                pData.ReferenceId = this._PageConfig.SelectedProviderPackage.ReferenceId;
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Vas, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this.Stage = 1;
                        this._PageConfig.InitializeResponse = _Response.Result;
                        // this._HelperService.NotifyToast(_Response.Message);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveDealReference);
                    }
                    else {
                        this._HelperService.NotifyToast(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }




    async OpenPaymentModal() {
        var Payment =
        {
            PaymentAmount: this._PageConfig.Amount,
            PaymentTitle: 'Make payment',
            PaymentSubtitle: 'process payment to proceed'
        }
        const modal = await this._ModalController.create({
            component: PaymentManagerModal,
            componentProps: Payment
        });
        modal.onDidDismiss().then(data => {
            if (data.data.Status == 'success') {
                this.ConfirmPayment();
            }
        });
        return await modal.present();
    }


    ConfirmPayment() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "vaspay_confirm",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            ReferenceId: this._PageConfig.InitializeResponse.ReferenceId,
            ReferenceKey: this._PageConfig.InitializeResponse.ReferenceKey,
            PaymentReference: this._PageConfig.InitializeResponse.PaymentReference,
            ProductId: this._PageConfig.SelectedProvider.ReferenceId,
            AccountNumber: this._PageConfig.AccountNumber,
            Amount: this._PageConfig.Amount,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Vas, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Stage = 1;
                    this._PageConfig.ConfirmResponse = _Response.Result;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._PageConfig);

                    if (this._PageConfig.ConfirmResponse != undefined && this._PageConfig.ConfirmResponse != null && this._PageConfig.ConfirmResponse.UserRewardAmount > 0) {
                        this._HelperService.ShowModalSuccess("Topup successful", "You have earned N" + this._PageConfig.ConfirmResponse.UserRewardAmount + " ThankUCash rewards");
                    }
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Vas.vaspaymentdetails);
                    // this._HelperService.NotifyToast(_Response.Message);
                }
                else {
                    this._HelperService.NotifyToast(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }




    CancelPayment() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "vaspay_cancel",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            ReferenceId: this._PageConfig.InitializeResponse.ReferenceId,
            ReferenceKey: this._PageConfig.InitializeResponse.ReferenceKey,
            PaymentReference: this._PageConfig.InitializeResponse.PaymentReference,
            AccountNumber: this._PageConfig.AccountNumber,
            Amount: this._PageConfig.Amount,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Vas, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.NavDashboard();
                }
                else {
                    this._HelperService.NavDashboard();
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    ProviderClick(Item) {
        this._PageConfig.SelectedProvider = Item;
        // this.Stage = 1;
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviderpackage);
        //vasproviderpackage
    }

    public PaymentConfiguration =
        {
            AccountId: null,
            AccountKey: null,
            Amount: 0,
            Charge: 0,
            TotalAmount: 0,
            PaymentMode: 'paystack',
            PaymentReference: null,
            ReferenceId: null,
            TransactionDate: null,
            StatusName: null,
            StatusCode: null,
            UserCards: [],
            Banks: [],
            Balance:
            {
                Credit: 0,
                Debit: 0,
                Balance: 0
            }
        }

    public GetPaymentConfiguration() {
        this._HelperService.ShowSpinner('Please wait..')
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "getpaymentsconfiguration",
            UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            ReferenceId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Type: "buypoint",
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PaymentConfiguration = _Response.Result;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
                    this.PaymentConfiguration.PaymentReference = this.PaymentConfiguration.PaymentReference;
                    this.PaymentConfiguration.AccountId = this._HelperService.AccountInfo.UserAccount.AccountId;
                    this.PaymentConfiguration.AccountKey = this._HelperService.AccountInfo.UserAccount.AccountKey;
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public QuickAmountClick(Amount) {
        this.PaymentConfiguration.Amount = Amount;
        this.CalculateReward();
    }

    CalculateReward() {
        // this.PaymentConfiguration.RewardAmount = 0;
        // this.PaymentConfiguration.TotalAmount = 0;
        // if (this.PaymentConfiguration.Amount == undefined || this.PaymentConfiguration.Amount == null || isNaN(this.PaymentConfiguration.Amount) == true) {
        // }
        // // else if (this.PaymentConfiguration.Amount < this.PaymentConfiguration.MinAmount) {
        // // }
        // // else if (this.PaymentConfiguration.Amount > this.PaymentConfiguration.MaxAmount) {
        // // }
        // else {
        //     if (this.PaymentConfiguration.RewardType == "percentage") {
        //         this.PaymentConfiguration.RewardAmount = (parseFloat(this.PaymentConfiguration.Amount) / parseFloat(this.PaymentConfiguration.RewardValue)) / 100;
        //     }
        //     else if (this.PaymentConfiguration.RewardType == "fixedamount") {
        //         this.PaymentConfiguration.RewardAmount = this.PaymentConfiguration.RewardValue;
        //     }
        //     this.PaymentConfiguration.TotalAmount = parseFloat(this.PaymentConfiguration.RewardAmount) + parseFloat(this.PaymentConfiguration.Amount);
        // }
    }
    async InitializeTransaction(Mode) {
        if (this.PaymentConfiguration.Amount == undefined || this.PaymentConfiguration.Amount == null || isNaN(this.PaymentConfiguration.Amount) == true) {
            this._HelperService.Notify('Invalid amount', "Please enter valid amount for purchase");
        }
        else if (this.PaymentConfiguration.Amount < 0) {
            this._HelperService.Notify("Invalid amount", "Amount must be greater than  0");
        }
        else {

            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "buypointinitialize",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                Amount: this.PaymentConfiguration.Amount,
                PaymentMode: Mode,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
            _OResponse.subscribe(
                _Response => {

                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        // this._DataService.GetTUCBalance();
                        this.PaymentConfiguration.Amount = _Response.Result.Amount;
                        this.PaymentConfiguration.Charge = _Response.Result.Charge;
                        this.PaymentConfiguration.TotalAmount = _Response.Result.TotalAmount;
                        this.PaymentConfiguration.PaymentMode = _Response.Result.PaymentMode;
                        this.PaymentConfiguration.PaymentReference = _Response.Result.PaymentReference;
                        this.PaymentConfiguration.Banks = _Response.Result.Banks;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response);
                        // this._HelperService.Notify(_Response.Status, _Response.Message);
                        if (this.PaymentConfiguration.PaymentMode == 'paystack') {
                            setTimeout(() => {
                                this._HelperService.HideSpinner();
                                this._HelperService.AppConfig.IsProcessing = false;
                                document.getElementById('paymentbutton').click();
                            }, 200);
                        }
                        else {
                            this.Stage = 1;
                            this._HelperService.HideSpinner();
                            this._HelperService.AppConfig.IsProcessing = false;
                        }
                    }
                    else {
                        this._HelperService.HideSpinner();
                        this._HelperService.AppConfig.IsProcessing = false;
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    this._HelperService.HandleException(_Error);
                });


            // this.CalculateReward();
            // if (this.PaymentConfiguration.UserCards != undefined && this.PaymentConfiguration.UserCards != null && this.PaymentConfiguration.UserCards.length > 0) {
            //     this.PaymentConfiguration.Amount = this.PaymentConfiguration.Amount;
            //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
            //     const modal = await this._ModalController.create({
            //         component: PaymentTypeSelectModal,
            //         componentProps: this.PaymentConfiguration
            //     });
            //     modal.onDidDismiss().then(data => {
            //         if (data.data.PaymentSource == 'noaction') {
            //             this._HelperService.Notify('Payment method required', "Select payment method");
            //         }
            //         else if (data.data.PaymentSource == 'new') {
            //             document.getElementById('paymentbutton').click();
            //         }
            //         else {
            //             if (data.data.PaymentSource != undefined && data.data.PaymentSource != null && data.data.PaymentSource != 0) {
            //                 this.ProcessOnline_Charge(data.data.PaymentSource);
            //             }
            //         }
            //     });
            //     return await modal.present();
            // }
            // else {
            //     document.getElementById('paymentbutton').click();
            // }



        }
    }

    DialCodeDetails =
        {
            IsSelected: false,
            SystemName: null,
            Name: null,
            IconUrl: null,
            LocalUrl: null,
            Code: null,
            BankCode: null,
            DialCode: null,
        }

    CallNumber() {
        this.callNumber.callNumber(this.DialCodeDetails.DialCode, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
    BankItemClick(Item) {
        this.DialCodeDetails = Item;
        this.DialCodeDetails.IsSelected = true;
    }
    ProcessPaystack() {
        document.getElementById('paymentbutton').click();
    }


    ProcessOnline_Cancel() {
        // this._HelperService.Notify('Cancelled', "Payment cancelled.");
        // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointcancel",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    this._HelperService.NotifyToast(_Response.Message);
                    // // this._DataService.GetTUCBalance();
                    // this.PaymentConfiguration.Amount = _Response.Result.Amount;
                    // this.PaymentConfiguration.Charge = _Response.Result.Charge;
                    // this.PaymentConfiguration.TotalAmount = _Response.Result.TotalAmount;
                    // this.PaymentConfiguration.PaymentMode = _Response.Result.PaymentMode;
                    // this.PaymentConfiguration.PaymentReference = _Response.Result.PaymentReference;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response);
                    // // this._HelperService.Notify(_Response.Status, _Response.Message);
                    // // this._HelperService.NavPaymentDetails();
                    // setTimeout(() => {

                    //     document.getElementById('paymentbutton').click();
                    // }, 200);
                }
                else {
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    this._HelperService.NotifyToast(_Response.Message);
                    // this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }

    public Stage = 0;

    ProcessOnline_Confirm(Item) {
        if (Item.status == "success") {
            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "buypointverify",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                Amount: this.PaymentConfiguration.Amount,
                PaymentReference: this.PaymentConfiguration.PaymentReference,
                RefTransactionId: Item.transaction,
                RefStatus: Item.status,
                RefMessage: Item.message,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._DataService.GetTUCBalance();
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this.Stage = 0;
                        if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                            this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                        }
                        this._HelperService.NavBuyPointsConfirm();
                        // this._HelperService.Notify(_Response.Status, _Response.Message);
                        // this._HelperService.NavPaymentDetails();
                    }
                    else {
                        this.Stage = 0;
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }


    }
    ProcessOnline_Charge(PaymentSource) {
        this._HelperService.ShowSpinner('processing payement...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "paymentcharge",
            Type: "buypoint",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            ReferenceId: PaymentSource,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                    this.GetPaymentConfiguration();
                    this._DataService.GetTUCBalance();

                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    ProcessUssd_Confirm() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointverify",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                    this.Stage = 0;
                    if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                        this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                    }
                    this._HelperService.NavBuyPointsConfirm();
                }
                else {
                    this.Stage = 0;
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    // LoadData() {
    //     this.TranList_Data =
    //         {
    //             SearchContent: "",
    //             TotalRecords: 0,
    //             Offset: -1,
    //             Limit: 10,
    //             Data: []
    //         };
    //     this.TranList_Setup();
    // }
    // public TranList_Data =
    //     {
    //         SearchContent: "",
    //         TotalRecords: 0,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    // TranList_Setup() {
    //     // if (this.TranList_Data.Offset == -1) {
    //     //     this._HelperService.ShowSpinner();
    //     //     this.TranList_Data.Offset = 0;
    //     // }
    //     var SCon = "";
    //     // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', 'number', this._HelperService.AccountInfo.UserAccount.AccountId, "=");
    //     // if (this.TranList_Data.SearchContent != undefined && this.TranList_Data.SearchContent != null && this.TranList_Data.SearchContent != '') {
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     // }
    //     // SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this.StartTime, this.EndTime);
    //     // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountOwner.AccountId, '==');
    //     var pData = {
    //         Task: 'getsaletransactions',
    //         TotalRecords: this.TranList_Data.TotalRecords,
    //         Offset: this.TranList_Data.Offset,
    //         Limit: this.TranList_Data.Limit,
    //         RefreshCount: true,
    //         SearchCondition: SCon,
    //         SortExpression: 'TransactionDate desc',
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCTransCore, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.HideSpinner();
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
    //                 this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
    //                 var TranList_Data = _Response.Result.Data;
    //                 TranList_Data.forEach(element => {
    //                     if (element.CardBrandName != undefined) {
    //                         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
    //                     }
    //                     element.TypeName = element.TypeName.toLowerCase().trim();
    //                     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
    //                     this.TranList_Data.Data.push(element);
    //                 });
    //                 if (this.LoaderEvent != undefined) {
    //                     this.LoaderEvent.target.complete();
    //                     if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
    //                         this.LoaderEvent.target.disabled = true;
    //                     }
    //                 }
    //             }
    //             else {
    //                 this._HelperService.HideSpinner();
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // TranList_RowSelected(ReferenceData) {
    //     if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
    //     }
    //     else {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
    //     }
    // }
    // public LoaderEvent: any = undefined;
    // NextLoad(event) {
    //     this.LoaderEvent = event;
    //     this.TranList_Setup();

    // }

    // private delayTimer;
    // doSearch(text) {
    //     clearTimeout(this.delayTimer);
    //     this.delayTimer = setTimeout(x => {
    //         this.TranList_Data =
    //             {
    //                 SearchContent: text,
    //                 TotalRecords: 0,
    //                 Offset: -1,
    //                 Limit: 10,
    //                 Data: []
    //             };
    //         this.TranList_Setup();
    //     }, 1000);
    // }

    // async OpenAddModal(Item) {
    //     const modal = await this._ModalController.create({
    //         component: TransactionDetailsModal,
    //         componentProps: Item
    //     });
    //     return await modal.present();
    // }




}