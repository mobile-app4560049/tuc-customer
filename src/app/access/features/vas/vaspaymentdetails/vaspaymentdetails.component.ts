import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../../service/object.service';
import { PaymentTypeSelectModal } from '../../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../../service/data.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { PaymentManagerModal } from '../../../modals/payment/payment.modal.component';
declare var moment: any;

@Component({
    selector: 'app-vaspaymentdetails',
    templateUrl: 'vaspaymentdetails.component.html'
})
export class VasPaymentDetailsPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public callNumber: CallNumber,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Payment-BuyPoints-Initialize");
    }
    _PageConfig =
        {
            Type: "",
            Title: "",
            SubTitle: "",
            IconClass: "",
            Provider: null,
            SelectedProvider: null,
            SelectedProviderPackage: null,
            AccountNumber: null,
            Amount: null,
            InitializeResponse: null,
            ConfirmResponse: null
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._PageConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
    }



    CloseBox() {
        this._HelperService.NavDashboard();
    }
}