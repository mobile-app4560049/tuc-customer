import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../../service/object.service';
import { PaymentTypeSelectModal } from '../../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../../service/data.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
declare var moment: any;

@Component({
    selector: 'app-vasproviders',
    templateUrl: 'vasprovider.component.html'
})
export class VasProvidersPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public callNumber: CallNumber,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Payment-BuyPoints-Initialize");
    }
    LccTopup() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Payments.LccTopup);
    }

    _PageConfig =
        {
            Type: "",
            Title: "",
            SubTitle: "",
            IconClass: "",
            Provider: null,
            SelectedProvider: null,
            SelectedProviderPackage: null,
            AccountNumber: null,
            Amount: null
        }


    ChangeReference(Type) {
        this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, Type);
        this.ngOnInit();
    }
    countryName = ""
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        var SelectedCountryName = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig);
        this.countryName = SelectedCountryName.SelectedCountry.Name;
        this._PageConfig.Type = this._HelperService.GetStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        var PackageItems = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.VasServices);
        if (PackageItems != undefined && PackageItems != null && PackageItems.length > 0) {
            var Item = PackageItems.filter(x => x.SystemName == this._PageConfig.Type)[0];
            if (Item != undefined && Item != null) {

                if (this._PageConfig.Type == 'airtime') {
                    if (Item.MaximumRewardPercentage != undefined && Item.MaximumRewardPercentage != null && Item.MaximumRewardPercentage > 0) {
                        this._PageConfig =
                        {
                            Type: 'airtime',
                            Title: "Buy Airtime Or Data",
                            SubTitle: "Earn upto " + Item.MaximumRewardPercentage,
                            IconClass: 'las la-mobile-alt',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        }
                    }
                    else {
                        this._PageConfig =
                        {
                            Type: 'airtime',
                            Title: "Buy Airtime Or Data",
                            SubTitle: "Earn cashback on each purchase",
                            IconClass: 'las la-mobile-alt',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        }
                    }

                }
                else if (this._PageConfig.Type == 'tv') {
                    if (Item.MaximumRewardPercentage != undefined && Item.MaximumRewardPercentage != null && Item.MaximumRewardPercentage > 0) {
                        this._PageConfig =
                        {
                            Type: 'tv',
                            Title: "Buy Service for Your Tv",
                            SubTitle: "Earn upto " + Item.MaximumRewardPercentage,
                            IconClass: 'las la-satellite-dish',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        }
                    }
                    else {
                        this._PageConfig =
                        {
                            Type: 'tv',
                            Title: "Buy Service for Your Tv",
                            SubTitle: "Earn cashback on each purchase",
                            IconClass: 'las la-satellite-dish',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        };
                    }
                }
                else if (this._PageConfig.Type == 'electricity') {

                    if (Item.MaximumRewardPercentage != undefined && Item.MaximumRewardPercentage != null && Item.MaximumRewardPercentage > 0) {
                        this._PageConfig =
                        {
                            Type: 'electricity',
                            Title: "Pay your electricity bill",
                            SubTitle: "Earn upto " + Item.MaximumRewardPercentage,
                            IconClass: 'las la-lightbulb',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        };
                    }
                    else {
                        this._PageConfig =
                        {
                            Type: 'electricity',
                            Title: "Pay your electricity bill",
                            SubTitle: "Earn cashback on each purchase",
                            IconClass: 'las la-lightbulb',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        }
                    }
                }
                else if (this._PageConfig.Type == 'utilities') {
                    if (Item.MaximumRewardPercentage != undefined && Item.MaximumRewardPercentage != null && Item.MaximumRewardPercentage > 0) {

                        this._PageConfig =
                        {
                            Type: 'utilities',
                            Title: "Pay fdr Utilities",
                            SubTitle: "Earn upto " + Item.MaximumRewardPercentage,
                            IconClass: 'las la-lightbulb',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        }
                    }
                    else {
                        this._PageConfig =
                        {
                            Type: 'utilities',
                            Title: "Pay fdr Utilities",
                            SubTitle: "Earn cashback on each purchase",
                            IconClass: 'las la-lightbulb',
                            Provider: Item,
                            SelectedProvider: null,
                            AccountNumber: null,
                            Amount: 0,
                            SelectedProviderPackage: null,
                        }
                    }
                }
                this.SourceChange();
            }
        }
    }

    ProviderClick(Item) {
        if (this._HelperService.AccountInfo.UserCountry.CountryId == 1) {
            this._PageConfig.SelectedProvider = Item;
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._PageConfig);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviderpackage);
        }
        else {
            this._PageConfig.SelectedProvider = Item;
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._PageConfig);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasprovideraccount);
        }
    }


    public TranList_Data =
        {
            Type: "success",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    SourceChange() {
        this.TranList_Data =
        {
            Type: "success",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 2,
            Data: []
        };
        this.TranList_Setup();
    }
    TranList_Setup() {
        this._HelperService.ShowProgress();
        if (this.TranList_Data.Offset == -1) {
            this.TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this._PageConfig.Type == 'airtime') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ProductCategoryName', this._HelperService.AppConfig.DataType.Text, 'Airtime', '=');
        }
        else if (this._PageConfig.Type == 'tv') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ProductCategoryName', this._HelperService.AppConfig.DataType.Text, 'Tv', '=');
        }
        else if (this._PageConfig.Type == 'electricity') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ProductCategoryName', this._HelperService.AppConfig.DataType.Text, 'Electricity', '=');
        }
        else if (this._PageConfig.Type == 'utilities') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ProductCategoryName', this._HelperService.AppConfig.DataType.Text, 'Utilities', '=');
        }
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'paymentstatus.success', '=');
        var pData = {
            Task: 'getvaspurchasehistory',
            TotalRecords: this.TranList_Data.TotalRecords,
            Offset: this.TranList_Data.Offset,
            Limit: this.TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'StartDate desc',
            Type: this.TranList_Data.Type
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Vas, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
                    this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TranList_Data = _Response.Result.Data;
                    TranList_Data.forEach(element => {
                        var Item = this._PageConfig.Provider.Products.find(x => x.Name == element.ProductName);
                        if (Item != undefined && Item != null) {
                            var ItemPackage = Item.ProductItems.find(x => x.Name == element.ProductItemName);
                            if (ItemPackage != undefined && ItemPackage != null) {
                                element.SelectedProvider = Item;
                                element.SelectedProviderPackage = ItemPackage;
                            }
                        }
                        if (element.PaymentMethodName != undefined) {
                            element.PaymentMethodName = element.PaymentMethodName.toLowerCase().trim();
                        }
                        element.StartDate = this._HelperService.GetDateTimeS(element.StartDate);
                        this.TranList_Data.Data.push(element);
                    });
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    TranList_DetailsModal(ReferenceData) {
        var Item = this._PageConfig.Provider.Products.find(x => x.Name == ReferenceData.ProductName);
        if (Item != undefined && Item != null) {
            var ItemPackage = Item.ProductItems.find(x => x.Name == ReferenceData.ProductItemName);
            if (ItemPackage != undefined && ItemPackage != null) {
                this._PageConfig.SelectedProvider = Item;
                this._PageConfig.SelectedProviderPackage = ItemPackage;
                this._PageConfig.AccountNumber = ReferenceData.AccountNumber;
                this._PageConfig.Amount = ReferenceData.Amount;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._PageConfig);
                this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasprovideraccount);
            }
            else {
                this._HelperService.NotifyToastError("Selected topup is not available at the moment. Please try again later");
            }
        }
        else {
            this._HelperService.NotifyToastError("Selected topup is not available at the moment. Please try again later");
        }
        // if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
        //     var _OrderDetails =
        //     {
        //         OrderId: ReferenceData.OrderId,
        //         OrderKey: ReferenceData.OrderReference,
        //     };
        //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
        //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        // }
        // else {
        //     var _OrderDetails =
        //     {
        //         OrderId: ReferenceData.OrderId,
        //         OrderKey: ReferenceData.OrderReference,
        //     };
        //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
        //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        // }


    }
}