import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'modaladdcashout.component.html',
    selector: 'modal-modaladdcashout'
})
export class AddCashoutModal {
    public Stage = 0;

    public _Configuration =
        {
            Name: null,
            AccountNumber: null,
            BankName: null,
            BankCode: null,

            TransferBalance: 0,
            MinimumTransferBalance: 0,
            MinimumTransferAmount: 0,
            MaximumTransferAmount: 0,
            Title: "",
            Description: "",
            Amount: 0,
            Charge: 0,
            TotalAmount: 0,
            Banks: [],
            BankReferenceId: 0,
            BankReferenceKey: "",
            RedeemPin: null,
        };

    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    BankCodes = [];
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.Stage = 0;
        this.LoadConfiguration();
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
    AddbankAccount() {
        this.ModalDismiss(null);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BankManager.list);
    }

    LoadConfiguration() {
        // this._HelperService.Notify('Cancelled', "Payment cancelled.");
        // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
        this._HelperService.ShowSpinner('Please wait');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "getcashoutconfiguration",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._Configuration =
                    {
                        Name: null,
                        AccountNumber: null,
                        BankName: null,
                        BankCode: null,
                        TransferBalance: _Response.Result.TransferBalance,
                        MinimumTransferBalance: _Response.Result.MinimumTransferBalance,
                        MinimumTransferAmount: _Response.Result.MinimumTransferAmount,
                        MaximumTransferAmount: _Response.Result.MaximumTransferAmount,
                        Title: _Response.Result.Title,
                        Description: _Response.Result.Description,
                        Amount: 0,
                        Charge: _Response.Result.Charge,
                        TotalAmount: 0,
                        Banks: _Response.Result.Banks,
                        BankReferenceId: 0,
                        BankReferenceKey: "",
                        RedeemPin: null,
                    }
                    if (this._Configuration.Banks != undefined && this._Configuration.Banks != null && this._Configuration.Banks.length > 0) {
                        var Item = this._Configuration.Banks[0];
                        this._Configuration.BankReferenceId = Item.ReferenceId;
                        this._Configuration.BankReferenceKey = Item.ReferenceKey;
                        this._Configuration.AccountNumber = Item.AccountNumber;
                        this._Configuration.BankName = Item.BankName;
                        this._Configuration.Name = Item.Name;
                    }
                    if (this._Configuration.TransferBalance > this._Configuration.MaximumTransferAmount) {
                        this._Configuration.TransferBalance = this._Configuration.MaximumTransferAmount;
                    }
                    if (this._Configuration.TransferBalance < this._Configuration.MinimumTransferAmount) {
                        this._Configuration.TransferBalance = 0;
                    }
                }
                else {
                    this._HelperService.NotifyToast(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }
    BankSelected(Item) {
        this._Configuration.BankReferenceId = Item.ReferenceId;
        this._Configuration.BankReferenceKey = Item.ReferenceKey;
        this._Configuration.AccountNumber = Item.AccountNumber;
        this._Configuration.BankName = Item.BankName;
        this._Configuration.Name = Item.Name;
    }
    CalculateAmount() {
        var TAmt = parseInt(this._Configuration.Amount.toString()) + parseFloat(this._Configuration.Charge.toString());
        this._Configuration.TotalAmount = TAmt;
    }
    async InitializeTransferContinue() {
        if (this._Configuration.Amount == undefined || this._Configuration.Amount == null || this._Configuration.Amount < 0) {
            this._HelperService.NotifyToast("Enter amount");
        }
        else if (this._Configuration.Amount < this._Configuration.MinimumTransferAmount) {
            this._HelperService.NotifyToast("Minimum N" + this._Configuration.MinimumTransferAmount + " required for transfer.");
        }
        else if (this._Configuration.Amount > this._Configuration.MaximumTransferAmount) {
            this._HelperService.NotifyToast("Maximum N" + this._Configuration.MaximumTransferAmount + " required for transfer.");
        }
        else if (this._Configuration.BankReferenceId == undefined || this._Configuration.BankReferenceId == null || this._Configuration.BankReferenceId < 0) {
            this._HelperService.NotifyToast("Select bank for transfer");
        }
        else if (this._Configuration.AccountNumber == undefined || this._Configuration.AccountNumber == null || this._Configuration.AccountNumber == "") {
            this._HelperService.NotifyToast("Select bank for transfer");
        }
        else {
            this.Stage = 1;
        }
    }


    async InitializeTransfer() {
        if (this._Configuration.RedeemPin == undefined || this._Configuration.RedeemPin == null || this._Configuration.RedeemPin == "") {
            this._HelperService.NotifyToastError("Enter redeem pin");
        }
        else if (this._Configuration.Amount < 1) {
            this._HelperService.NotifyToastError("Enter valid amount for cashout");
        }
        else {
            const alert = await this._AlertController.create({
                // cssClass: 'my-custom-class',
                header: 'Initiate transfer ?',
                message: 'Once process initiated it cannot be cancelled. Do you want to continue?',

                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                        }
                    }, {
                        text: 'Continue',
                        handler: (data) => {
                            this.InitializeTransferConfirm();
                        }
                    }
                ]
            });
            await alert.present();
        }

    }


    InitializeTransferConfirm() {

        this._HelperService.ShowSpinner('Please wait');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "cashoutinitialize",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this._Configuration.Amount,
            BankId: this._Configuration.BankReferenceId,
            BankKey: this._Configuration.BankReferenceKey,
            RedeemPin: this._Configuration.RedeemPin
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.NotifyToast(_Response.Message);
                    this._HelperService.ModalDismiss('success');
                }
                else {
                    this._HelperService.NotifyToast(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });
        // if (this._Configuration.Name == undefined || this._Configuration.Name == null || this._Configuration.Name == "") {
        //     this._HelperService.NotifyToast("Enter name");
        // }
        // else if (this._Configuration.AccountNumber == undefined || this._Configuration.AccountNumber == null || this._Configuration.AccountNumber == "") {
        //     this._HelperService.NotifyToast("Enter your bank account number");
        // }
        // else if (this._Configuration.BankName == undefined || this._Configuration.BankName == null || this._Configuration.BankName == "") {
        //     this._HelperService.NotifyToast("Enter bank name");
        // }
        // else if (this._Configuration.BankCode == undefined || this._Configuration.BankCode == null || this._Configuration.BankCode == "") {
        //     this._HelperService.NotifyToast("Enter bank code");
        // }
        // else {

        // }
    }

}