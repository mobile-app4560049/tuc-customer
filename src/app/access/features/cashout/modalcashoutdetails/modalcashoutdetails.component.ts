import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'modalcashoutdetails.component.html',
    selector: 'modal-modalcashoutdetails'
})
export class CashOutDetailsModal {
    public _TransactionDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,
            BankAccountName: null,
            BankName: null,
            BankAccountNumber: null,
            StartDate: null,
            EndDate: null,
            Amount: null,
            Charge: null,
            TotalAmount: null,
            ReferenceNumber: null,
            StatusCode: null,
            StatusName: null,
            SystemComment: null,
        }
    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    ngOnInit() {
        this._HelperService.PageLoaded();

        this._TransactionDetails = this.navParams.data as any;
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
}