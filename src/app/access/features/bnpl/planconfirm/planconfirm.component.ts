import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';
import { DataService } from '../../../../service/data.service';
import { DomSanitizer } from "@angular/platform-browser";
import { ScanMyCodeModal } from '../../tucpay/scanmycode/scanmycode.modal.component';
import { BnplInfoModal } from '../modalhowitworks/bnplinfo.modal.component';
import { BnplPlanModal } from '../modalplandetails/bnplplan.modal.component';
@Component({
    selector: 'app-bnpl-planconfirm',
    templateUrl: 'planconfirm.component.html'
})
export class BnplPlanConfirmPage {
    public DeviceInformation: ODeviceInformation;
    public tAmount: number;
    public _Loan =
        {
            Amount: 0,
            Charge: 0,
            MonthlyAmount: 0,
            MonthlyLaterAmount: 0,
            PaymentReference: null,
            // InterestRate: 6,
            // Tenture: 4,
            // PlanName: 'Pay by 4',
            // PlanId: 1,
            Plan:
            {
                ReferenceId: null,
                ReferenceKey: null,
                Name: null,
                Description: null,
                Policy: null,
                Tenture: null,
                MinimumLoanAmount: null,
                CustomerInterestRate: null,
                Shedule: []
            }
        }




    constructor(
        public _MenuController: MenuController,
        public _Sanitizer: DomSanitizer,
        public _DataService: DataService,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ModalController: ModalController
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ActiveStore =
        {
            DisplayName: null,
            IconUrl: null,
            Locations: null,
            ReferenceId: null,
            ReferenceKey: null,
            RewardPercentage: null,
        }
    AccountConfig =
        {
            IsLoanActive: false,
            ReferenceId: 0,
            ReferenceKey: "",
            ActiveLoan: 0,
            MaximumLoanAmount: 0,
            MinimumLoanAmount: 0,
            Plans: [],
            HowItWorks: "",
        };
    ngOnInit() {
        this._HelperService.PageLoaded();
        var Details = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        this._Loan = Details.Loan;
        this.AccountConfig = Details.AccountConfig;
        this.ActiveStore = Details.Merchant;
        if (this.AccountConfig.Plans != undefined && this.AccountConfig.Plans != null && this.AccountConfig.Plans.length > 0) {
            this.AccountConfig.Plans.forEach(element => {
                var BaseAmount = 50000;
                var PaymentAmount = this._HelperService.RoundNumber(BaseAmount / element.Tenture);
                var InterestRate = this._HelperService.RoundNumber((PaymentAmount / element.CustomerInterestRate) / 100);
                var PaymentAmountLater = this._HelperService.RoundNumber(PaymentAmount + InterestRate);
                var PaymentShedule = [];
                PaymentShedule.push(
                    {
                        Title: "Now",
                        InterestRate: "0%",
                        TotalAmount: PaymentAmount,
                    });
                for (let index = 1; index < element.Tenture; index++) {
                    var TodaysDate = new Date();
                    var newDate = new Date(TodaysDate.setMonth(TodaysDate.getMonth() + index));
                    PaymentShedule.push(
                        {
                            // Title: index + " Month",
                            Title: this._HelperService.GetDateS(newDate),
                            InterestRate: element.CustomerInterestRate + "%",
                            TotalAmount: PaymentAmountLater,
                        });
                }
                element.Shedule = PaymentShedule;

            });
        }
        this._Loan.Plan = this.AccountConfig.Plans[0];
        this._Loan.PaymentReference = this._HelperService.GenerateGuid();
    }

    OnAmountChange() {
        if (this.tAmount != undefined && this.tAmount != null) {
            if (isNaN(this.tAmount) == false) {
                this._Loan.Amount = this.tAmount;
                this._Loan.MonthlyAmount = this._HelperService.RoundNumber(this.tAmount / this._Loan.Plan.Tenture);
                this._Loan.MonthlyLaterAmount = this._HelperService.RoundNumber(this._Loan.MonthlyAmount + (this.tAmount * this._Loan.Plan.CustomerInterestRate) / 100);
            }
        }


    }
    PlanSelected(Item) {
        this._Loan.Plan = Item;
        this.OnAmountChange();
    }


    async ModalBnplInfo() {
        const modal = await this._ModalController.create({
            component: BnplInfoModal,
            componentProps: this.AccountConfig
        });
        return await modal.present();
    }

    async ModalBnplPlan() {
        const modal = await this._ModalController.create({
            component: BnplPlanModal,
            componentProps: this.AccountConfig
        });
        return await modal.present();
    }


    InitializePayment() {
        if (this._Loan.Amount < 1) {
            this._HelperService.NotifyToastError("Enter valid loan amount");
        }
        else {
            setTimeout(() => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                document.getElementById('paymentbuttonModal').click();
            }, 200);
        }

    }





    ProcessOnline_Confirm(Item) {
        var DebitMandate = this._HelperService.GetStorage("DebitMandate");

        if (Item.status == "success") {
            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "initializeloan",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                PlanId: this._Loan.Plan.ReferenceId,
                Amount: this._Loan.Amount,
                PaymentReference: this._Loan.PaymentReference,
                RefTransactionId: Item.transaction,
                RefStatus: Item.status,
                RefMessage: Item.message,
                MerchantId: this.ActiveStore.ReferenceId,
                LoanRequestId: DebitMandate.Data.LoanRequestId
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.LoanDetails);
                        // this._DataService.GetTUCBalance(); 
                        // this._HelperService.NotifyToast(_Response.Message);
                        // if (_Response.Result.StatusCode == 'transaction.success') {
                        //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        //     // this.Stage = 0;
                        //     if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                        //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                        //         this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                        //     }
                        //     this._PaymentStatus =
                        //     {
                        //         Status: 'success',
                        //         Balance: _Response.Result.Balance,
                        //     }
                        //     // this.ModalDismiss(this._PaymentStatus);
                        // }
                        // else {
                        //     // this.NavBuyPointModalDismiss();
                        // }
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Bnpl.LoanDetails);
                    }
                    else {
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }
    }

    ProcessOnline_Cancel() {

    }



}