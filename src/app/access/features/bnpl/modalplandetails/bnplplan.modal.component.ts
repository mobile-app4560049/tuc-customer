import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
@Component({
    templateUrl: 'bnplplan.modal.component.html',
    selector: 'modal-bnplplan'
})
export class BnplPlanModal {
    public acttab = 'by4';
    TabChange(type) {
        this.acttab = type;
    }
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    tItem = "";
    AccountConfig =
        {
            IsLoanActive: false,
            ReferenceId: 0,
            ReferenceKey: "",
            ActiveLoan: 0,
            MaximumLoanAmount: 0,
            MinimumLoanAmount: 0,
            Plans: [],
            HowItWorks: "",
            ActivePlan:
            {
                ReferenceId: null,
                ReferenceKey: null,
                Name: null,
                Description: null,
                Policy: null,
                Tenture: null,
                MinimumLoanAmount: null,
                CustomerInterestRate: null,
                ProviderInterestRate: null,
                SystemInterestRate: null,
            },
            Shedule:
            {
                Title: null,
                InterestRate: null,
                TotalAmount: null
            }
        };

    ngOnInit() {
        this._HelperService.PageLoaded();
        this.AccountConfig = this.navParams.data as any;
        this.AccountConfig.ActivePlan = this.AccountConfig.Plans[0];
    }

    PlanSelected(Item) {
        this.AccountConfig.ActivePlan = Item;
    }

    async ModalDismiss() {
        await this._ModalController.dismiss();
    }
}