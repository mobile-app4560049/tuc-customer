import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';
import { DataService } from '../../../../service/data.service';
import { DomSanitizer } from "@angular/platform-browser";
import { ScanMyCodeModal } from '../../tucpay/scanmycode/scanmycode.modal.component';
import { BnplInfoModal } from '../modalhowitworks/bnplinfo.modal.component';
import { BnplPlanModal } from '../modalplandetails/bnplplan.modal.component';
import { ModaldebitmandateComponent } from '../modaldebitmandate/modaldebitmandate.component';
@Component({
    selector: 'app-bnpl-planselector',
    templateUrl: 'planselector.component.html'
})
export class BnplPlanSelectorPage {
    public DeviceInformation: ODeviceInformation;
    public tAmount: number;
    public _Loan =
        {
            Amount: 0,
            Charge: 0,
            MonthlyAmount: 0,
            MonthlyLaterAmount: 0,
            PaymentReference: null,
            // InterestRate: 6,
            // Tenture: 4,
            // PlanName: 'Pay by 4',
            // PlanId: 1,
            Plan:
            {
                ReferenceId: null,
                ReferenceKey: null,
                Name: null,
                Description: null,
                Policy: null,
                Tenture: null,
                MinimumLoanAmount: null,
                CustomerInterestRate: null,
                Shedule: []
            }
        }




    constructor(
        public _MenuController: MenuController,
        public _Sanitizer: DomSanitizer,
        public _DataService: DataService,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ModalController: ModalController
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ActiveStore =
        {
            DisplayName: null,
            IconUrl: null,
            Locations: null,
            ReferenceId: null,
            ReferenceKey: null,
            RewardPercentage: null,
        }
    AccountConfig =
        {
            IsLoanActive: false,
            ReferenceId: 0,
            ReferenceKey: "",
            ActiveLoan: 0,
            MaximumLoanAmount: 0,
            MinimumLoanAmount: 0,
            Plans: [],
            HowItWorks: "",
        };
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.ActiveStore = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        this.AccountConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Bnpl.AccountConfig);
        if (this.AccountConfig.Plans != undefined && this.AccountConfig.Plans != null && this.AccountConfig.Plans.length > 0) {
            this.AccountConfig.Plans.forEach(element => {
                var BaseAmount = 50000;
                var PaymentAmount = this._HelperService.RoundNumber(BaseAmount / element.Tenture);
                var InterestRate = this._HelperService.RoundNumber((PaymentAmount / element.CustomerInterestRate) / 100);
                var PaymentAmountLater = this._HelperService.RoundNumber(PaymentAmount + InterestRate);
                var PaymentShedule = [];
                PaymentShedule.push(
                    {
                        Title: "Now",
                        InterestRate: "0%",
                        TotalAmount: PaymentAmount,
                    });
                for (let index = 1; index < element.Tenture; index++) {
                    var TodaysDate = new Date();
                    var newDate = new Date(TodaysDate.setMonth(TodaysDate.getMonth() + index));
                    PaymentShedule.push(
                        {
                            // Title: index + " Month",
                            Title: this._HelperService.GetDateS(newDate),
                            InterestRate: element.CustomerInterestRate + "%",
                            TotalAmount: PaymentAmountLater,
                        });
                }
                element.Shedule = PaymentShedule;

            });
        }
        this._Loan.Plan = this.AccountConfig.Plans[0];
    }

    OnAmountChange() {
        if (this.tAmount != undefined && this.tAmount != null) {
            if (isNaN(this.tAmount) == false) {
                this._Loan.Amount = this.tAmount;
                this._Loan.MonthlyAmount = this._HelperService.RoundNumber(this._Loan.Amount / this._Loan.Plan.Tenture);
                this._Loan.MonthlyLaterAmount = this._HelperService.RoundNumber(this._Loan.MonthlyAmount + (this._Loan.Amount * this._Loan.Plan.CustomerInterestRate) / 100);
            }
        }
    }
    PlanSelected(Item) {
        this._Loan.Plan = Item;
        this.OnAmountChange();
    }

    async ModalBnplInfo() {
        const modal = await this._ModalController.create({
            component: BnplInfoModal,
            componentProps: this.AccountConfig
        });
        return await modal.present();
    }

    async ModalBnplPlan() {
        const modal = await this._ModalController.create({
            component: BnplPlanModal,
            componentProps: this.AccountConfig
        });
        return await modal.present();
    }


    InitializePayment() {
        if (this._Loan.Amount < 1) {
            this._HelperService.NotifyToastError("Enter valid loan amount");
        }
        else {
            this._Loan.PaymentReference = this._HelperService.GenerateGuid();
            setTimeout(() => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                document.getElementById('paymentbuttonModal').click();
            }, 200);
        }
    }


    Next() {
        var _Storage =
        {
            Loan: this._Loan,
            AccountConfig: this.AccountConfig,
            Merchant: this.ActiveStore
        }
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Storage);
        this.CreateLoanRequest();
    }





    ProcessOnline_Confirm(Item) {
        if (Item.status == "success") {
            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "initializeloan",
                AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                PlanId: this._Loan.Plan.ReferenceId,
                Amount: this._Loan.Amount,
                PaymentReference: this._Loan.PaymentReference,
                RefTransactionId: Item.transaction,
                RefStatus: Item.status,
                RefMessage: Item.message,
                MerchantId: this.ActiveStore.ReferenceId
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        // this._DataService.GetTUCBalance();
                        // this._HelperService.NotifyToast(_Response.Message);
                        // if (_Response.Result.StatusCode == 'transaction.success') {
                        //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        //     // this.Stage = 0;
                        //     if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                        //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                        //         this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                        //     }
                        //     this._PaymentStatus =
                        //     {
                        //         Status: 'success',
                        //         Balance: _Response.Result.Balance,
                        //     }
                        //     // this.ModalDismiss(this._PaymentStatus);
                        // }
                        // else {
                        //     // this.NavBuyPointModalDismiss();
                        // }
                    }
                    else {
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }


    }


    public DebitMandateRequest =
        {
            Status: "",
            Message: "",
            Data: {
                StatusCode: "",
                RequestId: "",
                MandateId: "",
                RemitaTransRef: "",
                LoanRequestId: "",
                Status: "",
                Message: "",
                AuthParams: [
                    {
                        Param1: "OTP",
                        Description1: "Please enter your Bank OTP",
                        Value1: "",
                        Param2: "Please specify the last 4 digits of your bank card",
                        Description2: "",
                        Value2: "",
                        Param3: "",
                        Description3: "",
                        Value3: "",
                        Param4: "",
                        Description4: "",
                        Value4: "",
                    }
                ]
            }
        }
    CreateLoanRequest() {
        var mode = this._HelperService.GetStorageValue('bnplmode');
        this._HelperService.ShowSpinner("Please wait");
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.V3.Bnpl.createloanrequest,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            amount: this._Loan.Amount,
            MerchantId: this.ActiveStore.ReferenceId,
            Mode: mode,
            purpose: "loan",
            PlanId: this._Loan.Plan.ReferenceId
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Evolve, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.DebitMandateRequest = _Response.Result;
                    this._HelperService.SaveStorage("DebitMandate", this.DebitMandateRequest);
                    this.OpenDebitMandatePopup();

                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            }
        )
    }

    async OpenDebitMandatePopup() {
        const modal = await this._ModalController.create({
            component: ModaldebitmandateComponent,
            componentProps: this.DebitMandateRequest
        });
        return await modal.present();

        // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.planconfirm);

    }


}