import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BnplPlanSelectorPage } from './planselector.component';
import { Angular4PaystackModule } from 'angular4-paystack';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        Angular4PaystackModule.forRoot('pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12'),
        RouterModule.forChild([
            {
                path: '',
                component: BnplPlanSelectorPage
            }
        ])
    ],
    declarations: [BnplPlanSelectorPage]
})
export class BnplPlanSelectorPageModule { }
