import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import MonoConnect from '@mono.co/connect.js';
import { Observable } from 'rxjs/internal/Observable';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';
import { UploadkycComponent } from '../uploadkyc/uploadkyc.component';


@Component({
  selector: 'app-checklimit',
  templateUrl: './checklimit.component.html'
})
export class ChecklimitComponent implements OnInit {
  public monoInstance: any;
  id: any;
  AccountConfig: {
    IsConfigLoaded: false,
    IsBnplEnable: false,
    ReferenceId: 0,
    ReferenceKey: null,
    IsLoanActive: false,
    ActiveLoan: 0,
    TotalLoan: 0,
    MaximumLoanAmount: 0,
    MinimumLoanAmount: 0,
    Plans: [],
    SystemConfig:
    {
      monoKey: null,
    }
  };
  constructor(public _HelperService: HelperService,
    public _ModalController: ModalController) {
    var bnplConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Bnpl.AccountConfig);
    if (bnplConfig != null) {
      this.BnplConfiguration = bnplConfig;
    }

    this.monoInstance = new MonoConnect({
      key: this.BnplConfiguration.SystemConfig.monoKey,
      onClose: () => console.log('Widget closed'),
      onLoad: () => console.log('Widget loaded successfully'),
      onSuccess: ({ code }) => (
        console.log(`Linked successfully: ${code}`),
        this.id = code,
        this.getInfo())
    })

    this.monoInstance.setup()
  }
  BnplConfiguration = {
    IsConfigLoaded: false,
    IsBnplEnable: false,
    ReferenceId: 0,
    ReferenceKey: null,
    IsLoanActive: false,
    ActiveLoan: 0,
    TotalLoan: 0,
    MaximumLoanAmount: 0,
    MinimumLoanAmount: 0,
    Plans: [],
    SystemConfig:
    {
      monoKey: null,
    }
  };
  ngOnInit() {
    this._HelperService.PageLoaded();


  }

  NavBnpl() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.home);
  }

  getInfo() {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.ShowSpinner();
    let pData = {
      Task: 'connectmono',
      code: this.id
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.monoauth, pData);
    _OResponse.subscribe(
      async _Response => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HideProgress();
        this._HelperService.HideSpinner();
        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
          this._HelperService.SaveStorageValue('userId', _Response.Result.MonoReference);
          this._HelperService.SaveStorage('accountDetails', _Response.Result);
          const modal = await this._ModalController.create({
            component: UploadkycComponent,
            componentProps: _Response.Result,
            id: 'uploadkycmodal'
          });
          return await modal.present();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HideProgress();
          this._HelperService.HideSpinner();
          this._HelperService.NotifyToastError(_Response.Message);
          // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
        }
      }
    )
  }


}
