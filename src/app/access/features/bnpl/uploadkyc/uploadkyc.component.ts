import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OAccountInfo } from '../../../../service/object.service';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { ModalbacktohomeComponent } from '../modalbacktohome/modalbacktohome.component'
@Component({
  selector: 'app-uploadkyc',
  templateUrl: './uploadkyc.component.html'
})
export class UploadkycComponent implements OnInit {

  public accountInfo: OAccountInfo;
  public IsAccountLoaded = false;
  public userDetails = {
    MonoReference: "",
    BVN: "",
    Name: null,
    Currency: null,
    Type: null,
    AccountNumber: null,
    MobileNumber: null,
    Balance: 0,
    Institution: {
      Name: null,
      BankCode: null,
      Type: null
    }
  };
  user: any;
  userInfo: any;
  constructor(public _HelperService: HelperService,
    public navParams: NavParams,
    public _NavParameters: NavParams,
    public _ModalController: ModalController,
    public _AlertController: AlertController,
  ) { }

  ngOnInit() {
    this._HelperService.PageLoaded();
    this._HelperService.IsFormProcessing = false;
    this.IsAccountLoaded = true;
    this.userDetails = this._NavParameters.data as any;
    this.userDetails.MobileNumber = this._HelperService.AccountInfo.User.MobileNumber;
  }

  NavMono() {
    this._ModalController.dismiss();
    // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.checkCreditLimit);
  }
  Next() {
    if (this.userDetails.BVN == undefined || this.userDetails.BVN == null || this.userDetails.BVN == '') {
      this._HelperService.NotifyToastError("Please enter your BVN number");
    }
    else {
      var Account = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
      if (Account != null) {
        this.accountInfo = Account as OAccountInfo;
        let pData = {
          user: {
            phone: this.userDetails.MobileNumber
          },
          bvn: this.userDetails.BVN,
          monoReference: this.userDetails.MonoReference,
          balance: this.userDetails.Balance,
          Task: 'createuser',
          Bank:
          {
            Name: this.userDetails.Institution.Name,
            Code: this.userDetails.Institution.BankCode,
            Type: this.userDetails.Institution.Type,
            AccountName: this.userDetails.Name,
            AccountNumber: this.userDetails.AccountNumber,
            Bvn: this.userDetails.BVN,
            Balance: this.userDetails.Balance
          }

        }
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner();
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.evolve, pData);
        _OResponse.subscribe(
          async _Response => {
            this._HelperService.IsFormProcessing = true;
            this._HelperService.HideSpinner();
            if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
              this.userInfo = _Response.Result.Data;
              this._HelperService.SaveStorage('userInfo', this.userInfo);
              this.GetBnplConfiguration(_Response.Message);
            }
            else {
              this._ModalController.dismiss();
              this._HelperService.NotifyToastError(_Response.Message);
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
            }
          });
      }
    }

  }
  BnplConfiguration = {
    IsConfigLoaded: false,
    IsBnplEnable: false,
    ReferenceId: 0,
    ReferenceKey: null,
    IsLoanActive: false,
    ActiveLoan: 0,
    TotalLoan: 0,
    MaximumLoanAmount: 0,
    MinimumLoanAmount: 0,
    Plans: [],
    SystemConfig:
    {
      monoKey: null,
    }
  };
  GetBnplConfiguration(Message) {
    var mode = this._HelperService.GetStorageValue('bnplmode');
    var pData = {
      Task: this._HelperService.AppConfig.NetworkApi.V3.Bnpl.getcustomerconfiguration,
      AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
      AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
      Mode: mode,
      user: {
        email: this._HelperService.AccountInfo.User.EmailAddress,
      }
    }
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
          this.BnplConfiguration = _Response.Result;
          this.BnplConfiguration.IsConfigLoaded = true;
          this._HelperService.NotifyToastSuccess(Message);
          this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Bnpl.AccountConfig, _Response.Result);
          // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.home);
          this.dismiss('uploadkycmodal');
        } else {
          this._HelperService.NotifySimple(_Response.Message);
        }
      }
    )
  }

  getAccountDetails() {
    let pData = {
      Task: 'getaccount',
      id: localStorage.getItem("userId")
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.monoauth, pData);
    _OResponse.subscribe(
      _Response => {
        this.user = _Response.Result.account;

        this._HelperService.SaveStorage('accountBalance', this.user.Balance)
      }
    )
  }
  getAccountStatement() {
    let pData = {
      Task: 'getaccountstatement',
      id: localStorage.getItem("userId")
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.monoauth, pData);
    _OResponse.subscribe(
      _Response => {
        this.user = _Response.Result;

        this._HelperService.SaveStorage('accountStatement', this.user.data)
      }
    )
  }

  getIncome() {
    let pData = {
      Task: 'getincome',
      id: localStorage.getItem("userId")
    }

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.monoauth, pData);
    _OResponse.subscribe(
      _Response => {
        this.user = _Response.Result;
        this._HelperService.SaveStorage('accountAverageIncome', this.user.Average_Income);
        this._HelperService.SaveStorage('accountMonthlyIncome', this.user.Monthly_Income);
        this._HelperService.SaveStorage('accountYearlyIncome', this.user.Yearly_Income);
      }
    )
  }
  public async dismiss(id?: string) {
    await this._ModalController.dismiss(undefined, undefined, id);
  }

}
