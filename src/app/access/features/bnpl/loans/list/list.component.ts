import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../../service/object.service';
import { TransactionDetailsModal } from '../../../../modals/transactiondetails/transactiondetails.modal.component';
import { BuyPointDetailsModal } from '../../../../modals/buypointdetails/buypointdetails.modal.component';
declare var moment: any;

@Component({
    selector: 'app-bnpl-loans-history-list',
    templateUrl: 'list.component.html'
})
export class LoansHistoryPage {

    public acttab = 'repayments';
    TabChange(type) {
        this.acttab = type;
    }
    @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("WalletTopupHistory");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.LoadData();
    }
    LoadData() {
        this.TranList_Data =
        {
            Type: "success",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.TranList_Setup();
    }

    public TranList_Data =
        {
            Type: "success",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    SourceChange(Type) {
        this.TranList_Data =
        {
            Type: Type,
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.TranList_Setup();
    }
    TranList_Setup() {
        this._HelperService.ShowProgress();
        if (this.TranList_Data.Offset == -1) {
            this.TranList_Data.Offset = 0;
        }
        var SCon = "";

        // if (this.TranList_Data.SearchContent != undefined && this.TranList_Data.SearchContent != null && this.TranList_Data.SearchContent != '') {
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     // SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     // SCon = this._HelperService.GetSearchCondition(SCon, 'CreatedByDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     // SCon = this._HelperService.GetSearchCondition(SCon, 'SubParentDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        // }
        // SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this.StartTime, this.EndTime);
        // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountInfo.UserAccount.AccountId, '==');

        if (this.TranList_Data.Type == 'success') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'transaction.success', '=');
        }
        else if (this.TranList_Data.Type == 'pending') {
            SCon = this._HelperService.GetSearchConditionSubStrictFromArray(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text,
                ['transaction.initialized',
                    'transaction.processing'
                ], '=');
        }
        else if (this.TranList_Data.Type == 'cancelled') {
            // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'transaction.cancelled', '=');

            SCon = this._HelperService.GetSearchConditionSubStrictFromArray(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text,
                ['transaction.cancelled',
                    'transaction.refunded'
                ], '=');
        }
        else if (this.TranList_Data.Type == 'failed') {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'transaction.failed', '=');
        }
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.V3.Bnpl.getloans,
            TotalRecords: this.TranList_Data.TotalRecords,
            Offset: this.TranList_Data.Offset,
            Limit: this.TranList_Data.Limit,
            RefreshCount: true,
            // SearchCondition: SCon,
            SortExpression: 'ReferenceId desc',
            Type: this.TranList_Data.Type
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
                    this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TranList_Data = _Response.Result.Data;
                    TranList_Data.forEach(element => {
                        if (element.PaymentMethodName != undefined) {
                            element.PaymentMethodName = element.PaymentMethodName.toLowerCase().trim();
                        }
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.TranList_Data.Data.push(element);
                    });
                    if (this.TranList_LoaderEvent != undefined) {
                        this.TranList_LoaderEvent.target.complete();
                        if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
                            this.TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    TranList_RowSelected(ReferenceData) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, ReferenceData);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.LoanDetails);
        // if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
        //     var _OrderDetails =
        //     {
        //         OrderId: ReferenceData.OrderId,
        //         OrderKey: ReferenceData.OrderReference,
        //     };
        //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
        //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        // }
        // else {
        //     var _OrderDetails =
        //     {
        //         OrderId: ReferenceData.OrderId,
        //         OrderKey: ReferenceData.OrderReference,
        //     };
        //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
        //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        // }


    }
    public TranList_LoaderEvent: any = undefined;
    TranList_NextLoad(event) {
        this.TranList_LoaderEvent = event;
        this.TranList_Setup();

    }
    private TranList_DelayTimer;
    TranList_DoSearch(text) {
        clearTimeout(this.TranList_DelayTimer);
        this.TranList_DelayTimer = setTimeout(x => {
            this.TranList_Data =
            {
                Type: 'payments',
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.TranList_Setup();
        }, 1000);
    }
    async TranList_DetailsModal(Item) {
        const modal = await this._ModalController.create({
            component: BuyPointDetailsModal,
            componentProps: Item
        });
        return await modal.present();
    }

    // LoadData() {
    //     // this.TranList_Data =
    //     //     {
    //     //         SearchContent: "",
    //     //         TotalRecords: 0,
    //     //         Offset: -1,
    //     //         Limit: 10,
    //     //         Data: []
    //     //     };
    //     // this.TranList_Setup();
    // }
    // public TranList_Data =
    //     {
    //         SearchContent: "",
    //         TotalRecords: 0,
    //         Offset: -1,
    //         Limit: 10, 
    //         Data: []
    //     };
    // TranList_Setup() {
    //     if (this.TranList_Data.Offset == -1) {
    //         this._HelperService.ShowSpinner();
    //         this.TranList_Data.Offset = 0;
    //     }
    //     var SCon = "";
    //     SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', 'number', this._HelperService.AccountInfo.UserAccount.AccountId, "=");
    //     var pData = {
    //         Task: 'getsaletransactions',
    //         TotalRecords: this.TranList_Data.TotalRecords,
    //         Offset: this.TranList_Data.Offset,
    //         Limit: this.TranList_Data.Limit,
    //         RefreshCount: true,
    //         SearchCondition: SCon,
    //         SortExpression: 'TransactionDate desc',
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCTransCore, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.HideSpinner();
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
    //                 this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
    //                 var TranList_Data = _Response.Result.Data;
    //                 TranList_Data.forEach(element => {
    //                     if (element.CardBrandName != undefined) {
    //                         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
    //                     }
    //                     element.TypeName = element.TypeName.toLowerCase().trim();
    //                     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
    //                     this.TranList_Data.Data.push(element);
    //                 });
    //                 if (this.LoaderEvent != undefined) {
    //                     this.LoaderEvent.target.complete();
    //                     if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
    //                         this.LoaderEvent.target.disabled = true;
    //                     }
    //                 }
    //             }
    //             else {
    //                 this._HelperService.HideSpinner();
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // TranList_RowSelected(ReferenceData) {
    //     if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
    //     }
    //     else {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
    //     }
    // }
    // public LoaderEvent: any = undefined;
    // NextLoad(event) {
    //     this.LoaderEvent = event;
    //     this.TranList_Setup();
    // }

    // private delayTimer;
    // doSearch(text) {
    //     clearTimeout(this.delayTimer);
    //     this.delayTimer = setTimeout(x => {
    //         this.TranList_Data =
    //             {
    //                 SearchContent: text,
    //                 TotalRecords: 0,
    //                 Offset: -1,
    //                 Limit: 10,
    //                 Data: []
    //             };
    //         this.TranList_Setup();
    //     }, 1000);
    // }

    // async OpenAddModal(Item) {
    //     const modal = await this._ModalController.create({
    //         component: TransactionDetailsModal,
    //         componentProps: Item
    //     });
    //     return await modal.present();
    // }


    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavSaleHistory() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.SalesHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }

}