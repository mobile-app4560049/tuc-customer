import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
@Component({
    templateUrl: 'bnplinfo.modal.component.html',
    selector: 'modal-bnplinfo'
})
export class BnplInfoModal {

    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    ItemDetails =
        {
            IsLoanActive: false,
            ReferenceId: 0,
            ReferenceKey: "",
            ActiveLoan: 0,
            MaximumLoanAmount: 0,
            MinimumLoanAmount: 0,
            Plans: [],
            HowItWorks: "",
        };
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.ItemDetails = this.navParams.data as any;
    }
    async ModalDismiss() {
        await this._ModalController.dismiss();
    }
}