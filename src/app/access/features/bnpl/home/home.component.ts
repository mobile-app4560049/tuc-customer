import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import MonoConnect from '@mono.co/connect.js';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OBankStatement } from '../../../../service/object.service';
import { DataService } from '../../../../service/data.service';
import { DomSanitizer } from "@angular/platform-browser";
import { ScanMyCodeModal } from '../../tucpay/scanmycode/scanmycode.modal.component';
@Component({
    selector: 'app-bnplhome',
    templateUrl: 'home.component.html'
})
export class BnplHomePage {

    checkUserExists: boolean;
    public monoInstance: any;
    userDetails: any;
    public bankDetails: OBankStatement;
    id: any;
    public UserCustomAddress =
        {
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityName: null,
            StateName: null,
            CountryName: null,
            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: null,
            Longitude: null,
        }

    public DeviceInformation: ODeviceInformation;
    constructor(
        public _MenuController: MenuController,
        public _Sanitizer: DomSanitizer,
        public _DataService: DataService,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ModalController: ModalController
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }

        var BankDetailsStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.BankStatement);
        if (BankDetailsStorage != null) {
            this.bankDetails = BankDetailsStorage;
        }
    }

    userExists() {
        var account = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
        if (account != null) {
            var pData = {
                Task: 'checkuserexists',
                user: {
                    email: account.User.EmailAddress
                }
            }
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.evolve, pData);
        _OResponse.subscribe(
            _Response => {
                this.checkUserExists = _Response.Result;
            }
        )
    }

    NavTucPay() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode);
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Stores");
    }
    public TUCCategories: any[] = [];
    NavProfile() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
    }
    async NavOpenQR() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    ngOnInit() {
        this._HelperService.PageLoaded();

        var bnplConfig = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Bnpl.AccountConfig);
        if (bnplConfig != null) {
            this.BnplConfiguration = bnplConfig;
        }
        this.monoInstance = new MonoConnect({
            key: this.BnplConfiguration.SystemConfig.monoKey,
            onClose: () => console.log('Widget closed'),
            onLoad: () => console.log('Widget loaded successfully'),
            onSuccess: ({ code }) => (
                this.id = code
            )
        })

        this.monoInstance.setup()


        var UserCustomAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
        if (this._DataService.TUCCategories != undefined && this._DataService.TUCCategories != null && this._DataService.TUCCategories.length > 0) {
            var TCat: any[] = this._DataService.TUCCategories.sort((obj1, obj2) => {
                if (obj1.Merchants > obj2.Merchants) {
                    return -1;
                }
                if (obj1.Merchants < obj2.Merchants) {
                    return 1;
                }
                return 0;
            });
            this.TUCCategories = TCat;
        }
        var ActiveCategory = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TCategory);
        // if (ActiveCategory != undefined && ActiveCategory != null) {
        //     this.CategoryClick(ActiveCategory);
        // }
        // else {
        //     this.TUCNearByMerchants = this._DataService.TUCMerchants;
        // } 
        // this.getInfo();
        this.LoadData();
        this.userExists();
    }
    CategoryClick(Item) {
        this.NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
        this.NearByStores_Data.ActiveCategoryId = Item.ReferenceId;
        this.NearByStores_Setup();

    }
    CategoryClickAll() {
        this.NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
        this.NearByStores_Data.ActiveCategoryId = 0;
        this.NearByStores_Setup();

    }




    LoadData() {
        this.NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
        this.NearByStores_Setup();
        this.ValidateCustomerBnpl(false);
    }
    public NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
    NearByStores_Setup() {
        this._HelperService.ShowProgress();
        if (this.NearByStores_Data.Offset == -1) {
            this.NearByStores_Data.Offset = 0;
        }
        var SCon = "";
        if (this.NearByStores_Data.SearchContent != undefined && this.NearByStores_Data.SearchContent != null && this.NearByStores_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition('', 'DisplayName', 'text', this.NearByStores_Data.SearchContent);
        }
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.V3.Bnpl.getmerchants,
            TotalRecords: this.NearByStores_Data.TotalRecords,
            Offset: this.NearByStores_Data.Offset,
            Limit: this.NearByStores_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
            SubReferenceId: this.NearByStores_Data.ActiveCategoryId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.NearByStores_Data.Offset = this.NearByStores_Data.Offset + this.NearByStores_Data.Limit;
                    this.NearByStores_Data.TotalRecords = _Response.Result.TotalRecords;
                    var NearByStores_Data = _Response.Result.Data;
                    NearByStores_Data.forEach(element => {
                        this.NearByStores_Data.Data.push(element);
                    });
                    if (this.NearByStores_LoaderEvent != undefined) {
                        this.NearByStores_LoaderEvent.target.complete();
                        if (this.NearByStores_Data.TotalRecords == this.NearByStores_Data.Data.length) {
                            this.NearByStores_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public NearByStores_LoaderEvent: any = undefined;
    NearByStores_NextLoad(event) {
        this.NearByStores_LoaderEvent = event;
        this.NearByStores_Setup();

    }
    private NearByStores_DelayTimer;
    NearByStores_DoSearch() {
        clearTimeout(this.NearByStores_DelayTimer);
        this.NearByStores_DelayTimer = setTimeout(x => {
            this.NearByStores_Data.TotalRecords = -1;
            this.NearByStores_Data.Offset = -1;
            this.NearByStores_Data.Limit = 20;
            this.NearByStores_Data.Data = [];
            this.NearByStores_Setup();
        }, 1000);
    }
    async ShowMyCode() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    OpenLocationSelector() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    // NavOrderCart() {
    //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.OrderManager.OrderCart);
    // }
    NavNotification() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.notifications);
    }

    // NavStore(Item) {
    //     if (this.checkUserExists == true) {
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
    //         if (this._AccConfig.IsLoanActive == true) {
    //             this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.planselector);
    //         }
    //         else {
    //             this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.loanprofile);
    //         }
    //     }
    //     else {
    //         this._HelperService.NotifyToastError('Please check your credit Limit');
    //     }
    // }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    NavDeals() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.List);
    }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }


    //#region BNPL
    BnplConfiguration = {
        IsConfigLoaded: false,
        IsBnplEnable: false,
        ReferenceId: 0,
        ReferenceKey: null,
        IsLoanActive: false,
        CreditLimit: 0,
        AvailableCredit: 0,
        ActiveLoan: 0,
        TotalLoan: 0,
        MaximumLoanAmount: 0,
        MinimumLoanAmount: 0,
        Plans: [],
        SystemConfig:
        {
            monoKey: null,
        }
    };
    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this.NavigateCustomer();
    }
    NavCreditCheck() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.checkCreditLimit);
    }
    NavigateCustomer() {
        if (this.BnplConfiguration.IsBnplEnable == true && this.BnplConfiguration.IsConfigLoaded == true) {
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.home);
            if (this.BnplConfiguration.IsLoanActive == true) {

                this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.planselector);
            }
            else {
                this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.loanprofile);
            }
        }
        else {
            if (this.BnplConfiguration.IsConfigLoaded == false) {
                this.ValidateCustomerBnpl(true);
            }
            else {
                this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.checkCreditLimit);
            }
        }
    }



    ValidateCustomerBnpl(NavigateCustomer = false) {
        var mode = this._HelperService.GetStorage('bnplmode');
        var pData = {
            Task: this._HelperService.AppConfig.NetworkApi.V3.Bnpl.getcustomerconfiguration,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            Mode: mode,
            user: {
                email: this._HelperService.AccountInfo.User.EmailAddress,
            }
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log(_Response);
                    this.BnplConfiguration = _Response.Result;
                    this.BnplConfiguration.IsConfigLoaded = true;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Bnpl.AccountConfig, this.BnplConfiguration);
                    if (NavigateCustomer == true) {
                        this.NavigateCustomer();
                    }
                } else {
                    this.BnplConfiguration.IsConfigLoaded = true;
                    this._HelperService.NotifySimple(_Response.Message);
                }
            }
        )
    }
    //#endregion
    // _AccConfig =
    //     {
    //         IsLoanActive: false,
    //         ActiveLoan: 0,
    //         MaximumLoanAmount: 0,
    //         MinimumLoanAmount: 0,
    //     }

    // GetAccountConfiguration() {
    //     this._HelperService.ShowSpinner('Please wait');
    //     this._HelperService.AppConfig.IsProcessing = true;
    //     var pData = {
    //         Task: this._HelperService.AppConfig.NetworkApi.V3.Bnpl.getcustomerconfiguration,
    //         AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
    //         AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Bnpl, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.HideSpinner();
    //             this._HelperService.AppConfig.IsProcessing = false;
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.BnPlAccountReference, _Response.Result);
    //                 this._AccConfig = _Response.Result;
    //             }
    //             else {
    //                 this._HelperService.NotifyToast(_Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HideSpinner();
    //             this._HelperService.AppConfig.IsProcessing = false;
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
}