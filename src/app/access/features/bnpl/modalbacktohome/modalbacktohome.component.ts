import { Component, Input, OnInit } from '@angular/core';
import { ModalController, AlertController, NavParams, NavController } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OUserInfo } from 'src/app/service/object.service';

@Component({
  selector: 'app-modalbacktohome',
  templateUrl: './modalbacktohome.component.html'
})
export class ModalbacktohomeComponent implements OnInit {

  checkUserExists: boolean;
  userDetails: any;
  public userInfo: OUserInfo;
  limitarray: any;
  // userId: any;
  constructor(
    public navParams: NavParams,
    public _ModalController: ModalController,
    public _AlertController: AlertController,
    public _HelperService: HelperService,
    public _NavController: NavController
  ) {
  }
  ngOnInit() { }


  async backToHome() {
    var account = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
    if (account != null) {
      var pData = {
        Task: 'checkuserexists',
        user: {
          email: account.User.EmailAddress
        }
      }
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.evolve, pData);
    _OResponse.subscribe(
      _Response => {
        this.checkUserExists = _Response.Result;
      }
    )


    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    this.ModalDismiss();
    this.availableCredit();
  }

  async ModalDismiss() {
    this._ModalController.dismiss({
      'dismissed': true
    })
  }


  availableCredit() {
    var pData = {
      Task: 'addbankstatements',
      transactions: this._HelperService.GetStorage('accountStatement'),
      balance: localStorage.getItem('accountBalance'),
      income: {
        average_income: localStorage.getItem('accountAverageIncome'),
        monthly_income: localStorage.getItem('accountAverageIncome'),
        yearly_income: localStorage.getItem('accountAverageIncome')
      }
    }
    var Account = this._HelperService.GetStorage('userInfo');
    if (Account != null) {
      this.userInfo = Account as OUserInfo;
      var userId = this.userInfo.Id;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.CheckLimit(this._HelperService.AppConfig.NetworkApi.V3.Bnpl.evolve, userId, pData);
      _OResponse.subscribe(
        _Response => {
          this.limitarray = _Response.Result;
          this._HelperService.SaveStorage('bankstatements', this.limitarray);
        }
      )
    }
  }
}