import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse } from 'src/app/service/object.service';

@Component({
  selector: 'app-modaldebitmandate',
  templateUrl: './modaldebitmandate.component.html'
})
export class ModaldebitmandateComponent implements OnInit {

  public DebitMandateRequest: any =
    {
      Status: "",
      Message: "",
      Data: {
        StatusCode: "",
        RequestId: "",
        MandateId: "",
        RemitaTransRef: "",
        LoanRequestId: "",
        Status: "",
        Message: "",
        AuthParams: [
          {
            Param1: "OTP",
            Description1: "Please enter your Bank OTP",
            Value1: "",
            Param2: "Please specify the last 4 digits of your bank card",
            Description2: "",
            Value2: "",
            Param3: "",
            Description3: "",
            Value3: "",
            Param4: "",
            Description4: "",
            Value4: "",
          }
        ]
      }
    }
  constructor(
    public navParams: NavParams,
    public _HelperService: HelperService,
    public _ModalController: ModalController

  ) {
  }

  ngOnInit() {
    this._HelperService.PageLoaded();
    this.DebitMandateRequest = this.navParams.data as any;
  }


  SubmitRequest() {
    var pData = this.DebitMandateRequest.Data.AuthParams[0];
    pData.loanRequestId = this.DebitMandateRequest.Data.LoanRequestId;
    pData.Task = this._HelperService.AppConfig.NetworkApi.V3.Bnpl.validateotp
    var items = 0;
    if (pData.Value1 != undefined && pData.Value1 != null && pData.Value1 != '') {
      items = items + 1;
    }
    if (pData.Value2 != undefined && pData.Value2 != null && pData.Value2 != '') {
      items = items + 1;
    }
    if (pData.Value3 != undefined && pData.Value3 != null && pData.Value3 != '') {
      items = items + 1;
    }
    if (pData.Value4 != undefined && pData.Value4 != null && pData.Value4 != '') {
      items = items + 1;
    }
    pData.reqs = items;
    var mode = this._HelperService.GetStorageValue('bnplmode');
    pData.Mode = mode;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Evolve, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
          this._ModalController.dismiss(true);
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.planconfirm);
        } else {
          this._HelperService.NotifySimple(_Response.Message);
        }
      }
    )
  }

  Cancel() {
    this._ModalController.dismiss(true);
  }
}
