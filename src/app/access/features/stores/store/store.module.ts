import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StorePage } from './store.component';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CountdownModule,
        RouterModule.forChild([
            {
                path: '',
                component: StorePage
            }
        ])
    ],
    declarations: [StorePage]
})
export class StorePageModule { }
