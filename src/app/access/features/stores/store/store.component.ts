import { Component, Sanitizer, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OListResponse } from '../../../../service/object.service';
import { TransactionDetailsModal } from '../../../modals/transactiondetails/transactiondetails.modal.component';
import { DomSanitizer } from "@angular/platform-browser";
import { DataService } from '../../../../service/data.service';
import { Geolocation } from "@ionic-native/geolocation/ngx";

declare var google;
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { ImagesGalModal } from '../../../modals/imagesgal/imagesgal.modal.component';
@Component({
    selector: 'app-store',
    templateUrl: 'store.component.html'
})
export class StorePage {
    slideOpts_Deals = {
        initialSlide: 0,
        autoplay: true,
        slidesPerView: 1.1,
        spaceBetween: 0
    };
    @ViewChild('map', { static: true }) mapElement: ElementRef;
    map: any;
    public ActiveTab = 1;
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _Geolocation: Geolocation,
        public _MenuController: MenuController,
        private _LaunchNavigator: LaunchNavigator,
        public _DataService: DataService,
        public _Sanitizer: DomSanitizer,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Store-Home");
    }
    public StoreInfo =
        {
            Distance: 0,
            MerchantIconUrl: null,
            MerchantId: null,
            MerchantKey: null,
            MerchantName: null,
            RewardPercentage: null,
            StoreAddress: null,
            StoreId: null,
            StoreKey: null,
            StoreLatitude: null,
            StoreLongitude: null,
            StoreName: null,
            AverageRating: 0,
            Categories: [],
            Description: null,
            PosterUrl: null,
            Images: [],
        }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this._HelperService.RefreshLocation();
        this.StoreInfo = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        if (this.StoreInfo.PosterUrl == undefined || this.StoreInfo.PosterUrl == null || this.StoreInfo.PosterUrl == '') {
            this.StoreInfo.PosterUrl = '../../../assets/images/bgempty.png';
        }
        if (this.StoreInfo.AverageRating == undefined || this.StoreInfo.AverageRating == null) {
            this.StoreInfo.AverageRating = 0;
        }
        this.loadMap();
        this.Table_Reviews_Config =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };

        this.NearByDeals_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.NearByDeals_Setup();
        this.Table_Reviews_GetData();
    }

    async ShowGallery() {
        if (this.StoreInfo.Images != undefined && this.StoreInfo.Images != null && this.StoreInfo.Images.length > 0) {
            var DataItem =
            {
                Title: this.StoreInfo.StoreName,
                Images: this.StoreInfo.Images
            }
            const modal = await this._ModalController.create({
                component: ImagesGalModal,
                componentProps: DataItem
            });
            modal.onDidDismiss().then(data => {
                console.log(data);
            });
            return await modal.present();
        }
        else {
            this._HelperService.NotifyToastError("Images not available");
        }
    }
    NavStoreDetails() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }
    loadMap() {
        var GeoLocationOptions = {
            maximumAge: 600000,
            timeout: 20000
        };
        var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        this._HelperService.AppConfig.ActiveLocation.Lat = DeviceStorage.Latitude;
        this._HelperService.AppConfig.ActiveLocation.Lon = DeviceStorage.Longitude;
        if (DeviceStorage != null) {
            this._Geolocation
                .getCurrentPosition()
                .then(resp => {
                    DeviceStorage.Latitude = resp.coords.latitude;
                    DeviceStorage.Longitude = resp.coords.longitude;
                    this._HelperService.AppConfig.ActiveLocation.Lat = resp.coords.latitude;
                    this._HelperService.AppConfig.ActiveLocation.Lon = resp.coords.longitude;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Device, DeviceStorage);
                    this.ShopMap();
                })
                .catch(error => { });
        }


        // this.TUCNearByMerchants.forEach(element => {
        //     let StorelatLng = new google.maps.LatLng(element.Latitude, element.Longitude);
        //     new google.maps.Marker({
        //         map: this.map,
        //         animation: google.maps.Animation.DROP,
        //         position: StorelatLng,
        //         icon: '../../../assets/images/storemarker.png'
        //     });
        // });
    }
    ShopMap() {
        let userlatLng = new google.maps.LatLng(this._HelperService.AppConfig.ActiveLocation.Lat, this._HelperService.AppConfig.ActiveLocation.Lon);
        let mapOptions = {
            center: userlatLng,
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            styles: this.JData
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        if (this._HelperService.AppConfig.ActiveLocation.Lat != undefined && this._HelperService.AppConfig.ActiveLocation.Lon != undefined) {
            new google.maps.Marker({
                map: this.map,
                animation: google.maps.Animation.DROP,
                position: userlatLng,
                icon: '../../../assets/images/mapcar.png'
            });
        }
        let StorelatLng = new google.maps.LatLng(this.StoreInfo.StoreLatitude, this.StoreInfo.StoreLongitude);
        new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: StorelatLng,
            icon: '../../../assets/images/storemarker.png'
        });
    }

    LocateStore() {
        let options: LaunchNavigatorOptions = {
            app: this._LaunchNavigator.APP.USER_SELECT,
            destinationName: this.StoreInfo.StoreName,
        };
        if (this.StoreInfo.StoreLatitude != undefined && this.StoreInfo.StoreLongitude != undefined) {
            var CoOrd = [this.StoreInfo.StoreLatitude, this.StoreInfo.StoreLongitude];
            this._LaunchNavigator.navigate(CoOrd, options)
                .then(
                    success => console.log('Launched navigator'),
                    error => console.log('Error launching navigator', error)
                );
        }
        else {
            this._LaunchNavigator.navigate(this.StoreInfo.StoreAddress, options)
                .then(
                    success => console.log('Launched navigator'),
                    error => console.log('Error launching navigator', error)
                );
        }
    }


    public UserRating = 0;
    public UserComment = "";
    onRatingChange(Rating) {
        this.UserRating = Rating;
    }

    PostReview() {
        if (this._HelperService.CheckIsLogin()) {

            if (this.UserRating == 0) {
                this._HelperService.NotifySimple('Please rate store to post review');
            }
            else {
                this._HelperService.ShowSpinner();
                var pData = {
                    Task: 'saveuserparameter',
                    TypeCode: "hcore.userreview",
                    Name: this.UserComment,
                    Value: this.UserRating,
                    CountValue: this.UserRating,
                    UserAccountKey: this.StoreInfo.StoreKey,
                    StatusCode: 'default.active',
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.HideSpinner();
                        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                            this._HelperService.NotifySimple('Thank U for your review');
                            this.UserRating = 0;
                            this.UserComment = null;
                            this.Table_Reviews_GetData();
                            // this.GetDetails();
                        }
                        else {
                            this._HelperService.NotifySimple("Unable to post review, try after some time");
                        }
                    },
                    _Error => {
                        this._HelperService.HideSpinner();
                        this._HelperService.HandleException(_Error);
                    });
            }
        }
        else {
            this._HelperService.NotifyToastError("Please login to post review.");
        }
    }

    public Table_Reviews_Config =
        {
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: [],
        };
    Table_Reviews_GetData() {
        if (this.Table_Reviews_Config.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.Table_Reviews_Config.Offset = 0;
        }
        var SCon = "";
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'TypeCode', 'text', 'hcore.userreview', "=");
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountKey', 'text', this.StoreInfo.StoreKey, "=");
        var pData = {
            Task: 'getuserparameters',
            TotalRecords: this.Table_Reviews_Config.TotalRecords,
            Offset: this.Table_Reviews_Config.Offset,
            Limit: this.Table_Reviews_Config.Limit,
            SearchCondition: SCon,
            SortExpression: 'CreateDate desc',
            RefreshCount: true,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // console.log(_Response);
                    this.Table_Reviews_Config.Offset = this.Table_Reviews_Config.Offset + this.Table_Reviews_Config.Limit;
                    this.Table_Reviews_Config.TotalRecords = _Response.Result.TotalRecords;
                    var Table_Reviews_Config = _Response.Result.Data;

                    Table_Reviews_Config.forEach(element => {
                        element.CreateDateS = this._HelperService.GetDateTimeS(element.CreateDate);
                        this.Table_Reviews_Config.Data.push(element);
                    });
                    if (this.Table_Reviews_LoaderEvent != undefined) {
                        this.Table_Reviews_LoaderEvent.target.complete();
                        if (this.Table_Reviews_Config.TotalRecords == this.Table_Reviews_Config.Data.length) {
                            this.Table_Reviews_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }


    public Table_Reviews_LoaderEvent: any = undefined;
    Table_Reviews_NextLoad(event) {
        this.Table_Reviews_LoaderEvent = event;
        this.Table_Reviews_GetData();
    }

    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    // NavDealerLocations() {
    //     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrderManager.DealerLocations);
    // }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }


    public NearByDeals_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    NearByDeals_Setup() {
        this._HelperService.ShowProgress();
        if (this.NearByDeals_Data.Offset == -1) {
            this.NearByDeals_Data.Offset = 0;
        }
        var SCon = "";
        if (this.NearByDeals_Data.SearchContent != undefined && this.NearByDeals_Data.SearchContent != null && this.NearByDeals_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition('', 'Title', 'text', this.NearByDeals_Data.SearchContent);
            // SCon = this._HelperService.GetSearchCondition(SCon, 'MerchantDisplayName', 'text', this.NearByDeals_Data.SearchContent);
        }
        if (this.NearByDeals_Data.ActiveCategoryId != null && this.NearByDeals_Data.ActiveCategoryId > 0) {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'CategoryId', 'number', this.NearByDeals_Data.ActiveCategoryId, "=");
        }
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'MerchantId', 'number', this.StoreInfo.MerchantId, "=");

        var pData = {
            Task: 'getdeals',
            TotalRecords: this.NearByDeals_Data.TotalRecords,
            Offset: this.NearByDeals_Data.Offset,
            Limit: this.NearByDeals_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
        };
        var FlashStorage = this._HelperService.GetStorage('dashdeals_' + this.StoreInfo.MerchantId);
        if (FlashStorage != undefined && FlashStorage != null) {
            this.NearByDeals_Data = FlashStorage;
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {

                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.NearByDeals_Data.Data = [];
                    this.NearByDeals_Data.Offset = this.NearByDeals_Data.Offset + this.NearByDeals_Data.Limit;
                    this.NearByDeals_Data.TotalRecords = _Response.Result.TotalRecords;
                    _Response.Result.Data.forEach(element => {
                        element.TotalSaving = Math.round(element.ActualPrice - element.Amount);
                        if (element.TotalSaving < 0) {
                            element.TotalSaving = 0;
                        }
                        element.leftTime = Math.round((new Date(element.EndDate).getTime() - new Date().getTime()) / 1000);
                        if (element.leftTime == NaN) {
                            element.leftTime = 0;
                        }
                        element.BackgroundColor = this._HelperService.CardColors[Math.floor(Math.random() * this._HelperService.CardColors.length)];
                        element.leftTime = 10;
                    });
                    this.NearByDeals_Data.Data = _Response.Result.Data;
                    this._HelperService.SaveStorage('dashdeals_' + this.StoreInfo.MerchantId, this.NearByDeals_Data);
                    // console.log(this.NearByDeals_Data.Data);
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }





    NavDealDetails(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
    }


    JData = [
        {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "saturation": "100"
                },
                {
                    "lightness": "-53"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#efefe9"
                }
            ]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ebe6e1"
                },
                {
                    "lightness": "0"
                }
            ]
        },
        {
            "featureType": "landscape.natural.landcover",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape.natural.terrain",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#cdec9d"
                }
            ]
        },
        {
            "featureType": "poi.attraction",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.attraction",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ff0000"
                }
            ]
        },
        {
            "featureType": "poi.business",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.business",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ee9334"
                }
            ]
        },
        {
            "featureType": "poi.government",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "lightness": "-22"
                },
                {
                    "saturation": "15"
                },
                {
                    "color": "#b8a1cd"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "saturation": "0"
                },
                {
                    "lightness": "-25"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "lightness": "-82"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": "0"
                },
                {
                    "lightness": "0"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "saturation": "1"
                },
                {
                    "lightness": "11"
                },
                {
                    "gamma": "2.60"
                },
                {
                    "color": "#aa85e5"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "gamma": "6.13"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "saturation": "75"
                },
                {
                    "color": "#818fe6"
                },
                {
                    "lightness": "14"
                },
                {
                    "gamma": "1.61"
                }
            ]
        }
    ]
}