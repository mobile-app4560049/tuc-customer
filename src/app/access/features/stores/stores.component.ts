import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../service/object.service';
import { DataService } from '../../../service/data.service';
import { DomSanitizer } from "@angular/platform-browser";
import { ScanMyCodeModal } from '../tucpay/scanmycode/scanmycode.modal.component';
@Component({
    selector: 'app-stores',
    templateUrl: 'stores.component.html'
})
export class StoresPage {
    async NavOpenQR() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }

    public UserCustomAddress =
        {
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityName: null,
            StateName: null,
            CountryName: null,
            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: null,
            Longitude: null,
        }

    public DeviceInformation: ODeviceInformation;
    constructor(
        public _MenuController: MenuController,
        public _Sanitizer: DomSanitizer,
        public _DataService: DataService,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ModalController: ModalController
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    NavTucPay() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode);
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Stores");
    }
    public TUCCategories: any[] = [];
    NavProfile() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        var UserCustomAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (UserCustomAddress != null) {
            this.UserCustomAddress = UserCustomAddress;
        }
        if (this._DataService.TUCCategories != undefined && this._DataService.TUCCategories != null && this._DataService.TUCCategories.length > 0) {
            var TCat: any[] = this._DataService.TUCCategories.sort((obj1, obj2) => {
                if (obj1.Merchants > obj2.Merchants) {
                    return -1;
                }
                if (obj1.Merchants < obj2.Merchants) {
                    return 1;
                }
                return 0;
            });
            this.TUCCategories = TCat;
        }
        var ActiveCategory = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TCategory);
        // if (ActiveCategory != undefined && ActiveCategory != null) {
        //     this.CategoryClick(ActiveCategory);
        // }
        // else {
        //     this.TUCNearByMerchants = this._DataService.TUCMerchants;
        // } 
        this.LoadData();
    }
    CategoryClick(Item) {
        this.NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
        this.NearByStores_Data.ActiveCategoryId = Item.ReferenceId;
        this.NearByStores_Setup();

    }
    CategoryClickAll() {
        this.NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
        this.NearByStores_Data.ActiveCategoryId = 0;
        this.NearByStores_Setup();

    }

    LoadData() {
        this.NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
        this.NearByStores_Setup();
    }
    public NearByStores_Data =
        {
            ActiveCategoryId: 0,
            SearchContent: "",
            TotalRecords: -1,
            Offset: -1,
            Limit: 20,
            Data: []
        };
    NearByStores_Setup() {
        this._HelperService.ShowProgress();

        if (this.NearByStores_Data.Offset == -1) {
            this.NearByStores_Data.Offset = 0;
        }
        var SCon = "";
        if (this.NearByStores_Data.SearchContent != undefined && this.NearByStores_Data.SearchContent != null && this.NearByStores_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition('', 'MerchantName', 'text', this.NearByStores_Data.SearchContent);
        }
        var pData = {
            Task: 'getnearbystores',
            TotalRecords: this.NearByStores_Data.TotalRecords,
            Offset: this.NearByStores_Data.Offset,
            Limit: this.NearByStores_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            Latitude: this._HelperService.AppConfig.ActiveLocation.Lat,
            Longitude: this._HelperService.AppConfig.ActiveLocation.Lon,
            SubReferenceId: this.NearByStores_Data.ActiveCategoryId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCCustomerApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.NearByStores_Data.Offset = this.NearByStores_Data.Offset + this.NearByStores_Data.Limit;
                    this.NearByStores_Data.TotalRecords = _Response.Result.TotalRecords;
                    var NearByStores_Data = _Response.Result.Data;
                    NearByStores_Data.forEach(element => {
                        this.NearByStores_Data.Data.push(element);
                    });
                    if (this.NearByStores_LoaderEvent != undefined) {
                        this.NearByStores_LoaderEvent.target.complete();
                        if (this.NearByStores_Data.TotalRecords == this.NearByStores_Data.Data.length) {
                            this.NearByStores_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public NearByStores_LoaderEvent: any = undefined;
    NearByStores_NextLoad(event) {
        this.NearByStores_LoaderEvent = event;
        this.NearByStores_Setup();

    }
    private NearByStores_DelayTimer;
    NearByStores_DoSearch() {
        clearTimeout(this.NearByStores_DelayTimer);
        this.NearByStores_DelayTimer = setTimeout(x => {
            this.NearByStores_Data.TotalRecords = -1;
            this.NearByStores_Data.Offset = -1;
            this.NearByStores_Data.Limit = 20;
            this.NearByStores_Data.Data = [];
            this.NearByStores_Setup();
        }, 1000);
    }


    // public TUCNearByMerchants: any[] = [];
    //ngOnInit() {
    //     this._HelperService.RefreshLocation();
    //     this._DataService.InitializeData();
    //     if (this._DataService.TUCCategories != undefined && this._DataService.TUCCategories != null && this._DataService.TUCCategories.length > 0) {
    //         var TCat: any[] = this._DataService.TUCCategories.sort((obj1, obj2) => {
    //             if (obj1.Merchants > obj2.Merchants) {
    //                 return -1;
    //             }
    //             if (obj1.Merchants < obj2.Merchants) {
    //                 return 1;
    //             }
    //             return 0;
    //         });
    //         this.TUCCategories = TCat;
    //     }
    //     var ActiveCategory = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.TCategory);
    //     if (ActiveCategory != undefined && ActiveCategory != null) {
    //         this.CategoryClick(ActiveCategory);
    //     }
    //     else {
    //         this.TUCNearByMerchants = this._DataService.TUCMerchants;
    //     }
    // }
    // CategoryClickAll() {
    //     this.TUCNearByMerchants = this._DataService.TUCMerchants;
    // }

    // CategoryClick(Item) {
    //     var CatMerchants = [];
    //     var MerchantsByCategory = this._DataService.TUCMerchantCategories.filter(x => x.CategoryId == Item.ReferenceId);
    //     if (MerchantsByCategory != undefined && MerchantsByCategory.length > 0) {
    //         MerchantsByCategory.forEach(element => {
    //             var TMerchant = this._DataService.TUCMerchants.find(x => x.ReferenceId == element.MerchantId);
    //             if (TMerchant != undefined) {
    //                 CatMerchants.push(TMerchant);
    //             }
    //         });
    //     }
    //     if (CatMerchants != undefined && CatMerchants.length > 0) {
    //         this.TUCNearByMerchants = CatMerchants;
    //     }
    //     else {
    //         this.TUCNearByMerchants = [];
    //     }
    // }

    // chunkify(a, n, balanced) {

    //     if (n < 2)
    //         return [a];

    //     var len = a.length,
    //         out = [],
    //         i = 0,
    //         size;

    //     if (len % n === 0) {
    //         size = Math.floor(len / n);
    //         while (i < len) {
    //             out.push(a.slice(i, i += size));
    //         }
    //     }

    //     else if (balanced) {
    //         while (i < len) {
    //             size = Math.ceil((len - i) / n--);
    //             out.push(a.slice(i, i += size));
    //         }
    //     }

    //     else {

    //         n--;
    //         size = Math.floor(len / n);
    //         if (len % size === 0)
    //             size--;
    //         while (i < size * n) {
    //             out.push(a.slice(i, i += size));
    //         }
    //         out.push(a.slice(size * n));

    //     }

    //     return out;
    // }



    // private delayTimer;
    // doSearch(text) {
    //     clearTimeout(this.delayTimer);
    //     this.delayTimer = setTimeout(x => {
    //         if (text != undefined && text != null && text != "") {
    //             text = text.trim().toLowerCase();
    //             this.TUCNearByMerchants = this._DataService.TUCMerchants.filter(x => x.DisplayName.toLowerCase().indexOf(text) > -1);
    //         }
    //         else {
    //             this.TUCNearByMerchants = this._DataService.TUCMerchants;
    //         }

    //     }, 1000);
    // }
    async ShowMyCode() {
        const modal = await this._ModalController.create({
            component: ScanMyCodeModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    OpenLocationSelector() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
    }
    // NavOrderCart() {
    //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.OrderManager.OrderCart);
    // }
    NavNotification() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.notifications);
    }

    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Store.Details);
    }

    NavDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    NavStores() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
    }
    NavDeals() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.List);
    }
    NavPoints() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
    }
    NavMore() {
        this._MenuController.open('end');
    }
}