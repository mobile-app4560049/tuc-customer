import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../../service/object.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
    templateUrl: 'scanmycode.modal.component.html',
    selector: 'modal-scanmycode'
})
export class ScanMyCodeModal {

    public _TransactionDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Title: null,

            ItemCode: null,
            CategoryName: null,

            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            ImageUrl: null,


            DealReferenceId: null,
            DealReferenceKey: null,

            Description: null,
            UsageInformation: null,
            Terms: null,

            MerchantReferenceId: null,
            MerchantReferenceKey: null,
            MerchantDisplayName: null,
            MerchantIconUrl: null,
            RedeemInstruction: null,


            StartDate: null,
            StartDateT: null,
            EndDate: null,
            EndDateT: null,


            UseDate: null,
            UseDateT: null,
            UseLocationId: null,
            UseLocationKey: null,
            UseLocationDisplayName: null,
            UseLocationAddress: null,

            CreateDate: null,
            CreateDateT: null,

            StatusCode: null,
            StatusName: null,
            Locations: [],
        };
    constructor(
        public _LaunchNavigator: LaunchNavigator,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
    }



    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }

}