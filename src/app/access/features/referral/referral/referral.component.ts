import { Component, Sanitizer, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation, OListResponse, OBalance } from '../../../../service/object.service';
import { DomSanitizer } from "@angular/platform-browser";
import { DataService } from '../../../../service/data.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
declare var google;
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { ReferralEditModal } from '../../../modals/referraledit/referraledit.modal.component';
@Component({
    selector: 'app-referral',
    templateUrl: 'referral.component.html'
})
export class ReferralDetailsPage {


    constructor(
        private _SocialSharing: SocialSharing,
        public _NavController: NavController,
        public _LaunchNavigator: LaunchNavigator,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService,
        public _Platform: Platform
    ) {
        this._HelperService.RefreshAppConfiguration();
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }
    Share(way) {
        var RefUrl = 'https://thankucash.com/download.html?uid=' + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // var RefUrl = '';
        // if (this._Platform.is("ios") || this._Platform.is("iphone") || this._Platform.is("ipad")) {
        //     RefUrl = "https://itunes.apple.com/us/app/thank-u-cash/id1386747905?mt=8&uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        // else {
        //     RefUrl = "https://play.google.com/store/apps/details?id=com.hm.thankucard?uid=" + this._HelperService.AccountInfo.UserAccount.ReferralCode;
        // }
        this._SocialSharing.share(this._HelperService._AppConfigurations.Referral.ShareMessageDescription, this._HelperService._AppConfigurations.Referral.ShareMessageTitle, null, RefUrl);
        // if (way == 'wa') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // else if (way == 'fb') {
        //     this._SocialSharing.shareViaWhatsApp("Earn Money On Every Purchase. Download ThankUCash App.", "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", RefUrl)
        // }
        // // else if (way == 'sms') {
        // //     // this._SocialSharing.shareViaSMS("Earn Money On Every Purchase. Download ThankUCash App. https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode, "https://storage.googleapis.com/cdn.thankucash.com/defaults/tucicon.png", "https://thankucash.com/download.html?uid="+this._HelperService.AccountInfo.UserAccount.ReferralCode)
        // // }
        // else {
        //     this._SocialSharing.share("Earn Money On Every Purchase. Download ThankUCash App.", "ThankUCash : Earn Money On Every Purchase", null,RefUrl);
        // }
    }

    async OpenEditModal() {
        const modal = await this._ModalController.create({
            component: ReferralEditModal,
        });
        modal.onDidDismiss().then(data => {

        });
        return await modal.present();
    }

    ModalDismiss() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }
    //
    //ModalDismiss
}