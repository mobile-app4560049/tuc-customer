import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams, Platform } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Market } from '@ionic-native/market/ngx';
@Component({
    templateUrl: 'appupdate.modal.component.html',
    selector: 'modal-appupdate'
})
export class AppUpdateModal {
    constructor(
        private market: Market,
        public _Platform: Platform,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {

    }

    FeaturesContent =
        {
            Version: null,
            Content: ""
        };
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.FeaturesContent = this.navParams.data as any;
    }
    OpenStore() {
        try {
            this.market.open("com.hm.thankucard");
        } catch (error) {
            if (this._Platform.is("android")) {
            } else if (this._Platform.is("ios")) {
            } else if (this._Platform.is("ipad")) {
            } else if (this._Platform.is("iphone")) {
            } else {
            }
        }
    }

    //ionic cordova plugin add cordova-plugin-facebook-connect --variable APP_ID="3544540802438966" --variable APP_NAME="ThankUCash"
    async ModalDismiss() {
    }
}
