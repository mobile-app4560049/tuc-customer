import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'buypointdetails.modal.component.html',
    selector: 'modal-buypointdetails'
})
export class BuyPointDetailsModal {

    public _TransactionDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Amount: 0,
            Charge: 0,
            TotalAmount: 0,
            TransactionDate: null,
            ReferenceNumber: null,
            PaymentMethodCode: null,
            PaymentMethodName: null,
            StatusCode: null,
            StatusName: null,
        };
    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._TransactionDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Amount: 0,
            Charge: 0,
            TotalAmount: 0,
            TransactionDate: null,
            ReferenceNumber: null,
            PaymentMethodCode: null,
            PaymentMethodName: null,
            StatusCode: null,
            StatusName: null,
        };
        this._TransactionDetails = this.navParams.data as any;
        // this._TransactionDetails.TransactionDate = this._HelperService.GetDateTimeS(this._TransactionDetails.TransactionDate);
        // this.GetTransactionDetails();
    }



    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }



    ProcessUssd_Confirm() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointverify",
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            Amount: this._TransactionDetails.Amount,
            PaymentReference: this._TransactionDetails.ReferenceNumber,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                    if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                        this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                    }
                    this._HelperService.NotifyToast(_Response.Message);
                    this._TransactionDetails.StatusName = _Response.Result.StatusName;
                    this._TransactionDetails.StatusCode = _Response.Result.StatusCode;
                    // this._HelperService.NavPaymentDetails();
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}