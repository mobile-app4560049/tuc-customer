import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
    templateUrl: 'imagesgal.modal.component.html',
    selector: 'modal-imagesgal'
})
export class ImagesGalModal {
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {

    }
    DataItem =
        {
            Title: null,
            Images: []
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.DataItem = this.navParams.data as any;
    }
    async ModalDismiss() {
        await this._ModalController.dismiss();
    }



}
