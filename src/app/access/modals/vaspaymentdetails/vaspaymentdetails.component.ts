import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'vaspaymentdetails.component.html',
    selector: 'modal-vaspaymentdetails'
})
export class VasPaymentDetailsModal {

    public _TransactionDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            ProductName: null,
            ProductItemName: null,
            ProductCategoryName: null,
            AccountNumber: null,
            Amount: null,
            UserRewardAmount: null,
            StartDate: null,
            EndDate: null,
            PaymentReference: null,
            Value: null,
            Charge: null,
            StatusCode: null,
            StatusName: null,
            TotalAmount: null,
        };
    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._TransactionDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            ProductName: null,
            ProductItemName: null,
            ProductCategoryName: null,
            AccountNumber: null,
            Amount: null,
            Charge: null,
            UserRewardAmount: null,
            StartDate: null,
            EndDate: null,
            TotalAmount: null,
            PaymentReference: null,
            Value: null,
            StatusCode: null,
            StatusName: null,
        };
        this._TransactionDetails = this.navParams.data as any;
        if (this._TransactionDetails.Charge == undefined || this._TransactionDetails.Charge == null) {
            this._TransactionDetails.Charge = 0;
        }
        if (this._TransactionDetails.UserRewardAmount == undefined || this._TransactionDetails.UserRewardAmount == null) {
            this._TransactionDetails.UserRewardAmount = 0;
        }
        if (this._TransactionDetails.TotalAmount == undefined || this._TransactionDetails.TotalAmount == null) {
            this._TransactionDetails.TotalAmount = this._TransactionDetails.Amount + this._TransactionDetails.Charge;
        }
        // this._TransactionDetails.StartDate = this._HelperService.GetDateTimeS(this._TransactionDetails.StartDate);
        // this.GetTransactionDetails();
    }



    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }



}