import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'deliveryaddress.modal.component.html',
    selector: 'modal-deliveryaddress'
})
export class DeliveryAddressModal {

    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    tItem = "";
    ItemDetails =
        {
            Name: null,
            Description: null,
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
}