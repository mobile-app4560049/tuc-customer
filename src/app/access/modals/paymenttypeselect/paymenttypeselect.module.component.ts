import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'paymenttypeselect.modal.component.html',
    selector: 'modal-paymenttypeselect'
})
export class PaymentTypeSelectModal {

    public PaymentConfiguration =
        {
            AccountNumber: null,
            MinAmount: 500,
            MaxAmount: 10000,
            ChargeType: "percentage",
            ChargeValue: 1.5,
            MaxChargeValue: 2500,
            FixedCharge: 100,
            RewardType: "percentage",
            RewardValue: 10,
            MaxRewardAmount: 100,
            UserCards: [],
            PaymentReference: null,
            PaymentType: null,
            PaymentAmount: 0,
            PaymentSource: 0,
            Task: 'confirmpayment',
            Type: 'buypoint',
            AccountId: null,
            AccountKey: null,
        }
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this.PaymentConfiguration =
        {
            AccountNumber: null,
            MinAmount: 500,
            MaxAmount: 10000,
            ChargeType: "percentage",
            ChargeValue: 1.5,
            MaxChargeValue: 2500,
            FixedCharge: 100,
            RewardType: "percentage",
            RewardValue: 10,
            MaxRewardAmount: 100,
            UserCards: [],
            PaymentReference: null,
            PaymentType: null,
            PaymentAmount: 0,
            PaymentSource: 0,
            Task: 'confirmpayment',
            Type: 'buypoint',
            AccountId: null,
            AccountKey: null,
        };
        this.PaymentConfiguration = this.navParams.data as any;
    }
    async ModalDismiss(Type) {
        this.PaymentConfiguration.PaymentSource = Type;
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
        await this._ModalController.dismiss(this.PaymentConfiguration);
    }
}