import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';

@Component({
    templateUrl: 'referraledit.modal.component.html',
    selector: 'modal-referraledit'
})
export class ReferralEditModal {
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {

    }
    tItem = "";


    ngOnInit() {
        this._HelperService.PageLoaded();
        // this.tItem = this._HelperService.FormatCardNumber(this._HelperService.AccountInfo.UserAccount.AccountCode);
        // this.MileStoneItem = this.navParams.data as any;
        // this._TransactionDetails = this.navParams.data as any;
    }


    async Confirm() {
        if (this.tItem == undefined || this.tItem == null || this.tItem == '') {
            this._HelperService.NotifyToast('Enter referral code');
        }
        else {
            var _RequestData = {
                Task: "updatereferralcode",
                ReferenceKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                ReferralCode: this.tItem,
            };
            this._HelperService.ShowSpinner('updating profile ...');
            try {
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Acc, _RequestData);
                _OResponse.subscribe(
                    (_Response) => {
                        this._HelperService.HideSpinner();
                        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                            this._HelperService.AccountInfo.UserAccount.ReferralCode = this.tItem;
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, this._HelperService.AccountInfo);
                            this._HelperService.NotifyToast('Referral code updated');
                            // await this._ModalController.dismiss(this.tItem);
                            this.Cancel();
                        } else {
                            this._HelperService.Notify('Update failed', _Response.Message);
                        }
                    },
                    (_Error) => {
                        this._HelperService.HideSpinner();
                        this._HelperService.HandleException(_Error);
                    }
                );
            } catch (_Error) {
                this._HelperService.HideSpinner();
                if (_Error.status == 0) {
                    this._HelperService.Notify('Operation failed', "Please check your internet connection");
                }
                else if (_Error.status == 401) {
                    var EMessage = JSON.parse(_Error._body).error;
                    this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
                }
                else {
                    this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
                }
            }
        }
    }

    async Cancel() {
        await this._ModalController.dismiss(this.tItem);
    }

}
