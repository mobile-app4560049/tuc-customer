import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'transactiondetails.modal.component.html',
    selector: 'modal-transactiondetails'
})
export class TransactionDetailsModal {

    public _TransactionDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,

            ParentKey: null,
            ParentDisplayName: null,
            ParentIconUrl: null,

            SubParentKey: null,
            SubParentDisplayName: null,
            SubParentAddress: null,
            SubParentLatitude: null,
            SubParentLongitude: null,
            SubParentIconUrl: null,
            ParentTransactionKey: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            UserAccountMobileNumber: null,
            UserAccountEmailAddress: null,
            UserAccountOwnerKey: null,
            UserAccountOwnerDisplayName: null,
            UserAccountOwnerIconUrl: null,
            UserAccountIconUrl: null,
            UserAccountTypeCode: null,
            UserAccountTypeName: null,
            ModeCode: null,
            ModeName: null,
            TypeCode: null,
            TypeName: null,
            SourceCode: null,
            SourceName: null,
            Amount: null,
            Charge: null,
            ComissionAmount: null,
            TotalAmount: null,
            TotalAmountPercentage: null,
            Balance: null,
            PurchaseAmount: null,
            ReferenceAmount: null,
            ReferenceInvoiceAmount: null,
            TransactionDate: null,
            InvoiceNumber: null,
            AccountNumber: null,
            ReferenceNumber: null,
            Comment: null,
            GroupKey: null,
            RequestKey: null,
            InvoiceKey: null,
            InvoiceItemKey: null,
            BankId: null,
            BankKey: null,
            BankDisplayName: null,
            BankIconUrl: null,
            ProviderId: null,
            ProviderKey: null,
            ProviderDisplayName: null,
            ProviderIconUrl: null,
            CardId: null,
            CardKey: null,
            CardName: null,
            CardBrandId: null,
            CardBrandKey: null,
            CardBrandDisplayName: null,
            CardSubBrandId: null,
            CardSubBrandKey: null,
            CardSubBrandName: null,
            CardTypeId: null,
            CardTypeKey: null,
            CardTypeName: null,
            CardBankId: null,
            CardBankKey: null,
            CardBankName: null,
            CashierId: null,
            CashierKey: null,
            CashierDisplayName: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
            StatusMessage: null,
            Items: [],
            RequestLog:
            {
                Request: null,
                Response: null,
            }

        };
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._TransactionDetails =
        {

            ReferenceId: 0,
            ReferenceKey: null,

            ParentKey: null,
            ParentDisplayName: null,
            ParentIconUrl: null,

            SubParentKey: null,
            SubParentDisplayName: null,
            SubParentAddress: null,
            SubParentLatitude: null,
            SubParentLongitude: null,
            SubParentIconUrl: null,
            ParentTransactionKey: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            UserAccountMobileNumber: null,
            UserAccountEmailAddress: null,
            UserAccountOwnerKey: null,
            UserAccountOwnerDisplayName: null,
            UserAccountOwnerIconUrl: null,
            UserAccountIconUrl: null,
            UserAccountTypeCode: null,
            UserAccountTypeName: null,
            ModeCode: null,
            ModeName: null,
            TypeCode: null,
            TypeName: null,
            SourceCode: null,
            SourceName: null,
            Amount: null,
            Charge: null,
            ComissionAmount: null,
            TotalAmount: null,
            TotalAmountPercentage: null,
            Balance: null,
            PurchaseAmount: null,
            ReferenceAmount: null,
            ReferenceInvoiceAmount: null,
            TransactionDate: null,
            InvoiceNumber: null,
            AccountNumber: null,
            ReferenceNumber: null,
            Comment: null,
            GroupKey: null,
            RequestKey: null,
            InvoiceKey: null,
            InvoiceItemKey: null,
            BankId: null,
            BankKey: null,
            BankDisplayName: null,
            BankIconUrl: null,
            ProviderId: null,
            ProviderKey: null,
            ProviderDisplayName: null,
            ProviderIconUrl: null,
            CardId: null,
            CardKey: null,
            CardName: null,
            CardBrandId: null,
            CardBrandKey: null,
            CardBrandDisplayName: null,
            CardSubBrandId: null,
            CardSubBrandKey: null,
            CardSubBrandName: null,
            CardTypeId: null,
            CardTypeKey: null,
            CardTypeName: null,
            CardBankId: null,
            CardBankKey: null,
            CardBankName: null,
            CashierId: null,
            CashierKey: null,
            CashierDisplayName: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
            StatusMessage: null,
            Items: [],
            RequestLog:
            {
                Request: null,
                Response: null,
            }

        };
        this._TransactionDetails = this.navParams.data as any;
        this.GetTransactionDetails();
    }



    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }


    GetTransactionDetails() {

        var pData = {
            Task: 'gettransaction',
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._TransactionDetails.ReferenceKey, '='),
        };

        this._HelperService.ShowSpinner('Getting transaction details...');
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._TransactionDetails =
                {

                    ReferenceId: 0,
                    ReferenceKey: null,

                    ParentKey: null,
                    ParentDisplayName: null,
                    ParentIconUrl: null,

                    SubParentKey: null,
                    SubParentDisplayName: null,
                    SubParentAddress: null,
                    SubParentLatitude: null,
                    SubParentLongitude: null,
                    SubParentIconUrl: null,
                    ParentTransactionKey: null,
                    UserAccountKey: null,
                    UserAccountDisplayName: null,
                    UserAccountMobileNumber: null,
                    UserAccountEmailAddress: null,
                    UserAccountOwnerKey: null,
                    UserAccountOwnerDisplayName: null,
                    UserAccountOwnerIconUrl: null,
                    UserAccountIconUrl: null,
                    UserAccountTypeCode: null,
                    UserAccountTypeName: null,
                    ModeCode: null,
                    ModeName: null,
                    TypeCode: null,
                    TypeName: null,
                    SourceCode: null,
                    SourceName: null,
                    Amount: null,
                    Charge: null,
                    ComissionAmount: null,
                    TotalAmount: null,
                    TotalAmountPercentage: null,
                    Balance: null,
                    PurchaseAmount: null,
                    ReferenceAmount: null,
                    ReferenceInvoiceAmount: null,
                    TransactionDate: null,
                    InvoiceNumber: null,
                    AccountNumber: null,
                    ReferenceNumber: null,
                    Comment: null,
                    GroupKey: null,
                    RequestKey: null,
                    InvoiceKey: null,
                    InvoiceItemKey: null,
                    BankId: null,
                    BankKey: null,
                    BankDisplayName: null,
                    BankIconUrl: null,
                    ProviderId: null,
                    ProviderKey: null,
                    ProviderDisplayName: null,
                    ProviderIconUrl: null,
                    CardId: null,
                    CardKey: null,
                    CardName: null,
                    CardBrandId: null,
                    CardBrandKey: null,
                    CardBrandDisplayName: null,
                    CardSubBrandId: null,
                    CardSubBrandKey: null,
                    CardSubBrandName: null,
                    CardTypeId: null,
                    CardTypeKey: null,
                    CardTypeName: null,
                    CardBankId: null,
                    CardBankKey: null,
                    CardBankName: null,
                    CashierId: null,
                    CashierKey: null,
                    CashierDisplayName: null,
                    CreateDate: null,
                    CreatedByKey: null,
                    CreatedByDisplayName: null,
                    CreatedByIconUrl: null,
                    ModifyDate: null,
                    ModifyByKey: null,
                    ModifyByDisplayName: null,
                    ModifyByIconUrl: null,
                    StatusId: null,
                    StatusCode: null,
                    StatusName: null,
                    StatusI: null,
                    StatusB: null,
                    StatusC: null,
                    StatusMessage: null,
                    Items: [],
                    RequestLog:
                    {
                        Request: null,
                        Response: null,
                    }

                };
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._TransactionDetails = _Response.Result;
                    if (this._TransactionDetails.RequestLog != undefined) {
                        if (this._TransactionDetails.RequestLog.Request != undefined && this._TransactionDetails.RequestLog.Request != null && this._TransactionDetails.RequestLog.Request != '') {
                            this._TransactionDetails.RequestLog.Request = JSON.parse(JSON.parse(this._TransactionDetails.RequestLog.Request));
                        }
                        if (this._TransactionDetails.RequestLog.Response != undefined && this._TransactionDetails.RequestLog.Response != null && this._TransactionDetails.RequestLog.Response != '') {
                            this._TransactionDetails.RequestLog.Response = JSON.parse(this._TransactionDetails.RequestLog.Response);
                        }
                    }
                    this._TransactionDetails.TransactionDate = this._HelperService.GetDateTimeS(this._TransactionDetails.TransactionDate);
                    this._TransactionDetails.CreateDate = this._HelperService.GetDateTimeS(this._TransactionDetails.CreateDate);
                    this._TransactionDetails.ModifyDate = this._HelperService.GetDateTimeS(this._TransactionDetails.ModifyDate);
                    this._TransactionDetails.TypeName = this._TransactionDetails.TypeName.replace('Reward', '');
                    this._TransactionDetails.Items.forEach(element => {
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        element.CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
                        element.ModifyDate = this._HelperService.GetDateTimeS(element.ModifyDate);
                    });
                }
                else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}