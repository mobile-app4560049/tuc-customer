import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'milestonedetails.modal.component.html',
    selector: 'modal-milestonedetails'
})
export class MileStoneDetailsModal {

    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    tItem = "";

    MileStoneItem =
        {
            Title: "purple",
            SystemName: "purple",
            Description: "",
            Limit: 0
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.tItem = this._HelperService.FormatCardNumber(this._HelperService.AccountInfo.UserAccount.AccountCode);
        this.MileStoneItem = this.navParams.data as any;
        // this._TransactionDetails = this.navParams.data as any;
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
}