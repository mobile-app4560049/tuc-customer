import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'billerpackageselect.modal.component.html',
    selector: 'modal-billerpackageselect'
})
export class BillerPackageSelectModal {
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this.GetTransactionDetails(this.navParams.data);
    }
    async ModalDismiss(Type) {
        if (Type != 'noaction') {
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Package, Type);
        }
        await this._ModalController.dismiss(Type);
    }

    public Items = [];

    GetTransactionDetails(Item) {
        var pData = {
            Task: 'getbillerpackages',
            Type: Item.slug
        };
        this._HelperService.ShowSpinner('Getting details...');
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Items = _Response.Result;
                    // alert(JSON.stringify(this.Items));
                }
                else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}