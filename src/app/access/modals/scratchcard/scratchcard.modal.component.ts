import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { ScratchCard, SCRATCH_TYPE } from 'scratchcard-js'

@Component({
    templateUrl: 'scratchcard.modal.component.html',
    selector: 'modal-scratchcard',
    styles: ['.modal-wrapper{ background: transparent !important; -background: transparent !important; }']
})
export class ScratchCardModal {

    public _TransactionDetails =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Title: null,

            ItemCode: null,
            CategoryName: null,

            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            ImageUrl: null,


            DealReferenceId: null,
            DealReferenceKey: null,

            Description: null,
            UsageInformation: null,
            Terms: null,

            MerchantReferenceId: null,
            MerchantReferenceKey: null,
            MerchantDisplayName: null,
            MerchantIconUrl: null,
            RedeemInstruction: null,


            StartDate: null,
            StartDateT: null,
            EndDate: null,
            EndDateT: null,


            UseDate: null,
            UseDateT: null,
            UseLocationId: null,
            UseLocationKey: null,
            UseLocationDisplayName: null,
            UseLocationAddress: null,

            CreateDate: null,
            CreateDateT: null,

            StatusCode: null,
            StatusName: null,
            Locations: [],
        };
    constructor(
        public _LaunchNavigator: LaunchNavigator,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ShowImage = false;

    AmountInfo =
        {
            ReferenceId: 0,
            TotalAmount: 0,
        };
    ngOnInit() {
        this._HelperService.PageLoaded();
        var Details = this.navParams.data as any;
        this.AmountInfo.ReferenceId = Details.ReferenceId;
        this.AmountInfo.TotalAmount = Math.round(Details.TotalAmount);
        this.createNewScratchCard();
    }


    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }


    createNewScratchCard() {
        const scContainer = document.getElementById('js--sc--container')
        const sc = new ScratchCard('#js--sc--container', {
            scratchType: SCRATCH_TYPE.CIRCLE,
            containerWidth: 256,//scContainer.offsetWidth,
            containerHeight: 256,
            imageForwardSrc: '../../../assets/images/sc.png',
            imageBackgroundSrc: '../../../assets/images/sco.png',
            htmlBackground: '<div class="cardamountcss"><div class="won-amnt">' + this.AmountInfo.TotalAmount + '</div><div class="won-text">Points Won!</div></div>',
            clearZoneRadius: 30,
            nPoints: 20,
            pointSize: 4,
            callback: () => {
                this.ProcessReward();
            }
        })
        // Init
        sc.init();
    }

    ProcessReward() {
        var _RequestData = {
            Task: "validatereferralbonus",
            ReferenceKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            ReferenceId: this.AmountInfo.ReferenceId,
            Amount: this.AmountInfo.TotalAmount,
        };
        this._HelperService.ShowSpinner('Please wait');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Acc, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.HideSpinner();

                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifyToast(_Response.Message);
                        this.ShowImage = true;
                        setTimeout(() => {
                            this.ShowImage = false;
                            this.ModalDismiss('');
                        }, 5000);
                    } else {
                        this._HelperService.Notify('Update failed', _Response.Message);
                        this.ModalDismiss('');
                    }
                },
                (_Error) => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this.ModalDismiss('');
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }
}