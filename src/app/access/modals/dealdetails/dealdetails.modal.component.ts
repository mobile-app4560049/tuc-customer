import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
    templateUrl: 'dealdetails.modal.component.html',
    selector: 'modal-dealdetails'
})
export class DealDetailsModal {
    public acttab = 'validity';
    TabChange(type) {
        this.acttab = type;
    }
    public _PurchaseDetails: any =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Title: null,

            ItemCode: null,
            CategoryName: null,

            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            ImageUrl: null,

            Comment: null,
            DealReferenceId: null,
            DealReferenceKey: null,

            Description: null,
            UsageInformation: null,
            Terms: null,

            AccountIconUrl: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            AccountMobileNumber: null,


            MerchantReferenceId: null,
            MerchantReferenceKey: null,
            MerchantDisplayName: null,
            MerchantIconUrl: null,
            RedeemInstruction: null,


            SecondaryCode: null,
            SecondaryCodeMessage: null,

            SharedCustomerId: null,
            SharedCustomerKey: null,
            SharedCustomerDisplayName: null,
            SharedCustomerMobileNumber: null,
            SharedCustomerIconUrl: null,

            StartDate: null,
            StartDateT: null,
            EndDate: null,
            EndDateT: null,


            UseDate: null,
            UseDateT: null,
            UseLocationId: null,
            UseLocationKey: null,
            UseLocationDisplayName: null,
            UseLocationAddress: null,

            CreateDate: null,
            CreateDateT: null,

            StatusCode: null,
            StatusName: null,
            Locations: [],
            DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
            DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,
        };
    constructor(
        public _LaunchNavigator: LaunchNavigator,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.PageLoaded();
        this._PurchaseDetails =
        {

            ReferenceId: 0,
            ReferenceKey: null,
            Title: null,

            ItemCode: null,
            CategoryName: null,

            ActualPrice: null,
            SellingPrice: null,
            Amount: null,
            ImageUrl: null,


            AccountIconUrl: null,
            AccountId: null,
            AccountKey: null,
            AccountDisplayName: null,
            AccountMobileNumber: null,

            DealReferenceId: null,
            DealReferenceKey: null,

            Description: null,
            UsageInformation: null,
            Terms: null,

            MerchantReferenceId: null,
            MerchantReferenceKey: null,
            MerchantDisplayName: null,
            MerchantIconUrl: null,
            RedeemInstruction: null,


            StartDate: null,
            StartDateT: null,
            EndDate: null,
            EndDateT: null,


            UseDate: null,
            UseDateT: null,
            UseLocationId: null,
            UseLocationKey: null,
            UseLocationDisplayName: null,
            UseLocationAddress: null,

            CreateDate: null,
            CreateDateT: null,

            StatusCode: null,
            StatusName: null,
            Comment: null,
            Locations: [],



            SecondaryCode: null,
            SecondaryCodeMessage: null,

            SharedCustomerId: null,
            SharedCustomerKey: null,
            SharedCustomerDisplayName: null,
            SharedCustomerMobileNumber: null,
            SharedCustomerIconUrl: null,
            DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
            DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,
        };
        this._PurchaseDetails = this.navParams.data as any;
        this.GetTransactionDetails();
    }



    public TimerConfig =
        {
            leftTime: 0,
        }

    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
        this._HelperService.NavDashboardDeals();
    }


    GetTransactionDetails() {
        this._HelperService.ShowProgress();
        var pData = {
            Task: "getdealcode",
            ReferenceId: this._PurchaseDetails.ReferenceId,
            ReferenceKey: this._PurchaseDetails.ReferenceKey,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideProgress();
                this._PurchaseDetails =
                {

                    ReferenceId: 0,
                    ReferenceKey: null,
                    Title: null,

                    ItemCode: null,
                    CategoryName: null,

                    ActualPrice: null,
                    SellingPrice: null,
                    Amount: null,
                    ImageUrl: null,

                    Comment: null,

                    AccountIconUrl: null,
                    AccountId: null,
                    AccountKey: null,
                    AccountDisplayName: null,
                    AccountMobileNumber: null,

                    DealReferenceId: null,
                    DealReferenceKey: null,

                    Description: null,
                    UsageInformation: null,
                    Terms: null,

                    MerchantReferenceId: null,
                    MerchantReferenceKey: null,
                    MerchantDisplayName: null,
                    MerchantIconUrl: null,

                    RedeemInstruction: null,

                    StartDate: null,
                    StartDateT: null,
                    EndDate: null,
                    EndDateT: null,

                    SecondaryCode: null,
                    SecondaryCodeMessage: null,

                    SharedCustomerId: null,
                    SharedCustomerKey: null,
                    SharedCustomerDisplayName: null,
                    SharedCustomerMobileNumber: null,
                    SharedCustomerIconUrl: null,

                    UseDate: null,
                    UseDateT: null,
                    UseLocationId: null,
                    UseLocationKey: null,
                    UseLocationDisplayName: null,
                    UseLocationAddress: null,

                    CreateDate: null,
                    CreateDateT: null,

                    StatusCode: null,
                    StatusName: null,
                    Locations: [],

                    DealTypeCode: this._HelperService.AppConfig.DealTypeCode.ServiceDeal,
                    DeliveryTypeCode: this._HelperService.AppConfig.DeliveryTypeCode.InStorePickUp,

                };
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._PurchaseDetails = _Response.Result;
                    this._PurchaseDetails.StartDateT = this._HelperService.GetDateTimeS(this._PurchaseDetails.StartDate);
                    this._PurchaseDetails.EndDateT = this._HelperService.GetDateTimeS(this._PurchaseDetails.EndDate);
                    this._PurchaseDetails.CreateDateT = this._HelperService.GetDateTimeS(this._PurchaseDetails.CreateDate);
                    this.TimerConfig.leftTime = Math.round((new Date(this._PurchaseDetails.EndDate).getTime() - new Date().getTime()) / 1000);
                }
                else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    LocateStore(Item) {
        let options: LaunchNavigatorOptions = {
            app: this._LaunchNavigator.APP.USER_SELECT,
            destinationName: Item.DisplayName,
        };
        if (Item.Latitude != undefined && Item.Longitude != undefined) {
            var CoOrd = [Item.StoreLatitude, Item.StoreLongitude];
            this._LaunchNavigator.navigate(CoOrd, options)
                .then(
                    success => console.log('Launched navigator'),
                    error => console.log('Error launching navigator', error)
                );
        }
        else {
            this._HelperService.NotifyToast('Store location not available')
        }
    }



    UpdateComment() {
        if (this._PurchaseDetails.Comment == undefined || this._PurchaseDetails.Comment == null || this._PurchaseDetails.Comment == "") {
            this._HelperService.NotifyToastError('Enter comment');
        }
        else {
            this._HelperService.ShowProgress();
            var pData = {
                Task: "updatedealcodecomment",
                ReferenceId: this._PurchaseDetails.ReferenceId,
                ReferenceKey: this._PurchaseDetails.ReferenceKey,
                Comment: this._PurchaseDetails.Comment
            };

            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideProgress();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifyToastSuccess(_Response.Message);
                    }
                    else {
                        this._HelperService.NotifyToastError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }

    }




    public SharedMobileNumber = null;
    async ShareDeal() {
        if (this.SharedMobileNumber == undefined || this.SharedMobileNumber == null || this.SharedMobileNumber == "") {
            this._HelperService.NotifyToastError('Enter mobile number');
        }
        else {
            let alert = await this._AlertController.create({
                header: 'Confirm deal sharing',
                message: 'Once deal is shared it cannot be cancelled. Do you want to continue ?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'text-light  alert-btn',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Confirm',
                        cssClass: 'text-primary alert-btn',
                        handler: () => {
                            this._HelperService.ShowProgress();
                            this._HelperService.IsFormProcessing = true;
                            var pData = {
                                Task: "sharedealcode",
                                ReferenceId: this._PurchaseDetails.ReferenceId,
                                ReferenceKey: this._PurchaseDetails.ReferenceKey,
                                MobileNumber: this.SharedMobileNumber,
                            };

                            let _OResponse: Observable<OResponse>;
                            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
                            _OResponse.subscribe(
                                _Response => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HideProgress();
                                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                                        this._HelperService.ShowModalSuccess("Deal Shared successfully", "Deal shared to " + this.SharedMobileNumber + " successfully");
                                        this.GetTransactionDetails();
                                    }
                                    else {
                                        this._HelperService.NotifyToastError(_Response.Message);
                                    }
                                },
                                _Error => {
                                    this._HelperService.IsFormProcessing = false;
                                    this._HelperService.HandleException(_Error);
                                });
                        }
                    }
                ]
            });
            alert.present();


        }

    }

}