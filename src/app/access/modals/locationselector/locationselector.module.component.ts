import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
    templateUrl: 'locationselector.modal.component.html',
    selector: 'modal-locationselector'
})
export class LocationSelectorModal {
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {

    }
    tItem = "";

    MileStoneItem =
        {
            Title: "purple",
            SystemName: "purple",
            Description: "",
            Limit: 0
        }
    ngOnInit() {
        this._HelperService.PageLoaded();
        // this.tItem = this._HelperService.FormatCardNumber(this._HelperService.AccountInfo.UserAccount.AccountCode);
        // this.MileStoneItem = this.navParams.data as any;
        // this._TransactionDetails = this.navParams.data as any;
    }
    async ModalDismiss(Type) {
        this._HelperService.AppConfig.DeliveryLocation.Lat = this.UserCustomAddress.Latitude;
        this._HelperService.AppConfig.DeliveryLocation.Lon = this.UserCustomAddress.Longitude;
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this.UserCustomAddress);
        await this._ModalController.dismiss(this.UserCustomAddress);
    }

    public UserCustomAddress =
        {
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityName: null,
            StateName: null,
            CountryName: null,
            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: null,
            Longitude: null,
        }

    public Form_UpdateUser_AddressChange(address: Address) {
        this.UserCustomAddress.Latitude = address.geometry.location.lat();
        this.UserCustomAddress.Longitude = address.geometry.location.lng();
        this.UserCustomAddress.MapAddress = address.formatted_address;
        this.UserCustomAddress.AddressLine1 = address.formatted_address;
        address.address_components.forEach(address_component => {
            if (address_component.types[0] == "locality") {
                this.UserCustomAddress.CityName = address_component.long_name;
            }
            if (address_component.types[0] == "country") {
                this.UserCustomAddress.CountryName = address_component.long_name;
            }
            if (address_component.types[0] == "postal_code") {
                this.UserCustomAddress.ZipCode = address_component.long_name;
            }

            if (address_component.types[0] == "administrative_area_level_1") {
                this.UserCustomAddress.StateName = address_component.long_name;
            }
        });
    }


}
