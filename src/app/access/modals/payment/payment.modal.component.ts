import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime, NavParams } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OBalance } from '../../../service/object.service';
import { PaymentTypeSelectModal } from '../../modals/paymenttypeselect/paymenttypeselect.module.component';
import { DataService } from '../../../service/data.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
declare var moment: any;
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { PaystackOptions } from 'angular4-paystack';
import { Flutterwave, InlinePaymentOptions, PaymentSuccessResponse } from "flutterwave-angular-v3"

@Component({
    selector: 'app-modal-payment',
    templateUrl: 'payment.modal.component.html',
    styles: [
        `
        .input{
    border: 1px solid #efefef;
    background-color: #fff;
    margin-top: 8px;
    border-radius: 6px;
    padding:16px !important;
    font-size: 14px;
    margin-bottom: 16px;
    box-shadow: none;
}
        `
    ]
})

export class PaymentManagerModal {
    make_payment_button: boolean = true; //helps to add validation to selection payment mode
    SuccessflutterwaveModalClose =false;
    paymentModes: any[] = [];
    public StoreInfo ={
        ReferenceId:0,
        ReferenceKey:null,
        AccountId:null
    }
    userCountry='';
    public DeviceInformation: ODeviceInformation;
    constructor(
        private faio: FingerprintAIO,
        public _NavParameters: NavParams,
        public callNumber: CallNumber,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
        private flutterwave: Flutterwave
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        let userdetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
        this.userCountry=userdetails.UserCountry.CountryName;
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public PaymentDetails =
        {
            PaymentTitle: 'Make payment',
            PaymentSubtitle: 'process payment to proceed',
            PaymentAmount: 0,
            PaymentRequestFrom:"",
            WalletBalance: 0,
            RemainingAmount: 0,
            WalletUsageAmount: 0,
            AccountId: 0,
            AccountKey: null,
        }
    public PaymentConfiguration =
        {
            AccountId: null,
            AccountKey: null,
            Amount: 0,
            Charge: 0,
            TotalAmount: 0,
            PaymentMode: 'paystack',
            PaymentReference: null,
            ReferenceId: null,
            TransactionDate: null,
            StatusName: null,
            StatusCode: null,
            UserCards: [],
            Banks: [],
            Balance:
            {
                Credit: 0,
                Debit: 0,
                Balance: 0
            },
            EmailAddress: '',
            PaystackKey: '',
        }
    public ionViewDidEnter() {
        // this._HelperService.SetPageName("Payment-BuyPoints-Initialize");
    }

    //     /**
    //  * Amount to withdraw (in kobo for NGN)
    //  */
    //      : number;
    //      /**
    //       * A flat fee to charge the subaccount for this transaction, in kobo.
    //       */
    //      transaction_charge?: number;
    //      /**
    //       * Your pubic Key from Paystack. Use test key for test mode and live key for live mode
    //       */
    //      key?: string;
    //      /**
    //       * The customer's email address
    //       */
    //      email: string;
    //      /**
    //       * Unique case sensitive transaction reference. Only -,., = and alphanumeric characters allowed.
    //       */
    //      ref: string;





    customerDetails = {
    name:'',
    email:'',
    phone_number:'',
   }
    public IsBalanceLoaded = false;
    _PaystackOptions: PaystackOptions =
        {
            email: '',
            ref: '',
            key: '',
            amount: 0,
            currency: '',
            metadata:
            {

            }
        }
        
   
     _FllutterwavePaymentOptions:InlinePaymentOptions={
            public_key: '',
            tx_ref: '',
            amount: 0,
            currency: ''

     }   
    ngOnInit() {
        this._HelperService.PageLoaded();
        this.Stage = 0;
        this.PaymentDetails = this._NavParameters.data as any;
        if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
            this.PaymentDetails.AccountId = this._HelperService.AccountInfo.UserAccount.AccountId;
            this.PaymentDetails.AccountKey = this._HelperService.AccountInfo.UserAccount.AccountKey;
        }
        this.PaymentConfiguration.Amount = this.PaymentDetails.PaymentAmount;
        this.PaymentDetails.RemainingAmount = this.PaymentDetails.PaymentAmount;
        if(this.PaymentDetails.PaymentRequestFrom == "Deals"){ //if condition helps to enalble or disable the ion-select(MULTIPLE PAYMENTS) element in the UI 
            this.paymentModes=[{
                mode: 'Paystack',
            }, {
                mode: 'Flutterwave',
            }];
            this.make_payment_button=false
            this.StoreInfo = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveDealReference);
        }else {
            this.paymentModes=[{
                mode: 'Paystack',
            }];
        }
        this.GetBalance();
    }

    public GetBalance() {
        if (this.PaymentDetails.AccountId > 0) {
            this.IsBalanceLoaded = false;
            this._HelperService.ShowSpinner('Loading balance')
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: 'getbalance',
                AccountId: this.PaymentDetails.AccountId,
                UserAccountKey: this.PaymentDetails.AccountKey,
                Source: this._HelperService.AppConfig.TransactionSource.App,
                Type: 'tuc',
                ForceNavigation: false,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Acc, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        var Balance = _Response.Result.ThankUCash;
                        this.PaymentDetails.WalletBalance = Balance.Balance;
                        if (_Response.Result.PaymentMode != undefined && _Response.Result.PaymentMode != null) {
                            this.PaymentMode = _Response.Result.PaymentMode;
                        }
                        else {
                            this.PaymentMode = 'card';
                        }
                        this.IsBalanceLoaded = true;
                    }
                    else {
                        this._PaymentStatus =
                        {
                            Status: 'error',
                            Balance: 0,
                        }
                        this.ModalDismiss(this._PaymentStatus);
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._PaymentStatus =
                    {
                        Status: 'error',
                        Balance: 0,
                    }
                    this.ModalDismiss(this._PaymentStatus);
                    this._HelperService.HandleException(_Error);
                });
        }
        else {
            this.IsBalanceLoaded = true;
        }
    }

    CancelPayment() {
        this._PaymentStatus =
        {
            Status: 'error',
            Balance: 0,
        }
        this.ModalDismiss(this._PaymentStatus);
    }
    public QuickAmountClick(Amount) {
        this.PaymentConfiguration.Amount = Amount;
        this.CalculateReward();
    }

    CalculateReward() {
    }

    public IsWalletBalanceUse = false;
    UseTucWalletClick() {
        if (this.IsWalletBalanceUse == true) {
            this.IsWalletBalanceUse = false;
            this.PaymentMode = 'ussd';
            this.PaymentDetails.RemainingAmount = this.PaymentDetails.PaymentAmount;
            this.PaymentConfiguration.Amount = this.PaymentDetails.PaymentAmount;
        }
        else {
            this.IsWalletBalanceUse = true;
            if (this.PaymentDetails.WalletBalance >= this.PaymentDetails.PaymentAmount) {
                this.PaymentDetails.RemainingAmount = 0;
                this.PaymentDetails.WalletUsageAmount = this.PaymentDetails.PaymentAmount;
                this.PaymentMode = 'wallet';
            }
            else {
                this.PaymentDetails.RemainingAmount = Math.round(((this.PaymentDetails.PaymentAmount - this.PaymentDetails.WalletBalance) + Number.EPSILON) * 100) / 100;
                this.PaymentDetails.WalletUsageAmount = this.PaymentDetails.WalletBalance;
            }
            this.PaymentConfiguration.Amount = this.PaymentDetails.RemainingAmount;
        }
    }
    PaymentMode = 'ussd';
    PaymentModeSelected(Mode) {
        this.PaymentMode = Mode;     
        this.IsWalletBalanceUse = false;
        this.selectedPaymentMode ='';
        if(this.PaymentDetails.PaymentRequestFrom == "Deals" && this.PaymentMode == 'card'){
            this.make_payment_button = false;
        }
        this.PaymentDetails.RemainingAmount = this.PaymentDetails.PaymentAmount;
        this.PaymentConfiguration.Amount = this.PaymentDetails.RemainingAmount;
    }

    ProcessPayment() {
        if (this.PaymentDetails.RemainingAmount == 0) {
            this._PaymentStatus =
            {
                Status: 'success',
                Balance: this.PaymentDetails.WalletBalance,
            }
            this.ModalDismiss(this._PaymentStatus);
        }else if(this.make_payment_button == false){
            this._HelperService.NotifyToast("Please Select Payment Mode");

        }
        else {
            this.InitializeTransaction();
        }
    }

    async InitializeTransaction() {
        if (this.PaymentConfiguration.Amount == undefined || this.PaymentConfiguration.Amount == null || isNaN(this.PaymentConfiguration.Amount) == true) {
            this._HelperService.Notify('Invalid amount', "Please enter valid amount for purchase");
        }
        else if (this.PaymentConfiguration.Amount < 0) {
            this._HelperService.Notify("Invalid amount", "Amount must be greater than  0");
        }
        else {

            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "buypointinitialize",
                AccountKey: this.PaymentDetails.AccountKey,
                AccountId: this.PaymentDetails.AccountId,
                Amount: this.PaymentConfiguration.Amount,
                PaymentMode: '',
                DealId:this.StoreInfo.ReferenceId,
                DealKey:this.StoreInfo.ReferenceKey,
                MerchantId:this.StoreInfo.AccountId

            };
            if(this.PaymentMode == 'ussd'){
                pData.PaymentMode = this.PaymentMode;
            }else if (this.PaymentMode == 'card'){
                pData.PaymentMode = this.selectedPaymentMode;
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        // this._DataService.GetTUCBalance();
                        this.PaymentConfiguration.Amount = _Response.Result.Amount;
                        this.PaymentConfiguration.Charge = _Response.Result.Charge;
                        this.PaymentConfiguration.TotalAmount = _Response.Result.TotalAmount;
                        this.PaymentConfiguration.PaymentMode = _Response.Result.PaymentMode;
                        this.PaymentConfiguration.PaymentReference = _Response.Result.PaymentReference;
                        this.PaymentConfiguration.PaystackKey = _Response.Result.PaystackKey;
                        this.PaymentConfiguration.EmailAddress = _Response.Result.EmailAddress;
                        this.PaymentConfiguration.Banks = _Response.Result.Banks;
                        if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
                            this.PaymentConfiguration.UserCards = _Response.Result.Cards;
                        }
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response);
                        this._PaystackOptions.amount = _Response.Result.TotalAmount * 100;
                        this._PaystackOptions.email = _Response.Result.EmailAddress;
                        this._PaystackOptions.currency = _Response.Result.Currency;
                        this._PaystackOptions.key = _Response.Result.PaystackKey;
                        this._PaystackOptions.ref = _Response.Result.PaymentReference;


                        if (this.PaymentConfiguration.PaymentMode == 'paystack') {
                            if (this.PaymentConfiguration.UserCards != undefined && this.PaymentConfiguration.UserCards != null && this.PaymentConfiguration.UserCards.length > 0) {
                                this.Stage = 2;
                                this._HelperService.HideSpinner();
                                this._HelperService.AppConfig.IsProcessing = false;
                            }
                            else {
                                setTimeout(() => {
                                    this._HelperService.HideSpinner();
                                    this._HelperService.AppConfig.IsProcessing = false;
                                    document.getElementById('paymentbuttonModal').click();
                                }, 200);
                            }
                        } else if (this.PaymentConfiguration.PaymentMode == 'flutterwave') {
                            this._FllutterwavePaymentOptions.amount = _Response.Result.TotalAmount;
                            this._FllutterwavePaymentOptions.public_key=_Response.Result.FlutterwaveKey;
                            this._FllutterwavePaymentOptions.tx_ref = _Response.Result.PaymentReference;
                            this._FllutterwavePaymentOptions.currency =_Response.Result.Currency;
                            this.customerDetails.name = _Response.Result.DisplayName;
                            this.customerDetails.email= _Response.Result.EmailAddress;
                            this.customerDetails.phone_number =_Response.Result.MobileNumber
                            this.makePayment()
                        }
                        else {
                            this.Stage = 1;
                            this._HelperService.HideSpinner();
                            this._HelperService.AppConfig.IsProcessing = false;
                        }
                    }
                    else {
                        this._HelperService.HideSpinner();
                        this._HelperService.AppConfig.IsProcessing = false;
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    OpenPayStack() {
        this._HelperService.HideSpinner();
        this._HelperService.AppConfig.IsProcessing = false;
        document.getElementById('paymentbuttonModal').click();
    }
    DialCodeDetails =
        {
            IsSelected: false,
            SystemName: null,
            Name: null,
            IconUrl: null,
            LocalUrl: null,
            Code: null,
            BankCode: null,
            DialCode: null,
        }

    CallNumber() {
        this.callNumber.callNumber(this.DialCodeDetails.DialCode, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
    BankItemClick(Item) {
        this.DialCodeDetails = Item;
        this.DialCodeDetails.IsSelected = true;
    }
    ProcessPaystack() {
        document.getElementById('paymentbuttonModal').click();
    }
    WalletConfirm() {
        this._PaymentStatus =
        {
            Status: 'success',
            Balance: this.PaymentDetails.WalletBalance,
        }
        this.ModalDismiss(this._PaymentStatus);
    }
    ProcessOnline_Cancel() {
        // this._HelperService.Notify('Cancelled', "Payment cancelled.");
        // this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointcancel",
            AccountKey: this.PaymentDetails.AccountKey,
            AccountId: this.PaymentDetails.AccountId,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    this._HelperService.NotifyToast(_Response.Message);
                    this._PaymentStatus =
                    {
                        Status: 'error',
                        Balance: 0,
                    }
                    this.ModalDismiss(this._PaymentStatus);
                }
                else {
                    this._PaymentStatus =
                    {
                        Status: 'error',
                        Balance: 0,
                    }
                    this.ModalDismiss(this._PaymentStatus);
                    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    this._HelperService.NotifyToast(_Response.Message);
                    // this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._PaymentStatus =
                {
                    Status: 'error',
                    Balance: 0,
                }
                this.ModalDismiss(this._PaymentStatus);
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }
    public Stage = 0;
    ProcessOnline_Confirm(Item) {
        let transactionId = 0; 
        if(Item.status =="successful"){
            transactionId = Item.transaction_id;
            this.SuccessflutterwaveModalClose = true;
        }else{
            transactionId= Item.transaction;
        }
        if (Item.status == "success" || Item.status == "successful") {
            this._HelperService.ShowSpinner('Processing...');
            this._HelperService.AppConfig.IsProcessing = true;
            var pData = {
                Task: "buypointverify",
                AccountKey: this.PaymentDetails.AccountKey,
                AccountId: this.PaymentDetails.AccountId,
                Amount: this.PaymentConfiguration.Amount,
                PaymentReference: this.PaymentConfiguration.PaymentReference,
                RefTransactionId: transactionId,
                RefStatus: Item.status,
                RefMessage: Item.message,
                PaymentMode:'',

            };
            if(this.PaymentMode == 'ussd'){
                pData.PaymentMode = this.PaymentMode;
            }else if (this.PaymentMode == 'card'){
                pData.PaymentMode = this.selectedPaymentMode;
            }
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.AppConfig.IsProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._DataService.GetTUCBalance();
                        this._HelperService.NotifyToast(_Response.Message);
                        if (_Response.Result.StatusCode == 'transaction.success') {
                            if (this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
                                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                            }
                            this.Stage = 0;
                            if (_Response.Result.Balance != undefined && _Response.Result.Balance != null && this._HelperService.AccountInfo.UserAccount.AccountId > 0) {
                                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                                this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                            }
                            this._PaymentStatus =
                            {
                                Status: 'success',
                                Balance: _Response.Result.Balance,
                            }
                            this.ModalDismiss(this._PaymentStatus);
                        }
                        else {

                            this.NavBuyPointModalDismiss();
                        }
                        // this._HelperService.Notify(_Response.Status, _Response.Message);
                        // this._HelperService.NavBuyPointsConfirm();
                        // // this._HelperService.Notify(_Response.Status, _Response.Message);
                        // // this._HelperService.NavPaymentDetails();
                    }
                    else {
                        this.Stage = 0;
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
        else {
            this._HelperService.Notify('Payment failed', 'Payment process could not be completed. Please process transactiona again');
        }


    }
    // PaymentSuccesful(){
    //     this.Stage=0;
    //      this.ModalDismiss(this._PaymentStatus);
    //  }
    ProcessOnline_Charge(PaymentSource) {
        this._HelperService.ShowSpinner('processing payement...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "paymentcharge",
            Type: "buypoint",
            AccountKey: this.PaymentDetails.AccountKey,
            AccountId: this.PaymentDetails.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            ReferenceId: PaymentSource,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                    // this.GetPaymentConfiguration();
                    this._DataService.GetTUCBalance();

                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    ProcessUssd_Confirm() {
        this._HelperService.ShowSpinner('Validating payment');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointverify",
            AccountKey: this.PaymentDetails.AccountKey,
            AccountId: this.PaymentDetails.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    if (_Response.Result.StatusCode == 'transaction.success') {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this.Stage = 0;
                        if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                            this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                        }
                        this._PaymentStatus =
                        {
                            Status: 'success',
                            Balance: _Response.Result.Balance,
                        }
                        this.ModalDismiss(this._PaymentStatus);
                    }
                    else {
                        this._HelperService.NotifyToast(_Response.Message);
                        this.ModalDismiss(this._PaymentStatus);
                    }
                }
                else {
                    this.Stage = 0;
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public _PaymentStatus =
        {
            Status: 'error',
            Balance: 0,
        }
    async ModalDismiss(_PaymentStatus) {
        await this._ModalController.dismiss(_PaymentStatus);
    }
    async NavBuyPointModalDismiss() {
        this._HelperService.NavBuyPointsConfirm();
        await this._ModalController.dismiss();
    }

    public BuyPointCharge(Ref) {
        this.faio.isAvailable()
            .then(x => {
                this.faio.show(
                    {
                        title: 'Security Check',
                        subtitle: 'Authenticate yourself to process payment',
                        cancelButtonTitle: 'cancel',
                        description: "ThankUCash security check",
                    }
                ).then((result: any) => {
                    if (result == "Success") {
                        this.PostAuth(Ref, 'device', this._HelperService.AccountInfo.UserAccount.AccountCode);
                    }
                    else {
                        this._HelperService.NotifyToast('Unable to validate your identity');
                        this.PinAuth(Ref);
                    }
                }).catch((error: any) => {
                    this.PinAuth(Ref);
                    this._HelperService.NotifyToast('Unable to validate your identity')
                });
            })
            .catch(x => {
                this.PinAuth(Ref);
                // this.PostAuth(Ref);
            });
    }
    private TxtType: any = "password";
    async PinAuth(Ref) {
        const alert = await this._AlertController.create({
            // cssClass: 'my-custom-class',
            header: 'Enter you pin',
            message: 'Enter your 4 digit ThankUCash pin',
            inputs: [
                {
                    name: 'accesspin',
                    // type: this.TxtType,
                    type: 'tel',
                    min: 4,
                    max: 4,

                    // placeholder: 'Enter your ',
                    // cssClass: 'specialClass',
                    // attributes: {
                    //     maxlength: 4,
                    //     minlength: 4,
                    //     inputmode: 'number'
                    // }
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                    }
                }, {
                    text: 'Continue',
                    handler: (data) => {
                        this.PostAuth(Ref, 'pin', data.accesspin);
                    }
                }
            ]
        });

        await alert.present();
    }








    public PostAuth(Ref, AuthMode, AuthPin) {
        this._HelperService.ShowSpinner('Please wait..')
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointcharge",
            AccountKey: this.PaymentDetails.AccountKey,
            AccountId: this.PaymentDetails.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
            ReferenceId: Ref.ReferenceId,
            AuthMode: AuthMode,
            AuthPin: AuthPin,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    // this.PaymentConfiguration = _Response.Result;
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.PaymentConfig, this.PaymentConfiguration);
                    // this.PaymentConfiguration.PaymentReference = this.PaymentConfiguration.PaymentReference;
                    // this.PaymentConfiguration.AccountId = this._HelperService.AccountInfo.UserAccount.AccountId;
                    // this.PaymentConfiguration.AccountKey = this._HelperService.AccountInfo.UserAccount.AccountKey;
                    this.Vx();
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    Vx() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.AppConfig.IsProcessing = true;
        var pData = {
            Task: "buypointverify",
            AccountKey: this.PaymentDetails.AccountKey,
            AccountId: this.PaymentDetails.AccountId,
            Amount: this.PaymentConfiguration.Amount,
            PaymentReference: this.PaymentConfiguration.PaymentReference,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Payments, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.AppConfig.IsProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._DataService.GetTUCBalance();
                    this._HelperService.NotifyToast(_Response.Message);
                    if (_Response.Result.StatusCode == 'transaction.success') {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this.Stage = 0;
                        if (_Response.Result.Balance != undefined && _Response.Result.Balance != null) {
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, _Response.Result.Balance);
                            this._DataService.UpdateBalance(_Response.Result.Balance as OBalance);
                        }
                        this._PaymentStatus =
                        {
                            Status: 'success',
                            Balance: _Response.Result.Balance,
                        }
                        this.ModalDismiss(this._PaymentStatus);
                    }
                    else {

                        this.NavBuyPointModalDismiss();
                    }
                    // this._HelperService.Notify(_Response.Status, _Response.Message);
                    // this._HelperService.NavBuyPointsConfirm();
                    // // this._HelperService.Notify(_Response.Status, _Response.Message);
                    // // this._HelperService.NavPaymentDetails();
                }
                else {
                    this.Stage = 0;
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
//Change_payment_mode() method helps to set the Payment mode(paystack or flutterway)
// based on the user selection for all deal purchase
selectedPaymentMode:any;
    Change_payment_mode(mode:any) {
        this.make_payment_button = true;
        if (mode.mode == "Paystack") {
            this.selectedPaymentMode = "paystack"
        } else {
            this.selectedPaymentMode = "flutterwave"
        }
    }

    closedPaymentModal() {
       if(this.SuccessflutterwaveModalClose){
        this.flutterwave.closePaymentModal();
       }else{
        this._HelperService.HideSpinner();
        this.flutterwave.closePaymentModal();
       }   
    }

    //makePayment() helps to open the flutterwave payment modal  
    makePayment() {
        this.flutterwave.inlinePay({
            public_key: this._FllutterwavePaymentOptions.public_key,
            tx_ref: this._FllutterwavePaymentOptions.tx_ref,
            amount: this._FllutterwavePaymentOptions.amount,
            currency: this._FllutterwavePaymentOptions.currency,
            payment_options: "card, banktransfer, ussd",
            meta: {},
            customer: this.customerDetails,
            customizations:{},
            callback: this.ProcessOnline_Confirm,
            onclose: this.closedPaymentModal,
            callbackContext: this
        });
    }
}