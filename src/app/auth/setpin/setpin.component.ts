import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation, OAddressComponent } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ModalSelect } from '../modalselect/modalselect.component';

@Component({
    selector: 'app-authsetpin',
    templateUrl: 'setpin.component.html',
    styles: ['.ionic-selectable-cover{height: 40px !important;  position: absolute !important;  left: auto !important;  top: auto !important;}']
})
export class AuthSetPinPage {
    otpConfig =
        {
            allowNumbersOnly: true,
            isPasswordInput: false,
            disableAutoFocus: true,
            length: 4,
            inputClass: 'otpinput'
        }
    IsAccountExists = false;
    public DeviceInformation: ODeviceInformation;
    constructor(
        private _StatusBar: StatusBar,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _ModalController: ModalController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
        if (this.IsAccountExists == true) {
        }
        else {
            // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.SetPin)
        }
        this._HelperService.RefreshLocation();
    }
    public ionViewWillEnter() {
        // this._StatusBar.backgroundColorByHexString('#f8f8f8');
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Auth-Profile");
    }
    NewPin = "";
    ConfimedPin = "";
    onOtpChange(value) {
        if (value != undefined && value != null) {
            this.NewPin = value;
        }
    }
    onOtpChangeConfirm(value) {
        if (value != undefined && value != null) {
            this.ConfimedPin = value;
        }
    }

    public _AccountActiveAddress: OAddressComponent =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            CountryId: null,
            CountryKey: null,
            CountryName: null,

            Name: null,
            ContactNumber: null,
            EmailAddress: null,
            AddressLine1: null,
            AddressLine2: null,
            Landmark: null,
            AlternateMobileNumber: null,

            CityAreaId: null,
            CityAreaKey: null,
            CityAreaName: null,


            CityId: null,
            CityKey: null,
            CityName: null,

            StateId: null,
            StateKey: null,
            StateName: null,

            ZipCode: null,
            Instructions: null,

            MapAddress: null,
            Latitude: -1,
            Longitude: -1,
        };

    VerificationDetails =
        {
            MobileNumber: null,
            Stage: null,
            Type: null,
            FirstName: null,
            LastName: null,
            EmailAddress: null,
            GenderCode: null,
            DateOfBirth: null,
            AccountStatus: null,
            ReferralCode: null,
            IsAccountExists: false,
            CountryId: null,
            CountryKey: null,
            CountryName: null,
            StateId: null,
            StateKey: null,
            StateName: null,
            CityName: null,
            CityKey: null,
            CityId: null
        };
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        var Location = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation);
        if (Location != null) {
            this._AccountActiveAddress = Location;
        }

        var VerificationDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
        if (VerificationDetails != null) {
            this.VerificationDetails = VerificationDetails;
            // this.VerificationDetails.MobileNumber = VerificationDetails.MobileNumber;
            // this.VerificationDetails.Type = VerificationDetails.Type;
            // this.VerificationDetails.AccountStatus = VerificationDetails.AccountStatus;
            // this.VerificationDetails.CountryId = VerificationDetails.CountryId;
            // this.VerificationDetails.CountryKey = VerificationDetails.CountryKey;
            // this.VerificationDetails.CountryName = VerificationDetails.CountryName;
            this.VerificationDetails.Stage = 3;
            // var TempProfile = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.UserAppTempProfile);
            // if (TempProfile != null) {
            //     if (TempProfile.IsAccountExists != undefined || TempProfile.IsAccountExists != null) {
            //         this.IsAccountExists = TempProfile.IsAccountExists;
            //     }
            //     if (TempProfile.FirstName != undefined || TempProfile.FirstName != null || TempProfile.FirstName != "") {
            //         this.VerificationDetails.FirstName = TempProfile.FirstName;
            //     }
            //     if (TempProfile.LastName != undefined || TempProfile.LastName != null || TempProfile.LastName != "") {
            //         this.VerificationDetails.LastName = TempProfile.LastName;
            //     }
            //     if (TempProfile.EmailAddress != undefined || TempProfile.EmailAddress != null || TempProfile.EmailAddress != "") {
            //         this.VerificationDetails.EmailAddress = TempProfile.EmailAddress;
            //     }
            //     if (TempProfile.GenderCode != undefined || TempProfile.GenderCode != null || TempProfile.GenderCode != "") {
            //         this.VerificationDetails.GenderCode = TempProfile.GenderCode;
            //     }
            //     if (TempProfile.DateOfBirth != undefined || TempProfile.DateOfBirth != null) {
            //         this.VerificationDetails.DateOfBirth = TempProfile.DateOfBirth;
            //     }
            //     if (TempProfile.StateId != undefined || TempProfile.StateId != null) {
            //         this.VerificationDetails.StateId = TempProfile.StateId;
            //         this.VerificationDetails.StateKey = TempProfile.StateKey;
            //         this.VerificationDetails.StateName = TempProfile.StateName;

            //         this._AccountActiveAddress.StateId = TempProfile.StateId;
            //         this._AccountActiveAddress.StateKey = TempProfile.StateKey;
            //         this._AccountActiveAddress.StateName = TempProfile.StateName;

            //         this.SelectedState =
            //         {
            //             ReferenceId: TempProfile.StateId,
            //             ReferenceKey: TempProfile.StateKey,
            //             Name: TempProfile.StateName,
            //         }
            //     }
            //     if (TempProfile.CityId != undefined || TempProfile.CityId != null) {

            //         this.VerificationDetails.CityId = TempProfile.CityId;
            //         this.VerificationDetails.CityKey = TempProfile.CityKey;
            //         this.VerificationDetails.CityName = TempProfile.CityName;

            //         this._AccountActiveAddress.CityId = TempProfile.CityId;
            //         this._AccountActiveAddress.CityKey = TempProfile.CityKey;
            //         this._AccountActiveAddress.CityName = TempProfile.CityName;

            //         this.SelectedCity =
            //         {
            //             ReferenceId: TempProfile.CityId,
            //             ReferenceKey: TempProfile.CityKey,
            //             Name: TempProfile.CityName,
            //         }
            //     }
            //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._AccountActiveAddress);
            //     this.VerificationDetails.IsAccountExists = TempProfile.IsAccountExists;
            // }
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
        }
        else {
            // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.NumberAdd);
        }
    }
    ToggleGender(Gender) {
        this.VerificationDetails.GenderCode = Gender;
    }

    SkipClick() {
        this.ProcessAccount();
    }

    ValidateAccountDetails() {
        if (this.NewPin == undefined || this.NewPin == null || this.NewPin.length != 4) {
            this._HelperService.NotifyToastError('Enter 4 digit transaction pin');
        }
        else if (this.ConfimedPin == undefined || this.ConfimedPin == null || this.ConfimedPin.length != 4) {
            this._HelperService.NotifyToastError('Confirm 4 digit transaction pin');
        }
        else if (this.NewPin != this.ConfimedPin) {
            this._HelperService.NotifyToastError('Pin does not match please enter again');
        }
        else {
            const oneDay = 24 * 60 * 60 * 1000;
            const firstDate: any = new Date();
            const secondDate: any = new Date(this.VerificationDetails.DateOfBirth);
            const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
            if (diffDays < 5475) {
                this._HelperService.NotifySimple('Enter valid date of birth');
            }
            else {
                this.ProcessAccount();
            }
        }
    }


    ProcessAccount() {
        var NotificationUrl = null;
        var DeviceNotify = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeviceNotifications);
        if (DeviceNotify != null) {
            NotificationUrl = DeviceNotify.NotificationUrl;
        }
        var Not = this._HelperService.GetStorageValue('dnot');
        this._HelperService.ShowSpinner('Please wait ...');
        var _RequestData = {
            Task: "appusersigninv2",
            MobileNumber: this.VerificationDetails.MobileNumber,
            FirstName: this.VerificationDetails.FirstName,
            LastName: this.VerificationDetails.LastName,
            EmailAddress: this.VerificationDetails.EmailAddress,
            GenderCode: this.VerificationDetails.GenderCode,
            DateOfBirth: this.VerificationDetails.DateOfBirth,
            DisplayName: this.VerificationDetails.FirstName,
            ReferralCode: this.VerificationDetails.ReferralCode,
            CountryId: this.VerificationDetails.CountryId,
            CountryKey: this.VerificationDetails.CountryKey,
            CountryName: this.VerificationDetails.CountryName,
            CountryIso: "ng",
            OsName: this.DeviceInformation.OsName,
            OsVersion: this.DeviceInformation.OsVersion,
            SerialNumber: this.DeviceInformation.SerialNumber,
            Brand: this.DeviceInformation.Brand,
            Model: this.DeviceInformation.Model,
            Width: this.DeviceInformation.Width,
            Height: this.DeviceInformation.Height,
            CarrierName: this.DeviceInformation.CarrierName,
            Mcc: this.DeviceInformation.Mcc,
            Mnc: this.DeviceInformation.Mnc,
            NotificationUrl: Not,
            AddressComponent: this._AccountActiveAddress,
            AccessPin: this.NewPin
        };
        if (this._AccountActiveAddress.AddressLine1 != undefined && this._AccountActiveAddress.AddressLine1 != '') {
            this._HelperService.AppConfig.DeliveryLocation.Lat = this._AccountActiveAddress.Latitude;
            this._HelperService.AppConfig.DeliveryLocation.Lon = this._AccountActiveAddress.Longitude;
        }
        this._AccountActiveAddress.CountryId = this.VerificationDetails.CountryId;
        this._AccountActiveAddress.CountryKey = this.VerificationDetails.CountryKey;
        this._AccountActiveAddress.CountryName = this.VerificationDetails.CountryName;
        this._AccountActiveAddress.Name = _RequestData.FirstName + " " + _RequestData.LastName;
        this._AccountActiveAddress.ContactNumber = _RequestData.MobileNumber;
        this._AccountActiveAddress.EmailAddress = _RequestData.EmailAddress;
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._AccountActiveAddress);
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this.VerificationDetails.Stage = 4;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.UserAppTempProfile);
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                        this._HelperService.HideSpinner();
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.AccountSetup);
                    } else {
                        this._HelperService.HideSpinner();
                        this._HelperService.Notify('Operation failed', _Response.Message);
                        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Login)
                    }
                },
                (_Error) => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }



    placeMarker(Item) {
        this._AccountActiveAddress.Latitude = Item.coords.lat;
        this._AccountActiveAddress.Longitude = Item.coords.lng;
    }

    public Form_UpdateUser_AddressChange(address: Address) {
        this._AccountActiveAddress.Latitude = address.geometry.location.lat();
        this._AccountActiveAddress.Longitude = address.geometry.location.lng();
        this._AccountActiveAddress.MapAddress = address.formatted_address;
        this._AccountActiveAddress.AddressLine1 = address.formatted_address;
        this._AccountActiveAddress.AddressLine2 = address.formatted_address;
        address.address_components.forEach(address_component => {
            // if (address_component.types[0] == "locality") {
            //     this.UserCustomAddress.CityName = address_component.long_name;
            // }
            // if (address_component.types[0] == "country") {
            //     this.UserCustomAddress.CountryName = address_component.long_name;
            // }
            if (address_component.types[0] == "postal_code") {
                this._AccountActiveAddress.ZipCode = address_component.long_name;
            }
            // if (address_component.types[0] == "administrative_area_level_1") {
            //     this.UserCustomAddress.StateName = address_component.long_name;
            // }
        });
    }
    States = [];
    SelectedState: any = {};
    GetStates() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var pData = {
            Task: 'getstates',
            ReferenceId: this.VerificationDetails.CountryId,
            ReferenceKey: this.VerificationDetails.CountryKey,
            Offset: 0,
            Limit: 1000
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.States = _Response.Result.Data;
                    this.StateClick();
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async StateClick() {
        var _Request =
        {
            Title: "State",
            SubTitle: "Select your state",
            Items: []
        };
        this.States.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this.SelectedState = data.data;
                this._AccountActiveAddress.StateId = this.SelectedState.ReferenceId;
                this._AccountActiveAddress.StateKey = this.SelectedState.ReferenceKey;
                this._AccountActiveAddress.StateName = this.SelectedState.Name;
                this._AccountActiveAddress.CountryId = this.VerificationDetails.CountryId;
                this._AccountActiveAddress.CityId = null;
                this._AccountActiveAddress.CityKey = null;
                this._AccountActiveAddress.CityName = null;
                this.SelectedCity = {};
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._AccountActiveAddress);
            }
        });
        return await modal.present();
    }
    Cities = [];
    SelectedCity: any = {};
    GetCities() {
        if (this.SelectedState == undefined || this.SelectedState == null) {
            this._HelperService.NotifyToastError("Please select state");
        }
        else if (this.SelectedState.ReferenceKey == undefined || this.SelectedState.ReferenceKey == null || this.SelectedState.ReferenceKey == '') {
            this._HelperService.NotifyToastError("Please select state");
        }
        else {
            this._HelperService.IsFormProcessing = true;
            this._HelperService.ShowSpinner('Please wait');
            setTimeout(() => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
            }, 2000);
            var pData = {
                Task: 'getcities',
                ReferenceId: this.SelectedState.ReferenceId,
                ReferenceKey: this.SelectedState.ReferenceKey,
                Offset: 0,
                Limit: 1000
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.AddressManager, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this.Cities = _Response.Result.Data;
                        this.CityClick();
                    }
                    else {
                        this._HelperService.Notify('Operation failed', _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    async CityClick() {
        var _Request =
        {
            Title: " City",
            SubTitle: "Select your city",
            Items: []
        };
        this.Cities.forEach(element => {
            _Request.Items.push(
                {
                    ReferenceId: element.ReferenceId,
                    ReferenceKey: element.ReferenceKey,
                    Name: element.Name
                }
            )
        });
        const modal = await this._ModalController.create({
            component: ModalSelect,
            componentProps: _Request,
        });
        modal.onDidDismiss().then(data => {
            if (data.data != undefined && data.data != null) {
                this.SelectedCity = data.data;
                this._AccountActiveAddress.CityId = this.SelectedCity.ReferenceId;
                this._AccountActiveAddress.CityKey = this.SelectedCity.ReferenceKey;
                this._AccountActiveAddress.CityName = this.SelectedCity.Name;
                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.DeliveryLocation, this._AccountActiveAddress);
            }
        });
        return await modal.present();
    }
}