import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
    selector: 'app-numberverify',
    templateUrl: 'numberverify.component.html'
})
export class NumberVerifyPage {

    // isUssdActive = false;

    otpConfig =
        {
            allowNumbersOnly: true,
            isPasswordInput: false,
            length: 4,
            inputClass: 'otpinput'
        }

    public AuthMobileNumber = "";
    public DeviceInformation: ODeviceInformation;
    constructor(
        private callNumber: CallNumber, private _StatusBar: StatusBar,
        private _MenuController: MenuController,
        public _HelperService: HelperService,
        private _AlertController: AlertController,
    ) {
        _MenuController.enable(false);
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
        this._HelperService.RefreshAppActiveFeature();
        // this.isUssdActive = _HelperService.ValidateFeature(this._HelperService.Features.ussdverification);
        // this.setPlatformListener();
    }
    public ionViewWillEnter() {
        // this._StatusBar.backgroundColorByHexString('#f8f8f8');
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Auth-NumberVerify");
    }
    public ionViewWillLeave() {

    }

    public VerificationDetails;
    public VMobileNumber: string;
    ngOnInit() {
        this._HelperService.RefreshAppActiveFeature();
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.VerificationDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
        if (this.VerificationDetails != null) {
            this.VMobileNumber = this.VerificationDetails.VNumber;
            this._HelperService.RefreshLocation();
        }
        else {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login)
        }
    }
    ChangeNumber() {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
    }





    //#region SMS Verification - Start
    onOtpChange(item) {
        if (item != undefined && item != null) {
            if (item.length == 4) {
                this.AuthMobileNumber = item;
            }
        }
    }
    async VerifyCode() {
        if (this.AuthMobileNumber == undefined || this.AuthMobileNumber == null || this.AuthMobileNumber == "") {
            this._HelperService.Notify('Code required', 'Please enter verification code received on ' + this.VMobileNumber);
        }
        else if (this.AuthMobileNumber != undefined && isNaN(parseInt(this.AuthMobileNumber)) == true) {
            this._HelperService.Notify('Invalid code', 'Please enter valid verification code received on ' + this.VMobileNumber);
        }
        else if (this.AuthMobileNumber.length < 4 || this.AuthMobileNumber.length > 4) {
            this._HelperService.Notify('Invalid code', 'Please enter valid verification code received on ' + this.VMobileNumber);
        }
        else {
            this.vCounter = 1;
            this.isCallUssdCancel = false;
            this._HelperService.ShowSpinner();
            var pData = {
                Task: 'verifyotpcon',
                RequestToken: this.VerificationDetails.RequestToken,
                AccessCode: this.AuthMobileNumber,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        // clearTimeout(this.CTimer);
                        // clearTimeout(this.Ftimer);
                        this.VerificationDetails.Status = 3;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
                        this.DeviceConnect();
                        // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.NumberVerified)
                    }
                    else {
                        this._HelperService.Notify('Invalid code', _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    //#endregion SMS Verification - End

    //#region  USSD Verification - USSD Verification - Start
    async RequestUssdOtp() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Requesting verification');
        var pData = {
            Task: 'requestotpussd',
            CountryIsd: this.VerificationDetails.CountryIsd,
            Type: 1,
            MobileNumber: this.VMobileNumber,
            AppSessionId: this.DeviceInformation.SerialNumber,
            DeviceId: this.DeviceInformation.SerialNumber,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    var VerificationDetails =
                    {
                        Stage: 2,
                        Status: 0,
                        Type: this._HelperService.AppConfig.VerificationType.System,
                        Mode: "ussd",
                        CountryIsd: this.VerificationDetails.CountryIsd,
                        MobileNumber: _Response.Result.MobileNumber,
                        CodeStart: _Response.Result.CodeStart,
                        RequestToken: _Response.Result.RequestToken,
                        VNumber: this.VMobileNumber,
                        UssdCode: _Response.Result.UssdCode,
                        RequestKey: _Response.Result.RequestKey,
                        SessionCode: _Response.Result._Response
                    };
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, VerificationDetails);
                    this.ConfirmDial(_Response.Message, _Response.Result.UssdCode);
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    async ConfirmDial(Message, DialCode) {
        let alert = await this._AlertController.create({
            header: 'Dial Code',
            message: Message,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'text-light  alert-btn',
                    handler: () => {
                    }
                },
                {
                    text: 'Dial Code',
                    cssClass: 'text-primary alert-btn',
                    handler: () => {
                        this.callNumber.callNumber(DialCode, true)
                            .then(() => {
                                setTimeout(() => {
                                    this.vCounter = 1;
                                    this.isCallUssdCancel = false;
                                    this.callRequest();
                                }, 6000);
                            })
                            .catch(err => console.log('Error launching dialer', err));
                    }
                }
            ]
        });
        alert.present();
    }
    public vCounter = 1;
    public isCallUssdCancel = false;
    callRequest() {
        if (this.isCallUssdCancel == false) {
            var itemT = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
            if (itemT != undefined && itemT != null) {
                if (itemT.Mode != undefined && itemT.Mode != null && itemT.Mode == "ussd") {
                    this.vCounter = this.vCounter + 1;
                    if (this.vCounter < 3) {
                        this.VerifyUssdOtp(itemT.RequestKey, itemT.MobileNumber);
                    }
                    else {
                        this.vCounter = 1;
                        this.isCallUssdCancel = true;
                        this._HelperService.NotifyToast("Unable to verify your number with Ussd. Please try again")
                    }
                }
            }
        }
    }
    async VerifyUssdOtp(RequestKey, MobileNumber) {
        var pData = {
            Task: 'verifyotpussd',
            RequestKey: RequestKey,
            MobileNumber: MobileNumber,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.NotifyToastSuccess(_Response.Message);
                    // clearTimeout(this.CTimer);
                    // clearTimeout(this.Ftimer);
                    this.VerificationDetails.Status = 3;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
                    this.DeviceConnect();
                }
                else {
                    setTimeout(() => {
                        this.callRequest();
                    }, 4000);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }
    //#endregion USSD Verification - USSD Verification - End


    //#region Connect Device - Start
    DeviceConnect() {
        this._HelperService.ShowSpinner('Please wait ...');
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            DeviceInformationStorage.Task = "connectdevice";
            DeviceInformationStorage.MobileNumber = this.VMobileNumber;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, DeviceInformationStorage);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        var Result = _Response.Result;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.UserAppTempProfile, Result);
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                    }
                    else {
                        this._HelperService.Notify("Error occured", _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login)
                });
        }
        else {
        }
    }
    //#endregion Connect Device - End
}