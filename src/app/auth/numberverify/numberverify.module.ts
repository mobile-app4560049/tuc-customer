import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NumberVerifyPage } from './numberverify.component';
import { NgOtpInputModule } from 'ng-otp-input';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgOtpInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: NumberVerifyPage
            }
        ])
    ],
    declarations: [NumberVerifyPage]
})
export class NumberVerifyPageModule { }
