import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
    selector: 'app-verifypin',
    templateUrl: 'verifypin.component.html'
})
export class VerifyPinPage {
    public DeviceInformation: ODeviceInformation;
    public Pin = "";
    constructor(
        private _StatusBar: StatusBar,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewWillEnter() {
        // this._StatusBar.backgroundColorByHexString('#f8f8f8');
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Auth-VerifyPin");
    }
    VerificationDetails;
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.VerificationDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
        if (this.VerificationDetails != null) {
            this.VerificationDetails.Stage = 2;
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
        }
        else {
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Login);
        }
    }
    async VerifyPin() {
        if (this.Pin == undefined || this.Pin == null || this.Pin == "") {
            this._HelperService.NotifySimple('Enter verification pin received on your ' + this.VerificationDetails.MobileNumber + ' mobile number');
        }
        else if (this.Pin.length < 4 || this.Pin.length > 4) {
            this._HelperService.NotifySimple('Enter valid verification pin received on your ' + this.VerificationDetails.MobileNumber + ' mobile number');
        }
        else {
            this.VerifyOtp();
            // if (this.Pin == "0000") {

            //        }
            // else {
            //     this.Pin = "";
            //     this._HelperService.NotifySimple('Invalid pin');
            // }
        }
    }


    VerifyOtp() {
        var _RequestData = {
            Task: "verifyotp",
            AccountTypeCode: "customer",
            CountryIso: "ng",
            OsName: this.DeviceInformation.OsName,
            OsVersion: this.DeviceInformation.OsVersion,
            SerialNumber: this.DeviceInformation.SerialNumber,
            Brand: this.DeviceInformation.Brand,
            Model: this.DeviceInformation.Model,
            Width: this.DeviceInformation.Width,
            Height: this.DeviceInformation.Height,
            CarrierName: this.DeviceInformation.CarrierName,
            Mcc: this.DeviceInformation.Mcc,
            Mnc: this.DeviceInformation.Mnc,
            MobileNumber: this.VerificationDetails.MobileNumber,
            Pin: this.Pin
        };
        this._HelperService.ShowSpinner('Verifying pin ...');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.Account, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this.VerificationDetails.AccountStatus = _Response.Result.AccountStatus;
                        this.VerificationDetails.Stage = 3;
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
                        // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.AuthProfile);
                        // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                        // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                        // var VDetails =
                        // {
                        //     MobileNumber: VMobileNumber,
                        //     Stage: 1,
                        //     Type: 1,
                        // }
                        // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Verification, VDetails);
                        // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.VerifyPin);
                    } else {
                        this._HelperService.Notify('Invalid pin', "Enter valid pin received on mobile number");
                    }
                },
                (_Error) => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')

            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }
    ChangeNumber() {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
        // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.NumberAdd);
    }
}