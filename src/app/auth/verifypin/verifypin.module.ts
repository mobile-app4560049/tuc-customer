import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VerifyPinPage } from './verifypin.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: VerifyPinPage
            }
        ])
    ],
    declarations: [VerifyPinPage]
})
export class VerifyPinPageModule { }
