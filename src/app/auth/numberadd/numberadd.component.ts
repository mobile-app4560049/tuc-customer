import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ModalSelectCountry } from '../modalselectcountry/modalselectcountry.component';

@Component({
    selector: 'app-numberadd',
    templateUrl: 'numberadd.component.html'
})
export class NumberAddPage {
    public AuthMobileNumber = "";
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ModalController: ModalController,
        private _StatusBar: StatusBar,
        private _MenuController: MenuController,
        public _HelperService: HelperService,
        private _AlertController: AlertController,
    ) {
        _MenuController.enable(false);
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
        this.GetConfiguration();
    }

    _AppConfig =
        {
            HomeSlider: [],
            SelectedCountry: null,
            Countries: [
            ],
        }
    GetConfiguration() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        var pData;
        if (DeviceI != null) {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: DeviceI.SerialNumber,
                OsName: DeviceI.OsName,
                OsVersion: DeviceI.OsVersion,
                Brand: DeviceI.Brand,
                Model: DeviceI.Model,
                Width: DeviceI.Width,
                Height: DeviceI.Height,
                CarrierName: DeviceI.CarrierName,
                CountryCode: DeviceI.CountryCode,
                Mcc: DeviceI.Mcc,
                Mnc: DeviceI.Mnc,
                NotificationUrl: DeviceI.NotificationUrl,
                Latitude: DeviceI.Latitude,
                Longitude: DeviceI.Longitude,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }
        else {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: 'xx',
                OsName: "android",
                Brand: 'android',
                Model: "android",
                Width: 0,
                Height: 0,
                CarrierName: "airtel",
                CountryCode: "ng",
                Mcc: "000",
                Mnc: "000",
                NotificationUrl: "00",
                Latitude: 0,
                Longitude: 0,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.App, pData);
        _OResponse.subscribe(
            _Response => {
                setTimeout(() => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                }, 2000);
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._AppConfig = _Response.Result;
                    if (this._AppConfig.HomeSlider != undefined || this._AppConfig.HomeSlider != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Slider.Home, this._AppConfig.HomeSlider);
                    }
                    this._AppConfig.SelectedCountry = this._AppConfig.Countries[0];
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, _Response.Result);
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }

    public ionViewDidEnter() {
        this._HelperService.SetPageName("Auth-NumberAdd");
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
    }

    async SetNumber() {
        if (this._AppConfig.SelectedCountry == undefined || this._AppConfig.SelectedCountry == null) {
            this._HelperService.NotifyToastError('Select your country');
        }
        else if (this.AuthMobileNumber == undefined || this.AuthMobileNumber == null || this.AuthMobileNumber == "") {
            this._HelperService.NotifyToastError('Enter mobile number');
        }
        else if (this.AuthMobileNumber != undefined && isNaN(parseInt(this.AuthMobileNumber)) == true) {
            this._HelperService.NotifyToastError('Enter valid mobile number');
        }
        else if (this.AuthMobileNumber.length < this._AppConfig.SelectedCountry.NumberLength || this.AuthMobileNumber.length > (this._AppConfig.SelectedCountry.NumberLength + 3)) {
            this._HelperService.NotifyToastError('Enter valid mobile number');
        }
        else {
            let alert = await this._AlertController.create({
                header: 'Confirm your number',
                message: this.AuthMobileNumber + '  number will be verified, do you want to continue ?',
                buttons: [
                    {
                        text: 'Edit number',
                        role: 'cancel',
                        cssClass: 'text-light  alert-btn',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Confirm',
                        cssClass: 'text-primary alert-btn',
                        handler: () => {
                            var VMobileNumber = this.AuthMobileNumber.toString();
                            VMobileNumber = VMobileNumber.replace("+", "");
                            VMobileNumber = VMobileNumber.replace(" ", "");
                            var ExPrefix = this._AppConfig.SelectedCountry.Isd + "0";
                            var CountryIsd = this._AppConfig.SelectedCountry.Isd;
                            if (VMobileNumber.startsWith(ExPrefix)) {
                                VMobileNumber = VMobileNumber.replace(ExPrefix, "");
                            }
                            if (VMobileNumber.startsWith("0")) {
                                VMobileNumber = CountryIsd + VMobileNumber.substr(1, (VMobileNumber.length - 1));
                            }
                            else if (VMobileNumber.startsWith(CountryIsd)) {
                                VMobileNumber = "" + VMobileNumber;
                            }
                            else {
                                VMobileNumber = CountryIsd + VMobileNumber;
                            }
                            this.RequestOtp(VMobileNumber);
                        }
                    }
                ]
            });
            alert.present();
        }
    }

    RequestOtp(MobileNumber) {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Requesting verification');
        var pData = {
            Task: 'requestotpcon',
            CountryIsd: this._AppConfig.SelectedCountry.Isd,
            Type: 1,
            MobileNumber: MobileNumber,
            CountryId: this._AppConfig.SelectedCountry.ReferenceId,
            CountryKey: this._AppConfig.SelectedCountry.ReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    var VerificationDetails =
                    {
                        Stage: 2,
                        Status: 0,
                        Type: this._HelperService.AppConfig.VerificationType.System,
                        MobileNumber: _Response.Result.MobileNumber,
                        CodeStart: _Response.Result.CodeStart,
                        RequestToken: _Response.Result.RequestToken,
                        VNumber: MobileNumber,
                        CountryIsd: this._AppConfig.SelectedCountry.Isd,
                        CountryId: this._AppConfig.SelectedCountry.ReferenceId,
                        CountryKey: this._AppConfig.SelectedCountry.ReferenceKey,
                        CountryName: this._AppConfig.SelectedCountry.Name,
                    };
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, VerificationDetails);
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.VerifyNumber)
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }

    async CountryChangeClick() {
        const modal = await this._ModalController.create({
            component: ModalSelectCountry,
            componentProps: this._AppConfig
        });
        modal.onDidDismiss().then(data => {
            if (data.data.PaymentSource == 'noaction') {
            }
            else {
                if (data.data != undefined && data.data != null && data.data != "") {
                    this._AppConfig.SelectedCountry = data.data;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, this._AppConfig);
                }
                else {
                    this._AppConfig.SelectedCountry = this._AppConfig.Countries[0];
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, this._AppConfig);
                    // {
                    //     ReferenceId: 1,
                    //     ReferenceKey: "nigeria",
                    //     Name: "Nigeria",
                    //     IconUrl: "https://s3.eu-west-2.amazonaws.com/cdn.thankucash.com/flag/ng.png",
                    //     Isd: "234",
                    //     Iso: "ng",
                    //     CurrencyName: "Naira",
                    //     CurrencyNotation: "NGN",
                    //     CurrencySymbol: "₦",
                    //     CurrencyHex: "&#8358;",
                    //     NumberLength: 10
                    // };
                }
            }
        });
        return await modal.present();
    }
}