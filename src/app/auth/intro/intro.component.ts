import { Component, HostListener, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, IonSlides } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
    selector: 'app-intro',
    templateUrl: 'intro.component.html',
    styles: []
})
export class IntroPage {
    @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
    }
    constructor(
        private _StatusBar: StatusBar,
        private _MenuController: MenuController,
        public _HelperService: HelperService,
        private _AlertController: AlertController,
    ) {
        this.GetConfiguration();
        _MenuController.enable(false);
        this.getScreenSize();
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        setTimeout(() => {
            this.IsShowItem = true;
        }, 1000);
    }
    public IsShowItem = false;
    public screenHeight = window.innerHeight;
    public screenWidth = window.innerWidth;

    public ionViewDidEnter() {
        this._StatusBar.backgroundColorByHexString('#FFFFFF');
        this._HelperService.SetPageName("Intro");
    }
    public ionViewWillEnter() {
        this._StatusBar.backgroundColorByHexString('#FFFFFF');
        this._HelperService.SetPageName("Intro");
    }
    public ionViewWillLeave() {
        this._StatusBar.backgroundColorByHexString('#B11A83');
    }
    NavNumber() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login)
    }

    @ViewChild('mySlider', { static: true }) slides: IonSlides;

    swipeNext() {
        this.slides.slideNext();
    }

    GetConfiguration() {
        setTimeout(() => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
        }, 2000);
        var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        var pData;
        if (DeviceI != null) {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: DeviceI.SerialNumber,
                OsName: DeviceI.OsName,
                OsVersion: DeviceI.OsVersion,
                Brand: DeviceI.Brand,
                Model: DeviceI.Model,
                Width: DeviceI.Width,
                Height: DeviceI.Height,
                CarrierName: DeviceI.CarrierName,
                CountryCode: DeviceI.CountryCode,
                Mcc: DeviceI.Mcc,
                Mnc: DeviceI.Mnc,
                NotificationUrl: DeviceI.NotificationUrl,
                Latitude: DeviceI.Latitude,
                Longitude: DeviceI.Longitude,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }
        else {
            pData = {
                Task: this._HelperService.AppConfig.NetworkApi.Feature.getappconfiguration,
                SerialNumber: 'xx',
                OsName: "android",
                Brand: 'android',
                Model: "android",
                Width: 0,
                Height: 0,
                CarrierName: "airtel",
                CountryCode: "ng",
                Mcc: "000",
                Mnc: "000",
                NotificationUrl: "00",
                Latitude: 0,
                Longitude: 0,
                AppVersion: "1.1.10",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
            };
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.App, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    if (_Response.Result.HomeSlider != undefined || _Response.Result.HomeSlider != null) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Slider.Home, _Response.Result.HomeSlider);
                    }
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig, _Response.Result);
                }
                else {
                    this._HelperService.Notify('Operation failed', _Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                this._HelperService.HandleException(_Error);
            });
    }

}