import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
    selector: 'app-numberverified',
    templateUrl: 'numberverified.component.html'
})
export class NumberVerifiedPage {
    public AuthMobileNumber = "";
    public DeviceInformation: ODeviceInformation;
    constructor(
        private _StatusBar: StatusBar,
        private _MenuController: MenuController,
        private _HelperService: HelperService,
        private _AlertController: AlertController,
    ) {
        _MenuController.enable(false);
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewWillEnter() {
        // this._StatusBar.backgroundColorByHexString('#f8f8f8');
    }
    public ionViewDidEnter() {
        this._HelperService.SetPageName("Auth-NumberVerified");
    }
    public VerificationDetails;
    public VMobileNumber: string;
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        this.VerificationDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
        if (this.VerificationDetails != null) {
            this.VMobileNumber = this.VerificationDetails.VNumber;
            // this.ProcessAccount();
        }
        else {
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Login)
        }
    }
    // public UserAccountKey = null;
    // ProcessAccount() {
    //     var DeviceInformation = {
    //         SerialNumber: null,
    //         OsName: null,
    //         OsVersion: null,
    //         Brand: null,
    //         Model: null,
    //         Width: null,
    //         Height: null,
    //         CarrierName: null,
    //         CountryCode: null,
    //         Mcc: null,
    //         Mnc: null,
    //         Latitude: 0,
    //         Longitude: 0,
    //     };
    //     var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppSettings.StorageHelper.Device);
    //     if (DeviceInformationStorage != null) {
    //         DeviceInformation = DeviceInformationStorage;
    //     }
    //     var NotificationUrl = null;
    //     var DeviceNotify = this._HelperService.GetStorage(this._HelperService.AppSettings.StorageHelper.DeviceNotifications);
    //     if (DeviceNotify != null) {
    //         NotificationUrl = DeviceNotify.NotificationUrl;
    //     }
    //     // this._HelperService.ShowSpinnerCustom('Registering device ...');
    //     var _RequestData = {
    //         Task: "appusersignin",
    //         MobileNumber: this.VerificationDetails.VNumber,
    //         // DisplayName: this.ProfileDetails.FirstName,
    //         // FirstName: this.ProfileDetails.FirstName,
    //         // LastName: this.ProfileDetails.LastName,
    //         // EmailAddress: this.ProfileDetails.EmailAddress,
    //         // GenderCode: this.ProfileDetails.GenderCode,
    //         // ReferralCode: this.ProfileDetails.ReferralCode,
    //         // AccessPin: this.ProfileDetails.Pin,
    //         CountryIso: "ng",
    //         OsName: DeviceInformation.OsName,
    //         OsVersion: DeviceInformation.OsVersion,
    //         SerialNumber: DeviceInformation.SerialNumber,
    //         Brand: DeviceInformation.Brand,
    //         Model: DeviceInformation.Model,
    //         Width: DeviceInformation.Width,
    //         Height: DeviceInformation.Height,
    //         CarrierName: DeviceInformation.CarrierName,
    //         Mcc: DeviceInformation.Mcc,
    //         Mnc: DeviceInformation.Mnc,
    //         NotificationUrl: NotificationUrl,
    //     };
    //     try {
    //         let _OResponse: Observable<OResponse>;
    //         _OResponse = this._NetworkService.PostData(this._HelperService.AppSettings.NetworkLocations.System, _RequestData);
    //         _OResponse.subscribe(
    //             (_Response) => {

    //                 if (_Response.Status == this._HelperService.StatusSuccess) {
    //                     this.UserAccountKey = _Response.Result.UserAccount.AccountKey;
    //                     this._HelperService.DeleteStorage(this._HelperService.AppSettings.StorageHelper.NumberVerification);
    //                     this._HelperService.DeleteStorage(this._HelperService.AppSettings.StorageHelper.UserAppStatus);
    //                     this._HelperService.DeleteStorage(this._HelperService.AppSettings.StorageHelper.UserAppTempProfile);
    //                     this._HelperService.SaveStorage(this._HelperService.AppSettings.StorageHelper.Account, _Response.Result);
    //                     // this._GoogleAnalytics.setUserId(_Response.Result.UserAccount.AccountKey);
    //                     this._HelperService.DeleteStorage(this._HelperService.AppSettings.StorageHelper.UserAppStatus);
    //                     this.FetchDataFromServer();
    //                 } else {
    //                     this._HelperService.Notify('Operation failed', _Response.Message);
    //                     this._App.getRootNav().setRoot(AccessCheckPage);
    //                 }
    //             },
    //             (_Error) => {
    //                 this._HelperService.HideSpinner();
    //                 this._HelperService.HandleException(_Error);
    //             }
    //         );
    //     } catch (_Error) {
    //         this._HelperService.HideSpinner();
    //         if (_Error.status == 0) {
    //             this._HelperService.Notify('Operation failed', "Please check your internet connection");
    //         }
    //         else if (_Error.status == 401) {
    //             var EMessage = JSON.parse(_Error._body).error;
    //             this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
    //         }
    //         else {
    //             this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
    //         }
    //     }
    // }
    // DeviceConnect() {
    //     this._HelperService.ShowSpinner('Checking device ...');
    //     var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    //     if (DeviceInformationStorage != null) {
    //         DeviceInformationStorage.Task = "connectdevice";
    //         DeviceInformationStorage.MobileNumber = this.VMobileNumber;
    //         let _OResponse: Observable<OResponse>;
    //         _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, DeviceInformationStorage);
    //         _OResponse.subscribe(
    //             _Response => {
    //                 this._HelperService.HideSpinner();
    //                 if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                     var Result = _Response.Result;
    //                     if (Result.IsAccountExists == true) {
    //                         var ProfileInformation =
    //                         {
    //                             FirstName: null,
    //                             LastName: null,
    //                             EmailAddress: null,
    //                             GenderCode: null,
    //                             DateOfBirth: null,
    //                             ReferralCode: null,
    //                             Pin: null
    //                         };
    //                         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.UserAppTempProfile, ProfileInformation);
    //                         this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.AccountSetup)
    //                         // this._App.getRootNav().setRoot(AuthPinVerified);
    //                     }
    //                     else {
    //                         this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.AuthProfile)
    //                         // this._App.getRootNav().setRoot(AuthSetProfilePage);
    //                     }
    //                 }
    //                 else {
    //                     this._HelperService.Notify("Error occured", _Response.Message);
    //                 }
    //             },
    //             _Error => {
    //                 this._HelperService.HideSpinner();
    //                 this._HelperService.HandleException(_Error);
    //                 this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.NumberAdd)
    //             });
    //     }
    //     else {

    //     }

    // }

}