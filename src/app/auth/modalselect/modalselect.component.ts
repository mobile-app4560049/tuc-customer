import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'modalselect.component.html',
    selector: 'modal-modalselectcountry'
})
export class ModalSelect {


    public _Data =
        {
            Title: "",
            SubTitle: "",
            SearchContent: "",
            Original: [],
            Sorted: [],
        }

    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        var Data = this.navParams.data as any;
        this._Data.Title = Data.Title;
        this._Data.SubTitle = Data.SubTitle;
        this._Data.Original = Data.Items;
        this._Data.Sorted = Data.Items;
    }
    SearchItem() {
        if (this._Data.SearchContent != undefined && this._Data.SearchContent != null && this._Data.SearchContent != "") {
            this._Data.Sorted = this._Data.Original.filter(o =>
                Object.keys(o).some(k => o[k].toString().toLowerCase().includes(this._Data.SearchContent.toLowerCase())));
        }
        else {
            this._Data.Sorted = this._Data.Original;
        }
    }
    async ModalDismiss() {
        await this._ModalController.dismiss(undefined);
    }
    async ItemSelected(Item) {
        await this._ModalController.dismiss(Item);
    }


}