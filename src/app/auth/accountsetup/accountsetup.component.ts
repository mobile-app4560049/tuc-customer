import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation, OAccountInfo, OAccount } from '../../service/object.service';
import { DataService } from '../../service/data.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
declare let fbq: Function;
@Component({
    selector: 'app-accountsetup',
    templateUrl: 'accountsetup.component.html'
})
export class AccountSetupPage {
    public AuthMobileNumber = "";
    public DeviceInformation: ODeviceInformation;
    constructor(
        private _StatusBar: StatusBar,
        private _DataHelperService: DataService,
        private _MenuController: MenuController,
        private _HelperService: HelperService,
        private _AlertController: AlertController,
    ) {
        _MenuController.enable(false);
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewWillEnter() {
        // this._StatusBar.backgroundColorByHexString('#f8f8f8');
    }
    public VerificationDetails =
        {
            AccessKey: null,
            LoginTime: null,
            PublicKey: null,
            UserAccount:
            {
                AccountKey: null,
            },
            User:
            {
                FirstName: null,
                Address: null,
                AddressLatitude: 0,
                AddressLongitude: null,
                ContactNumber: null,
                ContactNumberVerificationStatus: 0,
                EmailAddress: null,
                EmailVerificationStatus: 0,
                LastName: null,
                MobileNumber: null,
                Name: null,
                UserName: null,
            }
        };
    public VMobileNumber: string;
    ngOnInit() {
        this._HelperService.PageLoaded();
        this._HelperService.TrackPixelPageView();
        fbq('track', 'CompleteRegistration');
        // (window as any).fbq('track', 'CompleteRegistration');
        // fbq('track', 'CompleteRegistration');
        this.VerificationDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
        if (this.VerificationDetails != null) {
        }
        else {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login)
        }
    }

    NavigateHome() {

    }

    public GetTUCBalance() {
        this._DataHelperService.GetTUCCategories();
        this._HelperService.ShowSpinner("Loading your account");
        var pData = {
            Task: 'getuseraccountbalance',
            UserAccountKey: this.VerificationDetails.UserAccount.AccountKey,
            Source: "transactionsource.app",
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAcc, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    var Balance = _Response.Result;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.AccountBalance, Balance);
                    this._HelperService.HideSpinner();
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard)
                }
                else {
                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }


    // public UserAccountKey = null;
    // ProcessAccount() {
    //     var DeviceInformation = {
    //         SerialNumber: null,
    //         OsName: null,
    //         OsVersion: null,
    //         Brand: null,
    //         Model: null,
    //         Width: null,
    //         Height: null,
    //         CarrierName: null,
    //         CountryCode: null,
    //         Mcc: null,
    //         Mnc: null,
    //         Latitude: 0,
    //         Longitude: 0,
    //     };
    //     var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    //     if (DeviceInformationStorage != null) {
    //         DeviceInformation = DeviceInformationStorage;
    //     }
    //     var NotificationUrl = null;
    //     var DeviceNotify = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.DeviceNotifications);
    //     if (DeviceNotify != null) {
    //         NotificationUrl = DeviceNotify.NotificationUrl;
    //     }
    //     this._HelperService.ShowSpinner('Registering device ...');
    //     var _RequestData = {
    //         Task: "appusersignin",
    //         MobileNumber: this.VerificationDetails.VNumber,
    //         // DisplayName: this.ProfileDetails.FirstName,
    //         // FirstName: this.ProfileDetails.FirstName,
    //         // LastName: this.ProfileDetails.LastName,
    //         // EmailAddress: this.ProfileDetails.EmailAddress,
    //         // GenderCode: this.ProfileDetails.GenderCode,
    //         // ReferralCode: this.ProfileDetails.ReferralCode,
    //         // AccessPin: this.ProfileDetails.Pin,
    //         CountryIso: "ng",
    //         OsName: DeviceInformation.OsName,
    //         OsVersion: DeviceInformation.OsVersion,
    //         SerialNumber: DeviceInformation.SerialNumber,
    //         Brand: DeviceInformation.Brand,
    //         Model: DeviceInformation.Model,
    //         Width: DeviceInformation.Width,
    //         Height: DeviceInformation.Height,
    //         CarrierName: DeviceInformation.CarrierName,
    //         Mcc: DeviceInformation.Mcc,
    //         Mnc: DeviceInformation.Mnc,
    //         NotificationUrl: NotificationUrl,
    //     };
    //     try {
    //         let _OResponse: Observable<OResponse>;
    //         _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, _RequestData);
    //         _OResponse.subscribe(
    //             (_Response) => {
    //                 this._HelperService.HideSpinner();
    //                 if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                     this.VerificationDetails.Status = 4;
    //                     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification, this.VerificationDetails);
    //                     this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
    //                     this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.UserAppTempProfile);
    //                     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
    //                     // 
    //                     this.FetchDataFromServer();
    //                 } else {
    //                     this._HelperService.Notify('Operation failed', _Response.Message);
    //                     this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.NumberAdd)
    //                 }
    //             },
    //             (_Error) => {
    //                 this._HelperService.HideSpinner();
    //                 this._HelperService.HandleException(_Error);
    //             }
    //         );
    //     } catch (_Error) {
    //         this._HelperService.HideSpinner();
    //         if (_Error.status == 0) {
    //             this._HelperService.Notify('Operation failed', "Please check your internet connection");
    //         }
    //         else if (_Error.status == 401) {
    //             var EMessage = JSON.parse(_Error._body).error;
    //             this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
    //         }
    //         else {
    //             this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
    //         }
    //     }
    // }



    // public TCategories = [];
    // public TMerchants = [];
    // public TStores = [];
    // public TMerchantCategories = [];
    // // public TTransactions = [];
    // public TUserMerchantVisits = [];
    // public FetchDataFromServer() {
    //     this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.TSyncTime, new Date());
    //     this.GetTUCCategories();
    // }
    // public GetTUCCategories() {
    //     var pData = {
    //         Task: this._HelperService.AppConfig.NetworkApi.v2.getcategories,
    //         Offset: 0,
    //         Limit: 100,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TCategories = _Response.Result.Data;
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TCategories, _Response.Result.Data);
    //                 this.GetTUCMerchantCategories();
    //             }
    //             else {
    //                 this._HelperService.Notify(_Response.Status, _Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // public GetTUCMerchantCategories() {
    //     var pData = {
    //         Task: this._HelperService.AppConfig.NetworkApi.v2.getmerchantcategories,
    //         Offset: 0,
    //         Limit: 200,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TMerchantCategories = _Response.Result.Data;
    //                 this.TMerchantCategories.forEach(element => {
    //                     var Details = this.TCategories.find(x => x.ReferenceId == element.CategoryId);
    //                     if (Details != undefined) {
    //                         element.Name = Details.Name;
    //                     }
    //                 });
    //                 this.TCategories.forEach(_Category => {
    //                     var _MCat = this.TMerchantCategories.filter(x => x.CategoryId == _Category.ReferenceId);
    //                     if (_MCat != undefined) {
    //                         _Category.Merchants = _MCat.length;
    //                     }
    //                 });
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TCategories, this.TCategories);
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TMerchantCategories, _Response.Result.Data);
    //                 this.GetTUCMerchants();
    //             }
    //             else {
    //                 this._HelperService.Notify(_Response.Status, _Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // public GetTUCMerchants() {
    //     var pData = {
    //         Task: this._HelperService.AppConfig.NetworkApi.v2.getmerchants,
    //         Offset: 0,
    //         Limit: 200,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TMerchants = [];
    //                 this.TMerchants = _Response.Result.Data;
    //                 this.TMerchants.forEach(_Merchant => {
    //                     _Merchant.Categories = [];
    //                     var MerchantCategories = this.TMerchantCategories.filter(x => x.MerchantId == _Merchant.ReferenceId);
    //                     if (MerchantCategories != undefined && MerchantCategories.length > 0) {
    //                         _Merchant.Categories = MerchantCategories;
    //                     }
    //                 });
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TMerchants, this.TMerchants);
    //                 this.GetTUCMerchantStores();
    //             }
    //             else {
    //                 this._HelperService.Notify(_Response.Status, _Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // public GetTUCMerchantStores() {
    //     var pData = {
    //         Task: this._HelperService.AppConfig.NetworkApi.v2.getstores,
    //         Offset: 0,
    //         Limit: 200,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TStores = _Response.Result.Data;
    //                 this.TStores.forEach(_Store => {
    //                     var MerchantDetails = this.TMerchants.find(x => x.ReferenceId == _Store.MerchantId);
    //                     if (MerchantDetails != undefined && MerchantDetails != null) {
    //                         if (MerchantDetails.Locations == undefined) {
    //                             MerchantDetails.Locations = 0;
    //                         }
    //                         MerchantDetails.Locations = MerchantDetails.Locations + 1;
    //                         _Store.MerchantDisplayName = MerchantDetails.DisplayName;
    //                         _Store.IconUrl = MerchantDetails.IconUrl;
    //                         _Store.RewardPercentage = MerchantDetails.RewardPercentage;
    //                         if (MerchantDetails.Categories != undefined && MerchantDetails.Categories != null && MerchantDetails.Categories.length > 0) {
    //                             _Store.Categories = MerchantDetails.Categories;
    //                         }
    //                         else {
    //                             _Store.Categories = [];
    //                         }
    //                     }
    //                 });
    //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TStores, this.TStores);
    //                 this.GetTUCBalance();
    //             }
    //             else {
    //                 this._HelperService.Notify(_Response.Status, _Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // // public GetTUCTransactions() {
    // //     var pData = {
    // //         Task: this._HelperService.AppConfig.NetworkApi.v2.gettransactions,
    // //         Offset: 0,
    // //         Limit: 90,
    // //     };
    // //     let _OResponse: Observable<OResponse>;
    // //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCApp, pData);
    // //     _OResponse.subscribe(
    // //         _Response => {
    // //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    // //                 var TransactionsHistory = _Response.Result.Data;
    // //                 this.TTransactions = [];
    // //                 this.TUserMerchantVisits = [];
    // //                 TransactionsHistory.forEach(element => {
    // //                     var MerchantDetails = this.TMerchants.find(x => x.ReferenceId == element.MerchantId);
    // //                     if (MerchantDetails != undefined) {
    // //                         element.MerchantDisplayName = MerchantDetails.DisplayName;
    // //                         element.MerchantIconUrl = MerchantDetails.IconUrl;
    // //                     }
    // //                     var StoreDetails = this.TStores.find(x => x.ReferenceId == element.StoreId);
    // //                     if (StoreDetails != undefined) {
    // //                         element.StoreDisplayName = StoreDetails.DisplayName;
    // //                         element.StoreContactNumber = StoreDetails.ContactNumber;
    // //                         element.StoreEmailAddress = StoreDetails.EmailAddress;
    // //                         element.Address = StoreDetails.Address;
    // //                         element.Latitude = StoreDetails.Latitude;
    // //                         element.Longitude = StoreDetails.Longitude;
    // //                     }
    // //                     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
    // //                     this.TTransactions.push(element);
    // //                 });
    // //                 this.TMerchants.forEach(element => {
    // //                     var MerchantTransactions = this.TTransactions.filter(x => x.MerchantId == element.ReferenceId);
    // //                     if (MerchantTransactions.length > 0) {
    // //                         element.Visits = MerchantTransactions.length;
    // //                         var Rewards = 0;
    // //                         var InvoiceAmount = 0;
    // //                         MerchantTransactions.forEach(element => {
    // //                             Rewards = Rewards + element.TotalAmount;
    // //                             InvoiceAmount = InvoiceAmount + element.InvoiceAmount;
    // //                         });
    // //                         element.Rewards = Rewards;
    // //                         element.InvoiceAmount = InvoiceAmount;
    // //                         this.TUserMerchantVisits.push(element);
    // //                     }
    // //                 });
    // //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TTransactions, TransactionsHistory);
    // //                 this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.TUserMerchantVisits, this.TUserMerchantVisits);
    // //                 this.GetTUCBalance();
    // //             }
    // //             else {
    // //                 this._HelperService.Notify(_Response.Status, _Response.Message);
    // //             }
    // //         },
    // //         _Error => {
    // //             this._HelperService.HandleException(_Error);
    // //         });
    // // }

}