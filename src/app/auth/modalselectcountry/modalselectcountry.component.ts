import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse, OBalance } from '../../service/object.service';
import { DataService } from 'src/app/service/data.service';
@Component({
    templateUrl: 'modalselectcountry.component.html',
    selector: 'modal-modalselectcountry'
})
export class ModalSelectCountry {


    public _Countries =
        {
            SearchContent: "",
            Original: [],
            Sorted: [],
        }

    constructor(
        public _DataService: DataService,
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    ngOnInit() {
        this._HelperService.PageLoaded();
        var Data = this.navParams.data as any;
        this._Countries.Original = Data.Countries;
        this._Countries.Sorted = Data.Countries;
    }
    SearchCountry() {
        if (this._Countries.SearchContent != undefined && this._Countries.SearchContent != null && this._Countries.SearchContent != "") {
            this._Countries.Sorted = this._Countries.Original.filter(x => x.Name.toLowerCase() == this._Countries.SearchContent || x.Isd.toLowerCase().startsWith(this._Countries.SearchContent));
        }
        else {
            this._Countries.Sorted = this._Countries.Original;
        }
    }
    async ModalDismiss() {
        await this._ModalController.dismiss(undefined);
    }
    async CountrySelected(Item) {
        await this._ModalController.dismiss(Item);
    }


}