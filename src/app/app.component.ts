
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Platform, MenuController, AlertController, Config, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ODeviceInformation, OResponse } from './service/object.service';
import { HelperService } from './service/helper.service';
import { Sim } from '@ionic-native/sim/ngx';
import { Device } from "@ionic-native/device/ngx";
import { FCM } from '@ionic-native/fcm/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
// import { Deeplinks } from '@awesome-cordova-plugins/deeplinks/ngx';
import { NumberAddPage } from './auth/numberadd/numberadd.component';
import { DashboardPage } from './access/dashboard/dashboard.component';
import { AccessProfilePage } from './access/account/profile/profile.component';
import { NotificationsPage } from './access/general/notifications/notifications.component';
import { SelectBillerPage } from './access/payments/selectbiller/selectbiller.component';
import { StoresPage } from './access/features/stores/stores.component';
import { DashboardPageModule } from './access/features/maddeals/dashboard/dashboard.module';
import { FlashDealsPage } from './access/features/maddeals/flashdeals/flashdeals.component';
import { Mixpanel, MixpanelPeople } from '@awesome-cordova-plugins/mixpanel/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public isRemainder = 0;
  private _DeviceInformation: ODeviceInformation = {
    MobileNumber: null,
    SerialNumber: null,
    OsName: "website",
    OsVersion: "1",
    Brand: null,
    Model: null,
    Width: null,
    Height: null,
    CarrierName: null,
    CountryCode: null,
    Mcc: null,
    Mnc: null,
    Latitude: 0,
    Longitude: 0,
    NotificationUrl: null
  };
  constructor(
    // private _DeepLinks: Deeplinks,
    private iab: InAppBrowser,
    public config: Config,
    public _Firebase: FCM,
    public _AlertController: AlertController,
    public _HelperService: HelperService,
    private _Platform: Platform,
    private splashScreen: SplashScreen,
    private _StatusBar: StatusBar,
    private _Device: Device,
    private _Sim: Sim,
    private _MenuController: MenuController,
    private _NavController: NavController,
    private _Mixpanel:Mixpanel,
    private _MixpanelPeople:MixpanelPeople,
  ) {

    this.initializeApp();
    this.config.set('backButtonIcon', 'ios-arrow-back');
  }

  UpdateToken(Token) {
    this._HelperService.SaveStorageValue('dnot', Token);
    var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    if (DeviceI != null) {
      var _DeviceInformation = DeviceI;
      _DeviceInformation.NotificationUrl = Token;
      this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Device, _DeviceInformation);
    }
    if (this._HelperService.AccountInfo.UserAccount.AccountId != undefined && this._HelperService.AccountInfo.UserAccount.AccountId != null && this._HelperService.AccountInfo.UserAccount.AccountId != 0) {
      var DeviceInfo =
      {
        Task: "updatedevicenotificationurl",
        UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
        UserAccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
        NotificationUrl: Token,
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, DeviceInfo);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.HideSpinner();
          if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
            this._HelperService.HideSpinner();
          }
          else {
            this._HelperService.Notify(_Response.Status, _Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        });
    }
  }

  initializeApp() {
    this._Platform.ready().then(() => {
      // this.LoadDeeplinks();
      this._Mixpanel.init(this._HelperService.AppConfig.MixPanelToken)
      .then(
        ()=>{
          console.log("Mixpanel intialization success")
        }
      )
      .catch((e)=>{
        console.log(e)
      })
      var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
      if (DeviceI != null) {
        this._DeviceInformation = DeviceI;
      }
      this._Firebase.hasPermission().then(hasPermission => {
        if (hasPermission) {
        }
        else {
          this._Firebase.requestPushPermissionIOS()
            .then((success) => {
              if (success) {
                this._Firebase.getToken().then(token => {
                  this._Firebase.subscribeToTopic(this._HelperService.AppConfig.Topics.tuccustomer);
                  this._DeviceInformation.NotificationUrl = token;
                  this.UpdateToken(token);
                });
              }
            });
        }
      });
      this._Firebase.subscribeToTopic(this._HelperService.AppConfig.Topics.tuccustomer);
      this._Firebase.getToken().then(token => {
        this._DeviceInformation.NotificationUrl = token;
        this.UpdateToken(token);
      });
      this._Firebase.onTokenRefresh().subscribe(token => {
        this._DeviceInformation.NotificationUrl = token;
        this.UpdateToken(token);
      });

      this._Firebase.onNotification().subscribe(data => {
        var NotificationsList = this._HelperService.GetStorage('tucnots');
        if (NotificationsList != null) {
          const newList = [data, ...NotificationsList];
          NotificationsList.push(data);
          this._HelperService.SaveStorage('tucnots', newList)
        }
        else {
          var NotItem = [data];
          this._HelperService.SaveStorage('tucnots', NotItem)
        }
        if (data.wasTapped) {
          // background
          this.NavigatePush(data);
        } else {
          // foreground
          if (data != undefined && data != null) {
            if (data.ForceNavigation != undefined && data.ForceNavigation != null) {
              if (data.ForceNavigation) {
                this._HelperService.NotifyToast(data.Message);
                this.NavigatePush(data);
              }
              else {
                this._HelperService.NotifyToast(data.Message);
              }
            }
          }
        };
      });
      this.splashScreen.hide();
      this._StatusBar.overlaysWebView(false);
      this._StatusBar.styleDefault();
      this._StatusBar.backgroundColorByHexString('#B11A83');
      if (this._Device.uuid != undefined && this._Device.uuid != null) {
        this._DeviceInformation.SerialNumber = this._Device.uuid;
      }
      if (this._DeviceInformation.SerialNumber == undefined || this._DeviceInformation.SerialNumber == null || this._DeviceInformation.SerialNumber == '') {
        this._DeviceInformation.SerialNumber = 'cu_' + this._HelperService.GenerateGuid();
      }
      this._DeviceInformation.OsName = this._Device.platform;
      this._DeviceInformation.OsVersion = this._Device.version;
      this._DeviceInformation.Brand = this._Device.manufacturer;
      this._DeviceInformation.Model = this._Device.model;
      this._DeviceInformation.Width = window.screen.width;
      this._DeviceInformation.Height = window.screen.height;
      if (this._Platform.is("android")) {
        this._DeviceInformation.OsName = "android";
      } else if (this._Platform.is("ios")) {
        this._DeviceInformation.OsName = "ios";
      } else if (this._Platform.is("ipad")) {
        this._DeviceInformation.OsName = "ios";
      } else if (this._Platform.is("iphone")) {
        this._DeviceInformation.OsName = "ios";
      } else {
        this._DeviceInformation.OsName = "android";
      }
      this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Device, this._DeviceInformation);
      this.LoadSimCardInformation();
      try {
        var UserAccount = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
        if (UserAccount != null) {
          this._Mixpanel.identify(UserAccount.User.ContactNumber)
          this._HelperService.RefreshLocation();
          this._MenuController.enable(true);
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
        } else {
          this._Mixpanel.reset();
          this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Device, this._DeviceInformation);
          this._HelperService.RefreshLocation();
          this._MenuController.enable(false);
          var VerificationCheck = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.NumberVerification);
          if (VerificationCheck != null) {
            if (VerificationCheck.Stage == 2) {
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.VerifyNumber);
            }
            else if (VerificationCheck.Stage == 3) {
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
            }
            else if (VerificationCheck.Stage == 4) {
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
            }
            else {
              this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Login)
            }
          }
          else {
            var Config = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.hcAppConfig);
            if (Config == null) {
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CountrySelect);
            }
            else {
              if (Config.SelectedCountry != undefined && Config.SelectedCountry != null) {
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
              }
              else {
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CountrySelect);
              }
            }
          }
        }
      } catch (error) {
        this._HelperService.Notify("Error occured", "Please close app and open again. " + JSON.stringify(error));
      }
    });
  }
  _AppConfig =
    {
      HomeSlider: [],
      SelectedCountry: null,
      Countries: [
      ],
    }
  NavigatePush(data) {
    if (data != undefined && data != null) {
      if (data.Task != undefined && data.Task != null) {
        var Task = data.Task;
        if (Task == "dashboard") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
        }
        if (Task == "about") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.About);
        }
        if (Task == "profile") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Profile);
        }
        if (Task == "updatepin") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.ChangePin);
        }
        if (Task == "faq") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Faq);
        }
        if (Task == "cashout") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Cashout.list);
        }
        if (Task == "cardmanager") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CardsManager.cardmanger);
        }
        if (Task == "cardmanager") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CardsManager.cardmanger);
        }
        if (Task == "bankmanager") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BankManager.list);
        }
        if (Task == "faqdetails") {
          var _ItemFaq =
          {
            ReferenceId: data.ReferenceId,
            ReferenceKey: data.ReferenceKey,
            Name: data.Name,
            Description: data.Description,
          };
          this._HelperService.SaveStorage('ActiveFaqCat', _ItemFaq);
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.FaqDetails);
        }
        if (Task == "saleshistory") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.SalesHistory);
        }
        if (Task == "pointshistory") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.PointsHistory);
        }
        if (Task == "lcctopup") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Payments.LccTopup);
        }
        if (Task == "addresslocator") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.addresslocator);
        }

        if (Task == "deals") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.Dashboard);
        }
        if (Task == "flashdeals") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.FlashDeals);
        }

        if (Task == "scanredeemcode") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.MerchantRedeem.scanredeemcode);
        }
        if (Task == "referral") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.referral);
        }
        if (Task == "referralhistory") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.referralbonus);
        }
        if (Task == "ordershistory") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrdersHistory);
        }
        if (Task == "buypoints") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.initialize);
        }
        if (Task == "wallettopuphistory") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.list);
        }
        if (Task == "stores") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Stores);
        }

        if (Task == "airtime") {
          this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, "airtime");
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviders);
        }
        if (Task == "tv") {
          this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, "tv");
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviders);
        }
        if (Task == "electricity") {
          this._HelperService.SaveStorageValue(this._HelperService.AppConfig.StorageHelper.ActiveReference, "electricity");
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.vasproviders);
        }

        if (Task == "dealpurchasehistory") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.DealPurchaseHistory);
        }

        if (Task == "pointpurchasereceipt") {
          this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, data);
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.buypointconfirm);
        }
        if (Task == "dealpurchasedetails") {
          var _Item1 =
          {
            ReferenceId: data.ReferenceId,
            ReferenceKey: data.ReferenceKey
          };
          this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Item1);
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.DealPurchaseDetails);
        }
        if (Task == "paymentdetails") {
          this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, data);
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BuyPoints.buypointconfirm);
        }
        if (Task == "vaspaymenthistory") {
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.list);
        }
        if (Task == "deal") {
          var _Itemdeal =
          {
            ReferenceId: data.ReferenceId,
            ReferenceKey: data.ReferenceKey
          };
          this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Itemdeal);
          this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.Details);
        }
        if (Task == "order") {
          if (data.ReferenceId != undefined && data.ReferenceKey != null) {
            var _OrderDetails =
            {
              OrderId: data.ReferenceId,
              OrderKey: data.ReferenceKey
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
          }
        }
      }
    }
  }

  LoadSimCardInformation() {
    var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    if (DeviceStorage != null) {
      if (DeviceStorage.OsName == "android") {
        this._Sim.hasReadPermission().then(
          info => {
            this._Sim.getSimInfo().then(
              SimData => {
                this.SetSimCardData(SimData);
              },
              err => {
              }
            );
          },
          err => {
            this._Sim.requestReadPermission().then(
              () => {
                this._Sim.getSimInfo().then(
                  SimData => {
                    this.SetSimCardData(SimData);
                  },
                  err => {
                  }
                );
              },
              () => {
              }
            );
          }
        );
      } else {
        this._Sim.getSimInfo().then(
          SimData => {
            this.SetSimCardData(SimData);
          },
          err => {
          }
        );
      }
    }
  }
  SetSimCardData(SimData) {
    var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    if (DeviceStorage != null) {
      DeviceStorage.CarrierName = SimData.carrierName;
      DeviceStorage.CountryCode = SimData.countryCode;
      DeviceStorage.Mcc = SimData.mcc;
      DeviceStorage.Mnc = SimData.mnc;
      this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Device, DeviceStorage);
      this._HelperService.RefreshLocation();
    }
  }
  NavReferralEarning() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.referralbonus);
  }
  BuyPoints() {
    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.BuyPoints.initialize);
  }
  NavDashboard() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
  }

  NavStores() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Stores);
  }
  NavTerminals() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Terminals);
  }
  NavCashiers() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Cashiers);
  }
  NavSaleHistory() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.SalesHistory);
  }
  NavWallet() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Wallet);
  }

  NavTransactionsHistory() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.TransactionsHistory);
  }

  NavDealPurchase() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.DealPurchaseHistory);
  }
  NavProductPurchase() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.ProductPurchaseHistory);
  }

  NavFaq() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Faq);
  }
  NavSupport() {
    const options = 'location=yes,zoom=no,hideurlbar=yes,hidenavigationbuttons=yes,closebuttoncaption=Close,closebuttoncolor=#ffffff,footercolor=#AF1482,toolbarcolor=#AF1482,disallowoverscroll=yes';
    this.iab.create('https://thankucash.com/appsupport.html?accid=' + this._HelperService.AccountInfo.UserAccount.AccountId + "&acckey=" + this._HelperService.AccountInfo.UserAccount.AccountKey + 'mobilenumber=' + this._HelperService.AccountInfo.User.MobileNumber + "&account=" + this._HelperService.AccountInfo.UserAccount.AccountCode + "&email=" + this._HelperService.AccountInfo.User.EmailAddress + "&name=" + this._HelperService.AccountInfo.UserAccount.DisplayName, '_blank', options);
  }
  // NavSupport() {
  //   this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.General.Support);
  // }
  NavWalletTopupHistory() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BuyPoints.list);
  }
  NavBuyNowPayLater() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Bnpl.Loans);
  }
  NavBillPaymentsHistory() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Vas.list);
  }
  NavChangePin() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.ChangePin);
  }

  NavBankAccount() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BankManager.list);
  }
  NavCardsManager() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.CardsManager.cardmanger);
  }
  NavProfile() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
  }
  NavAbout() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.About);
  }
  async Logout() {
    const alert = await this._AlertController.create({
      header: 'Do you want to logout ?',
      message: 'You need to verify your number to login again, do you want to continue ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'text-light  alert-btn',
          handler: () => {
          }
        },
        {
          text: 'Confirm',
          cssClass: 'text-primary alert-btn',
          handler: () => {
            localStorage.clear();
            this.initializeApp();
            // this._Firebase.unsubscribeFromTopic(this._HelperService.AppConfig.Topics.ninjaorders);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Account);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Stations);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Products);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.PaymentsHistory);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.OrdersHistory);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder);
            // this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.SelectedProduct);
            // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Intro);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Auth.Login)
          }
        }
      ]
    });
    return await alert.present();
  }


  // LoadDeeplinks() {
  //   //#region DeepLinks
  //   this._DeepLinks.routeWithNavController(this._NavController, {
  //     '/dashboard': DashboardPage,
  //     '/profile': AccessProfilePage,
  //     '/notifications': NotificationsPage,
  //     '/selectbiller': SelectBillerPage,
  //     '/stores': StoresPage,
  //     '/dealsdashboard': DashboardPageModule,
  //     '/flashdeals': FlashDealsPage,
  //     '/about': AboutPage,
  //   }).subscribe(match => {
  //     alert(match);
  //     // match.$route - the route we matched, which is the matched entry from the arguments to route()
  //     // match.$args - the args passed in the link
  //     // match.$link - the full link data
  //   }, nomatch => {
  //     // nomatch.$link - the full link data
  //     console.error('Got a deeplink that didn\'t match', nomatch);
  //   });
  //   //#endregion 
  // }



}
