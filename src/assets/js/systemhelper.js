
var SystemHelper = (function () {
    "use strict";
    return {
        payWithPaystack(AccountInfo, Amount) {
            var handler = PaystackPop.setup({
                key: 'pk_test_8cf5cd7bd998b8dfc16ab844878bee323d7453bf',
                email: AccountInfo.Account.EmailAddress,
                amount: Amount * 100,
                currency: "NGN",
                metadata: {
                    custom_fields: [
                        {
                            display_name: AccountInfo.Account.DisplayName,
                            accountkey: AccountInfo.Account.AccountKey,
                        }
                    ]
                },
                callback: function (response) {
                    return response;
                },
                onClose: function (response) {
                    var Message =
                    {
                        message: "Canceled",
                    }
                    return Message;
                }
            });
            handler.openIframe();
        },
        TriggerClick: (function (Item) {
            $(Item).click();
            return 'done';
        }),
        GetUserTimeZone: (function () {
            return moment.tz.guess();
        }),
        GetDateTime: (function (Date, TimeZone) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone);
                    return FDate;
                }
                else {
                    return new Date();
                }
            } catch (error) {
                return new Date();
            }

        }),
        GetTimeS: (function (Date, TimeZone, Format) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone).format(Format);
                    return FDate;
                }
                else {
                    return null;
                }
            } catch (error) {
                return null;
            }
        }),
        GetDateS: (function (Date, TimeZone, Format) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone).format(Format);
                    return FDate;
                }
                else {
                    return null;
                }
            } catch (error) {
                return null;
            }
        }),
        GetDateTimeS: (function (Date, TimeZone, Format) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone).format(Format);
                    return FDate;
                }
                else {
                    return null;
                }
            } catch (error) {
                return null;
            }
        }),
        CheckDateIsAfter: (function (Date, CompareTo) {
            return moment(Date).isAfter(CompareTo);
        }),
        CheckDateIsBefore: (function (Date, CompareTo) {
            return moment(Date).isBefore(CompareTo);
        }),
        GetTimeDifference: (function (Date, CompareTo) {
            var date1 = moment.tz(Date, 'GMT'),
                date2 = moment.tz(CompareTo, 'GMT');
            var Duration = moment.duration(date2.diff(date1));
            var DiffernceO =
            {
                Years: 0,
                Month: 0,
                Weeks: 0,
                Days: 0,
                Hours: 0,
                Minutes: 0,
                Seconds: 0
            }
            DiffernceO.Years = Math.round(Math.abs(Duration.asYears()));
            DiffernceO.Month = Math.round(Math.abs(Duration.asMonths()));
            DiffernceO.Weeks = Math.round(Math.abs(Duration.asWeeks()));
            DiffernceO.Days = Math.round(Math.abs(Duration.asDays()));
            DiffernceO.Hours = Math.round(Math.abs(Duration.asHours()));
            DiffernceO.Minutes = Math.round(Math.abs(Duration.asMinutes()));
            DiffernceO.Seconds = Math.round(Math.abs(Duration.asSeconds()));
            return DiffernceO;
        }),
        GetTimeDifferenceS: (function (Date, CompareTo) {
            var date1 = moment.tz(Date, 'GMT'),
                date2 = moment.tz(CompareTo, 'GMT');
            var Duration = moment.duration(date2.diff(date1));
            var DiffernceO =
            {
                Years: 0,
                Month: 0,
                Weeks: 0,
                Days: 0,
                Hours: 0,
                Minutes: 0,
                Seconds: 0
            }
            DiffernceO.Years = Math.round(Math.abs(Duration.asYears()));
            DiffernceO.Month = Math.round(Math.abs(Duration.asMonths()));
            DiffernceO.Weeks = Math.round(Math.abs(Duration.asWeeks()));
            DiffernceO.Days = Math.round(Math.abs(Duration.asDays()));
            DiffernceO.Hours = Math.round(Math.abs(Duration.asHours()));
            DiffernceO.Minutes = Math.round(Math.abs(Duration.asMinutes()));
            DiffernceO.Seconds = Math.round(Math.abs(Duration.asSeconds()));
            if (DiffernceO.Years > 0) {
                return DiffernceO.Years + ' years';
            }
            else if (DiffernceO.Month > 0) {
                return DiffernceO.Years + ' months';
            }
            else if (DiffernceO.Days > 0) {
                return DiffernceO.Days + ' days';
            }
            else if ((DiffernceO.Hours) > 0) {
                return (DiffernceO.Hours) + ' hours ' + (DiffernceO.Minutes - ((DiffernceO.Hours - 1) * 60)) + ' mins';
            }
            else if (DiffernceO.Minutes > 0) {
                return DiffernceO.Minutes + ' min';
            }
            else {
                return DiffernceO.Seconds + ' sec';
            }
        }),
        GetTimeInterval: (function (Date, CompareTo, TimeZone) {
            var TDate = moment.tz(Date, 'GMT');
            var NDate = moment.tz(TDate, TimeZone);
            var TCompareTo = moment.tz(CompareTo, 'GMT');
            var NCompareToDate = moment.tz(TCompareTo, TimeZone);
            var Duration = moment.duration(NCompareToDate.diff(NDate));
            var DiffernceO =
            {
                Years: 0,
                Month: 0,
                Weeks: 0,
                Days: 0,
                Hours: 0,
                Minutes: 0,
                Seconds: 0
            }
            DiffernceO.Years = Math.round(Math.abs(Duration.asYears()));
            DiffernceO.Month = Math.round(Math.abs(Duration.asMonths()));
            DiffernceO.Weeks = Math.round(Math.abs(Duration.asWeeks()));
            DiffernceO.Days = Math.round(Math.abs(Duration.asDays()));
            DiffernceO.Hours = Math.round(Math.abs(Duration.asHours()));
            DiffernceO.Minutes = Math.round(Math.abs(Duration.asMinutes()));
            DiffernceO.Seconds = Math.round(Math.abs(Duration.asSeconds()));
            if (DiffernceO.Years > 0) {
                var TDate = moment.tz(Date, 'GMT');
                var FDate = moment.tz(TDate, TimeZone).format('DD MMM YYYY h:mm a');
                return FDate;
            }
            else if (DiffernceO.Month > 0) {
                return DiffernceO.Month + ' months';
            }
            else if (DiffernceO.Days > 0) {
                return DiffernceO.Days + ' days';
            }
            else if (DiffernceO.Hours > 0) {
                return DiffernceO.Hours + ' hours';
            }
            else if (DiffernceO.Minutes > 0) {
                return DiffernceO.Minutes + ' minutes';
            }
            else {
                return DiffernceO.Seconds + ' seconds';
            }
        })
    };
}());
